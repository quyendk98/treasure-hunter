﻿#if UNITY_EDITOR
using UnityEngine;

namespace GameAssets.Scripts.Inspector.Attribute
{
    public class EnumFlagsAttribute : PropertyAttribute
    {
        public EnumFlagsAttribute()
        {
        }
    }
}
#endif