﻿using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Story", menuName = "UI/Story", order = 0)]
    public class Story : ScriptableObject
    {
        [TextArea(0, 200)] public List<string> subDescriptions = new List<string>();

        [TextArea(0, 200)] public string mainDescription;

        [Space] public string title;

        public int chapter;
        public int stage;
    }
}