﻿using System.Collections.Generic;
using System.Linq;
using Firebase.Firestore;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Model.Item;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Map", menuName = "UI/Map", order = 0)]
    public class Map : BaseScriptableObject
    {
        public List<Quest> quests = new List<Quest>();
        public List<Treasure> treasures = new List<Treasure>();
        public List<Level> levels = new List<Level>();

        [Header("[OTHER]")] public Story story;
        [HideIf(nameof(opened))] public Item.Item deffectRequire;
        public AudioClip clip;

        [Header("[SPRITE]")] public Sprite icBackground;

        [Header("[INT]")] public int index;

        [Header("[BOOL]")] public bool opened;
    }

    [FirestoreData]
    public class MapData
    {
        [FirestoreProperty] public List<QuestData> Quests { get; set; } = new List<QuestData>();

        [FirestoreProperty] public List<string> Treasures { get; set; } = new List<string>();

        [FirestoreProperty] public List<bool> CompleteLevels { get; set; } = new List<bool>();

        [FirestoreProperty] public string Id { get; set; }

        [FirestoreProperty] public bool Opened { get; set; }

        public static MapData Create(Map m)
        {
            return new MapData
            {
                Quests = m.quests.Select(QuestData.Create).ToList(),
                CompleteLevels = m.levels.Select(x => x.completed).ToList(),
                Id = m.id,
                Opened = true
            };
        }
    }
}