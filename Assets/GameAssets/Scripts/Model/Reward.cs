﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using GameAssets.Scripts.Ex;

namespace GameAssets.Scripts.Model
{
    [Serializable]
    public class Reward
    {
        public List<Amount> rewards = new List<Amount>();

        [Range(1f, 100f)] public float drop = 100f;

        public bool random;

        public Amount Random => rewards[rewards.Select(x => x.drop).ToList().DropChance()];
    }

    [Serializable]
    public class Amount
    {
        public Item.Item item;

        [Range(1, 10000)] public int amount = 1;

        [Range(1f, 100f)] public float drop = 100f;
    }
}