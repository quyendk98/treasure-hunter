﻿using UnityEngine;

namespace GameAssets.Scripts.Model.Item
{
    [CreateAssetMenu(fileName = "Resource", menuName = "UI/Resource", order = 0)]
    public class Resource : Item
    {
        public AudioClip clip;
        
        public float value;

        [Range(50, 100)] public float slowSpeed;
    }
}