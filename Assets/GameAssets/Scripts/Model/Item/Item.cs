﻿using Firebase.Firestore;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay.Inventory;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIReward;
using UnityEngine;

namespace GameAssets.Scripts.Model.Item
{
    [CreateAssetMenu(fileName = "Item", menuName = "UI/Item", order = 0)]
    public class Item : BaseScriptableObject
    {
        public ItemType itemType;

        public int level;
        public int star;
        
        public float price;
        public float speed;
        public float defense;

        public virtual void Use(Item item)
        {
            /*if (itemType == ItemType.Piece)
            {
                UIManager.Instance.GetUI<UIReward>().Initiation(Inventory.Instance.Selected.Item);
            }*/
        }
    }

    [FirestoreData]
    public class ItemData
    {
        [FirestoreProperty] public string UserSellId { get; set; }
        [FirestoreProperty] public string UserBuyId { get; set; }

        [FirestoreProperty] public string Id { get; set; }
        [FirestoreProperty] public string Icon { get; set; }
        [FirestoreProperty] public string Description { get; set; }

        [FirestoreProperty] public int Amount { get; set; }

        [FirestoreProperty] public float Price { get; set; }

        public static ItemData Create(Item item, int amount = 1)
        {
            return new ItemData
            {
                UserSellId = string.Empty,
                UserBuyId = string.Empty,
                Id = item.id,
                Amount = amount,
                Icon = item.icon.name,
                Description = item.description,
                Price = 0f
            };
        }

        public static ItemData CreateSell(Item item, float price = 0f, int amount = 1)
        {
            return new ItemData
            {
                UserSellId = GameManager.User.Id,
                UserBuyId = string.Empty,
                Id = item.id,
                Amount = amount,
                Icon = item.icon.name,
                Description = item.description,
                Price = price
            };
        }

        public static ItemData CreateBuy(Item item)
        {
            return new ItemData
            {
                UserSellId = string.Empty,
                UserBuyId = GameManager.User.Id,
                Id = item.id,
                Amount = 1,
                Icon = item.icon.name,
                Description = item.description,
                Price = 0f
            };
        }

        public static ItemData CreateWeapon(Weapon w)
        {
            return new ItemData
            {
                Icon = w.icon.name,
                Id = w.id,
                Price = 0f,
                Description = w.description,
                Amount = 1,
                UserBuyId = string.Empty,
                UserSellId = string.Empty
            };
        }

        public static ItemData CreateSkin(Item skin)
        {
            return new ItemData
            {
                Icon = skin.icon.name,
                Description = skin.description,
                Id = skin.id,
                Price = 0f,
                Amount = 1,
                UserBuyId = string.Empty,
                UserSellId = string.Empty
            };
        }
    }
}