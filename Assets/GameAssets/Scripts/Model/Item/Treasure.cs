﻿using GameAssets.Scripts.Manager;
using System.Collections.Generic;
using GameAssets.Scripts.UI.UIReward;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Model.Item
{
    [CreateAssetMenu(fileName = "Treasure", menuName = "UI/Treasure", order = 0)]
    public class Treasure : Item
    {
        public List<Reward> rewards = new List<Reward>();

        [PreviewField(50, ObjectFieldAlignment.Left)]
        public Sprite opened;

        public float showChance;

        public void Use(int amount)
        {
            if (amount <= 0)
            {
                return;
            }

            UIManager.Instance.HideAllAndShowUI<UIReward>();
            UIManager.Instance.GetUI<UIReward>().Initiation(this, amount);
        }
    }
}