﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GamePlay.Inventory;
using GameAssets.Scripts.GamePlay.Slot;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace GameAssets.Scripts.Model.Item
{
    [System.Serializable]
    public class Special
    {
        public List<Item> items = new List<Item>();

        [Header("Beneficial Effects")] [Tooltip("Increase % the effectiveness of strength medicine")]
        public float strength;

        [Tooltip("Increase % pulling speed")] public float pullSpeed;

        [Tooltip("Increase % flying speed")] public float flySpeed;

        [Tooltip("Increase normal timer when pulling diamonds back")]
        public float timerWhenDiamondBack;

        [Tooltip("Increase normal timer when pulling golds back")]
        public float timerWhenGoldBack;

        [Tooltip("Increase damage to monster")]
        public float damageToBoss;

        [FormerlySerializedAs("damageToBoss")] [Tooltip("Increase damage to boss")]
        public float percentDamageToBoss;

        [Tooltip("Increase % resource points")]
        public float resourcePoint;
    }

    [CreateAssetMenu(fileName = "Weapon", menuName = "UI/Weapon", order = 0)]
    public class Weapon : Item
    {
        [ShowIf("@this.itemType == GameAssets.Scripts.General.ItemType.Line")]
        [PreviewField(50, ObjectFieldAlignment.Left)]
        public Sprite iconEquip;

        [ShowIf("@this.itemType == GameAssets.Scripts.General.ItemType.Line")]
        public Material material;

        [Header("Special")] public Special special;

        [Header("Beneficial Effects")] [Tooltip("Increase % the effectiveness of strength medicine")]
        public float strength;

        [Tooltip("Increase % pulling speed")] public float pullSpeed;

        [Tooltip("Increase % pulling stone speed")]
        public float stoneSpeed;

        [Tooltip("Increase % flying speed")] public float flySpeed;

        [Tooltip("Increase % gold when pulling back")]
        public float goldWhenBack;

        [Tooltip("Increase normal timer when pulling diamonds back")]
        public float timerWhenDiamondBack;

        [Tooltip("Increase normal timer when pulling golds back")]
        public float timerWhenGoldBack;

        [Tooltip("Increase normal timer when pulling stone back")]
        public float timerWhenStoneBack;

        [Tooltip("Increase damage to boss")] public float damageToBoss;

        [FormerlySerializedAs("damageToBoss")] [Tooltip("Increase damage to boss")]
        public float percentDamageToBoss;

        [Tooltip("Increase % win boss when dispute")]
        public float percentWinBoss;

        [Tooltip("% destroy item when pull back")]
        public float percentDestroyItem;

        [Tooltip("% increase point when detect stone")]
        public float stonePoint;

        [Tooltip("Increase % resource points")]
        public float resourcePoint;

        [Tooltip("Get free TNT when play game")]
        public int tntFree;

        [Tooltip("Get free Strength when play game")]
        public int strengthFree;

        [Tooltip("Increase timer when use bomb/firecracker")]
        public float seconds;

        [Tooltip("Increase timer when win dispute")]
        public float secondWins;

        [Tooltip("% when detect stone")] public float stonePercent;

        [Tooltip("x when detect stone then increase value")]
        public float stoneValue;

        [Tooltip("Boss when failed dispute then dont remove time")]
        public Item boss;

        public override void Use(Item item)
        {
            var weapon = ItemData.CreateWeapon(item as Weapon);
            
            switch (itemType)
            {
                case ItemType.Hook:
                    GameManager.SetHook(weapon);
                    break;

                case ItemType.Line:
                    GameManager.SetLine(weapon);
                    break;

                case ItemType.Pet:
                    GameManager.SetPet(weapon);
                    break;

                case ItemType.Skin:
                    GameManager.SetSkin(weapon);
                    break;
            }
        }
    }
}