﻿using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [CreateAssetMenu(fileName = "Default", menuName = "UI/Default", order = 0)]
    public class Default : ScriptableObject
    {
        [SerializeField] private Map map;
        [SerializeField] private Item.Item skin;

        [SerializeField] private Weapon hook;
        [SerializeField] private Weapon line;

        public ItemData HookData => ItemData.CreateWeapon(hook);
        public ItemData LineData => ItemData.CreateWeapon(line);
        public ItemData SkinData => ItemData.CreateSkin(skin);
        public MapData MapData => MapData.Create(map);
    }
}