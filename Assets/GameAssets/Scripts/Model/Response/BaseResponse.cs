﻿using System;

namespace GameAssets.Scripts.Model.Response
{
    [Serializable]
    public class BaseResponse
    {
        public bool stt;
        public string msg;
    }
}