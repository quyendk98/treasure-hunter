﻿using System;
using System.Collections.Generic;
using System.Linq;
using Firebase.Firestore;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Model
{
    [Serializable]
    public class Require
    {
        public BaseScriptableObject itemRequire;

        public int requireAmount;
        public int currentAmount;
    }

    [FirestoreData]
    [Serializable]
    public class RequireData
    {
        [FirestoreProperty] public string IdItemRequire { get; set; }

        [FirestoreProperty] public int RequireAmount { get; set; }

        [FirestoreProperty] public int CurrentAmount { get; set; }
    }

    [CreateAssetMenu(fileName = "Quest", menuName = "UI/Quest", order = 0)]
    public class Quest : ScriptableObject
    {
        [Header("[LIST]")] public List<Reward> rewards = new List<Reward>();
        public List<Require> requires = new List<Require>();

        [Header("[ENUM]")] public FilterType filterType;
        public QuestType questType;

        [Header("[STRING]")] [ReadOnly] public string id;
        public string title;
        [TextArea] public string description;

        [Header("[BOOL]")] public bool daily;
        public bool received;
        public bool killAllBoss;

        public void Initialize(QuestData qd)
        {
            switch (questType)
            {
                case QuestType.Collection:
                    GameEvent.OnQuestCollection += Collection;
                    break;

                case QuestType.CompleteMap:
                    GameEvent.OnQuestCompleteMap += CompleteMap;
                    break;

                case QuestType.CompleteLevel:
                    GameEvent.OnQuestCompleteLevel += CompleteLevel;
                    break;

                case QuestType.Kill:
                    GameEvent.OnQuestKill += Kill;
                    break;
            }

            daily = qd.Daily;
            received = qd.Received;
        }

        private void OnDestroy()
        {
            RemoveEvent();
        }

        private void OnDisable()
        {
            RemoveEvent();
        }

        private void RemoveEvent()
        {
            switch (questType)
            {
                case QuestType.Collection:
                    GameEvent.OnQuestCollection -= Collection;
                    break;

                case QuestType.CompleteMap:
                    GameEvent.OnQuestCompleteMap -= CompleteMap;
                    break;

                case QuestType.CompleteLevel:
                    GameEvent.OnQuestCompleteLevel -= CompleteLevel;
                    break;

                case QuestType.Kill:
                    GameEvent.OnQuestKill -= Kill;
                    break;
            }
        }

        private void Collection(Collection collection)
        {
            foreach (var y in from y in requires
                let z = y.itemRequire as Item.Item
                where z != null && (z.id == collection.id ||
                                    collection.gold && (z.itemType == ItemType.Diamond ||
                                                        z.itemType == ItemType.Gold || z.itemType == ItemType.Stone))
                select y)
            {
                if (y.currentAmount < y.requireAmount)
                {
                    y.currentAmount += collection.amount;
                }

                return;
            }
        }

        private void CompleteMap(Map map)
        {
            foreach (var x in from x in requires
                where x.itemRequire.id == map.id
                from y in GameManager.User.Maps.Where(y => y.Id == map.id && y.CompleteLevels.All(t => t))
                select x)
            {
                if (x.currentAmount < x.requireAmount)
                {
                    x.currentAmount++;
                }

                return;
            }
        }

        private void CompleteLevel(Level level)
        {
            foreach (var x in requires.Where(x =>
                x.itemRequire != null && level.id == x.itemRequire.id && level.completed))
            {
                if (x.currentAmount < x.requireAmount)
                {
                    x.currentAmount++;
                }

                break;
            }
        }

        private void Kill(string killId)
        {
            foreach (var x in requires.Where(x => x.itemRequire != null && (x.itemRequire.id == killId || killAllBoss)))
            {
                if (x.currentAmount < x.requireAmount)
                {
                    x.currentAmount++;
                }

                return;
            }
        }

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(id))
            {
                id = Guid.NewGuid().ToString();
            }
        }
    }

    [FirestoreData]
    public class QuestData
    {
        [FirestoreProperty] public List<RequireData> RequireData { get; set; } = new List<RequireData>();

        [FirestoreProperty] public string Id { get; set; }

        [FirestoreProperty] public bool Daily { get; set; }
        [FirestoreProperty] public bool Received { get; set; }

        public static QuestData Create(Quest q)
        {
            var requireData = q.requires.Select(x => new RequireData
            {
                IdItemRequire = x.itemRequire == null ? "null" : x.itemRequire.id,
                RequireAmount = x.requireAmount,
                CurrentAmount = x.currentAmount
            }).ToList();

            return new QuestData
            {
                Id = q.id,
                Daily = q.daily,
                RequireData = requireData,
                Received = q.received
            };
        }

        public void Set(Quest q)
        {
            foreach (var x in q.requires)
            {
                foreach (var y in RequireData.Where(y => x.itemRequire != null && x.itemRequire.id == y.IdItemRequire))
                {
                    y.CurrentAmount = x.currentAmount;
                    break;
                }
            }

            Received = q.received;
        }

        public void Set(QuestData q)
        {
            foreach (var x in q.RequireData)
            {
                foreach (var y in RequireData.Where(y => x.IdItemRequire == y.IdItemRequire))
                {
                    y.CurrentAmount = x.CurrentAmount;
                    break;
                }
            }

            Received = q.Received;
        }
    }
}