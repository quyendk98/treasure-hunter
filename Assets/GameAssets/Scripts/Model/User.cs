﻿using System.Collections.Generic;
using System.Linq;
using Firebase.Firestore;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Model.Item;

namespace GameAssets.Scripts.Model
{
    [FirestoreData]
    public class User
    {
        [FirestoreProperty] public List<ItemData> Items { get; set; } = new List<ItemData>();
        [FirestoreProperty] public List<MapData> Maps { get; set; } = new List<MapData>();

        [FirestoreProperty] public ItemData Hook { get; set; }
        [FirestoreProperty] public ItemData Line { get; set; }
        [FirestoreProperty] public ItemData Skin { get; set; }
        [FirestoreProperty] public ItemData Pet { get; set; }

        [FirestoreProperty] public string Password { get; set; }
        [FirestoreProperty] public string Reference { get; set; }
        [FirestoreProperty] public string Username { get; set; }
        [FirestoreProperty] public string Address { get; set; }
        [FirestoreProperty] public string Id { get; set; }

        [FirestoreProperty] public int Energy { get; set; }
        [FirestoreProperty] public int Ticket { get; set; }
        [FirestoreProperty] public int LevelSpin { get; set; }

        [FirestoreProperty] public float Gold { get; set; }
        [FirestoreProperty] public float Diamond { get; set; }
        [FirestoreProperty] public float Damage { get; set; }

        [FirestoreProperty] public float CurrentExpLevelSpin { get; set; }
        [FirestoreProperty] public float MaxExpLevelSpin { get; set; }

        public void RemoveItem(Item.Item item, float amount)
        {
            switch (item.itemType)
            {
                case General.ItemType.Hook:
                case General.ItemType.Line:
                case General.ItemType.Pet:
                case General.ItemType.Treasure:
                case General.ItemType.Piece:
                case General.ItemType.Booster:
                case General.ItemType.Skin:
                case General.ItemType.Craft:
                    foreach (var x in Items.Where(x => x.Id == item.id))
                    {
                        if (amount >= x.Amount)
                        {
                            Items.Remove(x);
                        }
                        else
                        {
                            x.Amount -= (int) amount;
                        }

                        return;
                    }

                    break;

                case General.ItemType.Gold:
                    Gold -= amount;
                    break;

                case General.ItemType.Diamond:
                    Diamond -= amount;
                    break;

                case General.ItemType.Energy:
                    Energy -= (int) amount;
                    break;
            }
        }

        public void AddItem(Item.Item item, float amount)
        {
            switch (item.itemType)
            {
                case General.ItemType.Hook:
                case General.ItemType.Line:
                case General.ItemType.Pet:
                case General.ItemType.Treasure:
                case General.ItemType.Piece:
                case General.ItemType.Booster:
                case General.ItemType.Skin:
                case General.ItemType.Craft:
                    foreach (var x in Items.Where(x => x.Id == item.id))
                    {
                        GameEvent.DoQuestCollection(new Collection
                        {
                            id = x.Id,
                            amount = (int) amount
                        });

                        x.Amount += (int) amount;
                        return;
                    }

                    GameEvent.DoQuestCollection(new Collection
                    {
                        id = item.id,
                        amount = (int) amount
                    });

                    break;

                case General.ItemType.Other:
                    GameEvent.DoQuestCollection(new Collection
                    {
                        id = item.id,
                        amount = (int) amount
                    });

                    break;

                case General.ItemType.Gold:
                    Gold += amount;
                    GameEvent.DoUpdateResource();
                    break;

                case General.ItemType.Diamond:
                    Diamond += amount;
                    GameEvent.DoUpdateResource();
                    break;

                case General.ItemType.Energy:
                    Energy += (int) amount;
                    GameEvent.DoUpdateResource();
                    break;
            }

            Items.Add(ItemData.Create(item, (int) amount));
        }

        public static User Create(Default d, string password, string reference,
            string username, string id)
        {
            var hook = d.HookData;
            var line = d.LineData;
            var skin = d.SkinData;

            return new User
            {
                Items = new List<ItemData> {hook, line, skin},
                Maps = new List<MapData> {d.MapData},
                Hook = hook,
                Line = line,
                Skin = skin,
                Password = password,
                Reference = reference,
                Username = username,
                Id = id,
                Energy = 50000,
                Ticket = 5000,
                LevelSpin = 1,
                Gold = 500000,
                Diamond = 500000,
                Damage = 1,
                CurrentExpLevelSpin = 0f,
                MaxExpLevelSpin = 30f
            };
        }
    }
}