﻿using Firebase.Firestore;

namespace GameAssets.Scripts.Model
{
    [FirestoreData]
    public class Config
    {
        [FirestoreProperty] public string DepositAddress { get; set; }

        [FirestoreProperty] public string TimerMaintenance { get; set; }
        
        [FirestoreProperty] public float SpinGold { get; set; }
        
        [FirestoreProperty] public float SpinRareItems { get; set; }
        
        [FirestoreProperty] public float SpinItemsInShop { get; set; }
    }
}