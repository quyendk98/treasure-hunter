﻿namespace GameAssets.Scripts.Base.Interface
{
    public interface IInitialize
    {
        void Initialize();
    }
}