﻿namespace GameAssets.Scripts.Base.Interface
{
    public interface ICommand
    {
        void Execute();
        void Undo();
        bool IsFinished();
    }
}
