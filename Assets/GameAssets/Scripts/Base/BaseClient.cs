﻿using System;
using System.Collections;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Model.Response;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace GameAssets.Scripts.Base
{
    public static class BaseClient
    {
        public static IEnumerator Register(string username, string password, string reference,
            Action onRegisterSuccess = null, Action onRegisterFailed = null)
        {
            var url = $"{Const.BASE_URL}{Const.API_REGISTER}?username={username}&password={password}&ref={reference}";
            var request = UnityWebRequest.Get(url);

            Debug.Log($"REQUEST => {url}");
            
            yield return request.SendWebRequest();

            try
            {
                var data = request.downloadHandler.text;
                var response = JsonConvert.DeserializeObject<BaseResponse>(data);
                
                Debug.Log($"DATA => {data}");

                if (response != null)
                {
                    if (response.stt)
                    {
                        onRegisterSuccess?.Invoke();
                    }
                    else
                    {
                        onRegisterFailed?.Invoke();
                    }
                }
                else
                {
                    onRegisterFailed?.Invoke();
                }
            }
            catch
            {
                onRegisterFailed?.Invoke();
            }
        }
    }
}