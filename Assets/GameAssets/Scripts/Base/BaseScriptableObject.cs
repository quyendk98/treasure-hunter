﻿using System;
using GameAssets.Scripts.Base.Interface;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseScriptableObject : ScriptableObject, IInitialize
    {
        [PreviewField(50, ObjectFieldAlignment.Left)]
        public Sprite icon;

        [ReadOnly] public string id;

        public string description;

        private void OnValidate()
        {
            if (string.IsNullOrEmpty(id))
            {
                Initialize();
            }
        }
        
        [Button]
        public void Initialize()
        {
            id = Guid.NewGuid().ToString();
        }
    }

    [Serializable]
    public class BaseScriptableObjectData
    {
        public string Id { get; set; }

        public static BaseScriptableObjectData Create(BaseScriptableObject bso)
        {
            return new BaseScriptableObjectData
            {
                Id = bso.id
            };
        }
    }
}