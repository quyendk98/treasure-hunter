﻿using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using Newtonsoft.Json;
using Tamarin.FirebaseX;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    public class BaseUI : MonoBehaviour
    {
        protected static Action onLoading;

        protected static bool firebaseReady;
        protected static bool isSignedIn = true;

        private bool _loading;

        #region MonoBehaviour

        protected virtual void OnValidate()
        {
            name = GetType().Name;
        }

        #endregion

        #region Public

        public void DoAnimation(bool active)
        {
            if (active)
            {
                if (!gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(true);
                }
            }
            else
            {
                if (gameObject.activeInHierarchy)
                {
                    gameObject.SetActive(false);
                }
            }
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        protected void Click(Action callback)
        {
            if (_loading)
            {
                return;
            }

            _loading = true;

            if (!firebaseReady)
            {
                Debug.Log("Firebase is initializing");
                return;
            }

            callback();

            _loading = false;
        }

        protected static void SignOut()
        {
            isSignedIn = false;

            FirebaseAPI.Instance.SignOut();
            FirebaseAPI.Instance.UnsubscribeAll();
            Manager.SceneManager.LoadScene("Game Menu");
        }

        protected static void RegisterEventUserChanged(string id)
        {
            try
            {
                FirebaseAPI.Instance.QueryListenAs<User>(Const.PATH_USER,
                    $"userId-{id}", GameManager.SetUser);
            }
            catch
            {
                SignOut();
            }
        }

        protected static async Task AuthChanged()
        {
#if UNITY_EDITOR || UNITY_IOS || UNITY_ANDROID
            static async Task StateChanged(FirebaseUser user)
            {
                Debug.Log($"AUTH CHANGED: {JsonConvert.SerializeObject(user)}");

                if (user is {status: true})
                {
                    if (isSignedIn)
                    {
                        try
                        {
                            var data = await FirebaseAPI.Instance.QueryAs<User>(Const.PATH_USER,
                                $"userId-{user.UserId}");

                            if (data != null)
                            {
                                RegisterEventUserChanged(data.Id);
                            }
                            else
                            {
                                isSignedIn = false;

                                Debug.Log("User doesn't exist");
                            }
                        }
                        catch (Exception ex)
                        {
                            isSignedIn = false;

                            Debug.Log(ex.ToString());
                        }
                    }
                }
                else
                {
                    isSignedIn = false;

                    Debug.Log("User is logged out or deleted");
                }
            }

            FirebaseAPI.Instance.AuthChanged(async user => await StateChanged(user));
            await UniTask.Delay(TimeSpan.FromSeconds(1f));
#else
            isSignedIn = false;
#endif
        }

        #endregion
    }
}