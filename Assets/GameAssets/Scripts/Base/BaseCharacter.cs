﻿using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Base.Interface;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Base
{
    [RequireComponent(typeof(BaseStat))]
    [RequireComponent(typeof(Rigidbody))]
    public abstract class BaseCharacter : MonoBehaviour, ISingleton
    {
        [FoldoutGroup("Initialize")] [ReadOnly]
        public new Collider2D collider;

        [FoldoutGroup("Initialize")] [ReadOnly]
        public Animator animator;

        [FoldoutGroup("Initialize")] [ReadOnly]
        public Rigidbody2D rb;

        [FoldoutGroup("Initialize")] [ReadOnly]
        public BaseStat stat;

        [FoldoutGroup("Initialize")] [ReadOnly]
        public string stateName;

        [FoldoutGroup("Initialize")] public bool model3D;

        public IState currentState;

        [Button]
        public virtual void Initialize()
        {
            collider = GetComponent<Collider2D>();
            rb = GetComponent<Rigidbody2D>();
            stat = GetComponent<BaseStat>();

            if (model3D)
            {
                animator = GetComponentInChildren<Animator>();

                DestroyImmediate(GetComponent<Animator>(), true);
            }
            else
            {
                animator = GetComponent<Animator>();

                if (animator == null)
                {
                    animator = gameObject.AddComponent<Animator>();
                }
            }
        }

        protected virtual void Awake()
        {
            GameManager.Instance.AddBaseCharacter(this);
            GameEvent.OnStopBaseCharacter += DoStopBaseCharacter;
        }

        protected virtual void OnDestroy()
        {
            GameEvent.OnStopBaseCharacter -= DoStopBaseCharacter;
        }

        private void OnValidate()
        {
            Initialize();
        }

        private void Update()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing) && stat.IsAlive)
            {
                InnerUpdate();
            }
            else
            {
                PreInnerUpdate();
            }
        }

        private void FixedUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing) && stat.IsAlive)
            {
                InnerFixedUpdate();
            }
            else
            {
                PreInnerFixedUpdate();
            }
        }

        private void LateUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing) && stat.IsAlive)
            {
                InnerLateUpdate();
            }
            else
            {
                PreInnerLateUpdate();
            }
        }

        private void DoStopBaseCharacter()
        {
            if (this == null || !stat.IsAlive)
            {
                return;
            }

            ChangeState();

            if (rb != null)
            {
                rb.ResetInertia();
            }
        }

        public void ChangeState(IState state = null, MonoBehaviour target = null)
        {
            currentState?.ExitState();

            stateName = $"{state?.ToString().Match('.', firstLast: true)}";
            currentState = state;

            if (rb != null)
            {
                rb.ResetInertia();
            }

            if (state == null)
            {
                stat.ChangeCharacterState(CharacterState.IdleOrRespawn);
                return;
            }

            Debug.Log($"FSM STATE => {stateName}");

            currentState.Character = this;

            currentState.StartState(target);
        }

        public void PreInnerUpdate()
        {
        }

        public void InnerUpdate()
        {
        }

        public void PreInnerFixedUpdate()
        {
        }

        public void InnerFixedUpdate()
        {
        }

        public void PreInnerLateUpdate()
        {
        }

        public void InnerLateUpdate()
        {
        }
    }
}