﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Resource.Boss;
using GameAssets.Scripts.GamePlay.Resource.Boss.Range;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Hook
{
    public partial class Hook
    {
        private void Awake()
        {
            _renderer = GetComponent<SpriteRenderer>();
            _rb = GetComponent<Rigidbody2D>();

            _speed = speed;
            _speedOrigin = speed;
            _originalY = transform.localPosition.y;
            _timerUseStrength = timerUseStrength;

            GameEvent.OnHookShoot += DoShoot;
            GameEvent.OnHookRotation += DoRotation;

            GameEvent.OnChangeSpeed += DoChangeSpeed;
            GameEvent.OnChangeOriginSpeed += DoChangeOriginSpeed;

            GameEvent.OnUseBomb += UseBomb;
            GameEvent.OnUseFirecracker += UseFirecracker;
        }

        private void Start()
        {
            _uiPlaySgp = UIManager.Instance.GetUI<UIPlaySgp>();
        }

        private void OnDestroy()
        {
            GameEvent.OnHookShoot -= DoShoot;
            GameEvent.OnHookRotation -= DoRotation;

            GameEvent.OnChangeSpeed -= DoChangeSpeed;
            GameEvent.OnChangeOriginSpeed -= DoChangeOriginSpeed;

            GameEvent.OnUseBomb -= UseBomb;
            GameEvent.OnUseFirecracker -= UseFirecracker;
        }

        private void Update()
        {
            if (!(_timerUseStrength > 0f))
            {
                return;
            }

            _timerUseStrength -= Time.deltaTime;

            if (!(_timerUseStrength <= 0f))
            {
                return;
            }

            var tmp = speed;

            speed = _speedOrigin;

            if (tmp < 0)
            {
                speed *= -1;
            }
        }

        private void FixedUpdate()
        {
            if (DisputedAfterCollision || GameManager.Instance.IsGameState(GameState.Pause))
            {
                _rb.ResetInertia();
                return;
            }

            if (transform.localPosition.y < _originalY)
            {
                if (_speed > 0f)
                {
                    _speed *= -1;
                }

                _rb.velocity = _velocity * _speed;
            }
            else
            {
                _speed = speed;

                _rb.ResetInertia();
            }

            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            switch (line.hookAction)
            {
                case HookAction.Shoot:
                    if (_speed < 0f)
                    {
                        _speed *= -1f;
                    }

                    _rb.velocity = _velocity * _speed;
                    break;

                case HookAction.Rewind when transform.localPosition.y >= _originalY:
                    _uiPlaySgp.SetDestroyItem(false);
                    _rb.ResetInertia();

                    if (Resource != null)
                    {
                        Resource.Revoke().Forget();
                    }
                    else
                    {
                        DoRotation();
                    }

                    break;
            }

            CheckVisible();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.CompareTag("Boss"))
            {
                return;
            }

            var boss = other.GetComponent<Boss>();

            if (!(boss is BossRange br) || br.Hp <= 0f || line.hookAction != HookAction.Shoot || Resource != null)
            {
                return;
            }

            BossRange = br;
            DisputedAfterCollision = true;

            GameManager.Instance.SetGameState(GameState.Pause);

            _rb.ResetInertia();
            br.ShowUIChallenge();
        }
    }
}