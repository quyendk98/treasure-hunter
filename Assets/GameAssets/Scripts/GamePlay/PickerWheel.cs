using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Manager.Localization;
using GameAssets.Scripts.Model.Item;
using GameAssets.Scripts.UI.UISpin;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay
{
    public class PickerWheel : MonoBehaviour
    {
        [SerializeField] private List<Item> items = new List<Item>();
        [SerializeField] private Transform wheelCircle;
        [SerializeField] [Range(1f, 15f)] private float spinDuration = 12f;

        private UISpin _uiSpin;

        private bool _isSpinning;
        private float _randomRotateAngle;

        private void Start()
        {
            _uiSpin = UIManager.Instance.GetUI<UISpin>();
            _randomRotateAngle = 600f * spinDuration;
        }

        public void Spin()
        {
            /*AudioManager.Instance.PlayMusic("spinning");
            AudioManager.Instance.PlaySound("spin-button");*/
            
            if (GameManager.User.Ticket < 1)
            {
                SpawnerEx.CreateNotification(LocalizationManager.Format("no-ticket"));
                return;
            }

            if (_isSpinning)
            {
                SpawnerEx.CreateToast(LocalizationManager.Format("spinning"));
                return;
            }

            _isSpinning = true;

            wheelCircle
                .DORotate(Vector3.back * Random.Range(_randomRotateAngle, _randomRotateAngle + 360f), spinDuration)
                .SetEase(Ease.InOutQuart)
                .OnComplete(() =>
                {
                    var angle = 360f / items.Count;
                    var index = (int) ((wheelCircle.eulerAngles.z + angle / 2f) / angle);

                    index = index >= items.Count ? items.Count - 1 : index;

                    var item = items[index];
                    var dictionaries = new Dictionary<Item, float>();
                    var exp = Random.Range(1, 2f);
                    var maxExp = GameManager.User.MaxExpLevelSpin;
                    var currentExp = GameManager.User.CurrentExpLevelSpin;

                    _isSpinning = false;
                    
                    /*AudioManager.Instance.PlaySound("win");*/
                    /*Debug.Log($"Item received: {index}, {item.name}");*/

                    GameManager.User.Ticket -= 1;
                    GameManager.User.CurrentExpLevelSpin += exp;

                    if (GameManager.User.CurrentExpLevelSpin >= maxExp)
                    {
                        GameManager.User.CurrentExpLevelSpin -= maxExp;
                        GameManager.User.MaxExpLevelSpin +=
                            Random.Range(10f, 20f) * GameManager.User.CurrentExpLevelSpin;
                        GameManager.User.LevelSpin++;

                        _uiSpin.UpdateLevel();
                    }

                    _uiSpin.UpdateFill();

                    switch (item.itemType)
                    {
                        case ItemType.Gold:
                        case ItemType.Diamond:
                        case ItemType.Energy:
                            var value = ((Model.Item.Resource) item).value;

                            if (!dictionaries.ContainsKey(item))
                            {
                                dictionaries.Add(item, value);
                            }

                            GameManager.User.AddItem(item, value);
                            break;

                        case ItemType.Piece:
                            var treasure = (Treasure) item;

                            foreach (var data in from x in treasure.rewards
                                let rnd = Random.Range(0f, 100f)
                                where rnd < x.drop && x.random
                                select x.Random)
                            {
                                if (!dictionaries.ContainsKey(data.item))
                                {
                                    dictionaries.Add(data.item, 1);
                                }

                                GameManager.User.AddItem(data.item, 1);
                                break;
                            }

                            break;

                        default:
                            if (!dictionaries.ContainsKey(item))
                            {
                                dictionaries.Add(item, 1);
                            }

                            GameManager.User.AddItem(item, 1);
                            break;
                    }

                    GameManager.UpdateUser(() =>
                            {
                                foreach (var x in dictionaries)
                                {
                                    GameManager.User.RemoveItem(x.Key, x.Value);
                                }

                                GameManager.User.Ticket += 1;
                                GameManager.User.CurrentExpLevelSpin -= exp;

                                if (GameManager.User.CurrentExpLevelSpin < maxExp)
                                {
                                    GameManager.User.CurrentExpLevelSpin = currentExp;
                                    GameManager.User.LevelSpin--;

                                    _uiSpin.UpdateLevel();
                                }

                                _uiSpin.UpdateFill();

                                SpawnerEx.CreateNotification($"{LocalizationManager.Format("you-got")} "
                                                             + $"{items[index].name} {LocalizationManager.Format("failure")}");
                            },
                            () =>
                            {
                                SpawnerEx.CreateNotification($"{LocalizationManager.Format("you-got")} "
                                                             + $"{items[index].name} {LocalizationManager.Format("success")}");
                            })
                        .Forget();
                });
        }
    }
}