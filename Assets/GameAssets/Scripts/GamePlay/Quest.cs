﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Quest : MonoBehaviour
    {
        [SerializeField] private Page pageReward;

        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private TextMeshProUGUI txtDescription;
        [SerializeField] private TextMeshProUGUI txtReceive;

        [SerializeField] private UnityEngine.UI.Button btnReceive;

        public Model.Quest ModelQuest { get; private set; }

        private readonly List<Amount> _rewards = new List<Amount>();

        private int _index;

        private bool _loading;

        public void Initiation(Model.Quest quest)
        {
            ModelQuest = quest;
            txtTitle.text = quest.title;
            txtDescription.text = quest.description;

            var questCompleted = quest.requires.All(x => x.currentAmount == x.requireAmount);

            foreach (var x in quest.requires)
            {
                txtDescription.text += $"{UIEx.NewLine}{x.currentAmount}/{x.requireAmount}";
            }

            foreach (var y in from x in quest.rewards
                from y in x.rewards
                where y.item != null
                select y)
            {
                _rewards.Add(y);
            }

            _index = 0;

            pageReward.SetTotalItem(_rewards.Count);

            if (_rewards.Count > 0)
            {
                pageReward.OnCreateSlot(go =>
                {
                    var amount = _rewards[_index];
                    var reward = go.GetComponent<Reward>();

                    reward.Initiation(amount.item.icon, amount.amount, true);

                    _index++;
                    return amount.item.id;
                });
            }

            if (quest.received || questCompleted && quest.received)
            {
                btnReceive.gameObject.SetActive(true);

                btnReceive.interactable = false;
                txtReceive.text = "Completed";
                return;
            }

            if (questCompleted && !quest.received)
            {
                btnReceive.gameObject.SetActive(true);

                btnReceive.interactable = true;
                txtReceive.text = "Claim";
            }

            btnReceive.onClick.RemoveAllListeners();

            btnReceive.onClick.AddListener(async () =>
            {
                if (_loading)
                {
                    return;
                }

                _loading = true;

                foreach (var y in quest.rewards.SelectMany(x => x.rewards))
                {
                    GameManager.User.AddItem(y.item, y.amount);
                }

                ModelQuest.received = true;

                GameManager.UpdateQuest();

                await GameManager.UpdateUser(() =>
                {
                    foreach (var y in quest.rewards.SelectMany(x => x.rewards))
                    {
                        switch (y.item.itemType)
                        {
                            case ItemType.Gold:
                            case ItemType.Diamond:
                            case ItemType.Energy:
                                GameManager.User.RemoveItem(y.item, ((Model.Item.Resource) y.item).value);
                                break;

                            default:
                                GameManager.User.RemoveItem(y.item, 1);
                                break;
                        }
                    }

                    GameManager.RevokeUpdateQuest();
                    SpawnerEx.CreateNotification("Claim item failed");
                }, () =>
                {
                    quest.received = true;
                    btnReceive.interactable = false;
                    txtReceive.text = "Completed";

                    SpawnerEx.CreateNotification("Claim item success");
                });
            });
        }
    }
}