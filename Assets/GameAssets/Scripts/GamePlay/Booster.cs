using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class Booster : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI txtAmount;

        [SerializeField] private Image imgIcon;

        private UIPlaySgp _uiPlaySgp;

        private void Start()
        {
            _uiPlaySgp = UIManager.Instance.GetUI<UIPlaySgp>();
        }

        public void Initiation(GameAssets.Scripts.Model.Item.Booster b, int value)
        {
            imgIcon.sprite = b.icon;
            txtAmount.text = $"{value}";

            GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() =>
            {
                void Remove()
                {
                    GameManager.User.RemoveItem(b, 1);
                    GameManager.UpdateUser(() => { }, () => { }).Forget();
                }

                void PlaySound()
                {
                    if (b.clip != null)
                    {
                        AudioManager.Instance.PlaySound(b.clip.name);
                    }
                }

                switch (b.boosterType)
                {
                    case BoosterType.Bomb:
                        if (GameEvent.DoUseBomb())
                        {
                            PlaySound();
                            Remove();

                            _uiPlaySgp.ChangeTime(Player.Player.Instance.MoreSecondWhenUseBomb());
                            _uiPlaySgp.DrawItem();
                        }

                        break;

                    case BoosterType.Firecracker:
                        if (GameEvent.DoUseFirecracker())
                        {
                            PlaySound();
                            Remove();

                            _uiPlaySgp.ChangeTime(Player.Player.Instance.MoreSecondWhenUseBomb());
                            _uiPlaySgp.DrawItem();
                        }

                        break;

                    case BoosterType.Strength:
                        GameEvent.DoChangeOriginSpeed(b.strength + Player.Player.Instance.MoreAffectStrength());

                        PlaySound();
                        Remove();

                        _uiPlaySgp.DrawItem();
                        break;
                }
            });
        }

        public void Initiation(Sprite icon, int value, BoosterType b)
        {
            imgIcon.sprite = icon;
            txtAmount.text = $"{value}";

            GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() =>
            {
                switch (b)
                {
                    case BoosterType.Firecracker:
                        if (GameEvent.DoUseFirecracker())
                        {
                            _uiPlaySgp.ChangeTime(Player.Player.Instance.MoreSecondWhenUseBomb());
                            _uiPlaySgp.RemoveTotalFirecracker();
                            _uiPlaySgp.DrawItem();
                        }

                        break;

                    case BoosterType.Strength:
                        GameEvent.DoChangeOriginSpeed(30);

                        _uiPlaySgp.ChangeTime(Player.Player.Instance.MoreSecondWhenUseBomb());
                        _uiPlaySgp.RemoveTotalStrength();
                        _uiPlaySgp.DrawItem();
                        break;
                }
            });
        }
    }
}