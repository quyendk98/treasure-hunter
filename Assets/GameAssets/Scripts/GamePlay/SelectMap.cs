using System.Linq;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.UI.UISelectLevel;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class SelectMap : MonoBehaviour
    {
        [SerializeField] private Image imgMap;
        [SerializeField] private Map map;

        private void Awake()
        {
            GetComponent<Button>().AddListener(() =>
            {
                var open = map.opened || GameManager.User.Maps.Any(x => x.Id == map.id);

                if (open)
                {
                    if (map.clip != null)
                    {
                        AudioManager.Instance.PlayMusic(map.clip.name);
                    }

                    GameManager.SetSelectedMap(map);
                    UIManager.Instance.HideAllAndShowUI<UISelectLevel>();
                }
                else
                {
                    SpawnerEx.CreateNotification("Bản đồ vẫn chưa được mở");
                }
            });

            GameEvent.OnOpenNewMap += OpenNewMap;
        }

        private void OnEnable()
        {
            if (GameManager.User != null && GameManager.User.Maps.Any(x => x.Id == map.id))
            {
                imgMap.color = Color.white;
            }
        }

        private void OnDestroy()
        {
            GameEvent.OnOpenNewMap -= OpenNewMap;
        }

        private void OpenNewMap(string id)
        {
            if (map.id == id)
            {
                imgMap.color = Color.white;
            }
        }
    }
}