﻿using GameAssets.Scripts.Base;
using GameAssets.Scripts.Model.Item;
using System.Collections.Generic;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIChallenge;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Player
{
    public partial class Player : BaseCharacter
    {
        [SerializeField] private GameObject hook;

        [SerializeField] private SpriteRenderer skinSprite;
        [SerializeField] private SpriteRenderer hookSprite;
        [SerializeField] private SpriteRenderer lineSprite;
        [SerializeField] private SpriteRenderer petSprite;

        [SerializeField] private LineRenderer lineRenderer;

        public static Player Instance;

        public bool HasSpecial { get; set; }

        private Weapon _hook;
        private Weapon _line;
        private Weapon _pet;
        private Weapon _skin;

        public Hook.Hook Hook => hook.GetComponent<Hook.Hook>();

        // float 1 is gold, float 2 is time
        public KeyValuePair<float, float> MoreTimeAndGold(float gold)
        {
            var time = 0f;

            if (_skin != null)
            {
                gold += gold * _skin.goldWhenBack / 100 + gold * _skin.resourcePoint / 100;
                time += _skin.timerWhenGoldBack;

                if (HasSpecial)
                {
                    gold += gold * _skin.special.resourcePoint / 100;
                    time += _skin.special.timerWhenGoldBack;
                }
            }

            if (_line != null)
            {
                gold += gold * _line.goldWhenBack / 100 + gold * _line.resourcePoint / 100;
                time += _skin.timerWhenGoldBack;
            }

            if (_pet != null)
            {
                gold += gold * _pet.goldWhenBack / 100 + gold * _pet.resourcePoint / 100;
                time += _skin.timerWhenGoldBack;
            }

            if (_hook)
            {
                gold += gold * _hook.goldWhenBack / 100 + gold * _hook.resourcePoint / 100;
                time += _skin.timerWhenGoldBack;
            }

            if (GameManager.Config != null)
            {
                gold += (GameManager.User.LevelSpin + 1) * GameManager.Config.SpinGold / 100;
            }

            return new KeyValuePair<float, float>(gold, time);
        }

        public KeyValuePair<float, float> MoreTimeAndDiamond(float diamond)
        {
            var time = 0f;

            if (_skin != null)
            {
                diamond += diamond * _skin.resourcePoint / 100;
                time += _skin.timerWhenDiamondBack;

                if (HasSpecial)
                {
                    diamond += diamond * _skin.special.resourcePoint / 100;
                    time += _skin.special.timerWhenDiamondBack;
                }
            }

            if (_line != null)
            {
                diamond += diamond * _line.resourcePoint / 100;
                time += _line.timerWhenDiamondBack;
            }

            if (_pet != null)
            {
                diamond += diamond * _pet.resourcePoint / 100;
                time += _pet.timerWhenDiamondBack;
            }

            if (_hook)
            {
                diamond += diamond * _hook.resourcePoint / 100;
                time += _hook.timerWhenDiamondBack;
            }

            return new KeyValuePair<float, float>(diamond, time);
        }

        public KeyValuePair<float, float> MoreTimeAndStone(float stone)
        {
            var time = 0f;

            if (_skin != null)
            {
                stone += stone * _skin.stonePoint / 100 + stone * _skin.resourcePoint / 100;
                time += _skin.timerWhenStoneBack;

                if (HasSpecial)
                {
                    stone += stone * _skin.special.resourcePoint / 100;
                }
            }

            if (_line != null)
            {
                stone += stone * _line.stonePoint / 100 + stone * _line.resourcePoint / 100;
                time += _skin.timerWhenStoneBack;
            }

            if (_pet != null)
            {
                stone += stone * _pet.stonePoint / 100 + stone * _pet.resourcePoint / 100;
                time += _skin.timerWhenStoneBack;
            }

            if (_hook)
            {
                stone += stone * _hook.stonePoint / 100 + stone * _hook.resourcePoint / 100;
                time += _skin.timerWhenStoneBack;
            }

            return new KeyValuePair<float, float>(stone, time);
        }

        public bool CanDestroyItem()
        {
            if (_skin != null)
            {
                if (_skin.percentDestroyItem > 0f)
                {
                    return true;
                }
            }

            if (_line != null)
            {
                if (_line.percentDestroyItem > 0f)
                {
                    return true;
                }
            }

            if (_hook != null)
            {
                if (_hook.percentDestroyItem > 0f)
                {
                    return true;
                }
            }

            if (_pet != null)
            {
                if (_pet.percentDestroyItem > 0f)
                {
                    return true;
                }
            }

            return false;
        }

        public float MoreFlySpeed()
        {
            var speed = 0f;

            if (_skin != null)
            {
                speed += _skin.flySpeed;

                if (HasSpecial)
                {
                    speed += _skin.special.flySpeed;
                }
            }

            if (_line != null)
            {
                speed += _line.flySpeed;
            }

            if (_hook)
            {
                speed += _hook.flySpeed;
            }

            return speed;
        }

        public float MorePullSpeed()
        {
            var speed = 0f;

            if (_skin != null)
            {
                speed += _skin.pullSpeed;

                if (HasSpecial)
                {
                    speed += _skin.special.pullSpeed;
                }
            }

            if (_line != null)
            {
                speed += _line.pullSpeed;
            }

            if (_pet != null)
            {
                speed += _pet.pullSpeed;
            }

            if (_hook)
            {
                speed += _hook.pullSpeed;
            }

            return speed;
        }

        public float MoreStoneSpeed()
        {
            var speed = 0f;

            if (_skin != null)
            {
                speed += _skin.stoneSpeed;
            }

            if (_line != null)
            {
                speed += _line.stoneSpeed;
            }

            if (_pet != null)
            {
                speed += _pet.stoneSpeed;
            }

            if (_hook)
            {
                speed += _hook.stoneSpeed;
            }

            return speed;
        }

        public float MoreAffectStrength()
        {
            var strength = 0f;

            if (_skin != null)
            {
                strength += _skin.strength;

                if (HasSpecial)
                {
                    strength += _skin.special.strength;
                }
            }

            if (_line != null)
            {
                strength += _line.strength;
            }

            if (_hook)
            {
                strength += _hook.strength;
            }

            return strength;
        }

        public int MoreFirecracker()
        {
            var firecracker = 0;

            if (_skin != null)
            {
                firecracker += _skin.tntFree;
            }

            if (_line != null)
            {
                firecracker += _line.tntFree;
            }

            if (_hook)
            {
                firecracker += _hook.tntFree;
            }

            return firecracker;
        }

        public float MorePercentWin()
        {
            var win = 0f;

            if (_skin != null)
            {
                win += _skin.percentWinBoss;
            }

            if (_line != null)
            {
                win += _line.percentWinBoss;
            }

            if (_hook)
            {
                win += _hook.percentWinBoss;
            }

            return win;
        }

        public int MoreStrength()
        {
            var strength = 0;

            if (_skin != null)
            {
                strength += _skin.strengthFree;
            }

            if (_line != null)
            {
                strength += _line.strengthFree;
            }

            if (_hook)
            {
                strength += _hook.strengthFree;
            }

            return strength;
        }

        public float MoreSecondWhenUseBomb()
        {
            var seconds = 0f;

            if (_skin != null)
            {
                seconds += _skin.seconds;
            }

            if (_line != null)
            {
                seconds += _line.seconds;
            }

            if (_hook)
            {
                seconds += _hook.seconds;
            }

            return seconds;
        }

        public float MorePercentDestroyItem()
        {
            var percent = 0f;

            if (_skin != null)
            {
                percent += _skin.percentDestroyItem;
            }

            if (_line != null)
            {
                percent += _line.percentDestroyItem;
            }

            if (_hook)
            {
                percent += _hook.percentDestroyItem;
            }

            return percent;
        }
        
        public float MoreStonePercent()
        {
            var stone = 0f;

            if (_skin != null)
            {
                stone += _skin.stonePercent;
            }

            if (_line != null)
            {
                stone += _line.stonePercent;
            }

            if (_hook)
            {
                stone += _hook.stonePercent;
            }

            return stone;
        }
        
        public float MoreStoneValue()
        {
            var stone = 0f;

            if (_skin != null)
            {
                stone += _skin.stoneValue;
            }

            if (_line != null)
            {
                stone += _line.stoneValue;
            }

            if (_hook)
            {
                stone += _hook.stoneValue;
            }

            return stone;
        }

        public float MoreSecondWhenWinDispute()
        {
            var seconds = 0f;

            if (_skin != null)
            {
                seconds += _skin.secondWins;
            }

            if (_line != null)
            {
                seconds += _line.secondWins;
            }

            if (_hook)
            {
                seconds += _hook.secondWins;
            }

            return seconds;
        }

        public bool HasRemoveTime(Item item)
        {
            if (item == null)
            {
                return false;
            }

            if (_skin != null && _skin.boss != null)
            {
                if (item.id.Equals(_skin.boss.id))
                {
                    return true;
                }
            }

            if (_line != null && _line.boss != null)
            {
                if (item.id.Equals(_line.boss.id))
                {
                    return true;
                }
            }

            if (_hook != null && _hook.boss != null)
            {
                if (item.id.Equals(_hook.boss.id))
                {
                    return true;
                }
            }

            if (_pet == null || _pet.boss == null)
            {
                return false;
            }

            return item.id.Equals(_pet.boss.id);
        }

        public void Dispute()
        {
            if (Hook.Resource != null && Hook.Resource.Snake != null)
            {
                Hook.Resource.Snake.SetCrawl(false, null);
                UIManager.Instance.HideUI<UIChallenge>();
                CameraEx.StopShake();
            }
            else
            {
                GameEvent.DoDispute(stat.atk, false);
            }
        }
    }
}