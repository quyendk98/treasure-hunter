using System;
using GameAssets.Scripts.Ex;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class Notification : MonoBehaviour
    {
        [SerializeField] private GameObject notification;

        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private TextMeshProUGUI txtMessage;

        [SerializeField] private Button btnOk;

        public void Initiation(string message, string title = "Thông báo", Action onOk = null)
        {
            txtTitle.text = title;
            txtMessage.text = message;

            btnOk.AddListener(() =>
            {
                onOk?.Invoke();

                SpawnerEx.DestroySpawner(notification.transform);
            });
        }
    }
}