﻿using Cysharp.Threading.Tasks;

namespace GameAssets.Scripts.GamePlay.Interface
{
    public interface IResource
    {
        UniTaskVoid Revoke();
        void Caught();
        void Destroy();
    }
}