using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class Slot : BaseHandle
    {
        [SerializeField] protected Image imgIcon;
        [SerializeField] protected Image imgBg;

        [SerializeField] protected Sprite selected;
        [SerializeField] protected Sprite deselect;

        public Item Item { get; protected set; }

        public virtual void Initialize(ItemData itemData)
        {
            Item = GameManager.ResourceItem(itemData);
        }

        public void Remove()
        {
            gameObject.SetActive(false);
        }

        public void Select(bool value)
        {
            imgBg.sprite = value ? selected : deselect;
        }
    }
}
