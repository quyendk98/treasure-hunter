using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using GameAssets.Scripts.UI.UITooltip;
using GameAssets.Scripts.UI.UIUseable;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.GamePlay.Slot
{
    public class InventorySlot : Slot
    {
        [SerializeField] private Transform starParent;

        [SerializeField] private GameObject lbIcon;

        [SerializeField] private TextMeshProUGUI txtAmount;

        public ItemData ItemData { get; private set; }

        private CanvasGroup _canvasGroup;
        private Page _page;

        private bool _showTooltip;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
            _page = GetComponentInParent<Page>();
        }

        protected override void DoHandleHoldComplete()
        {
            if (Dragged)
            {
                return;
            }

            _showTooltip = true;

            if (Item is Treasure t)
            {
                UIManager.Instance.ShowUI<UIUseable>().Initialize(t, ItemData.Amount);
            }
            else
            {
                UIManager.Instance.ShowUI<UITooltip>().Setup(Item);
            }
        }

        public override void OnBeginDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = false;
        }

        public override void OnEndDrag(PointerEventData eventData)
        {
            _canvasGroup.blocksRaycasts = true;
        }

        protected override void DoHandleDrag()
        {
            base.DoHandleDrag();

            if (!Dragged)
            {
                return;
            }

            if (_showTooltip)
            {
                _showTooltip = false;

                UIManager.Instance.HideUI<UITooltip>();
            }

            imgIcon.transform.position = MathfEx.PlatformPosition;
        }

        protected override void DoHandleUp()
        {
            base.DoHandleUp();

            if (_showTooltip)
            {
                UIManager.Instance.HideUI<UITooltip>();
            }

            ReturnOrigin();
        }

        public override void Initialize(ItemData itemData)
        {
            base.Initialize(itemData);

            lbIcon.SetActive(true);

            ItemData = itemData;
            imgIcon.sprite = Item.icon;
            txtAmount.text = $"{itemData.Amount}";
        }

        public void SwapItem(Item item, int index)
        {
            if (item != null)
            {
                var itemData = ItemData.Create(item);
                
                Initialize(itemData);

                GameManager.User.Items[index] = itemData;
            }
            else
            {
                _page.OnRemoveSlot(Item.id, go => SpawnerEx.DestroySpawner(go));
            }
        }

        public void UpdateItem(ItemData itemData)
        {
            _page.OnUpdateSlot(Item.id, go =>
            {
                var inventorySlot = go.GetComponent<InventorySlot>();

                if (inventorySlot != null)
                {
                    inventorySlot.Initialize(itemData);
                }
            });
        }

        public void ReturnOrigin()
        {
            imgIcon.transform.localPosition = Origin;
        }
    }
}