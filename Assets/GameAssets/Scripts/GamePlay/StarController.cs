﻿using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay
{
    public class StarController : BaseSingleton<StarController>
    {
        [SerializeField] private GameObject starPrefab;
        
        [SerializeField] private Sprite icStar;

        public void Initialize(Transform parent, int amount, bool oldDestroy = true)
        {
            if (oldDestroy)
            {
                foreach (var x in parent.GetComponentsInChildren<ImageItem>())
                {
                    SpawnerEx.DestroySpawner(x.transform);
                }
            }

            for (var i = 0; i < amount; i++)
            {
                SpawnerEx.CreateUISpawner(parent, starPrefab)
                    .GetComponent<ImageItem>().SetupImage(icStar);
            }
        }
    }
}