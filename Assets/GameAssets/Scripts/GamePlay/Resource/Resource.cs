﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.GamePlay.Interface;
using GameAssets.Scripts.GamePlay.Resource.Monster.Snake;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.UI.UIChallenge;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource
{
    public enum DisputeWinner
    {
        None,
        Boss,
        Player
    }

    public partial class Resource : MonoBehaviour, IResource
    {
        [Header("[RESOURCE]")] [SerializeField]
        protected Model.Item.Resource resource;

        [ShowIf(nameof(snakeCanCrawl))] [SerializeField]
        private Sprite icSnakeCrawl;

        [SerializeField] protected bool snakeCanCrawl;

        public Model.Item.Resource ResourceItem => resource;

        public SpriteRenderer SpriteRenderer { get; private set; }
        public Hook.Hook Hook { get; private set; }
        public Snake Snake { get; private set; }

        public DisputeWinner BossResultDisputeWinner { get; set; }
        public DisputeWinner SnakeResultDisputeWinner { get; set; }

        public bool IsCaught { get; protected set; }
        public bool IsDisputing { get; set; }

        protected UIPlaySgp uiPlaySgp;
        protected BulletGreen bulletGreen;
        private Sprite _originSprite;

        protected bool received;

        private void CheckCollider(Component other)
        {
            if (this is Boss.Boss)
            {
                return;
            }

            switch (other.tag)
            {
                case "Hook":
                    if (BossResultDisputeWinner != DisputeWinner.None)
                    {
                        return;
                    }

                    Hook = other.GetComponent<Hook.Hook>();

                    if (Hook.DisputedAfterCollision)
                    {
                        return;
                    }

                    if (Hook.Line.hookAction == HookAction.Shoot && Hook.Resource == null)
                    {
                        Caught();
                    }

                    break;

                case "Snake":
                    if (SnakeResultDisputeWinner == DisputeWinner.None && IsCaught && snakeCanCrawl && resource != null)
                    {
                        Snake = other.GetComponent<Snake>();

                        Snake.SetCrawl(true, this);

                        SetSnakeSprite();

                        GameEvent.DoChangeSpeed(-Snake.DecSpeed, false);
                        UIManager.Instance.ShowUI<UIChallenge>();
                    }

                    break;
            }
        }
    }
}