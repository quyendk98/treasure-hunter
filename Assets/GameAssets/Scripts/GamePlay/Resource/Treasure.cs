using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource
{
    public class Treasure : Resource
    {
        [SerializeField] private float lerpSpeed;

        private Item _item;

        private bool _destroy;

        public void Initiation(Item item, Transform target, bool destroy)
        {
            if (item is Model.Item.Resource r)
            {
                resource = r;
            }

            _item = item;
            _destroy = destroy;

            SpriteRenderer.sprite = item.icon;
            transform.position = target.position + (Vector3) Random.insideUnitCircle / 2f;
        }

#pragma warning disable 1998
        public override async UniTaskVoid Revoke()
#pragma warning restore 1998
        {
            if (received)
            {
                return;
            }

            received = true;
            IsCaught = false;

            var sequence = DOTween.Sequence();

            sequence.Join(transform.DOMove(Line.Instance.Skin.position, lerpSpeed))
                .Join(transform.DOScale(transform.localScale / 2f, lerpSpeed))
                .OnComplete(() =>
                {
                    if (_destroy)
                    {
                        Destroy();
                    }
                    else
                    {
                        Disable();
                    }

                    GameManager.User.AddItem(_item, 1);
                    GameManager.UpdateUser(() => GameManager.User.RemoveItem(_item, 1), () => { }).Forget();
                });
        }
    }
}