﻿using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource
{
    public partial class Resource
    {
        protected virtual void Awake()
        {
            uiPlaySgp = UIManager.Instance.GetUI<UIPlaySgp>();
            SpriteRenderer = GetComponent<SpriteRenderer>();

            if (resource == null)
            {
                return;
            }

            resource = Instantiate(resource);

            foreach (var y in GameManager.User.Items.Select(GameManager.ResourceItem))
            {
                if (!(y is Model.Item.Booster {boosterType: BoosterType.Lucky} b))
                {
                    continue;
                }

                resource.value += (int) (resource.value * (b.lucky / 100f));
                
                GameManager.User.RemoveItem(y, 1);
                GameManager.UpdateUser(() => { }, () => { }).Forget();
                break;
            }
        }

        private void FixedUpdate()
        {
            if (GameManager.Instance.IsGameState(GameState.Playing) && IsCaught)
            {
                transform.DOMove(Hook.ExitPoint, 0.025f);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            CheckCollider(other);
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            CheckCollider(other);
        }
    }
}