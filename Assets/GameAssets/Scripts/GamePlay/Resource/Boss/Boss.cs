﻿using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.UI.UISquid;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.GamePlay.Resource.Boss
{
    [Serializable]
    internal struct BossDefeatReward
    {
        public Model.Item.Treasure treasure;

        public float drop;
    }

    public partial class Boss : Monster.Monster
    {
        [Header("[BOSS]")] [SerializeField] private List<BossDefeatReward> rewards = new List<BossDefeatReward>();

        [SerializeField] private LayerMask resourceLayerMask;

        [SerializeField] private GameObject treasurePrefab;

        [ShowIf(nameof(move))] [SerializeField]
        private GameObject bulletLokiPrefab;

        [SerializeField] protected Canvas cvHealth;

        [SerializeField] private Image imgFill;

        [SerializeField] private int maxHp;

        [SerializeField] private float lerpSpeed = 0.5f;
        [SerializeField] protected float removeTime = 2f;
        [SerializeField] private float radiusDetectResource = 3f;

        [Range(40f, 100f)] [SerializeField] protected float percentWin = 40f;

        [SerializeField] private bool move = true;
        [SerializeField] protected bool squid;
        [SerializeField] private bool flipWhenDispute = true;

        public float Hp { get; private set; }

        private readonly RaycastHit2D[] _hit2Ds = new RaycastHit2D[10];
        protected Resource otherResource;

        private float moveX;
        private float _originSpeed;

        protected bool previousSpriteFlip;

        protected virtual void StartDispute()
        {
            GameManager.Instance.SetGameState(GameState.Pause);

            if (!move)
            {
                transform.DOMove(otherResource.transform.position
                                 + new Vector3(otherResource.SpriteRenderer.bounds.size.x / 1.5f, 0f, 0f),
                        0.25f)
                    .OnStart(() =>
                    {
                        animator.ChangeState(AnimationType.Move);

                        /*if (clipMove != null)
                        {
                            AudioManager.Instance.PlaySound(clipMove.name, 10);
                        }*/
                    })
                    .OnUpdate(() =>
                    {
                        if (otherResource == null || otherResource.gameObject.activeInHierarchy)
                        {
                            return;
                        }

                        transform.DOKill();
                        StartMoveAgain();
                    })
                    .OnComplete(() =>
                    {
                        animator.ChangeState(AnimationType.Dispute);
                        ShowUIChallenge();
                    });
            }
            else
            {
                if (otherResource.transform.position.x > transform.position.x)
                {
                    SpriteRenderer.flipX = true;
                }
                else if (otherResource.transform.position.x < transform.position.x)
                {
                    SpriteRenderer.flipX = false;
                }

                bulletGreen = SpawnerEx.CreateSpawner(otherResource.transform.position, null, bulletLokiPrefab)
                    .GetComponent<BulletGreen>();

                bulletGreen.Initialize(otherResource.transform);
                otherResource.SetBulletGreen(bulletGreen);
                animator.ChangeState(AnimationType.Dispute);

                ShowUIChallenge();
            }
        }

        protected virtual void DoDispute(float dmg, bool useBomb)
        {
            var rnd = Random.Range(0f + Player.Player.Instance.MorePercentWin(), 1f);

            if (otherResource != null)
            {
                if (!useBomb)
                {
                    if (rnd >= percentWin / 100)
                    {
                        if (!Player.Player.Instance.HasRemoveTime(resource))
                        {
                            uiPlaySgp.ChangeTime(removeTime);
                        }

                        if (move)
                        {
                            bulletGreen.Destroy();

                            bulletGreen = null;
                        }

                        otherResource.SuccessDispute(uiPlaySgp.Random);
                    }
                    else
                    {
                        otherResource.BossResultDisputeWinner = DisputeWinner.Player;

                        GameManager.Instance.SetGameState(GameState.Playing);
                    }
                }
                else
                {
                    otherResource.BossResultDisputeWinner = DisputeWinner.Player;

                    SpawnerEx.CreateFxBomb(transform.position);
                    GameManager.Instance.SetGameState(GameState.Playing);
                }

                otherResource.Hook.LineBack();
                otherResource.SetLayer(11);

                otherResource.IsDisputing = false;
                otherResource = null;
            }

            /*if (clipDispute != null && GameManager.SelectedMap.clip != null)
            {
                AudioManager.Instance.PlayMusic(GameManager.SelectedMap.clip.name);
            }*/

            TakeDamage(dmg);

            if (!(Hp <= 0f))
            {
                StartMoveAgain();

                if (squid)
                {
                    UIManager.Instance.ShowUI<UISquid>();
                }

                return;
            }

            Death();
        }

        protected void Death()
        {
            /*if (clipDie != null)
            {
                AudioManager.Instance.PlaySound(clipDie.name);
            }*/

            this.IgnoreRaycast();
            animator.ChangeState(AnimationType.Death);

            animator.OnComplete(() =>
            {
                SpriteRenderer.DOFade(0f, 2f)
                    .OnComplete(() =>
                    {
                        gameObject.SetActive(false);
                        Revoke().Forget();

                        if (resource == null)
                        {
                            return;
                        }

                        GameEvent.DoQuestKill(resource.id);
                        GameManager.UpdateUser(() => { }, () => { }).Forget();
                    });
            });
        }

        protected void StartMoveAgain()
        {
            SpriteRenderer.flipX = previousSpriteFlip;
            otherResource = null;

            animator.ChangeState(AnimationType.Move);

            /*if (clipMove != null)
            {
                AudioManager.Instance.PlaySound(clipMove.name, 10);
            }*/
        }

        protected async void TakeDamage(float dmg)
        {
            Hp -= dmg;

            if (imgFill == null)
            {
                return;
            }

            imgFill.DOFillAmount(Hp / maxHp, lerpSpeed);

            await UniTask.Delay(TimeSpan.FromSeconds(3f));

            cvHealth.gameObject.SetActive(false);
        }

        private async void Flip(bool flip)
        {
            moveX = Random.Range(minX, maxX);

            if (flip)
            {
                idle = true;

                AudioManager.Instance.StopSound();

                animator.ChangeState(AnimationType.Idle);

                await UniTask.Delay(TimeSpan.FromSeconds(idleTime));
            }

            if (otherResource != null)
            {
                idle = false;
                return;
            }

            idle = false;

            animator.ChangeState(AnimationType.Move);

            /*if (clipMove != null)
            {
                AudioManager.Instance.PlaySound(clipMove.name, 10);
            }*/
        }

        private void ResetOriginSpeed()
        {
            var spd = _originSpeed;

            if (spd.Equals(Mathf.Abs(moveSpeed)))
            {
                return;
            }

            if (moveSpeed < 0)
            {
                spd *= -1;
            }

            moveSpeed = spd;
        }
    }
}