﻿using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.UI.UIChallenge;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Boss
{
    public partial class Boss
    {
        public override async UniTaskVoid Revoke()
        {
            if (received || rewards.Count < 1)
            {
                return;
            }

            received = true;

            var index = rewards.Select(x => x.drop).ToList().DropChance();
            var position = transform.position;
            var p = new Vector3(position.x, position.y, -0.01f);

            if (rewards[index].treasure != null)
            {
                var clone = SpawnerEx.CreateSpawner(p, null, treasurePrefab);
                var treasure = clone.GetComponent<Treasure>();
                
                clone.transform.localScale = Vector3.one / 2f;

                if (treasure != null)
                {
                    treasure.Initiation(rewards[index].treasure, transform, false);
                    treasure.IgnoreRaycast();

                    await UniTask.Delay(TimeSpan.FromSeconds(1f));
                    
                    treasure.Revoke().Forget();
                }
            }
        }

        public void ShowUIChallenge()
        {
            if (cvHealth != null)
            {
                cvHealth.gameObject.SetActive(true);
            }

            UIManager.Instance.ShowUI<UIChallenge>();
        }

        public void IncreaseSpeed()
        {
            moveSpeed *= 3;
        }
    }
}