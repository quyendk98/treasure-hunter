﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Resource.Boss.Range;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Boss.Empty
{
    public class BossEmpty : Boss
    {
        [SerializeField] private List<BossRange> bossRanges = new List<BossRange>();

        private void Update()
        {
            if (bossRanges.Any(x => x.Hp > 0f) || Hp <= 0f)
            {
                return;
            }

            TakeDamage(1);
            this.IgnoreRaycast();
            Revoke().Forget();
        }
    }
}