﻿using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Resource.Boss.Range;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Boss
{
    public partial class Boss
    {
        protected override void Awake()
        {
            base.Awake();
            _originSpeed = moveSpeed;
            Hp = maxHp;

            if (cvHealth != null)
            {
                cvHealth.worldCamera = GameManager.Camera;
            }

            /*if (clipAppear != null)
            {
                AudioManager.Instance.PlaySound(clipAppear.name);
            }*/

            animator.OnComplete(() =>
            {
                animator.ChangeState(AnimationType.Idle);

                animator.OnComplete(() =>
                {
                    animator.ChangeState(AnimationType.Move);

                    /*if (clipMove != null)
                    {
                        AudioManager.Instance.PlaySound(clipMove.name, 10);
                    }*/
                });
            });

            GameEvent.OnDispute += DoDispute;
        }

        private void OnDestroy()
        {
            GameEvent.OnDispute -= DoDispute;

            /*if (clipDispute != null && AudioManager.Instance != null)
            {
                AudioManager.Instance.PlayMusic("map-shop2");
            }*/
        }

        private void FixedUpdate()
        {
            if (!GameManager.Instance.IsGameState(GameState.Playing))
            {
                return;
            }

            if (otherResource != null || !(Hp > 0f))
            {
                return;
            }

            if (!idle && !Player.Player.Instance.Hook.DisputedAfterCollision)
            {
                if (transform.localPosition.x >= moveX && moveSpeed > 0f)
                {
                    Flip(true);
                    ResetOriginSpeed();

                    moveSpeed = -moveSpeed;
                    SpriteRenderer.flipX = true;
                }
                else if (transform.localPosition.x < moveX && moveSpeed < 0f)
                {
                    Flip(true);
                    ResetOriginSpeed();

                    moveSpeed = -moveSpeed;
                    SpriteRenderer.flipX = false;
                }

                transform.position += Vector3.right * (moveSpeed * Time.fixedDeltaTime);
            }

            var size = Physics2D.CircleCastNonAlloc(transform.position, radiusDetectResource,
                Vector2.up, _hit2Ds, radiusDetectResource, resourceLayerMask);

            if (size <= 0)
            {
                return;
            }

            foreach (var x in _hit2Ds)
            {
                if (x.collider == null)
                {
                    continue;
                }

                var other = x.collider.GetComponent<Resource>();

                if (other != null
                    && other.gameObject.activeInHierarchy
                    && other.IsCaught
                    && other.BossResultDisputeWinner == DisputeWinner.None)
                {
                    previousSpriteFlip = SpriteRenderer.flipX;

                    otherResource = other;
                    otherResource.SetLayer(30);
                    otherResource.IsDisputing = true;

                    if (this is BossRange || move)
                    {
                        previousSpriteFlip = SpriteRenderer.flipX;
                    }

                    SpriteRenderer.flipX = flipWhenDispute;

                    if (otherResource == null)
                    {
                        otherResource.Hook.LineBack();
                        otherResource.SetLayer(11);

                        otherResource.IsDisputing = false;
                    }
                    else if (otherResource != null && other.BossResultDisputeWinner == DisputeWinner.None)
                    {
                        /*if (clipDispute != null)
                    {
                        AudioManager.Instance.PlayMusic(clipDispute.name);
                    }*/

                        StartDispute();
                        return;
                    }
                }
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, radiusDetectResource);
        }
    }
}