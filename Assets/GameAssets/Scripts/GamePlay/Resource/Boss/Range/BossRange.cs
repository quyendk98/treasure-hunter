using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using GameAssets.Scripts.UI.UISquid;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Boss.Range
{
    public class BossRange : Boss
    {
        [SerializeField] private GameObject bulletPrefab;

        [SerializeField] private Transform exitPoint;

        [SerializeField] private float decreaseSpeed = 30;

        protected override void StartDispute()
        {
            if (otherResource.transform.position.x > transform.position.x)
            {
                SpriteRenderer.flipX = false;
            }
            else if (otherResource.transform.position.x < transform.position.x)
            {
                SpriteRenderer.flipX = true;
            }

            animator.ChangeState(AnimationType.Dispute);

            animator.OnComplete(() =>
            {
                if (otherResource == null)
                {
                    StartMoveAgain();
                    return;
                }

                var bullet = SpawnerEx.CreateSpawner(Vector3.zero, exitPoint, bulletPrefab);
                var bt = bullet.transform;

                bt.localPosition = Vector3.zero;

                var angle = MathfEx.AngleBetween(bt.position, otherResource.transform.position);

                bullet.transform.rotation = Quaternion.Euler(0f, 0f, angle);

                bullet.GetComponent<Bullet>()
                    ?.Initiation(otherResource.transform, () =>
                    {
                        if (otherResource == null)
                        {
                            return;
                        }

                        GameEvent.DoChangeSpeed(-decreaseSpeed, false);

                        otherResource.SetLayer(11);

                        otherResource.BossResultDisputeWinner = DisputeWinner.Player;
                        otherResource.IsDisputing = false;
                        otherResource = null;

                        if (!Player.Player.Instance.HasRemoveTime(resource))
                        {
                            uiPlaySgp.ChangeTime(removeTime);
                        }
                    }, StartMoveAgain);

                StartMoveAgain();
            });
        }

        protected override void DoDispute(float dmg, bool useBomb)
        {
            if (!GetInstanceID().Equals(Player.Player.Instance.Hook.BossRange.GetInstanceID()))
            {
                return;
            }

            if (useBomb)
            {
                UIManager.Instance.GetUI<UIPlaySgp>()?.ChangeTime(Player.Player.Instance.MoreSecondWhenWinDispute());
                SpawnerEx.CreateFxBomb(transform.position);
            }

            if (cvHealth != null)
            {
                cvHealth.gameObject.SetActive(true);
            }

            TakeDamage(dmg);
            Player.Player.Instance.Hook.LineBack();

            if (!(Hp <= 0f))
            {
                StartMoveAgain();

                if (squid)
                {
                    UIManager.Instance.ShowUI<UISquid>();
                }

                return;
            }

            Death();
        }
    }
}