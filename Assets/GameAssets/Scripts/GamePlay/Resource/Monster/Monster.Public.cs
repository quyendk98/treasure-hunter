﻿using DG.Tweening;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Monster
{
    public partial class Monster
    {
        public override void Caught()
        {
            animator.ChangeState(AnimationType.Idle);
            base.Caught();
        }

        public override void SuccessDispute(Vector3 position)
        {
            IsCaught = false;
            BossResultDisputeWinner = DisputeWinner.Boss;

            transform.DOMove(position, 0.5f)
                .OnComplete(() =>
                {
                    BossResultDisputeWinner = DisputeWinner.None;
                    animator.ChangeState(AnimationType.Move);
                });

            Hook.FailedDispute();
        }
    }
}