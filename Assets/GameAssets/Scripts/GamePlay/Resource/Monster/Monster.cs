using System;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using UnityEngine;

namespace GameAssets.Scripts.GamePlay.Resource.Monster
{
    public partial class Monster : Resource
    {
        [Header("[SOUND]")] [SerializeField] protected AudioClip clipAppear;
        [SerializeField] protected AudioClip clipMove;
        [SerializeField] protected AudioClip clipDispute;
        [SerializeField] protected AudioClip clipDie;

        [Header("[MONSTER]")] [SerializeField] protected float minX;
        [SerializeField] protected float maxX;

        [SerializeField] protected float moveSpeed;
        [SerializeField] protected float idleTime = 2f;

        protected Animator animator;

        protected bool idle;

        protected virtual void Move()
        {
            transform.Translate(Vector3.right * (moveSpeed * Time.fixedDeltaTime));

            if (transform.localPosition.x <= minX && moveSpeed < 0
                || transform.localPosition.x >= maxX && moveSpeed > 0)
            {
                Flip();
            }
        }

        private async void Flip()
        {
            idle = true;

            animator.ChangeState(AnimationType.Idle);

            await UniTask.Delay(TimeSpan.FromSeconds(idleTime));

            if (IsCaught)
            {
                return;
            }

            idle = false;
            moveSpeed *= -1;

            if (SpriteRenderer == null)
            {
                return;
            }

            SpriteRenderer.flipX = moveSpeed < 0;

            animator.ChangeState(AnimationType.Move);
        }
    }
}