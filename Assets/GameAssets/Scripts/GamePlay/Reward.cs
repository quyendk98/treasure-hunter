﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.GamePlay
{
    public class Reward : MonoBehaviour
    {
        [SerializeField] private Image imgIcon;

        [SerializeField] private TextMeshProUGUI txtAmount;

        public void Initiation(Sprite icon, int amount, bool black = false)
        {
            imgIcon.sprite = icon;
            txtAmount.text = $"x{amount}";
            txtAmount.color = black ? Color.black : Color.white;
        }
    }
}