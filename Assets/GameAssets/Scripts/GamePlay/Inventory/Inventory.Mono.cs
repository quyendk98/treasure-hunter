﻿using System;

namespace GameAssets.Scripts.GamePlay.Inventory
{
    public partial class Inventory
    {
        protected override void Awake()
        {
            base.Awake();

            foreach (var x in filters)
            {
                x.btnFilter.onClick.RemoveAllListeners();
                x.btnFilter.onClick.AddListener(() => DrawFilter(x));
            }
        }

        private void OnEnable()
        {
            DrawFilter(filters[0]);
        }
    }
}