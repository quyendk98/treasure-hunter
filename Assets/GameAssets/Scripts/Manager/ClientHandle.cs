﻿using Client.Base;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public static class ClientHandle
    {
        public static void Connected(Packet packet)
        {
            var msg = packet.ReadString();

            Debug.Log($"Connected to server: {msg}");
        }
    }
}