using System;
using System.Collections.Generic;
using System.Net;
using Client.Base;
using Client.Enum;
using Client.Ex;
using UnityEngine;

namespace GameAssets.Scripts.Manager
{
    public class ServerManager : Server
    {
        #region MonoBehaviour

        #endregion

        #region Public

        #endregion

        #region Private

        #endregion

        #region Protected

        protected override void OnServerStatus(ServerCode serverCode)
        {
            switch (serverCode)
            {
                case ServerCode.Connected:
                    Debug.Log("Connected to server successfully");
                    UdpClient.Connect(((IPEndPoint) TcpClient.Socket.Client.LocalEndPoint).Port);
                    break;

                case ServerCode.Disconnect:
                    Log.Debug("Disconnect from server event");
                    break;
            }
        }

        #endregion
    }
}