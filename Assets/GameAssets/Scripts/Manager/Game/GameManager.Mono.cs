﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager
    {
        protected override void Awake()
        {
            base.Awake();

#if UNITY_EDITOR || !UNITY_WEBGL
            Application.targetFrameRate = 120;
#endif

            GameEvent.OnWin += OnWin;
            GameEvent.OnDraw += OnDraw;
            GameEvent.OnLose += OnLose;

            GameEvent.OnFailedInternet += OnFailedInternet;
            GameEvent.OnConnectedInternet += OnConnectedInternet;

            Camera = Camera.main;
            QualitySettings.vSyncCount = 0;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Setup()
        {
            var items = new List<Item>();
            var maps = new List<Map>();

            items.AddRange(Resources.LoadAll<Item>("Craft"));
            items.AddRange(Resources.LoadAll<Weapon>("Hook"));
            items.AddRange(Resources.LoadAll<Weapon>("Line"));
            items.AddRange(Resources.LoadAll<Weapon>("Skin"));
            items.AddRange(Resources.LoadAll<Weapon>("Pet"));
            items.AddRange(Resources.LoadAll<Booster>("Booster"));
            items.AddRange(Resources.LoadAll<Treasure>("Treasure"));

            maps.AddRange(Resources.LoadAll<Map>("Map"));

            ResourceItemsDictionaries = items.ToDictionary(x => x.id, x => x);
            ResourceMapsDictionaries = maps.ToDictionary(x => x.id, x => x);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            GameEvent.OnWin -= OnWin;
            GameEvent.OnDraw -= OnDraw;
            GameEvent.OnLose -= OnLose;

            GameEvent.OnFailedInternet -= OnFailedInternet;
            GameEvent.OnConnectedInternet -= OnConnectedInternet;
        }

        public override void InnerUpdate()
        {
            foreach (var x in characters)
            {
                x.currentState?.UpdateState();
            }
        }
    }
}