﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Player;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.UI.UIGameComplete;
using GameAssets.Scripts.UI.UIGameDraw;
using GameAssets.Scripts.UI.UIGameOver;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Game
{
    public partial class GameManager : BaseSingleton<GameManager>
    {
        [SerializeField] private List<BaseCharacter> characters = new List<BaseCharacter>();
        [SerializeField] private Map finalMap;

        private void OnWin()
        {
            var t = UIManager.Instance.ShowUI<UIGameComplete>();

            if (t == null)
            {
                return;
            }

            UserDataManager.Instance.LevelUp();

            SetGameState(GameState.Win);

            SelectedLevel.completed = true;
            User.Maps[SelectedMapDataIndex].CompleteLevels[SelectedLevelIndex] = true;

            GameEvent.DoStopBaseCharacter();
            GameEvent.DoQuestCompleteMap(SelectedMap);
            GameEvent.DoQuestCompleteLevel(SelectedLevel);

            UpdateQuest();

            UpdateUser(() =>
                {
                    RevokeUpdateQuest();

                    SelectedLevel.completed = false;
                    User.Maps[SelectedMapDataIndex].CompleteLevels[SelectedLevelIndex] = false;

                    SceneManager.LoadScene("Game Menu");
                }, () => { })
                .Forget();
        }

        private void OnDraw()
        {
            var t = UIManager.Instance.ShowUI<UIGameDraw>();

            if (t == null)
            {
                return;
            }

            GameEvent.DoStopBaseCharacter();
            Player.Instance.Hook.LineBack();

            SetGameState(GameState.Draw);
        }

        private void OnLose()
        {
            var t = UIManager.Instance.ShowUI<UIGameOver>();

            if (t == null)
            {
                return;
            }

            GameEvent.DoStopBaseCharacter();

            SetGameState(GameState.Lose);
            UpdateQuest();
            UpdateUser(RevokeUpdateQuest, () => { }).Forget();
        }

        private void OnFailedInternet()
        {
            if (!IsGameState(GameState.Playing))
            {
                return;
            }

            SetGameState(GameState.LostConnected);

            SpawnerEx.CreateNotification("Please enable your wifi/3g to continue", "Thông báo",
                () => SceneManager.LoadScene("Game Menu"));
        }

        private void OnConnectedInternet()
        {
            if (!IsGameState(GameState.LostConnected))
            {
                return;
            }

            SetGameState(GameState.Playing);

            SpawnerEx.CreateNotification("Reconnected");
        }
    }
}