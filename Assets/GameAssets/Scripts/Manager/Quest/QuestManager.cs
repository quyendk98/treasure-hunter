﻿using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Quest
{
    public class QuestManager : BaseSingleton<QuestManager>
    {
        #region MonoBehaviour

        #endregion

        #region Public

        public override void Initialize()
        {
            foreach (var x in GameManager.User.Maps)
            {
                var map = GameManager.ResourceMap(x);

                foreach (var y in x.Quests)
                {
                    foreach (var z in map.quests.Where(z => y.Id == z.id))
                    {
                        z.Initialize(y);
                        break;
                    }
                }
            }
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}