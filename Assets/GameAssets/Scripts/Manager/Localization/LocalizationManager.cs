using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Manager.Localization
{
    [Serializable]
    public class LocalizationFormat
    {
        public string title;

        [TableList] public List<Localization<string>> localizations = new List<Localization<string>>();
    }

    public class LocalizationManager : BaseSingleton<LocalizationManager>
    {
        [TableList] [SerializeField]
        private List<LocalizationFormat> localizationFormats = new List<LocalizationFormat>();

        [DisableIf(nameof(awakeSystemLanguage))] [SerializeField]
        private SystemLanguage currentLanguage;

        [SerializeField] private bool awakeSystemLanguage;

        private Dictionary<string, List<Localization<string>>> _localizationFormatDictionaries =
            new Dictionary<string, List<Localization<string>>>();

        #region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            _localizationFormatDictionaries = localizationFormats.ToDictionary(x => x.title, x => x.localizations);
        }

        private void OnValidate()
        {
#if UNITY_EDITOR
            if (awakeSystemLanguage)
            {
                currentLanguage = Application.systemLanguage;
            }
#endif
        }

        #endregion

        #region Public

        public SystemLanguage LanguageDefault()
        {
            if (PlayerPrefs.HasKey(nameof(SystemLanguage)))
            {
                currentLanguage = (SystemLanguage) Enum.Parse(typeof(SystemLanguage),
                    PlayerPrefs.GetString(nameof(SystemLanguage)));
            }

            return currentLanguage;
        }

        public static string Format(string title)
        {
            if (Instance._localizationFormatDictionaries.TryGetValue(title, out var x))
            {
                foreach (var s in from y in x
                    where y.language == Instance.currentLanguage
                    select y.value)
                {
                    return s;
                }

                return "No Format Data";
            }

            return "No Format Data";
        }

        public static void ChangeLanguage(SystemLanguage localizationLanguage)
        {
            Instance.currentLanguage = localizationLanguage;

            PlayerPrefs.SetString(nameof(SystemLanguage), Instance.currentLanguage.ToString());
            GameEvent.DoChangeLanguage(localizationLanguage);
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}