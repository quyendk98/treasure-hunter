﻿using System.Linq;

namespace GameAssets.Scripts.Manager.Audio
{
    public partial class AudioManager
    {
        protected override void Awake()
        {
            base.Awake();

            _audioClipDictionaries = audioClips.ToDictionary(x => x.name, x => x);
        }
    }
}