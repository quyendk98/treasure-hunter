using DG.Tweening;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay.Player;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.UI.UIChallenge
{
    public partial class UIChallenge
    {
        private void Awake()
        {
            btnDispute.onClick.AddListener(() =>
            {
                _value += 1f / _totalDispute;

                if (_value >= 1f)
                {
                    btnDispute.interactable = false;
                }

                CameraEx.Shake(shakeDuration, shakeAmount);
                AudioManager.PlayVibrate();

                imgDisputeFill.DOFillAmount(_value, lerpSpeed)
                    .SetEase(Ease.InOutQuart)
                    .OnComplete(() =>
                    {
                        if (imgDisputeFill.fillAmount < 0.99f)
                        {
                            return;
                        }

                        Player.Instance.Dispute();
                        GameManager.Instance.SetGameState(GameState.Playing);
                        gameObject.SetActive(false);
                    });
            });
        }

        private void Update()
        {
            if (!(_value > 0f))
            {
                return;
            }

            _value -= Time.deltaTime / 5f;
            imgDisputeFill.DOFillAmount(_value, lerpSpeed);
        }

        private void OnEnable()
        {
            _totalDispute = Random.Range(10, 15);
            imgDisputeFill.DOFillAmount(0f, 0f);
            btnDispute.interactable = true;
            _value = 0f;
        }
    }
}