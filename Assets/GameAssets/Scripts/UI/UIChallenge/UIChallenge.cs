using GameAssets.Scripts.Base;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIChallenge
{
    public partial class UIChallenge : BaseUI
    {
        [SerializeField] private Button btnDispute;

        [SerializeField] private Image imgDisputeFill;

        [SerializeField] private float lerpSpeed = 0.5f;
        [SerializeField] private float shakeDuration = 1f;
        [SerializeField] private float shakeAmount = 1f;

        private int _totalDispute;

        private float _value;
    }
}