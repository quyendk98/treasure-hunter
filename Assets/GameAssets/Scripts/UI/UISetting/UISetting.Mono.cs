using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Localization;
using UnityEngine;

namespace GameAssets.Scripts.UI.UISetting
{
    public partial class UISetting
    {
        private void Awake()
        {
            btnSignout.AddListener(() => Click(SignOut));
            btnClose.onClick.AddListener(() => gameObject.SetActive(false));

            btnVietnamese.onClick.AddListener(() => LocalizationManager.ChangeLanguage(SystemLanguage.Vietnamese));
            btnEnglish.onClick.AddListener(() => LocalizationManager.ChangeLanguage(SystemLanguage.English));

            btnFacebook.onClick.AddListener(() => SpawnerEx.CreateNotification("Chức năng đang bảo trì"));
            btnTwitter.onClick.AddListener(() => SpawnerEx.CreateNotification("Chức năng đang bảo trì"));
        }
    }
}