using GameAssets.Scripts.Base;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UISetting
{
    public partial class UISetting : BaseUI
    {
        [SerializeField] private Button btnClose;
        [SerializeField] private GamePlay.Button btnSignout;
        [SerializeField] private Button btnEnglish;
        [SerializeField] private Button btnVietnamese;

        [SerializeField] private Button btnFacebook;
        [SerializeField] private Button btnTwitter;
    }
}
