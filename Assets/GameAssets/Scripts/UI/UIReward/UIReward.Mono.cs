using GameAssets.Scripts.Manager;

namespace GameAssets.Scripts.UI.UIReward
{
    public partial class UIReward
    {
        private void Awake()
        {
            btnBack.onClick.AddListener(() => UIManager.Instance.HideAllAndShowUI<UIInventory.UIInventory>());
        }
    }
}