using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Event;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Manager.Localization;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;

namespace GameAssets.Scripts.UI.UIReward
{
    public partial class UIReward : BaseUI
    {
        [SerializeField] private Page pageReward;

        [SerializeField] private Button btnBack;

        [SerializeField] private TextMeshProUGUI txtResult;

        private readonly List<Amount> _rewards = new List<Amount>();

        private int _index;

        private void UpdateResult(int amount)
        {
            txtResult.text = $"{LocalizationManager.Format("you-got")} {amount} {LocalizationManager.Format("item")}";
        }

        private void OpenMap(Item item)
        {
            if (item.itemType != ItemType.Piece)
            {
                return;
            }

            foreach (var x in GameManager.ResourceMapsDictionaries.Where(x =>
                x.Value.deffectRequire != null && x.Value.deffectRequire.id == item.id))
            {
                if (GameManager.User.Maps.Any(y => x.Value.id == y.Id))
                {
                    SpawnerEx.CreateNotification(LocalizationManager.Format("map-opened"));
                    return;
                }

                var md = MapData.Create(x.Value);

                GameManager.User.Maps.Add(md);
                GameManager.User.RemoveItem(item, 1);

                GameManager.UpdateUser(() =>
                        {
                            GameManager.User.Maps.Remove(md);
                            GameManager.User.AddItem(item, 1);
                        },
                        () => GameEvent.DoOpenNewMap(x.Value.id))
                    .Forget();

                return;
            }
        }
    }
}