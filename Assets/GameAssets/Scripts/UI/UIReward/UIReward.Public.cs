using System.Linq;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.GamePlay.Slot;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIReward
{
    public partial class UIReward
    {
        public void Initiation(Treasure treasure, int amount)
        {
            _index = 0;

            _rewards.Clear();

            for (var i = 0; i < amount; i++)
            {
                foreach (var x in from x in treasure.rewards
                    let rnd = Random.Range(0f, 100f)
                    where rnd <= x.drop
                    select x)
                {
                    if (x.random)
                    {
                        var rnd = x.Random;

                        if (rnd.item == null)
                        {
                            continue;
                        }

                        _rewards.Add(rnd);

                        GameManager.User.AddItem(rnd.item, rnd.amount);
                    }
                    else
                    {
                        foreach (var y in x.rewards)
                        {
                            _rewards.Add(y);

                            GameManager.User.AddItem(y.item, y.amount);
                        }
                    }
                }
            }

            UpdateResult(_rewards.Count);
            pageReward.SetTotalItem(_rewards.Count);

            if (_rewards.Count <= 0)
            {
                return;
            }

            pageReward.OnCreateSlot(go =>
            {
                var item = _rewards[_index];
                var slot = go.GetComponent<RewardSlot>();

                slot.Initialize(ItemData.Create(item.item));

                _index++;
                return item.item.id;
            });

            GameManager.User.RemoveItem(treasure, amount);

            GameManager.UpdateUser(() =>
                {
                    GameManager.User.AddItem(treasure, amount);

                    foreach (var x in _rewards)
                    {
                        GameManager.User.RemoveItem(x.item, x.amount);
                    }
                }, () => { })
                .Forget();
        }
    }
}