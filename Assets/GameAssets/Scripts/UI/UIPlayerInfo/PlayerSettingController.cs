﻿using GameAssets.Scripts.Manager.Localization;
using UnityEngine;
using Button = GameAssets.Scripts.GamePlay.Button;

namespace GameAssets.Scripts.UI.UIPlayerInfo
{
    public class PlayerSettingController : MonoBehaviour
    {
        [SerializeField] private Button btnEnglish;
        [SerializeField] private Button btnVietnamese;
        [SerializeField] private Button btnSignOut;

        private void Awake()
        {
            btnEnglish.AddListener(() => LocalizationManager.ChangeLanguage(SystemLanguage.English));
            btnVietnamese.AddListener(() => LocalizationManager.ChangeLanguage(SystemLanguage.Vietnamese));
            btnSignOut.AddListener(UIPlayerInfo.SignOut);
        }
    }
}