using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Model;
using Tamarin.FirebaseX;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIPlay.GameMenu
{
    public partial class UIPlaySgm
    {
        private void Awake()
        {
            _emailLoginValidator = inputUsernameLogin.GetComponent<ValidatorEx>();
            _passwordLoginValidator = inputPasswordLogin.GetComponent<ValidatorEx>();

            _emailRegisterValidator = inputUsernameRegister.GetComponent<ValidatorEx>();
            _passwordRegisterValidator = inputPasswordRegister.GetComponent<ValidatorEx>();
            _confirmPasswordRegisterValidator = inputConfirmPasswordRegister.GetComponent<ValidatorEx>();

            _txtLogin = btnLLogin.transform.GetChild(0);
            _txtRegister = btnRRegister.transform.GetChild(0);

            _imgLoginLoading = btnLLogin.transform.GetChild(1);
            _imgRegisterLoading = btnRRegister.transform.GetChild(1);

            btnPlay.AddListener(() => UIManager.Instance.HideAllAndShowUI<UISelectMap.UISelectMap>());

            btnLRegister.onClick.AddListener(() =>
            {
                loginPanel.SetActive(false);
                registerPanel.SetActive(true);
            });

            btnRLogin.onClick.AddListener(() =>
            {
                loginPanel.SetActive(true);
                registerPanel.SetActive(false);
            });

            formLogin.FunctionAfterAllInputEntered += () => Click(DoLogin);
            formRegister.FunctionAfterAllInputEntered += () => Click(DoRegister);

            async void DoLogin()
            {
                if (!_emailLoginValidator.Success)
                {
                    if (inputUsernameLogin.text.Length < 1)
                    {
                        SpawnerEx.CreateNotification("Username không được để trống");
                        return;
                    }

                    SpawnerEx.CreateNotification(_emailLoginValidator.Error);
                    return;
                }

                if (!_passwordLoginValidator.Success)
                {
                    if (inputPasswordLogin.text.Length < 1)
                    {
                        SpawnerEx.CreateNotification("Mật khẩu không được để trống");
                        return;
                    }

                    SpawnerEx.CreateNotification(_passwordLoginValidator.Error);
                    return;
                }

                ReadOnly(true);

                var user = await FirebaseAPI.Instance.SignInEmail($"{inputUsernameLogin.text}@gmail.com",
                    inputPasswordLogin.text);

                if (user != null)
                {
                    if (user.status)
                    {
                        var data = await FirebaseAPI.Instance.QueryAs<User>(Const.PATH_USER,
                            $"userId-{user.UserId}");

                        if (data == null)
                        {
                            ShowError("Đăng nhập lỗi");
                            return;
                        }

                        isSignedIn = true;

                        RegisterEventUserChanged(data.Id);
                        ReturnLogin(true);

                        Debug.Log("Sign in firebase success");
                    }
                    else
                    {
                        Debug.Log($"Sign in error: {user.message}");

                        ShowError(user.message.Contains("deleted")
                            ? "User không tồn tại hoặc đã bị xóa"
                            : "Sai tài khoản, mật khẩu");
                    }
                }
                else
                {
                    ShowError("Đăng nhập lỗi, thử lại");
                }
            }

            void DoRegister()
            {
                if (!_emailRegisterValidator.Success)
                {
                    if (inputUsernameRegister.text.Length < 1)
                    {
                        SpawnerEx.CreateNotification("Username không được để trống");
                        return;
                    }

                    SpawnerEx.CreateNotification(_emailRegisterValidator.Error);
                    return;
                }

                if (!_passwordRegisterValidator.Success)
                {
                    if (inputPasswordRegister.text.Length < 1)
                    {
                        SpawnerEx.CreateNotification("Mật khẩu không được để trống");
                        return;
                    }

                    SpawnerEx.CreateNotification(_passwordRegisterValidator.Error);
                    return;
                }

                if (!_confirmPasswordRegisterValidator.Success)
                {
                    if (inputConfirmPasswordRegister.text.Length < 1)
                    {
                        SpawnerEx.CreateNotification("Xác nhận lại mật khẩu không được để trống");
                        return;
                    }

                    SpawnerEx.CreateNotification(_confirmPasswordRegisterValidator.Error);
                    return;
                }

                ReadOnly(true);

                StartCoroutine(BaseClient.Register(inputUsernameRegister.text,
                    inputPasswordRegister.text, inputReferenceRegister.text,
                    OnRegisterSuccess, () => ShowError("Đăng ký lỗi, thử lại")));
            }

            btnLLogin.AddListener(() => Click(DoLogin));
            btnRRegister.AddListener(() => Click(DoRegister));
        }

        private void Start()
        {
            AudioManager.Instance.Initiation();

            if (isSignedIn)
            {
                Debug.Log("User is logged in");

                ReturnLogin(true);

                registerPanel.SetActive(false);
            }

#if !UNITY_EDITOR && UNITY_WEBGL
            Bridge.Bridge.Instance.StopLoading();
#endif
        }

        private void OnDestroy()
        {
            _imgLoginLoading.DOKill();
            _imgRegisterLoading.DOKill();
        }
    }
}