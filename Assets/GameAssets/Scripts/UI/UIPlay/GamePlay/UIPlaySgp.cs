using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIPlay.GamePlay
{
    public partial class UIPlaySgp : BaseUI
    {
        [Header("[TRANSFORM]")] [SerializeField]
        private Transform boosterParent;

        [SerializeField] private GameObject treasurePrefab;
        [SerializeField] private GameObject boosterPrefab;

        [SerializeField] private Collider2D playArea;

        [Header("[BUTTON]")] [SerializeField] private Button btnHookShoot;
        [SerializeField] private Button btnPause;
        [SerializeField] private Button btnDestroyItem;

        [Header("[SPRITE]")] [SerializeField] private Sprite icResume;
        [SerializeField] private Sprite icBoosterFirecracker;
        [SerializeField] private Sprite icBoosterStrength;

        [Header("[IMAGE]")] [SerializeField] private Image imgPause;

        [Header("[TEXT]")] [SerializeField] private TextMeshProUGUI txtTime;
        [SerializeField] private TextMeshProUGUI txtLevel;
        [SerializeField] private TextMeshProUGUI txtGold;
        [SerializeField] private TextMeshProUGUI txtDiamond;
        [SerializeField] private TextMeshProUGUI txtEnergy;

        [Header("[FLOAT]")] [SerializeField] private float slowTime = 2f;
        [SerializeField] private float lerpSpeed = 2f;

        public float Gold { get; private set; }
        public float Target { get; private set; }

        private int _totalFirecracker;
        private int _totalStrength;

        private float _second;

        private void UpdateResource()
        {
            txtGold.NumberCounter(0f, GameManager.User.Gold);
            txtDiamond.NumberCounter(0f, GameManager.User.Diamond);

            txtEnergy.text = $"{GameManager.User.Energy}/50";
        }
    }
}