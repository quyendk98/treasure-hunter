using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Manager.Localization;
using GameAssets.Scripts.Model;
using UnityEngine;

namespace GameAssets.Scripts.UI.UISelectLevel
{
    public partial class UISelectLevel : BaseUI
    {
        [Header("[OTHER]")] [SerializeField] private UIQuest.UIQuest uiQuest;

        [Header("[LIST]")] [SerializeField] private List<GameObject> maps = new List<GameObject>();

        [Header("[BUTTON]")] [SerializeField] private Button btnQuest;
        [SerializeField] private Button btnBack;
        [SerializeField] private Button btnInfo;

        private static void OpenLevel(Level level)
        {
            if (GameManager.User.Hook == null)
            {
                SpawnerEx.CreateNotification(LocalizationManager.Format("request-hook"));
                return;
            }

            if (GameManager.User.Line == null)
            {
                SpawnerEx.CreateNotification(LocalizationManager.Format("request-line"));
                return;
            }

            if (GameManager.User.Skin == null)
            {
                SpawnerEx.CreateNotification(LocalizationManager.Format("request-skin"));
                return;
            }

            var index = GameManager.SelectedMap.levels.IndexOf(level);

            if (index == 0 || GameManager.SelectedMapData.CompleteLevels[index - 1])
            {
                if (!GameManager.SelectedMapData.CompleteLevels[index])
                {
                    if (level.requires.Any(x => !x.CanBypass()))
                    {
                        SpawnerEx.CreateNotification(LocalizationManager.Format("not-enough-request"));
                        return;
                    }
                }

                if (GameManager.CheckSpentEnergy(level.energy))
                {
                    GameManager.SetSelectedLevel(level, index);
                    SceneManager.LoadScene("Game Play");
                }
                else
                {
                    SpawnerEx.CreateNotification(LocalizationManager.Format("not-enough-energy"));
                }
            }
            else
            {
                SpawnerEx.CreateNotification(LocalizationManager.Format("complete-prev-level"));
            }
        }
    }
}