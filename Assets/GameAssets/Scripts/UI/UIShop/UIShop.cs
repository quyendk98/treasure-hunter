﻿using System.Collections.Generic;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay.Slot;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIShop
{
    public partial class UIShop : BaseUI
    {
        [SerializeField] private List<Filter> filters = new List<Filter>();

        [SerializeField] private Button btnBack;

        private void CreateShop(Filter filter)
        {
            foreach (var x in filters)
            {
                filter.pageFilter.gameObject.SetActive(x == filter);
            }

            filter.pageFilter.SetTotalItem(Random.Range(5, 15));

            filter.pageFilter.OnCreateSlot(go =>
            {
                var rnd = GameManager.User.Items[Random.Range(0, GameManager.User.Items.Count)];

                go.GetComponent<Slot>().Initialize(rnd);
                return rnd.Id + Random.Range(0, 50000);
            });
        }
    }
}