﻿using System.Linq;
using GameAssets.Scripts.Manager;

namespace GameAssets.Scripts.UI.UIShop
{
    public partial class UIShop
    {
        private void Awake()
        {
            foreach (var x in filters)
            {
                x.btnFilter.onClick.RemoveAllListeners();

                x.btnFilter.onClick.AddListener(() =>
                {
                    x.pageFilter.gameObject.SetActive(true);

                    foreach (var y in filters.Where(y => y != x))
                    {
                        y.pageFilter.gameObject.SetActive(false);
                    }

                    CreateShop(x);
                });
            }

            CreateShop(filters[0]);

            btnBack.onClick.AddListener(() => UIManager.Instance.HideAllAndShowUI<UISelectMap.UISelectMap>());
        }
    }
}