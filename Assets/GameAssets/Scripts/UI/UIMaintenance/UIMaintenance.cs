using System;
using GameAssets.Scripts.Base;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIMaintenance
{
    public partial class UIMaintenance : BaseUI
    {
        [SerializeField] private TextMeshProUGUI txtDays;
        [SerializeField] private TextMeshProUGUI txtHours;
        [SerializeField] private TextMeshProUGUI txtMinutes;
        [SerializeField] private TextMeshProUGUI txtSeconds;

        private float _days;
        private float _hours;
        private float _minutes;
        private float _seconds;

        private void UpdateMaintenance()
        {
            txtDays.text = $"{_days:00}";
            txtHours.text = $"{_hours:00}";
            txtMinutes.text = $"{_minutes:00}";
            txtSeconds.text = $"{_seconds:00}";
        }

        private static DateTime TimeStampToDateTime(double timestamp)
        {
            var dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dt = dt.AddSeconds(timestamp).ToLocalTime();
            return dt;
        }
    }
}
