using System;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIMaintenance
{
    public partial class UIMaintenance
    {
        private void OnEnable()
        {
            var timestamp = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            var dt = TimeStampToDateTime(MathfEx.Abs(double.Parse(GameManager.Config.TimerMaintenance), timestamp));

            _days = dt.Day;
            _hours = dt.Hour;
            _minutes = dt.Minute;
            _seconds = dt.Second;

            UpdateMaintenance();
        }

        private void Update()
        {
            if (_seconds <= 0f)
            {
                if (_minutes <= 0f && _hours <= 0f && _days <= 0f)
                {
                    SceneManager.ResetCurrentScene();
                }
                else
                {
                    _seconds = 60f;

                    if (_minutes <= 0f)
                    {
                        _minutes = 59f;

                        if (_hours <= 0f)
                        {
                            _hours = 24f;

                            if (_days > 0f)
                            {
                                _days--;
                            }
                        }
                        else
                        {
                            _hours--;
                        }
                    }
                    else
                    {
                        _minutes--;
                    }
                }
            }
            else
            {
                _seconds -= Time.deltaTime;
            }

            UpdateMaintenance();
        }
    }
}