﻿using System.Linq;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using UnityEngine;
using Quest = GameAssets.Scripts.GamePlay.Quest;

namespace GameAssets.Scripts.UI.UIQuest
{
    public partial class UIQuest
    {
        public void DrawQuest(FilterType filterType)
        {
            Filter filter = null;

            foreach (var x in filters)
            {
                if (x.filterType != filterType)
                {
                    x.pageFilter.gameObject.SetActive(false);
                }
                else
                {
                    x.pageFilter.gameObject.SetActive(true);

                    filter = x;
                }
            }

            if (filter == null)
            {
                return;
            }

            var data = GameManager.SelectedMap.quests.Where(y => y.filterType == filterType).ToList();

            foreach (var x in data)
            {
                foreach (var y in GameManager.SelectedMapData.Quests.Where(y => x.id == y.Id))
                {
                    foreach (var m in x.requires)
                    {
                        foreach (var n in y.RequireData.Where(n => m.itemRequire.id == n.IdItemRequire))
                        {
                            m.requireAmount = n.RequireAmount;
                            m.currentAmount = n.CurrentAmount;
                            break;
                        }
                    }

                    x.received = y.Received;
                }
            }

            _index = 0;

            filter.pageFilter.SetTotalItem(data.Count);

            if (data.Count > 0)
            {
                filter.pageFilter.OnCreateSlot(go =>
                {
                    var item = data[_index];

                    go.GetComponent<Quest>().Initiation(item);

                    _index++;
                    return item.id;
                });
            }
        }
    }
}