using GameAssets.Scripts.Event;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;

namespace GameAssets.Scripts.UI.UISpin
{
    public partial class UISpin
    {
        private void Awake()
        {
            btnBack.AddListener(() =>
            {
                UIManager.Instance.HideAllAndShowUI<UIShop.UIShop>();
                //AudioManager.Instance.PlayMusic("");
            });
            
            btnSpin.AddListener(pickerWheel.Spin);

            GameEvent.OnUpdateResource += UpdateResource;
            
        }

        private void OnDestroy()
        {
            GameEvent.OnUpdateResource -= UpdateResource;
        }

        private void OnEnable()
        {
            GameEvent.DoUpdateResource();
            
            UpdateLevel();
            UpdateFill();
        }
    }
}