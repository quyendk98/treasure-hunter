using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Manager.Localization;

namespace GameAssets.Scripts.UI.UIMapInfo
{
    public partial class UIMapInfo
    {
        private void Awake()
        {
            btnBack.AddListener(() => gameObject.SetActive(false));
        }

        private void OnEnable()
        {
            if (GameManager.SelectedMap.story != null)
            {
                txtTitle.text = GameManager.SelectedMap.story.title;
                txtDescription.text = GameManager.SelectedMap.story.mainDescription;
            }
            else
            {
                txtTitle.text = LocalizationManager.Format("no-title");
                txtDescription.text = LocalizationManager.Format("no-description");
            }
        }
    }
}