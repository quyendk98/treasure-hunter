using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIMapInfo
{
    public partial class UIMapInfo : BaseUI
    {
        [SerializeField] private Button btnBack;

        [SerializeField] private TextMeshProUGUI txtTitle;
        [SerializeField] private TextMeshProUGUI txtDescription;
    }
}
