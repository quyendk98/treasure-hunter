using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;

namespace GameAssets.Scripts.UI.UIPause
{
    public partial class UIPause
    {
        private void Awake()
        {
            btnHome.AddListener(() => Click(() =>
            {
                onLoading = () => UIManager.Instance.HideAllAndShowUI<UISelectMap.UISelectMap>();
                
                SceneManager.LoadScene("Game Menu");
            }));

            btnPlay.AddListener(() =>
            {
                imgPause.sprite = icPause;
                
                gameObject.SetActive(false);
                
                GameManager.Instance.SetGameState(_gameState);
            });
        }
    }
}