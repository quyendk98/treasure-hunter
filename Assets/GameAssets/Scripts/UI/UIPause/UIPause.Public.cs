﻿using GameAssets.Scripts.Manager.Game;

namespace GameAssets.Scripts.UI.UIPause
{
    public partial class UIPause
    {
        public void GetCurrentState()
        {
            _gameState = GameManager.Instance.currentState;
        }
    }
}