﻿using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager.Game;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.UI.UISquid
{
    public class UISquid : BaseUI
    {
        [SerializeField] private SpriteRenderer m_Black;

        [SerializeField] private GameObject m_SpriteTouch;
        [SerializeField] private GameObject m_Point;

        [SerializeField] private Vector3 _dragScale = new Vector3(0.65f, 0.65f, 0.65f);

        [Range(100f, 150f)] [SerializeField] private float percentRemoveSquid;

        private Vector3 _target = Vector3.zero;

        private readonly List<GameObject> _points = new List<GameObject>();

        private float _totalPercent;

        private void OnEnable()
        {
            GameManager.Instance.SetGameState(General.GameState.Pause);

            ActiveBlack(30);
            TutorialDrag();
        }

        private void Update()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                case RuntimePlatform.IPhonePlayer:
                    if (Input.touchCount > 0)
                    {
                        var touch = Input.GetTouch(0);

                        switch (touch.phase)
                        {
                            case TouchPhase.Moved:
                            case TouchPhase.Stationary:
                                PositionTarget();
                                SpawnPoints();
                                break;
                        }
                    }

                    break;

                case RuntimePlatform.WindowsEditor:
                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WebGLPlayer:
                    if (Input.GetMouseButton(0))
                    {
                        PositionTarget();
                        SpawnPoints();
                    }

                    break;
            }
        }

        private void PositionTarget()
        {
            _target = MathfEx.PlatformPosition;
            _target = GameManager.Camera.ScreenToWorldPoint(_target);
        }

        private void SpawnPoints()
        {
            if (_points.Any(x => _target == x.transform.position))
            {
                return;
            }

            var point = SpawnerEx.CreateSpawner(_target, null, m_Point);
            var t = point.transform;
            var position = t.position;

            position = new Vector3(position.x, position.y, 0);
            t.position = position;

            _points.Add(point.gameObject);

            _totalPercent += m_Black.bounds.BoundsContainedPercentage(point.GetComponent<SpriteMask>().bounds);

            /*Debug.Log($"Current Percent: {_totalPercent}");*/

            if (_totalPercent >= percentRemoveSquid)
            {
                m_Black.DOFade(0f, 0.2f)
                    .OnComplete(() =>
                    {
                        foreach (var x in _points)
                        {
                            SpawnerEx.DestroySpawner(x.transform);
                        }

                        _totalPercent = 0f;

                        gameObject.SetActive(false);

                        GameManager.Instance.SetGameState(General.GameState.Playing);
                    });
            }
            else
            {
                if (!m_SpriteTouch.activeSelf)
                {
                    return;
                }

                m_SpriteTouch.transform.DOKill();
                m_SpriteTouch.SetActive(false);
            }
        }

        private void ActiveBlack(int layer)
        {
            m_Black.DOFade(1.0f, 0.2f);
            m_Black.transform.ResizeSpriteToScreen();
            m_SpriteTouch.SetActive(true);

            m_Black.sortingOrder = layer;
        }

        private void TutorialDrag()
        {
            m_SpriteTouch.transform.DOScale(_dragScale, 0.2f)
                .SetEase(Ease.OutBounce)
                .SetLoops(-1, LoopType.Yoyo);
        }
    }
}