using GameAssets.Scripts.Base;
using GameAssets.Scripts.Ex;
using GameAssets.Scripts.GamePlay;
using GameAssets.Scripts.Manager.Localization;
using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.UI.UITooltip
{
    public class UITooltip : BaseUI
    {
        [SerializeField] private Transform starParent;

        [SerializeField] private Image imgIcon;

        [SerializeField] private TextMeshProUGUI txtName;
        [SerializeField] private TextMeshProUGUI txtInfo;
        [SerializeField] private TextMeshProUGUI txtSpeed;
        [SerializeField] private TextMeshProUGUI txtDefense;
        [SerializeField] private TextMeshProUGUI txtLucky;
        [SerializeField] private TextMeshProUGUI txtLevel;

        private RectTransform _rt;
        private RectTransform _canvas;

        private void Awake()
        {
            _rt = GetComponent<RectTransform>();
            _canvas = GetComponentInParent<Canvas>().transform as RectTransform;
        }

        public void Setup(Item item)
        {
            _rt.KeepInsideCamera(_canvas, MathfEx.PlatformPosition);

            StarController.Instance.Initialize(starParent, Random.Range(1, 3));

            imgIcon.sprite = item.icon;
            txtName.text = item.name;
            txtInfo.text = item.description;
            txtSpeed.text = $"{LocalizationManager.Format("speed")}: {item.speed}";
            txtDefense.text = $"{LocalizationManager.Format("defense")}: {item.defense}";
            txtLevel.text = $"Level {item.level}";
        }
    }
}