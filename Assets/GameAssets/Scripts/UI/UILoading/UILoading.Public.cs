using DG.Tweening;

namespace GameAssets.Scripts.UI.UILoading
{
    public partial class UILoading
    {
        public void UpdateProgress(float value)
        {
            imgLoadingFill.DOFillAmount(value, fillSpeed)
                .OnUpdate(() => txtLoading.text = $"{imgLoadingFill.fillAmount * 100:0}%");
        }
    }
}