using GameAssets.Scripts.Base;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model;
using GameAssets.Scripts.Model.Item;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;
using GameAssets.Scripts.UI.UIPlay.GameMenu;
using Tamarin.FirebaseX;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UILoading
{
    public partial class UILoading : BaseUI
    {
        [SerializeField] private Image imgLoadingFill;

        [SerializeField] private TextMeshProUGUI txtLoading;

        [SerializeField] private float fillSpeed;

        private bool _fillComplete;

        private async void FirebaseInitiation()
        {
            FirebaseAPI.Instance.QueryListenAs<Config>(Const.PATH_CONFIG,
                Const.PATH_CONFIG_DOCUMENT, GameManager.SetConfig);

            await Task.WhenAll(AuthChanged());

            FirebaseAPI.Instance.RealQueryListenAsync(Const.PATH_EXCHANGE_SHOP,
                result =>
                {
                    if (result == null || result.Contains("Error"))
                    {
                        return;
                    }

                    var items = JsonConvert.DeserializeObject<List<Item>>(result);

                    if (items == null)
                    {
                        return;
                    }

                    GameManager.ShopItems.Clear();
                    GameManager.ShopItems.AddRange(items);
                });

            firebaseReady = true;
            
            Debug.Log("Firebase init complete");

            if (!_fillComplete)
            {
                return;
            }

            if (!UIManager.Instance.IsUI<UIMaintenance.UIMaintenance>())
            {
                UIManager.Instance.HideAllAndShowUI<UIPlaySgm>();
            }
        }
    }
}