using System;
using Tamarin.Common;
using DG.Tweening;
using Cysharp.Threading.Tasks;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.UI.UIPlay.GameMenu;
using Tamarin.FirebaseX;

namespace GameAssets.Scripts.UI.UILoading
{
    public partial class UILoading
    {
        private async void Awake()
        {
            async void Callback()
            {
                if (firebaseReady)
                {
                    await UniTask.Delay(TimeSpan.FromSeconds(0.5f));

                    if (UIManager.Instance.IsUI<UIMaintenance.UIMaintenance>())
                    {
                        return;
                    }

                    if (onLoading == null)
                    {
                        UIManager.Instance.HideAllAndShowUI<UIPlaySgm>();
                    }
                    else
                    {
                        onLoading();

                        onLoading = null;
                    }
                }
                else
                {
                    _fillComplete = true;
                }
            }

            imgLoadingFill.DOFillAmount(1f, fillSpeed)
                .OnUpdate(() => txtLoading.text = $"{imgLoadingFill.fillAmount * 100:0}%")
                .OnComplete(Callback);

            if (firebaseReady)
            {
                return;
            }

            FirebaseAPI.Instance.onInit.AddListener(FirebaseInitiation);

            await Waiter.Until(() => firebaseReady);
        }

        private void Start()
        {
            AudioManager.Instance.PlayMusic("login-game");
        }
    }
}