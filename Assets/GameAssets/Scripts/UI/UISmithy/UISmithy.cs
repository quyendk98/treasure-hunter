﻿using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UISmithy
{
    public class UISmithy : BaseUI
    {
        [SerializeField] private Button btnBack;

        private void Awake()
        {
            btnBack.onClick.AddListener(() => UIManager.Instance.HideAllAndShowUI<UIInventory.UIInventory>());
        }
    }
}