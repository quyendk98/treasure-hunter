using GameAssets.Scripts.Base;
using GameAssets.Scripts.UI.UIPlay.GamePlay;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.UI.UIGameComplete
{
    public partial class UIGameComplete : BaseUI
    {
        [SerializeField] private Button btnHome;
        [SerializeField] private Button btnRestart;
        [SerializeField] private Button btnNext;

        [SerializeField] private TextMeshProUGUI txtScore;

        private UIPlaySgp _uiPlaySgp;
    }
}