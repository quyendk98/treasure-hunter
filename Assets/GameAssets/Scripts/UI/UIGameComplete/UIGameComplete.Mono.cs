using GameAssets.Scripts.Ex;
using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Audio;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Manager.Localization;
using GameAssets.Scripts.UI.UIPlay.GamePlay;

namespace GameAssets.Scripts.UI.UIGameComplete
{
    public partial class UIGameComplete
    {
        private void Awake()
        {
            _uiPlaySgp = UIManager.Instance.GetUI<UIPlaySgp>();

            btnHome.onClick.AddListener(() => Click(() => SceneManager.LoadScene("Game Menu")));

            btnRestart.onClick.AddListener(() => Click(() =>
            {
                if (GameManager.CheckSpentEnergy(GameManager.SelectedLevel.energy))
                {
                    SceneManager.ResetCurrentScene();
                }
                else
                {
                    SpawnerEx.CreateNotification(LocalizationManager.Format("not-enough-energy"));
                }
            }));

            btnNext.onClick.AddListener(() => Click(() =>
            {
                var index = GameManager.SelectedMap.levels.IndexOf(GameManager.SelectedLevel);

                if (index + 1 > GameManager.SelectedMap.levels.Count - 1)
                {
                    SpawnerEx.CreateNotification(LocalizationManager.Format("final-level"));
                    return;
                }

                var level = GameManager.SelectedMap.levels[index + 1];

                if (GameManager.CheckSpentEnergy(level.energy))
                {
                    GameManager.SetSelectedLevel(level, index + 1);
                    SceneManager.ResetCurrentScene();
                }
                else
                {
                    SpawnerEx.CreateNotification(LocalizationManager.Format("not-enough-energy"));
                }
            }));

            txtScore.text = $"<size=55>{LocalizationManager.Format("your-point")}</size>{UIEx.NewLine}"
                            + $"<color=#dd5900>{_uiPlaySgp.Gold}/{_uiPlaySgp.Target}</color>";

            AudioManager.Instance.PlaySound("win");
        }
    }
}