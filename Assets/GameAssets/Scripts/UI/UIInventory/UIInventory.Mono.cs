using GameAssets.Scripts.Manager;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.UI.UIInventory
{
    public partial class UIInventory
    {
        private void Awake()
        {
            btnSmithy.AddListener(() => UIManager.Instance.HideAllAndShowUI<UISmithy.UISmithy>());
            btnBack.AddListener(() => UIManager.Instance.HideAllAndShowUI<UISelectMap.UISelectMap>());
            btnPlayerInfo.onClick.AddListener(() => UIManager.Instance.ShowUI<UIPlayerInfo.UIPlayerInfo>());
        }

        private void OnEnable()
        {
            UpdateSkin();

            txtPlayerName.text = GameManager.User.Username;
            txtLevel.text = $"{Random.Range(1, 15)}";
        }
    }
}