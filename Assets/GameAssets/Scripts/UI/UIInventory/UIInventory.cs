using GameAssets.Scripts.Base;
using GameAssets.Scripts.GamePlay.Inventory;
using GameAssets.Scripts.GamePlay.Slot;
using GameAssets.Scripts.Manager.Game;
using GameAssets.Scripts.Model.Item;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

namespace GameAssets.Scripts.UI.UIInventory
{
    public partial class UIInventory : BaseUI
    {
        [SerializeField] private EquipSlot equipSlotSkin;
        [SerializeField] private EquipSlot equipSlotHook;
        [SerializeField] private EquipSlot equipSlotLine;
        [SerializeField] private EquipSlot equipSlotPet;

        [Header("[BUTTON]")] [SerializeField] private GamePlay.Button btnBack;
        [SerializeField] private GamePlay.Button btnSmithy;
        [SerializeField] private Button btnPlayerInfo;

        [Header("[IMAGE]")] [SerializeField] private Image imgPlayer;
        [SerializeField] private Image imgLine;
        [SerializeField] private Image imgHook;
        [SerializeField] private Image imgPet;

        [SerializeField] private TextMeshProUGUI txtPlayerName;
        [SerializeField] private TextMeshProUGUI txtLevel;

        public void UpdateSlot(ItemData itemData)
        {
            var update = false;

            Inventory.Instance.CurrentPage
                .OnUpdateSlot(itemData.Id, go =>
                {
                    var inventorySlot = go.GetComponent<InventorySlot>();

                    if (inventorySlot == null)
                    {
                        return;
                    }

                    update = true;
                    itemData.Amount++;

                    inventorySlot.Initialize(itemData);
                });

            if (!update)
            {
                Inventory.Instance.CurrentPage
                    .OnAddSlot(go =>
                    {
                        var inventorySlot = go.GetComponent<InventorySlot>();

                        if (inventorySlot != null)
                        {
                            inventorySlot.Initialize(itemData);
                        }

                        return itemData.Id;
                    });
            }
        }

        public void UpdateSkin()
        {
            if (GameManager.User == null)
            {
                return;
            }
            
            if (GameManager.User.Skin != null)
            {
                var skin = (Weapon) GameManager.ResourceItem(GameManager.User.Skin);

                imgPlayer.gameObject.SetActive(true);
                equipSlotSkin.Initialize(ItemData.CreateWeapon(skin));

                imgPlayer.sprite = skin.icon;
            }
            else
            {
                imgPlayer.gameObject.SetActive(false);
                equipSlotSkin.Hide();
            }

            if (GameManager.User.Hook != null)
            {
                var hook = (Weapon) GameManager.ResourceItem(GameManager.User.Hook);

                imgHook.gameObject.SetActive(true);
                equipSlotHook.Initialize(ItemData.CreateWeapon(hook));

                imgHook.sprite = hook.icon;
            }
            else
            {
                imgHook.gameObject.SetActive(false);
                equipSlotHook.Hide();
            }

            if (GameManager.User.Line != null)
            {
                var line = (Weapon) GameManager.ResourceItem(GameManager.User.Line);

                imgLine.gameObject.SetActive(true);
                equipSlotLine.Initialize(ItemData.CreateWeapon(line));

                imgLine.sprite = line.icon;
            }
            else
            {
                imgLine.gameObject.SetActive(false);
                equipSlotLine.Hide();
            }

            if (GameManager.User.Pet != null)
            {
                var pet = (Weapon) GameManager.ResourceItem(GameManager.User.Pet);

                imgPet.gameObject.SetActive(true);
                equipSlotPet.Initialize(ItemData.CreateWeapon(pet));

                imgPet.sprite = pet.icon;
            }
            else
            {
                imgPet.gameObject.SetActive(false);
                equipSlotPet.Hide();
            }
        }
    }
}