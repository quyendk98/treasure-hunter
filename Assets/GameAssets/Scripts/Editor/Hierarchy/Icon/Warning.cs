using System.Linq;
using System.Text;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Warning : IconBase
    {
        private const int MAX_STRING_LEN = 750;
        private const float ICONS_WIDTH = 16f;

        private static readonly StringBuilder GOLogs = new StringBuilder(MAX_STRING_LEN);
        private static readonly StringBuilder GOWarnings = new StringBuilder(MAX_STRING_LEN);
        private static readonly StringBuilder GOErrors = new StringBuilder(MAX_STRING_LEN);
        private static readonly GUIContent TempTooltipContent = new GUIContent();

        private LogEntry _log;
        private LogEntry _warning;
        private LogEntry _error;

        public override string Name => "Logs, Warnings and Errors";

        public override float Width
        {
            get
            {
                var result = 0f;

                if (GOLogs.Length > 0)
                {
                    result += ICONS_WIDTH;
                }

                if (GOWarnings.Length > 0)
                {
                    result += ICONS_WIDTH;
                }

                if (GOErrors.Length > 0)
                {
                    result += ICONS_WIDTH;
                }

                return result;
            }
        }

        public override Texture2D PreferencesPreview => Styles.WarningIcon;

        public override void Init()
        {
            if (!EnhancedHierarchy.IsGameObject)
            {
                return;
            }

            _log = null;
            _warning = null;
            _error = null;

            GOLogs.Length = 0;
            GOWarnings.Length = 0;
            GOErrors.Length = 0;

            var components = EnhancedHierarchy.Components;

            foreach (var _ in components.Where(x => !x))
            {
                GOWarnings.AppendLine("Missing MonoBehaviour\n");
            }

            foreach (var entry in LogEntry.CompileEntries
                .Where(entry => entry.ClassType != null)
                .Where(entry => EnhancedHierarchy.Components
                    .Any(comp =>
                        comp && (comp.GetType() == entry.ClassType ||
                                 comp.GetType().IsAssignableFrom(entry.ClassType)))))
            {
                var isWarning = entry.HasMode(EntryMode.ScriptCompileWarning | EntryMode.AssetImportWarning);

                if (GOWarnings.Length < MAX_STRING_LEN && isWarning)
                {
                    GOWarnings.AppendLine(entry.ToString());
                }

                else if (GOErrors.Length < MAX_STRING_LEN && !isWarning)
                {
                    GOErrors.AppendLine(entry.ToString());
                }

                switch (isWarning)
                {
                    case true when _warning == null && !string.IsNullOrEmpty(entry.File):
                        _warning = entry;
                        break;

                    case false when _error == null && !string.IsNullOrEmpty(entry.File):
                        _error = entry;
                        break;
                }
            }

            if (!LogEntry.GameObjectEntries.TryGetValue(EnhancedHierarchy.CurrentGameObject,
                out var contextEntries))
            {
                return;
            }

            foreach (var entry in contextEntries)
            {
                var isLog = entry.HasMode(EntryMode.ScriptingLog);
                var isWarning = entry.HasMode(EntryMode.ScriptingWarning);
                var isError = entry.HasMode(EntryMode.ScriptingError |
                                            EntryMode.ScriptingException |
                                            EntryMode.ScriptingAssertion);

                if (isLog && GOLogs.Length < MAX_STRING_LEN)
                {
                    GOLogs.AppendLine(entry.ToString());
                }
                else if (isWarning && GOWarnings.Length < MAX_STRING_LEN)
                {
                    GOWarnings.AppendLine(entry.ToString());
                }
                else if (isError && GOErrors.Length < MAX_STRING_LEN)
                {
                    GOErrors.AppendLine(entry.ToString());
                }

                if (isLog && _log == null && !string.IsNullOrEmpty(entry.File))
                {
                    _log = entry;
                }

                if (isWarning && _warning == null && !string.IsNullOrEmpty(entry.File))
                {
                    _warning = entry;
                }

                if (isError && _error == null && !string.IsNullOrEmpty(entry.File))
                {
                    _error = entry;
                }
            }
        }

        public override void DoGUI(Rect rect)
        {
            if (!EnhancedHierarchy.IsRepaintEvent && !Preferences.openScriptsOfLogs || !EnhancedHierarchy.IsGameObject)
            {
                return;
            }

            rect.xMax = rect.xMin + 17f;
            rect.yMax += 1f;

            DoSingleGUI(ref rect, GOLogs, Styles.InfoIcon, _log);
            DoSingleGUI(ref rect, GOWarnings, Styles.WarningIcon, _warning);
            DoSingleGUI(ref rect, GOErrors, Styles.ErrorIcon, _error);
        }

        private static void DoSingleGUI(ref Rect rect, StringBuilder str, Texture icon, LogEntry entry)
        {
            if (str.Length == 0)
            {
                return;
            }

            if (Utility.ShouldCalculateTooltipAt(rect))
            {
                TempTooltipContent.tooltip = Preferences.tooltips
                    ? str.ToString().TrimEnd('\n', '\r')
                    : string.Empty;
            }

            TempTooltipContent.image = icon;

            if (GUI.Button(rect, TempTooltipContent, Styles.IconButton))
            {
                entry?.OpenToEdit();
            }

            rect.x += ICONS_WIDTH;
        }
    }
}