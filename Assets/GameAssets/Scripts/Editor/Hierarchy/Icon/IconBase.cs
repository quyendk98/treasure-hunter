using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    [Flags]
    public enum IconPosition
    {
        AfterObjectName = 1,
        BeforeObjectName = 2,
        RightMost = 4,
        SafeArea = AfterObjectName | RightMost,
        All = SafeArea | BeforeObjectName
    }

    public abstract class IconBase
    {
        private const float DEFAULT_WIDTH = 16f;

        public static readonly None None = new None();

        public virtual string Name => GetType().Name;

        public virtual float Width =>
            DEFAULT_WIDTH; // May be called multiple times in the same frame for the same object

        protected virtual IconPosition Side => IconPosition.SafeArea;

        public virtual Texture2D PreferencesPreview => null;

        public virtual string PreferencesTooltip => null;

        public static IconBase[] AllLeftIcons { get; }
        public static IconBase[] AllRightIcons { get; }
        public static IconBase[] AllLeftOfNameIcons { get; }

        static IconBase()
        {
            var baseType = typeof(IconBase);

            Icons = baseType.Assembly.GetTypes()
                .Where(t => t != baseType && baseType.IsAssignableFrom(t))
                .Select(t => (IconBase) Activator.CreateInstance(t))
                .ToDictionary(t => t.Name);

            AllLeftIcons = Icons.Select(i => i.Value)
                .Where(i => (i.Side & IconPosition.AfterObjectName) != 0).ToArray();
            AllRightIcons = Icons.Select(i => i.Value)
                .Where(i => (i.Side & IconPosition.RightMost) != 0).ToArray();
            AllLeftOfNameIcons = Icons.Select(i => i.Value)
                .Where(i => (i.Side & IconPosition.BeforeObjectName) != 0).ToArray();
        }

        public virtual void Init()
        {
        } // Guaranteed to be called only once for each obj in every frame before any DoGUI() and get Width calls

        public abstract void DoGUI(Rect rect);

        private static readonly Dictionary<string, IconBase> Icons;

        protected static ChildrenChangeMode AskChangeModeIfNecessary(List<GameObject> objs,
            ChildrenChangeMode reference, string title, string message)
        {
            var controlPressed = UnityEngine.Event.current.control || UnityEngine.Event.current.command;

            switch (reference)
            {
                case ChildrenChangeMode.ObjectOnly:
                    return controlPressed
                        ? ChildrenChangeMode.ObjectAndChildren
                        : ChildrenChangeMode.ObjectOnly;

                case ChildrenChangeMode.ObjectAndChildren:
                    return controlPressed
                        ? ChildrenChangeMode.ObjectOnly
                        : ChildrenChangeMode.ObjectAndChildren;

                default:
                    foreach (var _ in objs.Where(x => x && x.transform.childCount > 0))
                    {
                        try
                        {
                            return (ChildrenChangeMode) EditorUtility.DisplayDialogComplex(title,
                                message, "Yes, change children", "No, this object only", "Cancel");
                        }
                        finally
                        {
                            // Unity bug, DisplayDialogComplex makes the unity partially lose focus
                            if (EditorWindow.focusedWindow)
                            {
                                EditorWindow.focusedWindow.Focus();
                            }
                        }
                    }

                    return ChildrenChangeMode.ObjectOnly;
            }
        }

        protected static List<GameObject> GetSelectedObjectsAndCurrent()
        {
            if (!Preferences.changeAllSelected || Selection.gameObjects.Length < 2)
            {
                return EnhancedHierarchy.CurrentGameObject
                    ? new List<GameObject> {EnhancedHierarchy.CurrentGameObject}
                    : new List<GameObject>();
            }

            // Makes sure the object is part of the scene and not the project
            return Selection.gameObjects
                .Where(x => !EditorUtility.IsPersistent(x))
                .Union(EnhancedHierarchy.CurrentGameObject
                    ? new[] {EnhancedHierarchy.CurrentGameObject}
                    : Array.Empty<GameObject>())
                .Distinct()
                .ToList();
        }

        public static bool operator ==(IconBase left, IconBase right)
        {
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            if (ReferenceEquals(left, null))
            {
                return false;
            }

            if (ReferenceEquals(right, null))
            {
                return false;
            }

            return left.Name == right.Name;
        }

        public static bool operator !=(IconBase left, IconBase right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return obj as IconBase == this;
        }

        public static implicit operator IconBase(string name)
        {
            try
            {
                return Icons[name];
            }
            catch
            {
                return None;
            }
        }

        public static implicit operator string(IconBase icon)
        {
            return icon.ToString();
        }
    }
}