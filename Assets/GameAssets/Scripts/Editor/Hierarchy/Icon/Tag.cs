using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Tag : IconBase
    {
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview =>
            Utility.GetBackground(Styles.TagStyle, true);

        public override void DoGUI(Rect rect)
        {
            GUI.changed = false;

            EditorGUI.LabelField(rect, Styles.TagContent);

            var tag = EditorGUI.TagField(rect, Styles.TagContent, EnhancedHierarchy.GameObjectTag, Styles.TagStyle);

            if (GUI.changed && tag != EnhancedHierarchy.GameObjectTag)
            {
                ChangeTagAndAskForChildren(GetSelectedObjectsAndCurrent(), tag);
            }
        }

        public static void ChangeTagAndAskForChildren(List<GameObject> objs, string newTag)
        {
            var changeMode = AskChangeModeIfNecessary(objs, Preferences.tagAskMode, "Change Layer",
                "Do you want to change the tags of the children objects as well?");

            switch (changeMode)
            {
                case ChildrenChangeMode.ObjectOnly:
                    foreach (var obj in objs)
                    {
                        Undo.RegisterCompleteObjectUndo(obj, "Tag changed");

                        obj.tag = newTag;
                    }

                    break;

                case ChildrenChangeMode.ObjectAndChildren:
                    foreach (var obj in objs)
                    {
                        Undo.RegisterFullObjectHierarchyUndo(obj, "Tag changed");

                        obj.tag = newTag;

                        foreach (var transform in obj.GetComponentsInChildren<Transform>(true))
                        {
                            transform.tag = newTag;
                        }
                    }

                    break;
            }
        }
    }
}