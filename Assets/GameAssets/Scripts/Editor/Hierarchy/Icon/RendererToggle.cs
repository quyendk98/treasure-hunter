using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class RendererToggle : IconBase
    {
        private Renderer _renderer;

        protected override IconPosition Side => IconPosition.All;
        public override string Name => "Renderer";

        public override Texture2D PreferencesPreview => Utility.GetBackground(Styles.RendererToggleStyle, true);

        public override void Init()
        {
            _renderer = EnhancedHierarchy.Components.FirstOrDefault(c => c is Renderer) as Renderer;
        }

        public override float Width => _renderer ? base.Width : 0;

        public override void DoGUI(Rect rect)
        {
            if (!_renderer)
            {
                return;
            }

            using (new GUIBackgroundColor(_renderer.enabled
                ? Styles.BackgroundColorEnabled
                : Styles.BackgroundColorDisabled))
            {
                GUI.changed = false;

                GUI.Toggle(rect, _renderer, Styles.RendererContent, Styles.RendererToggleStyle);

                if (!GUI.changed)
                {
                    return;
                }

                var objs = GetSelectedObjectsAndCurrent()
                    .SelectMany(go => go.GetComponents<Renderer>()).ToList();
                var active = !_renderer.enabled;

                // ReSharper disable once CoVariantArrayConversion
                Undo.RecordObjects(objs.ToArray(), _renderer.enabled ? "Disabled renderer" : "Enabled renderer");

                foreach (var obj in objs)
                {
                    obj.enabled = active;
                }
            }
        }
    }
}