using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class PrefabApply : IconBase
    {
        public override string Name => "Apply Prefab";
        protected override IconPosition Side => IconPosition.All;

        public override void DoGUI(Rect rect)
        {
            var isPrefab = PrefabUtility.IsPartOfAnyPrefab(EnhancedHierarchy.CurrentGameObject);

            using (new GUIContentColor(isPrefab ? Styles.BackgroundColorEnabled : Styles.BackgroundColorDisabled))
            {
                if (!GUI.Button(rect, Styles.PrefabApplyContent, Styles.ApplyPrefabStyle))
                {
                    return;
                }

                var objs = GetSelectedObjectsAndCurrent();

                foreach (var obj in objs)
                {
                    Utility.ApplyPrefabModifications(obj, objs.Count <= 1);
                }
            }
        }
    }
}