using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class MonoBehaviourIcon : IconBase
    {
        private static readonly Dictionary<Type, string> MonoBehaviourNames =
            new Dictionary<Type, string>();

        private static readonly StringBuilder GOComponents = new StringBuilder(500);
        private static readonly GUIContent TempTooltipContent = new GUIContent();
        private static bool _hasMonoBehaviour;

        public override string Name => "MonoBehaviour Icon";
        public override float Width => _hasMonoBehaviour ? 15f : 0f;
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview =>
            AssetPreview.GetMiniTypeThumbnail(typeof(MonoScript));

        public override void Init()
        {
            _hasMonoBehaviour = false;

            if (!EnhancedHierarchy.IsGameObject)
            {
                return;
            }

            var components = EnhancedHierarchy.Components;

            foreach (var _ in components.OfType<MonoBehaviour>())
            {
                _hasMonoBehaviour = true;
                break;
            }
        }

        public override void DoGUI(Rect rect)
        {
            if (!EnhancedHierarchy.IsRepaintEvent || !EnhancedHierarchy.IsGameObject || !_hasMonoBehaviour)
            {
                return;
            }

            if (Utility.ShouldCalculateTooltipAt(rect) && Preferences.tooltips)
            {
                GOComponents.Length = 0;

                var components = EnhancedHierarchy.Components;

                foreach (var x in components.OfType<MonoBehaviour>())
                {
                    GOComponents.AppendLine(GetComponentName(x));
                }

                TempTooltipContent.tooltip = GOComponents.ToString().TrimEnd('\n', '\r');
            }
            else
            {
                TempTooltipContent.tooltip = string.Empty;
            }

            rect.yMin += 1f;
            rect.yMax -= 1f;
            rect.xMin += 1f;

            GUI.DrawTexture(rect, Styles.MonoBehaviourIconTexture, ScaleMode.ScaleToFit);
            EditorGUI.LabelField(rect, TempTooltipContent);
        }

        private static string GetComponentName(Component component)
        {
            var type = component.GetType();

            if (MonoBehaviourNames.TryGetValue(type, out var result))
            {
                return result;
            }

            return MonoBehaviourNames[type] = type.ToString();
        }
    }
}