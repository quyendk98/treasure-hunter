using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class GameObjectIcon : IconBase
    {
        private GUIContent _lastContent;

        public override string Name => "GameObject Icon";
        protected override IconPosition Side => IconPosition.All;
        public override float Width => _lastContent.image ? base.Width : 0f;

        public override Texture2D PreferencesPreview =>
            AssetPreview.GetMiniTypeThumbnail(typeof(GameObject));

        public override void Init()
        {
            _lastContent ??= new GUIContent();
            _lastContent.text = string.Empty;

            _lastContent.image = Preferences.hideDefaultIcon
                ? Reflected.GetObjectIcon(EnhancedHierarchy.CurrentGameObject)
                : AssetPreview.GetMiniThumbnail(EnhancedHierarchy.CurrentGameObject);

            _lastContent.tooltip = Preferences.tooltips && !Preferences.relevantTooltipsOnly
                ? "Change Icon"
                : string.Empty;
        }

        public override void DoGUI(Rect rect)
        {
            using (ProfilerSample.Get())
            {
                rect.yMin++;
                rect.xMin++;
                GUI.changed = false;

                GUI.Button(rect, _lastContent, EditorStyles.label);

                if (!GUI.changed)
                {
                    return;
                }

                var affectedList = GetSelectedObjectsAndCurrent();
                var affectedEnum = affectedList.AsEnumerable();
                var changeMode = AskChangeModeIfNecessary(affectedList,
                    Preferences.iconAskMode.Value, "Change Icons",
                    "Do you want to change children icons as well?");

                affectedEnum = changeMode switch
                {
                    ChildrenChangeMode.ObjectAndChildren => affectedEnum.SelectMany(go =>
                        go.GetComponentsInChildren<Transform>(true).Select(t => t.gameObject)),
                    _ => affectedEnum
                };

                affectedEnum = affectedEnum.Distinct();

                var affectedArray = affectedEnum.ToArray();

                foreach (var obj in affectedArray)
                {
                    Undo.RegisterCompleteObjectUndo(obj, "Icon Changed");
                }

                Reflected.ShowIconSelector(affectedArray, rect, true);
            }
        }
    }
}