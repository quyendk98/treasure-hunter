using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.Icon
{
    public sealed class Layer : IconBase
    {
        protected override IconPosition Side => IconPosition.All;

        public override Texture2D PreferencesPreview =>
            Utility.GetBackground(Styles.LayerStyle, true);

        public override void DoGUI(Rect rect)
        {
            GUI.changed = false;

            EditorGUI.LabelField(rect, Styles.LayerContent);

            var layer = EditorGUI.LayerField(rect, EnhancedHierarchy.CurrentGameObject.layer, Styles.LayerStyle);

            if (GUI.changed)
            {
                ChangeLayerAndAskForChildren(GetSelectedObjectsAndCurrent(), layer);
            }
        }

        public static void ChangeLayerAndAskForChildren(List<GameObject> objs, int newLayer)
        {
            var changeMode = AskChangeModeIfNecessary(objs, Preferences.layerAskMode,
                "Change Layer", "Do you want to change the layers of the children objects as well?");

            switch (changeMode)
            {
                case ChildrenChangeMode.ObjectOnly:
                    foreach (var obj in objs)
                    {
                        Undo.RegisterCompleteObjectUndo(obj, "Layer changed");

                        obj.layer = newLayer;
                    }

                    break;

                case ChildrenChangeMode.ObjectAndChildren:
                    foreach (var obj in objs)
                    {
                        Undo.RegisterFullObjectHierarchyUndo(obj, "Layer changed");

                        obj.layer = newLayer;

                        foreach (var transform in obj.GetComponentsInChildren<Transform>(true))
                        {
                            transform.gameObject.layer = newLayer;
                        }
                    }

                    break;
            }
        }
    }
}