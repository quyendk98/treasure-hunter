using System;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.MiniLabel
{
    public abstract class MiniLabelProvider
    {
        private readonly GUIContent _content = new GUIContent();

        public static readonly Type[] MiniLabelsTypes =
        {
            null,
            typeof(TagMiniLabel),
            typeof(LayerMiniLabel),
            typeof(SortingLayerMiniLabel)
        };

        protected abstract void FillContent(GUIContent content);
        protected abstract bool Faded();
        protected abstract void OnClick();

        public void Init()
        {
            FillContent(_content);
        }

        public bool HasValue()
        {
            return _content.text.Length > 0;
        }

        public virtual bool Draw(Rect rect, GUIContent content, GUIStyle style)
        {
            return GUI.Button(rect, content, style);
        }

        public float Measure()
        {
            var calculated = Styles.MiniLabelStyle.CalcSize(_content);
            return calculated.x;
        }

        public void Draw(ref Rect rect)
        {
            if (!HasValue())
            {
                return;
            }

            var color = EnhancedHierarchy.CurrentColor;
            var alpha = Faded()
                ? Styles.BackgroundColorDisabled.a
                : Styles.BackgroundColorEnabled.a;
            var finalColor = color * new Color(1f, 1f, 1f, alpha);

            using (ProfilerSample.Get())
            using (new GUIContentColor(finalColor))
            {
                Styles.MiniLabelStyle.fontSize = Preferences.smallerMiniLabel ? 8 : 9;
                rect.xMin -= Measure();

                if (Draw(rect, _content, Styles.MiniLabelStyle))
                {
                    OnClick();
                }
            }
        }
    }
}