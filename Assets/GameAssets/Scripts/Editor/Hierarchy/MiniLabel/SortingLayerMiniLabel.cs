using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace GameAssets.Scripts.Editor.Hierarchy.MiniLabel
{
    public class SortingLayerMiniLabel : MiniLabelProvider
    {
        private const string DEFAULT_SORTING_LAYER = "Default";

        private string _layerName;
        private int _sortingOrder;

        protected override void FillContent(GUIContent content)
        {
            var sortingGroup =
                EnhancedHierarchy.Components.FirstOrDefault(c => c is SortingGroup) as SortingGroup;
            var spriteRenderer =
                EnhancedHierarchy.Components.FirstOrDefault(c => c is SpriteRenderer) as
                    SpriteRenderer;
            var particleSystem =
                EnhancedHierarchy.Components.FirstOrDefault(c => c is ParticleSystemRenderer) as
                    ParticleSystemRenderer;

            Type comp = null;
            var hasSortingLayer = true;

            if (sortingGroup != null)
            {
                _layerName = sortingGroup.sortingLayerName;
                _sortingOrder = sortingGroup.sortingOrder;
                comp = sortingGroup.GetType();
            }
            else if (spriteRenderer != null)
            {
                _layerName = spriteRenderer.sortingLayerName;
                _sortingOrder = spriteRenderer.sortingOrder;
                comp = spriteRenderer.GetType();
            }
            else if (particleSystem != null)
            {
                _layerName = particleSystem.sortingLayerName;
                _sortingOrder = particleSystem.sortingOrder;
                comp = typeof(ParticleSystem);
            }
            else
            {
                hasSortingLayer = false;
            }

            content.text = hasSortingLayer
                ? $"{_layerName}:{_sortingOrder}"
                : string.Empty;

            content.tooltip = comp != null && Preferences.tooltips
                ? $"Sorting layer from {comp.Name}"
                : string.Empty;
        }

        protected override bool Faded()
        {
            return _layerName == DEFAULT_SORTING_LAYER && _sortingOrder == 0;
        }

        protected override void OnClick()
        {
        }
    }
}