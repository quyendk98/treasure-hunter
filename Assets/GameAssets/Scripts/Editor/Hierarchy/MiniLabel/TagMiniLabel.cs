using GameAssets.Scripts.Editor.Hierarchy.Icon;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.Hierarchy.MiniLabel
{
    public class TagMiniLabel : MiniLabelProvider
    {
        protected override void FillContent(GUIContent content)
        {
            content.text = EnhancedHierarchy.HasTag
                ? EnhancedHierarchy.GameObjectTag
                : string.Empty;
        }

        protected override bool Faded()
        {
            return EnhancedHierarchy.GameObjectTag == EnhancedHierarchy.UNTAGGED;
        }

        public override bool Draw(Rect rect, GUIContent content, GUIStyle style)
        {
            GUI.changed = false;

            var tag = EditorGUI.TagField(rect, EnhancedHierarchy.GameObjectTag, style);

            if (GUI.changed)
            {
                Tag.ChangeTagAndAskForChildren(EnhancedHierarchy.GetSelectedObjectsAndCurrent(), tag);
            }

            return GUI.changed;
        }

        protected override void OnClick()
        {
        }
    }
}