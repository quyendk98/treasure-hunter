﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Scroll for EasyLayout with Ellipse layout type.
    /// </summary>
    [RequireComponent(typeof(EasyLayout))]
    public class EasyLayoutEllipseScroll : UIBehaviour, IBeginDragHandler, IDragHandler,
        IEndDragHandler, IScrollHandler
    {
        #region Interactable

        /// <summary>
        /// The CanvasGroup cache.
        /// </summary>
        protected readonly List<CanvasGroup> canvasGroupCache = new List<CanvasGroup>();

        [SerializeField] private bool interactable = true;

        /// <summary>
        /// Is widget interactable.
        /// </summary>
        /// <value><c>true</c> if interactable; otherwise, <c>false</c>.</value>
        public bool Interactable
        {
            get => interactable;

            set
            {
                if (interactable == value)
                {
                    return;
                }

                interactable = value;
                InteractableChanged();
            }
        }

        /// <summary>
        /// If the canvas groups allow interaction.
        /// </summary>
        protected bool groupsAllowInteraction = true;

        /// <summary>
        /// Process the CanvasGroupChanged event.
        /// </summary>
        protected override void OnCanvasGroupChanged()
        {
            var groupAllowInteraction = true;
            var t = transform;

            while (t != null)
            {
                t.GetComponents(canvasGroupCache);
                var shouldBreak = false;

                foreach (var canvasGroup in canvasGroupCache)
                {
                    if (!canvasGroup.interactable)
                    {
                        groupAllowInteraction = false;
                        shouldBreak = true;
                    }

                    shouldBreak |= canvasGroup.ignoreParentGroups;
                }

                if (shouldBreak)
                {
                    break;
                }

                t = t.parent;
            }

            if (groupAllowInteraction == groupsAllowInteraction)
            {
                return;
            }

            groupsAllowInteraction = groupAllowInteraction;
            InteractableChanged();
        }

        /// <summary>
        /// Determines whether this widget is interactable.
        /// </summary>
        /// <returns><c>true</c> if this instance is interactable; otherwise, <c>false</c>.</returns>
        public virtual bool IsInteractable()
        {
            return groupsAllowInteraction && Interactable;
        }

        /// <summary>
        /// Process interactable change.
        /// </summary>
        protected virtual void InteractableChanged()
        {
            if (IsInteractable())
            {
                OnInteractableEnabled();
            }
            else
            {
                OnInteractableDisabled();
            }
        }

        /// <summary>
        /// What to do when widget became interactable.
        /// </summary>
        protected virtual void OnInteractableEnabled()
        {
        }

        /// <summary>
        /// What to do when widget became not interactable.
        /// </summary>
        protected virtual void OnInteractableDisabled()
        {
        }

        #endregion

        [NonSerialized] private EasyLayout _layout;

        /// <summary>
        /// Layout.
        /// </summary>
        protected EasyLayout Layout
        {
            get
            {
                if (_layout == null)
                {
                    _layout = GetComponent<EasyLayout>();
                }

                return _layout;
            }
        }

        /// <summary>
        /// Is scroll horizontal or vertical?
        /// </summary>
        [SerializeField] public bool isHorizontal;

        /// <summary>
        /// Drag sensitivity.
        /// </summary>
        [SerializeField] public float dragSensitivity = 0.5f;

        /// <summary>
        /// Scroll sensitivity.
        /// </summary>
        [SerializeField] public float scrollSensitivity = 2f;

        /// <summary>
        /// Layout scroll.
        /// </summary>
        protected float ScrollValue
        {
            get => Layout.EllipseSettings.AngleScroll;

            set
            {
                Layout.EllipseSettings.AngleScroll = value;

                scrollEvent.Invoke();
            }
        }

        /// <summary>
        /// Animate inertia scroll with unscaled time.
        /// </summary>
        [SerializeField] public bool unscaledTime = true;

        /// <summary>
        /// Time to stop.
        /// </summary>
        [SerializeField] public float timeToStop = 0.5f;

        /// <summary>
        /// Scroll event.
        /// </summary>
        [SerializeField] public UnityEvent scrollEvent = new UnityEvent();

        /// <summary>
        /// Velocity.
        /// </summary>
        [NonSerialized] protected float scrollVelocity;

        /// <summary>
        /// Inertia velocity.
        /// </summary>
        [NonSerialized] protected float intertiaVelocity;

        /// <summary>
        /// Current deceleration rate.
        /// </summary>
        [NonSerialized] protected float currentDecelerationRate;

        /// <summary>
        /// Inertia distance.
        /// </summary>
        [NonSerialized] protected float inertiaDistance;

        /// <summary>
        /// Is drag event occurring?
        /// </summary>
        [NonSerialized] protected bool isDragging;

        /// <summary>
        /// Is scrolling occurring?
        /// </summary>
        [NonSerialized] protected bool isScrolling;

        /// <summary>
        /// Previous scroll value.
        /// </summary>
        [NonSerialized] protected float prevScrollValue;

        /// <summary>
        /// Current scroll value.
        /// </summary>
        [NonSerialized] protected float currentScrollValue;

        /// <summary>
        /// Returns true if the GameObject and the Component are active.
        /// </summary>
        /// <returns>true if the GameObject and the Component are active; otherwise false.</returns>
        public override bool IsActive()
        {
            return base.IsActive() && IsInteractable() && Layout.LayoutType == LayoutTypes.Ellipse;
        }

        /// <summary>
        /// Process the begin drag event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public virtual void OnBeginDrag(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            if (!IsActive())
            {
                return;
            }

            isDragging = true;

            prevScrollValue = ScrollValue;
            currentScrollValue = ScrollValue;

            StopInertia();
        }

        /// <summary>
        /// Process the drag event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public virtual void OnDrag(PointerEventData eventData)
        {
            if (!isDragging)
            {
                return;
            }

            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            if (!IsActive())
            {
                return;
            }

            StopInertia();

            var v = isHorizontal ? -eventData.delta.x : eventData.delta.y;
            Scroll(v * dragSensitivity);
        }

        /// <summary>
        /// Process scroll event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public void OnScroll(PointerEventData eventData)
        {
            if (!IsActive())
            {
                return;
            }

            isScrolling = true;
            var v = isHorizontal ? -eventData.scrollDelta.x : eventData.scrollDelta.y;
            Scroll(v * scrollSensitivity);
        }

        /// <summary>
        /// Scroll.
        /// </summary>
        /// <param name="delta">Delta.</param>
        protected virtual void Scroll(float delta)
        {
            ScrollValue += delta;
            currentScrollValue += delta;

            var timeDelta = GetDeltaTime();
            var newVelocity = (prevScrollValue - currentScrollValue) / timeDelta;

            scrollVelocity = Mathf.Lerp(scrollVelocity, newVelocity, timeDelta * 10);
            prevScrollValue = currentScrollValue;
        }

        /// <summary>
        /// Process the end drag event.
        /// </summary>
        /// <param name="eventData">Event data.</param>
        public virtual void OnEndDrag(PointerEventData eventData)
        {
            if (!isDragging)
            {
                return;
            }

            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            isDragging = false;
            InitInertia();
        }

        /// <summary>
        /// Init inertia.
        /// </summary>
        protected virtual void InitInertia()
        {
            intertiaVelocity = -scrollVelocity;
            currentDecelerationRate = -intertiaVelocity / timeToStop;

            var direction = Mathf.Sign(intertiaVelocity);
            var timeToStopSq = Mathf.Pow(timeToStop, 2f);
            var distance = -Mathf.Abs(currentDecelerationRate) * timeToStopSq / 2f +
                           Mathf.Abs(intertiaVelocity) * timeToStop;

            inertiaDistance = distance;
            intertiaVelocity =
                (inertiaDistance - -Mathf.Abs(currentDecelerationRate) * (timeToStop * timeToStop) /
                    2f) / timeToStop;
            intertiaVelocity *= direction;
        }

        /// <summary>
        /// Late update.
        /// </summary>
        protected virtual void LateUpdate()
        {
            if (isScrolling)
            {
                isScrolling = false;
                InitInertia();
            }
            else if (!isDragging && inertiaDistance > 0f)
            {
                var delta = GetDeltaTime();
                var distance = intertiaVelocity > 0f
                    ? Mathf.Min(inertiaDistance, intertiaVelocity * delta)
                    : Mathf.Max(-inertiaDistance, intertiaVelocity * delta);

                ScrollValue += distance;
                inertiaDistance -= Mathf.Abs(distance);

                if (inertiaDistance > 0f)
                {
                    intertiaVelocity += currentDecelerationRate * delta;
                    scrollVelocity = -intertiaVelocity;
                }
                else
                {
                    StopInertia();
                }
            }
        }

        /// <summary>
        /// Stop inertia.
        /// </summary>
        protected void StopInertia()
        {
            currentDecelerationRate = 0f;
            inertiaDistance = 0f;
        }

        /// <summary>
        /// Get delta time.
        /// </summary>
        /// <returns>Delta time.</returns>
        protected float GetDeltaTime()
        {
            return unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
        }
    }
}