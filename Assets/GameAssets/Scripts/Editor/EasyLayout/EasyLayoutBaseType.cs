﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Base class for EasyLayout groups.
    /// </summary>
    public abstract class EasyLayoutBaseType
    {
        /// <summary>
        /// Layout.
        /// </summary>
        protected readonly EasyLayout layout;

        /// <summary>
        /// Elements group.
        /// </summary>
        protected readonly LayoutElementsGroup elementsGroup = new LayoutElementsGroup();

        /// <summary>
        /// Group position.
        /// </summary>
        protected static readonly List<Vector2> GroupPositions = new List<Vector2>
        {
            new Vector2(0.0f, 1.0f), // Anchors.UpperLeft
            new Vector2(0.5f, 1.0f), // Anchors.UpperCenter
            new Vector2(1.0f, 1.0f), // Anchors.UpperRight

            new Vector2(0.0f, 0.5f), // Anchors.MiddleLeft
            new Vector2(0.5f, 0.5f), // Anchors.MiddleCenter
            new Vector2(1.0f, 0.5f), // Anchors.MiddleRight

            new Vector2(0.0f, 0.0f), // Anchors.LowerLeft
            new Vector2(0.5f, 0.0f), // Anchors.LowerCenter
            new Vector2(1.0f, 0.0f), // Anchors.LowerRight
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutBaseType"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        protected EasyLayoutBaseType(EasyLayout layout)
        {
            this.layout = layout;
        }

        /// <summary>
        /// Perform layout.
        /// </summary>
        /// <param name="elements">Elements.</param>
        /// <param name="setPositions">Set elements positions.</param>
        /// <returns>Size of the group.</returns>
        public Vector2 PerformLayout(List<LayoutElementInfo> elements, bool setPositions)
        {
            elementsGroup.SetElements(elements);
            SetInitialSizes();
            Group();

            elementsGroup.Sort();
            CalculateSizes();

            var size = CalculateGroupSize();

            CalculatePositions(size);
            SetSizes();

            if (setPositions)
            {
                SetPositions();
            }

            return size;
        }

        /// <summary>
        /// Get target position in the group.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <returns>Position.</returns>
        public virtual EasyLayoutPosition GetElementPosition(RectTransform target)
        {
            return elementsGroup.GetElementPosition(target);
        }

        /// <summary>
        /// Group elements.
        /// </summary>
        protected abstract void Group();

        /// <summary>
        /// Calculate sizes of the elements.
        /// </summary>
        protected abstract void CalculateSizes();

        /// <summary>
        /// Calculate positions of the elements.
        /// </summary>
        /// <param name="size">Size.</param>
        protected abstract void CalculatePositions(Vector2 size);

        /// <summary>
        /// Calculate group size.
        /// </summary>
        /// <returns>Size.</returns>
        protected abstract Vector2 CalculateGroupSize();

        private readonly List<float> _mainAxisSizes = new List<float>();

        /// <summary>
        /// Calculate size of the group.
        /// </summary>
        /// <param name="horizontal">ElementsGroup are in horizontal order?</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="padding">Padding,</param>
        /// <returns>Size.</returns>
        protected virtual Vector2 CalculateGroupSize(bool horizontal, Vector2 spacing,
            Vector2 padding)
        {
            var perBlock = horizontal ? elementsGroup.Rows : elementsGroup.Columns;
            var subAxisSize = (perBlock - 1) * spacing.y + padding.y;

            for (var i = 0; i < perBlock; i++)
            {
                var block = horizontal ? elementsGroup.GetRow(i) : elementsGroup.GetColumn(i);
                var blockSubSize = 0f;

                for (var j = 0; j < block.Count; j++)
                {
                    if (_mainAxisSizes.Count == j)
                    {
                        _mainAxisSizes.Add(0);
                    }

                    _mainAxisSizes[j] = Mathf.Max(_mainAxisSizes[j], horizontal ? block[j].Width : block[j].Height);
                    blockSubSize = Mathf.Max(blockSubSize, horizontal ? block[j].Height : block[j].Width);
                }

                subAxisSize += blockSubSize;
            }

            var mainAxisSize = Sum(_mainAxisSizes) + (_mainAxisSizes.Count - 1) * spacing.x + padding.x;

            _mainAxisSizes.Clear();
            return new Vector2(mainAxisSize, subAxisSize);
        }

        /// <summary>
        /// Set elements sizes.
        /// </summary>
        protected void SetSizes()
        {
            foreach (var element in elementsGroup.Elements)
            {
                layout.SetElementSize(element);
            }
        }

        /// <summary>
        /// Set elements positions.
        /// </summary>
        protected virtual void SetPositions()
        {
            foreach (var element in elementsGroup.Elements.Where(element => element.IsPositionChanged))
            {
                element.Rect.localPosition = element.positionPivot;
            }
        }

        /// <summary>
        /// Sum values of the list.
        /// </summary>
        /// <param name="list">List.</param>
        /// <returns>Sum.</returns>
        protected static float Sum(IEnumerable<float> list)
        {
            return list.Sum();
        }

        #region Group

        /// <summary>
        /// Reverse list.
        /// </summary>
        /// <param name="list">List.</param>
        protected static void ReverseList(List<LayoutElementInfo> list)
        {
            list.Reverse();
        }

        /// <summary>
        /// Group elements by columns in the vertical order.
        /// </summary>
        /// <param name="maxColumns">Maximum columns count.</param>
        protected void GroupByColumnsVertical(int maxColumns)
        {
            var i = 0;

            for (var column = 0; column < maxColumns; column++)
            {
                var maxRows = Mathf.CeilToInt((float) (elementsGroup.Count - i) / (maxColumns - column));

                for (var row = 0; row < maxRows; row++)
                {
                    elementsGroup.SetPosition(i, row, column);
                    i += 1;
                }
            }
        }

        /// <summary>
        /// Group elements by columns in the horizontal order.
        /// </summary>
        /// <param name="maxColumns">Maximum columns count.</param>
        protected void GroupByColumnsHorizontal(int maxColumns)
        {
            var row = 0;

            for (var i = 0; i < elementsGroup.Count; i += maxColumns)
            {
                var column = 0;
                var end = Mathf.Min(i + maxColumns, elementsGroup.Count);

                for (var j = i; j < end; j++)
                {
                    elementsGroup.SetPosition(j, row, column);
                    column += 1;
                }

                row += 1;
            }
        }

        /// <summary>
        /// Group elements by rows in the vertical order.
        /// </summary>
        /// <param name="maxRows">Maximum rows count.</param>
        protected void GroupByRowsVertical(int maxRows)
        {
            var column = 0;

            for (var i = 0; i < elementsGroup.Count; i += maxRows)
            {
                var row = 0;
                var end = Mathf.Min(i + maxRows, elementsGroup.Count);

                for (var j = i; j < end; j++)
                {
                    elementsGroup.SetPosition(j, row, column);
                    row += 1;
                }

                column += 1;
            }
        }

        /// <summary>
        /// Group elements by rows in the horizontal order.
        /// </summary>
        /// <param name="maxRows">Maximum rows count.</param>
        protected void GroupByRowsHorizontal(int maxRows)
        {
            var i = 0;

            for (var row = 0; row < maxRows; row++)
            {
                var maxColumns = Mathf.CeilToInt((float) (elementsGroup.Count - i) / (maxRows - row));

                for (var column = 0; column < maxColumns; column++)
                {
                    elementsGroup.SetPosition(i, row, column);
                    i += 1;
                }
            }
        }

        /// <summary>
        /// Group the specified uiElements by columns.
        /// </summary>
        protected void GroupByColumns()
        {
            if (layout.IsHorizontal)
            {
                GroupByColumnsHorizontal(layout.ConstraintCount);
            }
            else
            {
                GroupByColumnsVertical(layout.ConstraintCount);
            }
        }

        /// <summary>
        /// Group the specified uiElements by rows.
        /// </summary>
        protected void GroupByRows()
        {
            if (layout.IsHorizontal)
            {
                GroupByRowsHorizontal(layout.ConstraintCount);
            }
            else
            {
                GroupByRowsVertical(layout.ConstraintCount);
            }
        }

        #endregion

        #region Sizes

        /// <summary>
        /// Resize elements.
        /// </summary>
        protected virtual void SetInitialSizes()
        {
            if (layout.ChildrenWidth == ChildrenSize.DoNothing && layout.ChildrenHeight == ChildrenSize.DoNothing)
            {
                return;
            }

            if (elementsGroup.Count == 0)
            {
                return;
            }

            var maxSize = FindMaxPreferredSize();

            foreach (var element in elementsGroup.Elements)
            {
                SetInitialSize(element, maxSize);
            }
        }

        private Vector2 FindMaxPreferredSize()
        {
            var maxSize = new Vector2(-1f, -1f);

            foreach (var element in elementsGroup.Elements)
            {
                maxSize.x = Mathf.Max(maxSize.x, element.PreferredWidth);
                maxSize.y = Mathf.Max(maxSize.y, element.PreferredHeight);
            }

            if (layout.ChildrenWidth != ChildrenSize.SetMaxFromPreferred)
            {
                maxSize.x = -1f;
            }

            if (layout.ChildrenHeight != ChildrenSize.SetMaxFromPreferred)
            {
                maxSize.y = -1f;
            }

            return maxSize;
        }

        private void SetInitialSize(LayoutElementInfo element, Vector2 maxSize)
        {
            if (layout.ChildrenWidth != ChildrenSize.DoNothing)
            {
                element.NewWidth = maxSize is {x: -1f} ? maxSize.x : element.PreferredWidth;
            }

            if (layout.ChildrenHeight != ChildrenSize.DoNothing)
            {
                element.NewHeight = maxSize is {y: -1f} ? maxSize.y : element.PreferredHeight;
            }
        }

        /// <summary>
        /// Resize elements width to fit.
        /// </summary>
        /// <param name="increaseOnly">Size can be only increased.</param>
        protected void ResizeWidthToFit(bool increaseOnly)
        {
            var width = layout.InternalSize.x;

            for (var row = 0; row < elementsGroup.Rows; row++)
            {
                ResizeToFit(width, elementsGroup.GetRow(row), layout.Spacing.x, RectTransform.Axis.Horizontal,
                    increaseOnly);
            }
        }

        /// <summary>
        /// Resize specified elements to fit.
        /// </summary>
        /// <param name="size">Size.</param>
        /// <param name="elements">Elements.</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="axis">Axis to fit.</param>
        /// <param name="increaseOnly">Size can be only increased.</param>
        protected static void ResizeToFit(float size, List<LayoutElementInfo> elements,
            float spacing, RectTransform.Axis axis, bool increaseOnly)
        {
            var sizes = axis == RectTransform.Axis.Horizontal
                ? SizeInfo.GetWidths(elements)
                : SizeInfo.GetHeights(elements);
            var freeSpace = size - sizes.totalPreferred - (elements.Count - 1) * spacing;

            if (increaseOnly)
            {
                freeSpace = Mathf.Max(0f, freeSpace);
                size = Mathf.Max(0f, size);
                sizes.totalMin = sizes.totalPreferred;
            }

            var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;
            var minPrefLerp = 1f;

            if (!sizes.totalMin.Equals(sizes.totalPreferred))
            {
                minPrefLerp = Mathf.Clamp01((size - sizes.totalMin - (elements.Count - 1) * spacing) /
                                            (sizes.totalPreferred - sizes.totalMin));
            }

            for (var i = 0; i < elements.Count; i++)
            {
                var elementSize = Mathf.Lerp(sizes.sizes[i].min, sizes.sizes[i].preferred, minPrefLerp) +
                                  perFlexible * sizes.sizes[i].flexible;

                elements[i].SetSize(axis, elementSize);
            }
        }

        /// <summary>
        /// Shrink elements width to fit.
        /// </summary>
        protected void ShrinkWidthToFit()
        {
            var width = layout.InternalSize.x;

            for (var row = 0; row < elementsGroup.Rows; row++)
            {
                ShrinkToFit(width, elementsGroup.GetRow(row), layout.Spacing.x,
                    RectTransform.Axis.Horizontal);
            }
        }

        /// <summary>
        /// Resize row height to fit.
        /// </summary>
        /// <param name="increaseOnly">Size can be only increased.</param>
        protected void ResizeRowHeightToFit(bool increaseOnly)
        {
            ResizeToFit(layout.InternalSize.y, elementsGroup, layout.Spacing.y,
                RectTransform.Axis.Vertical, increaseOnly);
        }

        /// <summary>
        /// Shrink row height to fit.
        /// </summary>
        protected void ShrinkRowHeightToFit()
        {
            ShrinkToFit(layout.InternalSize.y, elementsGroup, layout.Spacing.y,
                RectTransform.Axis.Vertical);
        }

        /// <summary>
        /// Shrink specified elements size to fit.
        /// </summary>
        /// <param name="size">Size.</param>
        /// <param name="elements">Elements.</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="axis">Axis to fit.</param>
        protected static void ShrinkToFit(float size, List<LayoutElementInfo> elements,
            float spacing, RectTransform.Axis axis)
        {
            var sizes = axis == RectTransform.Axis.Horizontal
                ? SizeInfo.GetWidths(elements)
                : SizeInfo.GetHeights(elements);
            var freeSpace = size - sizes.totalPreferred - (elements.Count - 1) * spacing;

            if (freeSpace > 0f)
            {
                return;
            }

            var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;
            var minPrefLerp = 0f;

            if (!sizes.totalMin.Equals(sizes.totalPreferred))
            {
                minPrefLerp = Mathf.Clamp01((size - sizes.totalMin - (elements.Count - 1) * spacing) /
                                            (sizes.totalPreferred - sizes.totalMin));
            }

            for (var i = 0; i < elements.Count; i++)
            {
                var elementSize = Mathf.Lerp(sizes.sizes[i].min, sizes.sizes[i].preferred, minPrefLerp) +
                                  perFlexible * sizes.sizes[i].flexible;

                elements[i].SetSize(axis, elementSize);
            }
        }

        /// <summary>
        /// Resize specified elements to fit.
        /// </summary>
        /// <param name="size">Size.</param>
        /// <param name="group">Group.</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="axis">Axis to fit.</param>
        /// <param name="increaseOnly">Size can be only increased.</param>
        protected static void ResizeToFit(float size, LayoutElementsGroup group, float spacing,
            RectTransform.Axis axis, bool increaseOnly)
        {
            var isHorizontal = axis == RectTransform.Axis.Horizontal;
            var sizes = isHorizontal ? SizeInfo.GetWidths(group) : SizeInfo.GetHeights(group);
            var n = isHorizontal ? group.Columns : group.Rows;
            var freeSpace = size - sizes.totalPreferred - (n - 1) * spacing;

            if (increaseOnly)
            {
                freeSpace = Mathf.Max(0f, freeSpace);
                size = Mathf.Max(0f, size);
                sizes.totalMin = sizes.totalPreferred;
            }

            var minPrefLerp = 1f;

            if (!sizes.totalMin.Equals(sizes.totalPreferred))
            {
                minPrefLerp = Mathf.Clamp01((size - sizes.totalMin - (@group.Rows - 1) * spacing) /
                                            (sizes.totalPreferred - sizes.totalMin));
            }

            if (isHorizontal)
            {
                var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;

                for (var column = 0; column < group.Columns; column++)
                {
                    var elementSize = Mathf.Lerp(sizes.sizes[column].min, sizes.sizes[column].preferred,
                        minPrefLerp) + perFlexible * sizes.sizes[column].flexible;

                    foreach (var element in group.GetColumn(column))
                    {
                        element.SetSize(axis, elementSize);
                    }
                }
            }
            else
            {
                var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;

                for (var rows = 0; rows < group.Rows; rows++)
                {
                    var elementSize = Mathf.Lerp(sizes.sizes[rows].min, sizes.sizes[rows].preferred,
                        minPrefLerp) + perFlexible * sizes.sizes[rows].flexible;

                    foreach (var element in group.GetRow(rows))
                    {
                        element.SetSize(axis, elementSize);
                    }
                }
            }
        }

        /// <summary>
        /// Shrink specified elements to fit.
        /// </summary>
        /// <param name="size">Size.</param>
        /// <param name="group">Elements.</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="axis">Axis to fit.</param>
        protected static void ShrinkToFit(float size, LayoutElementsGroup group, float spacing,
            RectTransform.Axis axis)
        {
            var isHorizontal = axis == RectTransform.Axis.Horizontal;
            var sizes = isHorizontal ? SizeInfo.GetWidths(group) : SizeInfo.GetHeights(group);
            var n = isHorizontal ? group.Columns : group.Rows;
            var freeSpace = size - sizes.totalPreferred - (n - 1) * spacing;

            if (freeSpace > 0f)
            {
                return;
            }

            var minPrefLerp = 0f;

            if (!sizes.totalMin.Equals(sizes.totalPreferred))
            {
                minPrefLerp = Mathf.Clamp01((size - sizes.totalMin - (@group.Rows - 1) * spacing) /
                                            (sizes.totalPreferred - sizes.totalMin));
            }

            if (isHorizontal)
            {
                var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;

                for (var column = 0; column < group.Columns; column++)
                {
                    var elementSize = Mathf.Lerp(sizes.sizes[column].min, sizes.sizes[column].preferred,
                        minPrefLerp) + perFlexible * sizes.sizes[column].flexible;

                    foreach (var element in group.GetColumn(column))
                    {
                        element.SetSize(axis, elementSize);
                    }
                }
            }
            else
            {
                var perFlexible = freeSpace > 0f ? freeSpace / sizes.totalFlexible : 0f;

                for (var rows = 0; rows < group.Rows; rows++)
                {
                    var elementSize = Mathf.Lerp(sizes.sizes[rows].min, sizes.sizes[rows].preferred,
                        minPrefLerp) + perFlexible * sizes.sizes[rows].flexible;

                    foreach (var element in group.GetRow(rows))
                    {
                        element.SetSize(axis, elementSize);
                    }
                }
            }
        }

        #endregion
    }
}