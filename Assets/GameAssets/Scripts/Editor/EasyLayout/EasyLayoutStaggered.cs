﻿using System.Collections.Generic;
using EasyLayoutNS;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Easy layout staggered layout.
    /// </summary>
    public class EasyLayoutStaggered : EasyLayoutFlexOrStaggered
    {
        private readonly List<float> _blocksSizes = new List<float>();

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutStaggered"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public EasyLayoutStaggered(EasyLayout layout)
            : base(layout)
        {
        }

        private int GetBlockSizes()
        {
            if (elementsGroup.Count == 0)
            {
                return 0;
            }

            if (layout.StaggeredSettings.FixedBlocksCount)
            {
                return Mathf.Max(1, layout.StaggeredSettings.BlocksCount);
            }

            var blocks = 1;
            var size = layout.SubAxisSize - elementsGroup[0].SubAxisSize;
            var spacing = !layout.IsHorizontal ? layout.Spacing.x : layout.Spacing.y;

            foreach (var element in elementsGroup.Elements)
            {
                size -= element.SubAxisSize + spacing;
                if (size < 0f)
                {
                    break;
                }

                blocks += 1;
            }

            return blocks;
        }

        private void InitBlockSizes(int blocks)
        {
            _blocksSizes.Clear();

            EnsureListSize(_blocksSizes, blocks);
            EnsureListSize(layout.StaggeredSettings.paddingInnerStart, blocks);
            EnsureListSize(layout.StaggeredSettings.paddingInnerEnd, blocks);

            var padding = layout.StaggeredSettings.paddingInnerStart;

            for (var i = 0; i < blocks; i++)
            {
                _blocksSizes[i] = padding[i];
            }
        }

        private static void EnsureListSize<T>(List<T> list, int size)
        {
            for (var i = list.Count; i < size; i++)
            {
                list.Add(default);
            }
        }

        private int NextBlockIndex()
        {
            var index = 0;
            var minSize = _blocksSizes[0];

            for (var i = 1; i < _blocksSizes.Count; i++)
            {
                var size = _blocksSizes[i];

                if (!(size < minSize))
                {
                    continue;
                }

                index = i;
                minSize = size;
            }

            return index;
        }

        private void InsertToBlock(int blockIndex, LayoutElementInfo element)
        {
            var block = layout.IsHorizontal
                ? elementsGroup.GetRow(blockIndex)
                : elementsGroup.GetColumn(blockIndex);
            if (layout.IsHorizontal)
            {
                elementsGroup.SetPosition(element, blockIndex, block.Count);
            }
            else
            {
                elementsGroup.SetPosition(element, block.Count, blockIndex);
            }

            _blocksSizes[blockIndex] += element.AxisSize;

            if (block.Count <= 0)
            {
                return;
            }

            var spacing = layout.IsHorizontal ? layout.Spacing.x : layout.Spacing.y;
            _blocksSizes[blockIndex] += spacing;
        }

        /// <summary>
        /// Group elements.
        /// </summary>
        protected override void Group()
        {
            var blocks = GetBlockSizes();

            InitBlockSizes(blocks);

            foreach (var element in elementsGroup.Elements)
            {
                InsertToBlock(NextBlockIndex(), element);
            }

            if (!layout.TopToBottom)
            {
                elementsGroup.BottomToTop();
            }

            if (layout.RightToLeft)
            {
                elementsGroup.RightToLeft();
            }
        }

        /// <summary>
        /// Calculate size of the group.
        /// </summary>
        /// <param name="horizontal">ElementsGroup are in horizontal order?</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="padding">Padding,</param>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize(bool horizontal, Vector2 spacing,
            Vector2 padding)
        {
            var size = default(Vector2);
            var paddingEnd = layout.StaggeredSettings.paddingInnerEnd;

            for (var i = 0; i < _blocksSizes.Count; i++)
            {
                size.x = Mathf.Max(size.x, _blocksSizes[i] + paddingEnd[i]);
            }

            size.y = GetSubAxisSize();
            return ByAxis(size);
        }

        private float GetSubAxisSize()
        {
            CalculateSubAxisSizes();

            var spacing = !layout.IsHorizontal ? layout.Spacing.x : layout.Spacing.y;
            return Sum(subAxisSizes) + (subAxisSizes.Count - 1) * spacing;
        }

        /// <summary>
        /// Get main axis data.
        /// </summary>
        /// <param name="blockIndex">Block index.</param>
        /// <returns>Main axis data.</returns>
        protected override AxisData MainAxisData(int blockIndex)
        {
            var axis = base.MainAxisData(blockIndex);
            
            axis.offset += layout.StaggeredSettings.paddingInnerStart[blockIndex];
            return axis;
        }
    }
}