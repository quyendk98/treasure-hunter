﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Settings for the staggered layout.
    /// </summary>
    [Serializable]
    public class EasyLayoutStaggeredSettings : INotifyPropertyChanged
    {
        [SerializeField] [Tooltip("Layout with fixed amount of blocks (row or columns) instead of the flexible.")]
        private bool fixedBlocksCount;

        /// <summary>
        /// Use fixed blocks count.
        /// </summary>
        public bool FixedBlocksCount
        {
            get => fixedBlocksCount;

            set
            {
                if (fixedBlocksCount == value)
                {
                    return;
                }

                fixedBlocksCount = value;

                Changed("FixedBlocksCount");
            }
        }

        [SerializeField] private int blocksCount = 1;

        /// <summary>
        /// Block (row or columns) count.
        /// </summary>
        public int BlocksCount
        {
            get => blocksCount;

            set
            {
                if (blocksCount == value)
                {
                    return;
                }

                blocksCount = value;

                Changed("BlocksCount");
            }
        }

        /// <summary>
        /// PaddingInner at the start of the blocks.
        /// Used by ListViewCustom to simulate the space occupied by non-displayable elements.
        /// </summary>
        [HideInInspector] public List<float> paddingInnerStart = new List<float>();

        /// <summary>
        /// PaddingInner at the end of the blocks.
        /// Used by ListViewCustom to simulate the space occupied by non-displayable elements.
        /// </summary>
        [HideInInspector] public List<float> paddingInnerEnd = new List<float>();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = EasyLayout.DefaultPropertyHandler;

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void Changed(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}