﻿using EasyLayoutNS;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Flexbox-like layout group.
    /// </summary>
    public class EasyLayoutFlex : EasyLayoutFlexOrStaggered
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutFlex"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public EasyLayoutFlex(EasyLayout layout)
            : base(layout)
        {
        }

        /// <summary>
        /// Group elements.
        /// </summary>
        protected override void Group()
        {
            if (!layout.FlexSettings.Wrap)
            {
                for (var i = 0; i < elementsGroup.Count; i++)
                {
                    elementsGroup.SetPosition(i, layout.IsHorizontal ? 0 : i,
                        layout.IsHorizontal ? i : 0);
                }

                return;
            }

            var baseSize = layout.MainAxisSize;
            var size = baseSize;
            var spacing = layout.IsHorizontal ? layout.Spacing.x : layout.Spacing.y;

            var mainAxisIndex = 0;
            var subAxisIndex = 0;

            for (var i = 0; i < elementsGroup.Count; i++)
            {
                var element = elementsGroup[i];

                if (subAxisIndex == 0)
                {
                    size -= element.AxisSize;
                }
                else if (size >= element.AxisSize + spacing)
                {
                    size -= element.AxisSize + spacing;
                }
                else
                {
                    size = baseSize - element.AxisSize;

                    mainAxisIndex++;
                    subAxisIndex = 0;
                }

                if (layout.IsHorizontal)
                {
                    elementsGroup.SetPosition(i, mainAxisIndex, subAxisIndex);
                }
                else
                {
                    elementsGroup.SetPosition(i, subAxisIndex, mainAxisIndex);
                }

                subAxisIndex++;
            }

            if (!layout.TopToBottom)
            {
                elementsGroup.BottomToTop();
            }

            if (layout.RightToLeft)
            {
                elementsGroup.RightToLeft();
            }
        }

        /// <summary>
        /// Calculate size of the group.
        /// </summary>
        /// <param name="horizontal">ElementsGroup are in horizontal order?</param>
        /// <param name="spacing">Spacing.</param>
        /// <param name="padding">Padding,</param>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize(bool horizontal, Vector2 spacing,
            Vector2 padding)
        {
            var size = base.CalculateGroupSize(horizontal, spacing, padding);

            // add is_horizontal check
            var stretchMainAxis = layout.FlexSettings.JustifyContent ==
                                  EasyLayoutFlexSettings.Content.SpaceAround
                                  || layout.FlexSettings.JustifyContent ==
                                  EasyLayoutFlexSettings.Content.SpaceBetween
                                  || layout.FlexSettings.JustifyContent ==
                                  EasyLayoutFlexSettings.Content.SpaceEvenly;

            if (stretchMainAxis)
            {
                size.x = ByAxis(layout.InternalSize).x;
            }

            var stretchSubAxis = layout.FlexSettings.AlignContent ==
                                 EasyLayoutFlexSettings.Content.SpaceAround
                                 || layout.FlexSettings.AlignContent ==
                                 EasyLayoutFlexSettings.Content.SpaceBetween
                                 || layout.FlexSettings.AlignContent ==
                                 EasyLayoutFlexSettings.Content.SpaceEvenly;

            if (stretchSubAxis)
            {
                size.y = ByAxis(layout.InternalSize).y;
            }

            return ByAxis(size);
        }

        /// <summary>
        /// Get axis data.
        /// </summary>
        /// <param name="isMainAxis">Is main axis?</param>
        /// <param name="elements">Elements count.</param>
        /// <param name="size">Total size of the elements.</param>
        /// <returns>Axis data.</returns>
        protected override AxisData GetAxisData(bool isMainAxis, int elements, float size)
        {
            var axis = base.GetAxisData(isMainAxis, elements, size);
            var outerSize = isMainAxis ? ByAxis(layout.InternalSize).x : ByAxis(layout.InternalSize).y;
            var align = isMainAxis
                ? layout.FlexSettings.JustifyContent
                : layout.FlexSettings.AlignContent;

            switch (align)
            {
                case EasyLayoutFlexSettings.Content.End:
                    axis.offset += outerSize - (size + (elements - 1) * axis.spacing);
                    break;

                case EasyLayoutFlexSettings.Content.Center:
                    axis.offset += (outerSize - (size + (elements - 1) * axis.spacing)) / 2f;
                    break;

                case EasyLayoutFlexSettings.Content.SpaceAround:
                {
                    var space = (outerSize - size) / (elements * 2);
                    axis.offset += space;
                    axis.spacing = space * 2;
                    break;
                }

                case EasyLayoutFlexSettings.Content.SpaceBetween:
                {
                    if (elements > 1)
                    {
                        axis.spacing = (outerSize - size) / (elements - 1);
                    }

                    break;
                }

                case EasyLayoutFlexSettings.Content.SpaceEvenly:
                {
                    var space = (outerSize - size) / (elements + 1);

                    axis.offset += space;
                    axis.spacing = space;
                    break;
                }
            }

            return axis;
        }

        /// <summary>
        /// Get align rate for the items.
        /// </summary>
        /// <returns>Align rate.</returns>
        protected override float GetItemsAlignRate()
        {
            switch (layout.FlexSettings.AlignItems)
            {
                case EasyLayoutFlexSettings.Items.Start:
                    return 0f;

                case EasyLayoutFlexSettings.Items.Center:
                    return 0.5f;

                case EasyLayoutFlexSettings.Items.End:
                    return 1f;

                default:
                    Debug.LogWarning($"Unknown items align: {layout.FlexSettings.AlignItems}");
                    break;
            }

            return 0f;
        }
    }
}