﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// LayoutElementInfo.
    /// Correctly works with multiple resizes during one frame.
    /// </summary>
    public class LayoutElementInfo
    {
        /// <summary>
        /// GameObject active state.
        /// </summary>
        public bool active;

        /// <summary>
        /// RectTransform.
        /// </summary>
        public RectTransform Rect { get; protected set; }

        /// <summary>
        /// Width.
        /// </summary>
        public float Width { get; protected set; }

        /// <summary>
        /// Height.
        /// </summary>
        public float Height { get; protected set; }

        /// <summary>
        /// Size on main axis.
        /// </summary>
        public float AxisSize => layout.IsHorizontal ? Width : Height;

        /// <summary>
        /// Size on sub axis.
        /// </summary>
        public float SubAxisSize => layout.IsHorizontal ? Height : Width;

        /// <summary>
        /// Minimal width.
        /// </summary>
        public float MinWidth { get; protected set; }

        /// <summary>
        /// Minimal height.
        /// </summary>
        public float MinHeight { get; protected set; }

        /// <summary>
        /// Preferred width.
        /// </summary>
        public float PreferredWidth { get; protected set; }

        /// <summary>
        /// Preferred height.
        /// </summary>
        public float PreferredHeight { get; protected set; }

        /// <summary>
        /// Flexible width.
        /// </summary>
        public float FlexibleWidth { get; protected set; }

        /// <summary>
        /// Flexible height.
        /// </summary>
        public float FlexibleHeight { get; protected set; }

        /// <summary>
        /// Current layout.
        /// </summary>
        protected EasyLayout layout;

        /// <summary>
        /// Scale.
        /// </summary>
        protected Vector3 scale;

        /// <summary>
        /// Is width changed?
        /// </summary>
        public bool ChangedWidth { get; protected set; }

        /// <summary>
        /// New width.
        /// </summary>
        protected float newWidth;

        /// <summary>
        /// New width.
        /// </summary>
        public float NewWidth
        {
            get => newWidth;

            set
            {
                if (newWidth.Equals(value))
                {
                    return;
                }

                newWidth = value;
                Width = value * scale.x;
                ChangedWidth = true;
            }
        }

        /// <summary>
        /// Is height changed?
        /// </summary>
        public bool ChangedHeight { get; protected set; }

        /// <summary>
        /// New height.
        /// </summary>
        protected float newHeight;

        /// <summary>
        /// New height.
        /// </summary>
        public float NewHeight
        {
            get => newHeight;

            set
            {
                if (newHeight.Equals(value))
                {
                    return;
                }

                newHeight = value;
                Height = value * scale.y;
                ChangedHeight = true;
            }
        }

        /// <summary>
        /// Is pivot changed?
        /// </summary>
        public bool ChangedPivot { get; protected set; }

        /// <summary>
        /// Pivot.
        /// </summary>
        public Vector2 Pivot { get; protected set; }

        /// <summary>
        /// New pivot.
        /// </summary>
        protected Vector2 newPivot;

        /// <summary>
        /// New pivot.
        /// </summary>
        public Vector2 NewPivot
        {
            get => newPivot;

            set
            {
                if (newPivot == value)
                {
                    return;
                }

                newPivot = value;
                Pivot = value;
                ChangedPivot = true;
            }
        }

        /// <summary>
        /// Position of the top left corner.
        /// </summary>
        public Vector2 PositionTopLeft
        {
            get => new Vector2(
                positionPivot.x - Width * NewPivot.x,
                positionPivot.y + Height * (1f - NewPivot.y));

            set => positionPivot = new Vector2(
                value.x + Width * NewPivot.x,
                value.y - Height * (1f - NewPivot.y));
        }

        /// <summary>
        /// Position with pivot.
        /// </summary>
        public Vector2 positionPivot;

        /// <summary>
        /// Is position changed?
        /// </summary>
        public bool IsPositionChanged
        {
            get
            {
                var newPos = positionPivot;
                var lp = Rect.localPosition;
                return !lp.x.Equals(newPos.x) || !lp.y.Equals(newPos.y);
            }
        }

        /// <summary>
        /// Is rotation changed?
        /// </summary>
        public bool ChangedRotation { get; protected set; }

        /// <summary>
        /// New EulerAngles.
        /// </summary>
        protected Vector3 newEulerAngles = Vector3.zero;

        /// <summary>
        /// New EulerAngles.Z.
        /// </summary>
        public float NewEulerAnglesZ
        {
            get => newEulerAngles.z;

            set
            {
                if (newEulerAngles.z.Equals(value))
                {
                    return;
                }

                newEulerAngles.z = value;
                ChangedRotation = true;
            }
        }

        /// <summary>
        /// New EulerAngles.
        /// </summary>
        public Vector3 NewEulerAngles
        {
            get => newEulerAngles;

            set
            {
                if (newEulerAngles == value)
                {
                    return;
                }

                newEulerAngles = value;
                ChangedRotation = true;
            }
        }

        /// <summary>
        /// Row.
        /// </summary>
        public int row;

        /// <summary>
        /// Column.
        /// </summary>
        public int column;

        /// <summary>
        /// Set element.
        /// </summary>
        /// <param name="rectTransform">RectTransform.</param>
        /// <param name="active">Is game object active?</param>
        /// <param name="layout">Current layout.</param>
        public void SetElement(RectTransform rectTransform, bool active, EasyLayout layout)
        {
            var pivot = rectTransform.pivot;
            var rect = rectTransform.rect;

            Rect = rectTransform;
            this.active = active;
            this.layout = layout;

            scale = rectTransform.localScale;
            Width = rect.width * scale.x;
            Height = rect.height * scale.y;
            Pivot = pivot;

            newWidth = rect.width;
            newHeight = rect.height;
            newPivot = pivot;

            newEulerAngles = rectTransform.localEulerAngles;
            ChangedWidth = false;
            ChangedHeight = false;

            if (this.layout.ChildrenWidth != ChildrenSize.DoNothing ||
                this.layout.ChildrenHeight != ChildrenSize.DoNothing)
            {
                RefreshLayoutElements();
            }

            if (this.layout.ChildrenWidth != ChildrenSize.DoNothing)
            {
                var sizes = GetWidthValues();

                MinWidth = sizes.min;
                PreferredWidth = sizes.preferred;
                FlexibleWidth = sizes.flexible;
            }
            else
            {
                MinWidth = 0f;
                PreferredWidth = 0f;
                FlexibleWidth = 0f;
            }

            if (this.layout.ChildrenHeight != ChildrenSize.DoNothing)
            {
                var sizes = GetHeightValues();

                MinHeight = sizes.min;
                PreferredHeight = sizes.preferred;
                FlexibleHeight = sizes.flexible;
            }
            else
            {
                MinHeight = 0f;
                PreferredHeight = 0f;
                FlexibleHeight = 0f;
            }

            row = -1;
            column = -1;
        }

        /// <summary>
        /// Get widths values.
        /// </summary>
        /// <returns>Widths values.</returns>
        protected Size GetWidthValues()
        {
            if (Rect == null)
            {
                return default;
            }

            if (Rect.gameObject.activeInHierarchy)
            {
                return new Size
                {
                    min = Mathf.Max(0f, LayoutUtility.GetMinWidth(Rect)),
                    preferred = Mathf.Max(0f, LayoutUtility.GetPreferredWidth(Rect)),
                    flexible = Mathf.Max(0f, LayoutUtility.GetFlexibleWidth(Rect)),
                };
            }

            return GetLayoutWidths();
        }

        /// <summary>
        /// Get heights values.
        /// </summary>
        /// <returns>Height values.</returns>
        protected Size GetHeightValues()
        {
            if (Rect == null)
            {
                return default;
            }

            if (Rect.gameObject.activeInHierarchy)
            {
                return new Size
                {
                    min = Mathf.Max(0f, LayoutUtility.GetMinHeight(Rect)),
                    preferred = Mathf.Max(0f, LayoutUtility.GetPreferredHeight(Rect)),
                    flexible = Mathf.Max(0f, LayoutUtility.GetFlexibleHeight(Rect)),
                };
            }

            return GetLayoutHeights();
        }

        /// <summary>
        /// Set size.
        /// </summary>
        /// <param name="axis">Axis.</param>
        /// <param name="size">New size.</param>
        public void SetSize(RectTransform.Axis axis, float size)
        {
            if (axis == RectTransform.Axis.Horizontal)
            {
                NewWidth = size;
            }
            else
            {
                NewHeight = size;
            }
        }

        /// <summary>
        /// All ILayoutElements Of current game object.
        /// </summary>
        protected readonly List<ILayoutElement> layoutElements = new List<ILayoutElement>();

        /// <summary>
        /// Get widths from LayoutElements.
        /// </summary>
        /// <returns>Widths values.</returns>
        protected Size GetLayoutWidths()
        {
            var maxPriority = MaxPriority(layoutElements);
            var result = default(Size);

            foreach (var elem in layoutElements.Where(elem => elem.layoutPriority == maxPriority))
            {
                result.min = Mathf.Max(result.min, elem.minWidth);
                result.preferred = Mathf.Max(result.preferred,
                    Mathf.Max(elem.preferredWidth, elem.minWidth));
                result.flexible = Mathf.Max(result.flexible, elem.flexibleWidth);
            }

            return result;
        }

        /// <summary>
        /// Refresh LayoutElements list.
        /// </summary>
        protected void RefreshLayoutElements()
        {
            layoutElements.Clear();
            Rect.GetComponents(layoutElements);
        }

        /// <summary>
        /// Get heights from LayoutElements.
        /// </summary>
        /// <returns>Heights values.</returns>
        protected Size GetLayoutHeights()
        {
            var maxPriority = MaxPriority(layoutElements);
            var result = default(Size);

            foreach (var elem in layoutElements.Where(elem => elem.layoutPriority == maxPriority))
            {
                result.min = Mathf.Max(result.min, elem.minHeight);
                result.preferred = Mathf.Max(result.preferred, Mathf.Max(elem.preferredHeight, elem.minHeight));
                result.flexible = Mathf.Max(result.flexible, elem.flexibleHeight);
            }

            return result;
        }

        /// <summary>
        /// Get maximum priority from LayoutElements.
        /// </summary>
        /// <param name="elements">LayoutElements list.</param>
        /// <returns>Maximum priority.</returns>
        protected static int MaxPriority(List<ILayoutElement> elements)
        {
            if (elements.Count == 0)
            {
                return 0;
            }

            var result = elements[0].layoutPriority;

            for (var i = 1; i < elements.Count; i++)
            {
                result = Mathf.Max(result, elements[i].layoutPriority);
            }

            return result;
        }
    }
}