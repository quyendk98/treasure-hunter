﻿using System.Collections.Generic;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Sizes info.
    /// </summary>
    public struct SizeInfo : System.IEquatable<SizeInfo>
    {
        /// <summary>
        /// The summary minimum size.
        /// </summary>
        public float totalMin;

        /// <summary>
        /// The summary preferred size.
        /// </summary>
        public float totalPreferred;

        /// <summary>
        /// The summary flexible size.
        /// </summary>
        public float totalFlexible;

        /// <summary>
        /// The sizes.
        /// </summary>
        public Size[] sizes;

        /// <summary>
        /// Serves as a hash function for a object.
        /// </summary>
        /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
        public override int GetHashCode()
        {
            return totalMin.GetHashCode() ^ totalPreferred.GetHashCode() ^
                   totalFlexible.GetHashCode() ^ sizes.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified System.Object is equal to the current size.
        /// </summary>
        /// <param name="obj">The System.Object to compare with the current size.</param>
        /// <returns><c>true</c> if the specified System.Object is equal to the current size; otherwise, <c>false</c>.</returns>
        public override bool Equals(object obj)
        {
            return obj is SizeInfo si && Equals(si);
        }

        /// <summary>
        /// Determines whether the specified size is equal to the current size.
        /// </summary>
        /// <param name="other">The size to compare with the current size.</param>
        /// <returns><c>true</c> if the specified size is equal to the current size; otherwise, <c>false</c>.</returns>
        public bool Equals(SizeInfo other)
        {
            return totalMin.Equals(other.totalMin) && totalPreferred.Equals(other.totalPreferred) &&
                   totalFlexible.Equals(other.totalFlexible) && sizes == other.sizes;
        }

        /// <summary>
        /// Compare sizes.
        /// </summary>
        /// <param name="sizeInfo1">First size.</param>
        /// <param name="sizeInfo2">Second size.</param>
        /// <returns>True if sizes are equals; otherwise false.</returns>
        public static bool operator ==(SizeInfo sizeInfo1, SizeInfo sizeInfo2)
        {
            return sizeInfo1.Equals(sizeInfo2);
        }

        /// <summary>
        /// Compare sizes.
        /// </summary>
        /// <param name="sizeInfo1">First size.</param>
        /// <param name="sizeInfo2">Second size.</param>
        /// <returns>True if sizes are not equals; otherwise false.</returns>
        public static bool operator !=(SizeInfo sizeInfo1, SizeInfo sizeInfo2)
        {
            return !sizeInfo1.Equals(sizeInfo2);
        }

        /// <summary>
        /// Gets the sizes info.
        /// </summary>
        /// <returns>The sizes info.</returns>
        /// <param name="sizes">Sizes.</param>
        public static SizeInfo GetSizesInfo(Size[] sizes)
        {
            var result = new SizeInfo {sizes = sizes};
            
            for (var i = 0; i < sizes.Length; i++)
            {
                result.totalMin += sizes[i].min;
                result.totalPreferred += sizes[i].preferred;
                result.totalFlexible += sizes[i].flexible;
            }

            if (result.totalFlexible != 0f)
            {
                return result;
            }

            for (var i = 0; i < sizes.Length; i++)
            {
                sizes[i].flexible = 1f;
            }

            result.totalFlexible += sizes.Length;
            return result;
        }

        /// <summary>
        /// Gets the widths sizes info.
        /// </summary>
        /// <returns>The widths sizes info.</returns>
        /// <param name="elems">Elements.</param>
        public static SizeInfo GetWidths(List<LayoutElementInfo> elems)
        {
            var sizes = new Size[elems.Count];

            for (var i = 0; i < elems.Count; i++)
            {
                sizes[i] = new Size
                {
                    min = elems[i].MinWidth,
                    preferred = elems[i].PreferredWidth,
                    flexible = elems[i].FlexibleWidth,
                };
            }

            return GetSizesInfo(sizes);
        }

        /// <summary>
        /// Gets the heights sizes info.
        /// </summary>
        /// <returns>The heights sizes info.</returns>
        /// <param name="elems">Elements.</param>
        public static SizeInfo GetHeights(List<LayoutElementInfo> elems)
        {
            var sizes = new Size[elems.Count];

            for (var i = 0; i < elems.Count; i++)
            {
                sizes[i] = new Size
                {
                    min = elems[i].MinHeight,
                    preferred = elems[i].PreferredHeight,
                    flexible = elems[i].FlexibleHeight,
                };
            }

            return GetSizesInfo(sizes);
        }

        /// <summary>
        /// Gets the widths sizes info.
        /// </summary>
        /// <returns>The widths sizes info.</returns>
        /// <param name="group">Elements group.</param>
        public static SizeInfo GetWidths(LayoutElementsGroup group)
        {
            var sizes = new Size[group.Columns];

            for (var column = 0; column < group.Columns; column++)
            {
                sizes[column] = Size.MaxWidths(group.GetColumn(column));
            }

            return GetSizesInfo(sizes);
        }

        /// <summary>
        /// Gets the heights sizes info.
        /// </summary>
        /// <returns>The heights sizes info.</returns>
        /// <param name="group">Elements group.</param>
        public static SizeInfo GetHeights(LayoutElementsGroup group)
        {
            var sizes = new Size[group.Rows];

            for (var row = 0; row < group.Rows; row++)
            {
                sizes[row] = Size.MaxHeights(group.GetRow(row));
            }

            return GetSizesInfo(sizes);
        }
    }
}