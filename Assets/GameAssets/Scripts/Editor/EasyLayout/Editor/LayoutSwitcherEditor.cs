﻿using System;
using System.Collections.Generic;
using EasyLayoutNS;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout.Editor
{
    /// <summary>
    /// LayoutSwitcher editor.
    /// </summary>
    [CustomEditor(typeof(LayoutSwitcher), true)]
    public sealed class LayoutSwitcherEditor : UnityEditor.Editor
    {
        private readonly Dictionary<string, SerializedProperty> _serializedProperties =
            new Dictionary<string, SerializedProperty>();

        private readonly string[] _properties =
        {
            "Objects",
            "Layouts",
            "DefaultDisplaySize",
            "LayoutChanged",
        };

        /// <summary>
        /// Init.
        /// </summary>
        private void OnEnable()
        {
            Array.ForEach(_properties, x =>
            {
                var p = serializedObject.FindProperty(x);
                _serializedProperties.Add(x, p);
            });
        }

        /// <summary>
        /// Draw inspector GUI.
        /// </summary>
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_serializedProperties["Objects"], true);

            DisplayLayouts();

            EditorGUILayout.PropertyField(_serializedProperties["DefaultDisplaySize"], true);
            EditorGUILayout.PropertyField(_serializedProperties["LayoutChanged"], true);

            serializedObject.ApplyModifiedProperties();
        }

        private void DisplayLayouts()
        {
            var switcher = target as LayoutSwitcher;
            var layouts = _serializedProperties["Layouts"];

            layouts.isExpanded = EditorGUILayout.Foldout(layouts.isExpanded, new GUIContent("Layouts"));

            if (!layouts.isExpanded)
            {
                return;
            }

            EditorGUI.indentLevel++;

            layouts.arraySize = EditorGUILayout.IntField("Size", layouts.arraySize);

            for (var i = 0; i < layouts.arraySize; i++)
            {
                var property = layouts.GetArrayElementAtIndex(i);

                EditorGUILayout.PropertyField(property, true);

                if (GUILayout.Button("Save") && switcher != null)
                {
                    switcher.SaveLayout(switcher.layouts[i]);
                }
            }

            EditorGUI.indentLevel--;
        }
    }
}