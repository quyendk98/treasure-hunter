﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout.Editor
{
    /// <summary>
    /// Conditional property drawer.
    /// </summary>
    public abstract class ConditionalPropertyDrawer : PropertyDrawer
    {
        /// <summary>
        /// The indent.
        /// </summary>
        private const float INDENT = 16;

        /// <summary>
        /// The height.
        /// </summary>
        private const float EMPTY_SPACE = 2;

        /// <summary>
        /// The indent level.
        /// </summary>
        private int _indentLevel;

        /// <summary>
        /// Fields to display.
        /// </summary>
        protected List<ConditionalFieldInfo> fields;

        /// <summary>
        /// Init this instance.
        /// </summary>
        protected abstract void Init();

        /// <summary>
        /// Check is field can be displayed.
        /// </summary>
        /// <param name="info">Field info.</param>
        /// <param name="property">Property data.</param>
        /// <returns>true if field can be displayed; otherwise false.</returns>
        private static bool CanShow(ConditionalFieldInfo info, SerializedProperty property)
        {
            var p = property.FindPropertyRelative(info.name);

            if (p != null)
            {
                return !(from condition in info.conditions
                    let field = property.FindPropertyRelative(condition.Key)
                    where !condition.Value(field)
                    select condition).Any();
            }

            Debug.LogWarning($"Field with name '{info.name}' not found");
            return false;
        }

        /// <summary>
        /// Draw GUI.
        /// </summary>
        /// <param name="position">Start position.</param>
        /// <param name="property">Property data.</param>
        /// <param name="label">Label.</param>
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Init();

            EditorGUI.BeginProperty(position, label, property);

            foreach (var field in fields.Where(field => CanShow(field, property)))
            {
                _indentLevel += field.indent;

                position = DrawProperty(position, property.FindPropertyRelative(field.name));

                _indentLevel -= field.indent;
            }

            EditorGUI.EndProperty();
        }

        /// <summary>
        /// Get GUI height for the specified property.
        /// </summary>
        /// <param name="property">Property data.</param>
        /// <param name="label">Label.</param>
        /// <returns>GUI height.</returns>
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            Init();
            return fields.Where(field => CanShow(field, property)).Sum(field =>
                EditorGUI.GetPropertyHeight(property.FindPropertyRelative(field.name)) + EMPTY_SPACE);
        }

        /// <summary>
        /// Draws the property.
        /// </summary>
        /// <returns>The new position.</returns>
        /// <param name="position">Position.</param>
        /// <param name="field">Field.</param>
        private Rect DrawProperty(Rect position, SerializedProperty field)
        {
            var height = EditorGUI.GetPropertyHeight(field);
            var indent = INDENT * _indentLevel;
            var rect = new Rect(position.x + indent, position.y, position.width - indent, height);

            EditorGUI.PropertyField(rect, field);

            position.y += rect.height + EMPTY_SPACE;
            return position;
        }
    }
}