﻿using System;
using System.Collections.Generic;
using EasyLayoutNS;
using UnityEditor;

namespace GameAssets.Scripts.Editor.EasyLayout.Editor
{
    /// <summary>
    /// Property drawer for the EasyLayoutEllipseSettings.
    /// </summary>
    [CustomPropertyDrawer(typeof(EasyLayoutEllipseSettings))]
    public class EasyLayoutEllipseSettingsPropertyDrawer : ConditionalPropertyDrawer
    {
        /// <summary>
        /// Init this instance.
        /// </summary>
        protected override void Init()
        {
            if (fields != null)
            {
                return;
            }

            var isNotWidthAuto = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"widthAuto", x => !x.boolValue},
            };
            var isNotHeightAuto = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"heightAuto", x => !x.boolValue},
            };
            var isAngleAuto = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"angleStepAuto", x => x.boolValue},
            };
            var isNotAngleAuto = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"angleStepAuto", x => !x.boolValue},
            };
            var isArc = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"angleStepAuto", x => x.boolValue},
                {"fill", x => (EllipseFill) x.enumValueIndex == EllipseFill.Arc},
            };
            var isRotate = new Dictionary<string, Func<SerializedProperty, bool>>
            {
                {"elementsRotate", x => x.boolValue},
            };

            fields = new List<ConditionalFieldInfo>
            {
                new ConditionalFieldInfo("widthAuto"),
                new ConditionalFieldInfo("width", 1, isNotWidthAuto),
                new ConditionalFieldInfo("heightAuto"),
                new ConditionalFieldInfo("height", 1, isNotHeightAuto),
                new ConditionalFieldInfo("angleStart"),
                new ConditionalFieldInfo("angleStepAuto"),
                new ConditionalFieldInfo("angleStep", 1, isNotAngleAuto),
                new ConditionalFieldInfo("fill", 1, isAngleAuto),
                new ConditionalFieldInfo("arcLength", 2, isArc),
                new ConditionalFieldInfo("align"),
                new ConditionalFieldInfo("elementsRotate"),
                new ConditionalFieldInfo("elementsRotationStart", 1, isRotate),
            };
        }
    }
}