﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace GameAssets.Scripts.Editor.EasyLayout.Editor
{
    /// <summary>
    /// Field info for conditional display.
    /// </summary>
    public class ConditionalFieldInfo
    {
        /// <summary>
        /// Indent level.
        /// </summary>
        public readonly int indent;

        /// <summary>
        /// Field name.
        /// </summary>
        public readonly string name;

        /// <summary>
        /// Conditions to display field.
        /// </summary>
        public readonly Dictionary<string, Func<SerializedProperty, bool>> conditions =
            new Dictionary<string, Func<SerializedProperty, bool>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ConditionalFieldInfo"/> class.
        /// </summary>
        /// <param name="fieldName">Field name.</param>
        /// <param name="indent">Indent level.</param>
        /// <param name="conditions">Conditions to display field.</param>
        public ConditionalFieldInfo(string fieldName, int indent = 0,
            Dictionary<string, Func<SerializedProperty, bool>> conditions = null)
        {
            name = fieldName;
            this.indent = indent;

            if (conditions != null)
            {
                this.conditions = conditions;
            }
        }
    }
}