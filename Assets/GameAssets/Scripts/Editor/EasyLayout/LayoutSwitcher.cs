﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Layout switcher.
    /// </summary>
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [AddComponentMenu("UI/Easy Layout/Layout Switcher")]
    public class LayoutSwitcher : MonoBehaviour
    {
        /// <summary>
        /// The tracked objects.
        /// </summary>
        [SerializeField] protected List<RectTransform> objects = new List<RectTransform>();

        /// <summary>
        /// The layouts.
        /// </summary>
        [SerializeField] public List<UILayout> layouts = new List<UILayout>();

        /// <summary>
        /// Layout changed event.
        /// </summary>
        [SerializeField] public LayoutSwitcherEvent layoutChanged = new LayoutSwitcherEvent();

        /// <summary>
        /// The default display size.
        /// </summary>
        [SerializeField] [Tooltip("Display size used when actual display size cannot be detected.")]
        public float defaultDisplaySize;

        /// <summary>
        /// Window width.
        /// </summary>
        protected int windowWidth;

        /// <summary>
        /// Window height.
        /// </summary>
        protected int windowHeight;

        /// <summary>
        /// Function to select layout.
        /// </summary>
        protected Func<List<UILayout>, float, float, UILayout> layoutSelector =
            DefaultLayoutSelector;

        /// <summary>
        /// Function to select layout.
        /// </summary>
        public virtual Func<List<UILayout>, float, float, UILayout> LayoutSelector
        {
            get => layoutSelector;

            set
            {
                layoutSelector = value;
                ResolutionChanged();
            }
        }

        /// <summary>
        /// Update this instance.
        /// </summary>
        protected virtual void Update()
        {
            if (windowWidth == Screen.width && windowHeight == Screen.height)
            {
                return;
            }

            windowWidth = Screen.width;
            windowHeight = Screen.height;

            ResolutionChanged();
        }

        /// <summary>
        /// Saves the layout.
        /// </summary>
        /// <param name="layout">Layout.</param>
        public virtual void SaveLayout(UILayout layout)
        {
            layout.Save(objects);
        }

        /// <summary>
        /// Load layout when resolution changed.
        /// </summary>
        public virtual void ResolutionChanged()
        {
            var currentLayout = GetCurrentLayout();

            if (currentLayout == null)
            {
                return;
            }

            currentLayout.Load();
            layoutChanged.Invoke(currentLayout);
        }

        /// <summary>
        /// Gets the current layout.
        /// </summary>
        /// <returns>The current layout.</returns>
        public virtual UILayout GetCurrentLayout()
        {
            return layouts.Count == 0
                ? null
                : LayoutSelector(layouts, DisplaySize(), AspectRatio());
        }

        /// <summary>
        /// Default layout select.
        /// </summary>
        /// <param name="layouts">Available layouts.</param>
        /// <param name="displaySize">Display size in inches.</param>
        /// <param name="aspectRatio">Aspect ratio.</param>
        /// <returns>Layout to use.</returns>
        public static UILayout DefaultLayoutSelector(List<UILayout> layouts, float displaySize,
            float aspectRatio)
        {
            var filtered = FilterLayouts(layouts, displaySize, aspectRatio);

            if (filtered.Count == 0)
            {
                return null;
            }

            foreach (var x in filtered.Where(x => displaySize < x.maxDisplaySize))
            {
                return x;
            }

            return filtered[0];
        }

        /// <summary>
        /// Filter layouts by aspect ratio and display size.
        /// </summary>
        /// <param name="layouts">Layouts.</param>
        /// <param name="displaySize">Display size.</param>
        /// <param name="aspectRatio">Aspect ratio.</param>
        /// <returns>Filtered layouts.</returns>
        protected static List<UILayout> FilterLayouts(IEnumerable<UILayout> layouts, float displaySize,
            float aspectRatio)
        {
            var layoutsAR = (from layout in layouts
                let diff = Mathf.Abs(aspectRatio - (layout.aspectRatio.x / layout.aspectRatio.y))
                where diff < 0.05f
                select layout).ToList();

            layoutsAR.Sort(new DisplaySizeComparer(displaySize).Compare);
            return layoutsAR;
        }

        /// <summary>
        /// Display size comparer.
        /// </summary>
        protected readonly struct DisplaySizeComparer : IEquatable<DisplaySizeComparer>
        {
            private readonly float _displaySize;

            /// <summary>
            /// Initializes a new instance of the <see cref="DisplaySizeComparer"/> struct.
            /// </summary>
            /// <param name="displaySize">Display size.</param>
            public DisplaySizeComparer(float displaySize)
            {
                _displaySize = displaySize;
            }

            /// <summary>
            /// Compare layouts by display size.
            /// </summary>
            /// <param name="x">First layout.</param>
            /// <param name="y">Second layout.</param>
            /// <returns>Result of the comparison.</returns>
            public int Compare(UILayout x, UILayout y)
            {
                var xDS = Mathf.Abs(x.maxDisplaySize - _displaySize);
                var yDS = Mathf.Abs(y.maxDisplaySize - _displaySize);

                return xDS.CompareTo(yDS);
            }

            /// <summary>
            /// Determines whether the specified object is equal to the current object.
            /// </summary>
            /// <param name="obj">The object to compare with the current object.</param>
            /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
            public override bool Equals(object obj)
            {
                return obj is DisplaySizeComparer dsc && Equals(dsc);
            }

            /// <summary>
            /// Determines whether the specified object is equal to the current object.
            /// </summary>
            /// <param name="other">The object to compare with the current object.</param>
            /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
            public bool Equals(DisplaySizeComparer other)
            {
                return _displaySize.Equals(other._displaySize);
            }

            /// <summary>
            /// Hash function.
            /// </summary>
            /// <returns>A hash code for the current object.</returns>
            public override int GetHashCode()
            {
                return _displaySize.GetHashCode();
            }

            /// <summary>
            /// Compare specified instances.
            /// </summary>
            /// <param name="left">Left instance.</param>
            /// <param name="right">Right instances.</param>
            /// <returns>true if the instances are equal; otherwise, false.</returns>
            public static bool operator ==(DisplaySizeComparer left, DisplaySizeComparer right)
            {
                return left.Equals(right);
            }

            /// <summary>
            /// Compare specified instances.
            /// </summary>
            /// <param name="left">Left instance.</param>
            /// <param name="right">Right instances.</param>
            /// <returns>true if the instances are now equal; otherwise, false.</returns>
            public static bool operator !=(DisplaySizeComparer left, DisplaySizeComparer right)
            {
                return !left.Equals(right);
            }
        }

        /// <summary>
        /// Current aspect ratio.
        /// </summary>
        /// <returns>The ratio.</returns>
        public virtual float AspectRatio()
        {
            return windowWidth / (float) windowHeight;
        }

        /// <summary>
        /// Current display size.
        /// </summary>
        /// <returns>The size.</returns>
        public virtual float DisplaySize()
        {
            return Screen.dpi == 0
                ? defaultDisplaySize
                : Mathf.Sqrt(windowWidth ^ 2 + windowHeight ^ 2) / Screen.dpi;
        }
    }
}