﻿// ReSharper disable All

using GameAssets.Scripts.Editor.EasyLayout;

namespace EasyLayoutNS
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Base class for the flex and staggered layout groups.
    /// </summary>
    public abstract class EasyLayoutFlexOrStaggered : EasyLayoutBaseType
    {
        /// <summary>
        /// Axis data.
        /// </summary>
        protected struct AxisData : IEquatable<AxisData>
        {
            /// <summary>
            /// Offset.
            /// </summary>
            public float offset;

            /// <summary>
            /// Spacing.
            /// </summary>
            public float spacing;

            /// <summary>
            /// Serves as a hash function for a object.
            /// </summary>
            /// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
            public override int GetHashCode()
            {
                return Mathf.RoundToInt(offset) ^ Mathf.RoundToInt(spacing);
            }

            /// <summary>
            /// Determines whether the specified System.Object is equal to the current axis data.
            /// </summary>
            /// <param name="obj">The System.Object to compare with the current axis data.</param>
            /// <returns><c>true</c> if the specified System.Object is equal to the current axis data; otherwise, <c>false</c>.</returns>
            public override bool Equals(object obj)
            {
                return obj is AxisData && Equals((AxisData) obj);
            }

            /// <summary>
            /// Determines whether the specified axis data is equal to the current axis data.
            /// </summary>
            /// <param name="other">The axis data to compare with the current axis data.</param>
            /// <returns><c>true</c> if the specified axis data is equal to the current axis data; otherwise, <c>false</c>.</returns>
            public bool Equals(AxisData other)
            {
                return offset == other.offset && spacing == other.spacing;
            }

            /// <summary>
            /// Compare axis data.
            /// </summary>
            /// <param name="obj1">First axis data.</param>
            /// <param name="obj2">Second axis data.</param>
            /// <returns>True if data are equals; otherwise false.</returns>
            public static bool operator ==(AxisData obj1, AxisData obj2)
            {
                return obj1.Equals(obj2);
            }

            /// <summary>
            /// Compare axis data.
            /// </summary>
            /// <param name="obj1">First axis data.</param>
            /// <param name="obj2">Seconds axis data.</param>
            /// <returns>True if data are not equals; otherwise false.</returns>
            public static bool operator !=(AxisData obj1, AxisData obj2)
            {
                return !obj1.Equals(obj2);
            }
        }

        /// <summary>
        /// Sizes of blocks at the sub axis.
        /// </summary>
        protected List<float> subAxisSizes = new List<float>();

        /// <summary>
        /// Initializes a new instance of the <see cref="EasyLayoutFlexOrStaggered"/> class.
        /// </summary>
        /// <param name="layout">Layout.</param>
        protected EasyLayoutFlexOrStaggered(EasyLayout layout)
            : base(layout)
        {
        }

        /// <summary>
        /// Calculate sizes of the elements.
        /// </summary>
        protected override void CalculateSizes()
        {
            if (layout.IsHorizontal)
            {
                switch (layout.ChildrenWidth)
                {
                    case ChildrenSize.FitContainer:
                        ResizeWidthToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeWidthToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkWidthToFit();
                        break;
                }

                switch (layout.ChildrenHeight)
                {
                    case ChildrenSize.FitContainer:
                        ResizeRowHeightToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeRowHeightToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkRowHeightToFit();
                        break;
                }
            }
            else
            {
                switch (layout.ChildrenWidth)
                {
                    case ChildrenSize.FitContainer:
                        ResizeColumnWidthToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeColumnWidthToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkColumnWidthToFit();
                        break;
                }

                switch (layout.ChildrenHeight)
                {
                    case ChildrenSize.FitContainer:
                        ResizeHeightToFit(false);
                        break;

                    case ChildrenSize.SetPreferredAndFitContainer:
                        ResizeHeightToFit(true);
                        break;

                    case ChildrenSize.ShrinkOnOverflow:
                        ShrinkHeightToFit();
                        break;
                }
            }
        }

        private void ResizeColumnWidthToFit(bool increaseOnly)
        {
            ResizeToFit(layout.InternalSize.x, elementsGroup, layout.Spacing.x,
                RectTransform.Axis.Horizontal, increaseOnly);
        }

        private void ShrinkColumnWidthToFit()
        {
            ShrinkToFit(layout.InternalSize.x, elementsGroup, layout.Spacing.x,
                RectTransform.Axis.Horizontal);
        }

        private void ResizeHeightToFit(bool increaseOnly)
        {
            var height = layout.InternalSize.y;

            for (var column = 0; column < elementsGroup.Columns; column++)
            {
                ResizeToFit(height, elementsGroup.GetColumn(column), layout.Spacing.y,
                    RectTransform.Axis.Vertical, increaseOnly);
            }
        }

        private void ShrinkHeightToFit()
        {
            var height = layout.InternalSize.y;

            for (var column = 0; column < elementsGroup.Columns; column++)
            {
                ShrinkToFit(height, elementsGroup.GetColumn(column), layout.Spacing.y,
                    RectTransform.Axis.Vertical);
            }
        }

        /// <summary>
        /// Convert input to match direction.
        /// </summary>
        /// <param name="input">Input.</param>
        /// <returns>Converted input.</returns>
        protected Vector2 ByAxis(Vector2 input)
        {
            return layout.IsHorizontal ? input : new Vector2(input.y, input.x);
        }

        /// <summary>
        /// Calculate group size.
        /// </summary>
        /// <returns>Size.</returns>
        protected override Vector2 CalculateGroupSize()
        {
            var spacing = ByAxis(layout.Spacing);
            var padding = ByAxis(new Vector2(layout.PaddingInner.Horizontal,
                layout.PaddingInner.Vertical));

            return CalculateGroupSize(layout.IsHorizontal, spacing, padding);
        }

        /// <summary>
        /// Calculate positions of the elements.
        /// </summary>
        /// <param name="size">Size.</param>
        protected override void CalculatePositions(Vector2 size)
        {
            var subAxis = SubAxisData();
            var axisDirection = layout.IsHorizontal ? -1f : 1f;

            var subPosition = subAxis.offset * axisDirection;
            var count = layout.IsHorizontal ? elementsGroup.Rows : elementsGroup.Columns;

            for (var i = 0; i < count; i++)
            {
                CalculatePositions(i, subPosition, subAxisSizes[i]);

                subPosition += (subAxisSizes[i] + subAxis.spacing) * axisDirection;
            }
        }

        /// <summary>
        /// Calculate sizes of the blocks at sub axis.
        /// </summary>
        protected void CalculateSubAxisSizes()
        {
            subAxisSizes.Clear();

            var count = layout.IsHorizontal ? elementsGroup.Rows : elementsGroup.Columns;

            for (var i = 0; i < count; i++)
            {
                var block = layout.IsHorizontal
                    ? elementsGroup.GetRow(i)
                    : elementsGroup.GetColumn(i);
                var blockSize = 0f;

                foreach (var element in block)
                {
                    blockSize = Mathf.Max(blockSize, element.SubAxisSize);
                }

                subAxisSizes.Add(blockSize);
            }
        }

        /// <summary>
        /// Get axis data.
        /// </summary>
        /// <param name="isMainAxis">Is main axis?</param>
        /// <param name="elements">Elements count.</param>
        /// <param name="size">Total size of the elements.</param>
        /// <returns>Axis data</returns>
        protected virtual AxisData GetAxisData(bool isMainAxis, int elements, float size)
        {
            var axis = new AxisData()
            {
                offset = BaseOffset(isMainAxis),
                spacing = isMainAxis ? ByAxis(layout.Spacing).x : ByAxis(layout.Spacing).y,
            };

            return axis;
        }

        /// <summary>
        /// Calculate base offset.
        /// </summary>
        /// <param name="isMainAxis">Is main axis?</param>
        /// <returns>Base offset.</returns>
        protected float BaseOffset(bool isMainAxis)
        {
            var rectTransform = layout.transform as RectTransform;

            return layout.IsHorizontal == isMainAxis
                ? rectTransform.rect.width * -rectTransform.pivot.x + layout.GetMarginLeft()
                : rectTransform.rect.height * (rectTransform.pivot.y - 1f) + layout.GetMarginTop();
        }

        /// <summary>
        /// Get sub axis data.
        /// </summary>
        /// <returns>Sub axis data.</returns>
        protected virtual AxisData SubAxisData()
        {
            CalculateSubAxisSizes();
            return GetAxisData(false, subAxisSizes.Count, Sum(subAxisSizes));
        }

        /// <summary>
        /// Get main axis data for the block with the specified index.
        /// </summary>
        /// <param name="blockIndex">Block index.</param>
        /// <returns>Main axis data.</returns>
        protected virtual AxisData MainAxisData(int blockIndex)
        {
            var block = layout.IsHorizontal
                ? elementsGroup.GetRow(blockIndex)
                : elementsGroup.GetColumn(blockIndex);
            var size = 0f;

            foreach (var element in block)
            {
                size += element.AxisSize;
            }

            return GetAxisData(true, block.Count, size);
        }

        /// <summary>
        /// Calculate positions.
        /// </summary>
        /// <param name="blockIndex">Index of the block to calculate positions.</param>
        /// <param name="subAxisOffset">Offset on the sub axis.</param>
        /// <param name="maxSubSize">Maximum size of the sub axis.</param>
        protected void CalculatePositions(int blockIndex, float subAxisOffset, float maxSubSize)
        {
            var block = layout.IsHorizontal
                ? elementsGroup.GetRow(blockIndex)
                : elementsGroup.GetColumn(blockIndex);

            var axisDirection = layout.IsHorizontal ? 1f : -1f;
            var axis = MainAxisData(blockIndex);

            var position = new Vector2(axis.offset, subAxisOffset);
            var subOffsetRate = GetItemsAlignRate();

            foreach (var element in block)
            {
                var subAxis = subAxisOffset - (maxSubSize - element.SubAxisSize) * subOffsetRate * axisDirection;

                element.PositionTopLeft = ByAxis(new Vector2(position.x * axisDirection, subAxis));
                position.x += element.AxisSize + axis.spacing;
            }
        }

        /// <summary>
        /// Get align rate for the items.
        /// </summary>
        /// <returns>Align rate.</returns>
        protected virtual float GetItemsAlignRate()
        {
            return 0f;
        }
    }
}