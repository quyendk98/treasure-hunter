﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// EasyLayout.
    /// Warning: using RectTransform relative size with positive size delta (like 100% + 10) with ContentSizeFitter can lead to infinite increased size.
    /// </summary>
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    [AddComponentMenu("UI/Easy Layout/Easy Layout")]
    public class EasyLayout : LayoutGroup, INotifyPropertyChanged
    {
        private readonly List<LayoutElementInfo> _elements = new List<LayoutElementInfo>();

        private readonly Stack<LayoutElementInfo> _elementsCache = new Stack<LayoutElementInfo>();

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = DefaultPropertyHandler;

        /// <summary>
        /// Occurs when a properties values changed except PaddingInner property.
        /// </summary>
        [SerializeField] public UnityEvent settingsChanged = new UnityEvent();

        [SerializeField] private Anchors groupPosition = Anchors.UpperLeft;

        /// <summary>
        /// The group position.
        /// </summary>
        public Anchors GroupPosition
        {
            get => groupPosition;

            set
            {
                if (groupPosition == value)
                {
                    return;
                }

                groupPosition = value;

                Changed("GroupPosition");
            }
        }

        [SerializeField] private Axis mainAxis = Axis.Horizontal;

        /// <summary>
        /// The stacking type.
        /// </summary>
        [Obsolete("Replaced with MainAxis.")]
        public Stacking Stacking
        {
            get => MainAxis == Axis.Horizontal ? Stacking.Horizontal : Stacking.Vertical;
            set => MainAxis = value == Stacking.Horizontal ? Axis.Horizontal : Axis.Vertical;
        }

        /// <summary>
        /// Main axis.
        /// </summary>
        private Axis MainAxis
        {
            get => mainAxis;

            set
            {
                if (mainAxis == value)
                {
                    return;
                }

                mainAxis = value;

                Changed("Axis");
            }
        }

        [SerializeField] private LayoutTypes layoutType = LayoutTypes.Compact;

        /// <summary>
        /// The type of the layout.
        /// </summary>
        public LayoutTypes LayoutType
        {
            get => layoutType;

            set
            {
                if (layoutType == value)
                {
                    return;
                }

                _layoutGroup = null;
                layoutType = value;
                Changed("LayoutType");
            }
        }

        private EasyLayoutBaseType _layoutGroup;

        /// <summary>
        /// Layout group.
        /// </summary>
        private EasyLayoutBaseType LayoutGroup => _layoutGroup ??= GetLayoutGroup();

        [SerializeField] private CompactConstraint compactConstraint = Scripts.Editor.EasyLayout.CompactConstraint.Flexible;

        /// <summary>
        /// Which constraint to use for the Grid layout.
        /// </summary>
        public CompactConstraint CompactConstraint
        {
            get => compactConstraint;

            set
            {
                if (compactConstraint == value)
                {
                    return;
                }

                compactConstraint = value;

                Changed("CompactConstraint");
            }
        }

        [SerializeField] private int compactConstraintCount = 1;

        /// <summary>
        /// How many elements there should be along the constrained axis.
        /// </summary>
        public int CompactConstraintCount
        {
            get => Mathf.Max(1, compactConstraintCount);

            set
            {
                if (compactConstraintCount == value)
                {
                    return;
                }

                compactConstraintCount = value;

                Changed("CompactConstraintCount");
            }
        }

        [SerializeField] private GridConstraint gridConstraint = Scripts.Editor.EasyLayout.GridConstraint.Flexible;

        /// <summary>
        /// Which constraint to use for the Grid layout.
        /// </summary>
        public GridConstraint GridConstraint
        {
            get => gridConstraint;

            set
            {
                if (gridConstraint == value)
                {
                    return;
                }

                gridConstraint = value;

                Changed("GridConstraint");
            }
        }

        [SerializeField] private int gridConstraintCount = 1;

        /// <summary>
        /// How many cells there should be along the constrained axis.
        /// </summary>
        public int GridConstraintCount
        {
            get => Mathf.Max(1, gridConstraintCount);

            set
            {
                if (gridConstraintCount == value)
                {
                    return;
                }

                gridConstraintCount = value;

                Changed("GridConstraintCount");
            }
        }

        /// <summary>
        /// Constraint count.
        /// </summary>
        public int ConstraintCount => LayoutType == LayoutTypes.Compact
            ? CompactConstraintCount
            : GridConstraintCount;

        [SerializeField] private HorizontalAligns rowAlign = HorizontalAligns.Left;

        /// <summary>
        /// The row align.
        /// </summary>
        public HorizontalAligns RowAlign
        {
            get => rowAlign;

            set
            {
                if (rowAlign == value)
                {
                    return;
                }

                rowAlign = value;

                Changed("RowAlign");
            }
        }

        [SerializeField] private InnerAligns innerAlign = InnerAligns.Top;

        /// <summary>
        /// The inner align.
        /// </summary>
        public InnerAligns InnerAlign
        {
            get => innerAlign;

            set
            {
                if (innerAlign == value)
                {
                    return;
                }

                innerAlign = value;

                Changed("InnerAlign");
            }
        }

        [SerializeField] private Anchors cellAlign = Anchors.UpperLeft;

        /// <summary>
        /// The cell align.
        /// </summary>
        public Anchors CellAlign
        {
            get => cellAlign;

            set
            {
                if (cellAlign == value)
                {
                    return;
                }

                cellAlign = value;

                Changed("CellAlign");
            }
        }

        [SerializeField] private Vector2 spacing = new Vector2(5, 5);

        /// <summary>
        /// The spacing.
        /// </summary>
        public Vector2 Spacing
        {
            get => spacing;

            set
            {
                if (spacing == value)
                {
                    return;
                }

                spacing = value;

                Changed("Spacing");
            }
        }

        [SerializeField] private bool symmetric = true;

        /// <summary>
        /// Symmetric margin.
        /// </summary>
        public bool Symmetric
        {
            get => symmetric;

            set
            {
                if (symmetric == value)
                {
                    return;
                }

                symmetric = value;

                Changed("Symmetric");
            }
        }

        [SerializeField] private Vector2 margin = new Vector2(5, 5);

        /// <summary>
        /// The margin.
        /// </summary>
        public Vector2 Margin
        {
            get => margin;

            set
            {
                if (margin == value)
                {
                    return;
                }

                margin = value;

                Changed("Margin");
            }
        }

        [SerializeField] [HideInInspector] private Padding marginInner;

        /// <summary>
        /// The margin.
        /// Should be used by ListView related scripts.
        /// </summary>
        public Padding MarginInner
        {
            get => marginInner;

            set
            {
                if (marginInner == value)
                {
                    return;
                }

                marginInner = value;

                Changed("MarginInner");
            }
        }

        [SerializeField] [HideInInspector] private Padding paddingInner;

        /// <summary>
        /// The padding.
        /// Should be used by ListView related scripts.
        /// </summary>
        public Padding PaddingInner
        {
            get => paddingInner;

            set
            {
                if (paddingInner == value)
                {
                    return;
                }

                paddingInner = value;

                Changed("PaddingInner", false);
            }
        }

        [SerializeField] private float marginTop = 5f;

        /// <summary>
        /// The margin top.
        /// </summary>
        public float MarginTop
        {
            get => marginTop;

            set
            {
                if (marginTop.Equals(value))
                {
                    return;
                }

                marginTop = value;

                Changed("MarginTop");
            }
        }

        [SerializeField] private float marginBottom = 5f;

        /// <summary>
        /// The margin bottom.
        /// </summary>
        public float MarginBottom
        {
            get => marginBottom;

            set
            {
                if (marginBottom.Equals(value))
                {
                    return;
                }

                marginBottom = value;

                Changed("MarginBottom");
            }
        }

        [SerializeField] private float marginLeft = 5f;

        /// <summary>
        /// The margin left.
        /// </summary>
        public float MarginLeft
        {
            get => marginLeft;

            set
            {
                if (marginLeft.Equals(value))
                {
                    return;
                }

                marginLeft = value;

                Changed("MarginLeft");
            }
        }

        [SerializeField] private float marginRight = 5f;

        /// <summary>
        /// The margin right.
        /// </summary>
        public float MarginRight
        {
            get => marginRight;

            set
            {
                if (marginRight.Equals(value))
                {
                    return;
                }

                marginRight = value;

                Changed("MarginRight");
            }
        }

        [SerializeField] private bool rightToLeft;

        /// <summary>
        /// The right to left stacking.
        /// </summary>
        public bool RightToLeft
        {
            get => rightToLeft;

            set
            {
                if (rightToLeft == value)
                {
                    return;
                }

                rightToLeft = value;

                Changed("RightToLeft");
            }
        }

        [SerializeField] private bool topToBottom = true;

        /// <summary>
        /// The top to bottom stacking.
        /// </summary>
        public bool TopToBottom
        {
            get => topToBottom;

            set
            {
                if (topToBottom == value)
                {
                    return;
                }

                topToBottom = value;

                Changed("TopToBottom");
            }
        }

        [SerializeField] private bool skipInactive = true;

        /// <summary>
        /// The skip inactive.
        /// </summary>
        public bool SkipInactive
        {
            get => skipInactive;

            set
            {
                if (skipInactive == value)
                {
                    return;
                }

                skipInactive = value;

                Changed("SkipInactive");
            }
        }

        [SerializeField] private bool resetRotation;

        /// <summary>
        /// Reset rotation for the controlled elements.
        /// </summary>
        public bool ResetRotation
        {
            get => resetRotation;

            set
            {
                if (resetRotation == value)
                {
                    return;
                }

                resetRotation = value;

                Changed("ResetRotation");
            }
        }

        private Func<IEnumerable<GameObject>, IEnumerable<GameObject>> _filter;

        /// <summary>
        /// The filter.
        /// </summary>
        public Func<IEnumerable<GameObject>, IEnumerable<GameObject>> Filter
        {
            get => _filter;

            set
            {
                if (_filter == value)
                {
                    return;
                }

                _filter = value;

                Changed("Filter");
            }
        }

        [SerializeField] private ChildrenSize childrenWidth;

        /// <summary>
        /// How to control width of the children.
        /// </summary>
        public ChildrenSize ChildrenWidth
        {
            get => childrenWidth;

            set
            {
                if (childrenWidth == value)
                {
                    return;
                }

                childrenWidth = value;

                Changed("ChildrenWidth");
            }
        }

        [SerializeField] private ChildrenSize childrenHeight;

        /// <summary>
        /// How to control height of the children.
        /// </summary>
        public ChildrenSize ChildrenHeight
        {
            get => childrenHeight;

            private set
            {
                if (childrenHeight == value)
                {
                    return;
                }

                childrenHeight = value;

                Changed("ChildrenHeight");
            }
        }

        [SerializeField] private EasyLayoutFlexSettings flexSettings = new EasyLayoutFlexSettings();

        /// <summary>
        /// Settings for the Flex layout type.
        /// </summary>
        public EasyLayoutFlexSettings FlexSettings
        {
            get => flexSettings;

            set
            {
                if (flexSettings == value)
                {
                    return;
                }

                flexSettings.PropertyChanged -= FlexSettingsChanged;

                flexSettings = value;

                flexSettings.PropertyChanged += FlexSettingsChanged;

                Changed("FlexSettings");
            }
        }

        [SerializeField] private EasyLayoutStaggeredSettings staggeredSettings = new EasyLayoutStaggeredSettings();

        /// <summary>
        /// Settings for the Staggered layout type.
        /// </summary>
        public EasyLayoutStaggeredSettings StaggeredSettings
        {
            get => staggeredSettings;

            set
            {
                if (staggeredSettings == value)
                {
                    return;
                }

                staggeredSettings.PropertyChanged -= StaggeredSettingsChanged;

                staggeredSettings = value;

                staggeredSettings.PropertyChanged += StaggeredSettingsChanged;

                Changed("StaggeredSettings");
            }
        }

        [SerializeField] private EasyLayoutEllipseSettings ellipseSettings = new EasyLayoutEllipseSettings();

        /// <summary>
        /// Settings for the Ellipse layout type.
        /// </summary>
        public EasyLayoutEllipseSettings EllipseSettings
        {
            get => ellipseSettings;

            set
            {
                if (ellipseSettings == value)
                {
                    return;
                }

                ellipseSettings.PropertyChanged -= EllipseSettingsChanged;

                ellipseSettings = value;

                ellipseSettings.PropertyChanged += EllipseSettingsChanged;

                Changed("EllipseSettings");
            }
        }

        /// <summary>
        /// Control width of children.
        /// </summary>
        private bool _controlWidth;

        /// <summary>
        /// Control height of children.
        /// </summary>
        private bool _controlHeight;

        /// <summary>
        /// Sets width of the children to maximum width from them.
        /// </summary>
        private bool _maxWidth;

        /// <summary>
        /// Sets height of the children to maximum height from them.
        /// </summary>
        private bool _maxHeight;

        /// <summary>
        /// Internal size.
        /// </summary>
        public Vector2 InternalSize
        {
            get
            {
                var size = rectTransform.rect.size;
                size.x -= MarginFullHorizontal + PaddingInner.Horizontal;
                size.y -= MarginFullVertical + PaddingInner.Vertical;

                return size;
            }
        }

        /// <summary>
        /// Gets or sets the size of the inner block.
        /// </summary>
        /// <value>The size of the inner block.</value>
        public Vector2 BlockSize { get; private set; }

        /// <summary>
        /// Gets or sets the UI size.
        /// </summary>
        /// <value>The UI size.</value>
        public Vector2 UISize { get; private set; }

        /// <summary>
        /// Size in elements.
        /// </summary>
        public Vector2 Size { get; protected set; }

        /// <summary>
        /// Gets the minimum height.
        /// </summary>
        /// <value>The minimum height.</value>
        public override float minHeight => BlockSize[1];

        /// <summary>
        /// Gets the minimum width.
        /// </summary>
        /// <value>The minimum width.</value>
        public override float minWidth => BlockSize[0];

        /// <summary>
        /// Gets the preferred height.
        /// </summary>
        /// <value>The preferred height.</value>
        public override float preferredHeight => BlockSize[1];

        /// <summary>
        /// Gets the preferred width.
        /// </summary>
        /// <value>The preferred width.</value>
        public override float preferredWidth => BlockSize[0];

        /// <summary>
        /// Summary horizontal margin.
        /// </summary>
        public float MarginHorizontal =>
            Symmetric ? Margin.x + Margin.x : MarginLeft + MarginRight;

        /// <summary>
        /// Summary vertical margin.
        /// </summary>
        public float MarginVertical =>
            Symmetric ? Margin.y + Margin.y : MarginTop + MarginBottom;

        /// <summary>
        /// Summary horizontal margin with MarginInner.
        /// </summary>
        public float MarginFullHorizontal
        {
            get
            {
                var marginExternal = Symmetric ? Margin.x + Margin.x : MarginLeft + MarginRight;
                return marginExternal + MarginInner.Horizontal;
            }
        }

        /// <summary>
        /// Summary vertical margin with MarginInner.
        /// </summary>
        public float MarginFullVertical
        {
            get
            {
                var marginExternal = Symmetric ? Margin.y + Margin.y : MarginTop + MarginBottom;
                return marginExternal + MarginInner.Vertical;
            }
        }

        /// <summary>
        /// Is horizontal stacking?
        /// </summary>
        public bool IsHorizontal => MainAxis == Axis.Horizontal;

        /// <summary>
        /// Size of the main axis.
        /// </summary>
        public float MainAxisSize =>
            IsHorizontal
                ? rectTransform.rect.width - MarginFullHorizontal
                : rectTransform.rect.height - MarginFullVertical;

        /// <summary>
        /// Size of the sub axis.
        /// </summary>
        public float SubAxisSize =>
            !IsHorizontal
                ? rectTransform.rect.width - MarginFullHorizontal
                : rectTransform.rect.height - MarginFullVertical;

        /// <summary>
        /// Properties tracker.
        /// </summary>
        protected DrivenRectTransformTracker propertiesTracker;

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        /// <param name="invokeSettingsChanged">Should invoke SettingsChanged event?</param>
        protected void Changed(string propertyName, bool invokeSettingsChanged = true)
        {
            SetDirty();

            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

            if (invokeSettingsChanged)
            {
                settingsChanged.Invoke();
            }
        }

        private void FlexSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            Changed("FlexSettings");
        }

        private void StaggeredSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            Changed("StaggeredSettings");
        }

        private void EllipseSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            Changed("EllipseSettings");
        }

        /// <summary>
        /// Start this instance.
        /// </summary>
        protected override void Start()
        {
            flexSettings.PropertyChanged += FlexSettingsChanged;
            staggeredSettings.PropertyChanged += StaggeredSettingsChanged;
            ellipseSettings.PropertyChanged += EllipseSettingsChanged;
        }

        /// <summary>
        /// Process the disable event.
        /// </summary>
        protected override void OnDisable()
        {
            propertiesTracker.Clear();
            base.OnDisable();
        }

        /// <summary>
        /// Process the destroy event.
        /// </summary>
        protected override void OnDestroy()
        {
            if (flexSettings != null)
            {
                flexSettings.PropertyChanged -= FlexSettingsChanged;
            }

            if (ellipseSettings != null)
            {
                ellipseSettings.PropertyChanged -= EllipseSettingsChanged;
            }

            if (staggeredSettings != null)
            {
                staggeredSettings.PropertyChanged -= StaggeredSettingsChanged;
            }

            base.OnDestroy();
        }

        /// <summary>
        /// Process the RectTransform removed event.
        /// </summary>
        protected virtual void OnRectTransformRemoved()
        {
            SetDirty();
        }

        /// <summary>
        /// Sets the layout horizontal.
        /// </summary>
        public override void SetLayoutHorizontal()
        {
            RepositionElements();
        }

        /// <summary>
        /// Sets the layout vertical.
        /// </summary>
        public override void SetLayoutVertical()
        {
            RepositionElements();
        }

        /// <summary>
        /// Calculates the layout input horizontal.
        /// </summary>
        public override void CalculateLayoutInputHorizontal()
        {
            base.CalculateLayoutInputHorizontal();
            CalculateLayoutSize();
        }

        /// <summary>
        /// Calculates the layout input vertical.
        /// </summary>
        public override void CalculateLayoutInputVertical()
        {
            CalculateLayoutSize();
        }

        /// <summary>
        /// Marks layout to update.
        /// </summary>
        public void NeedUpdateLayout()
        {
            SetDirty();
        }

        /// <summary>
        /// Calculates the size of the layout.
        /// </summary>
        public void CalculateLayoutSize()
        {
            UpdateElements();
            PerformLayout(false);
        }

        /// <summary>
        /// Repositions the user interface elements.
        /// </summary>
        private void RepositionElements()
        {
            UpdateElements();

            PerformLayout(true);
        }

        /// <summary>
        /// Updates the layout.
        /// </summary>
        public void UpdateLayout()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
        }

        /// <summary>
        /// Is IgnoreLayout enabled?
        /// </summary>
        /// <param name="rect">RectTransform</param>
        /// <returns>true if IgnoreLayout enabled; otherwise, false.</returns>
        protected static bool IsIgnoreLayout(Transform rect)
        {
#if UNITY_5_0 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_3_OR_NEWER
            var ignorer = rect.GetComponent<ILayoutIgnorer>();
#else
			var ignorer = rect.GetComponent(typeof(ILayoutIgnorer)) as ILayoutIgnorer;
#endif
            return ignorer is {ignoreLayout: true};
        }

        /// <summary>
        /// Get children.
        /// </summary>
        /// <returns>Children.</returns>
        protected List<RectTransform> GetChildren()
        {
            var children = rectChildren;

            if (!SkipInactive)
            {
                children = (from Transform child in transform
                    where !IsIgnoreLayout(child)
                    select child as RectTransform).ToList();
            }

            if (Filter != null)
            {
                children = ApplyFilter(children);
            }

            return children;
        }

        /// <summary>
        /// Update LayoutElements.
        /// </summary>
        protected void UpdateElements()
        {
            ClearElements();

            var children = GetChildren();

            foreach (var x in children)
            {
                _elements.Add(CreateElement(x));
            }
        }

        /// <summary>
        /// Reset layout elements list.
        /// </summary>
        protected void ClearElements()
        {
            foreach (var x in _elements)
            {
                _elementsCache.Push(x);
            }

            _elements.Clear();
        }

        /// <summary>
        /// Create layout element.
        /// </summary>
        /// <param name="elem">Element.</param>
        /// <returns>Element data.</returns>
        protected LayoutElementInfo CreateElement(RectTransform elem)
        {
            var info = _elementsCache.Count > 0 ? _elementsCache.Pop() : new LayoutElementInfo();
            var active = SkipInactive || elem.gameObject.activeInHierarchy;

            info.SetElement(elem, active, this);

            if (ResetRotation && LayoutType != LayoutTypes.Ellipse)
            {
                info.NewEulerAngles = Vector3.zero;
            }

            return info;
        }

        /// <summary>
        /// Apply filter.
        /// </summary>
        /// <param name="input">Original list.</param>
        /// <returns>Filtered list.</returns>
        protected List<RectTransform> ApplyFilter(List<RectTransform> input)
        {
            var objects = new GameObject[input.Count];

            for (var i = 0; i < input.Count; i++)
            {
                objects[i] = input[i].gameObject;
            }

            return Filter(objects).Select(elem => elem.transform as RectTransform).ToList();
        }

        /// <summary>
        /// Gets the margin top.
        /// </summary>
        /// <returns>Top margin.</returns>
        public float GetMarginTop()
        {
            return Symmetric ? Margin.y : MarginTop;
        }

        /// <summary>
        /// Gets the margin bottom.
        /// </summary>
        /// <returns>Bottom margin.</returns>
        public float GetMarginBottom()
        {
            return Symmetric ? Margin.y : MarginBottom;
        }

        /// <summary>
        /// Gets the margin left.
        /// </summary>
        /// <returns>Left margin.</returns>
        public float GetMarginLeft()
        {
            return Symmetric ? Margin.x : MarginLeft;
        }

        /// <summary>
        /// Gets the margin right.
        /// </summary>
        /// <returns>Right margin.</returns>
        public float GetMarginRight()
        {
            return Symmetric ? Margin.x : MarginRight;
        }

        /// <summary>
        /// Get layout group.
        /// </summary>
        /// <returns>Layout group.</returns>
        protected EasyLayoutBaseType GetLayoutGroup()
        {
            switch (LayoutType)
            {
                case LayoutTypes.Compact:
                    return new EasyLayoutCompact(this);

                case LayoutTypes.Grid:
                    return new EasyLayoutGrid(this);

                case LayoutTypes.Flex:
                    return new EasyLayoutFlex(this);

                case LayoutTypes.Staggered:
                    return new EasyLayoutStaggered(this);

                case LayoutTypes.Ellipse:
                    return new EasyLayoutEllipse(this);

                default:
                    Debug.LogWarning($"Unknown layout type: {LayoutType}");
                    break;
            }

            return null;
        }

        /// <summary>
        /// Perform layout.
        /// </summary>
        /// <param name="setPositions">Is need to set elements position?</param>
        protected void PerformLayout(bool setPositions)
        {
            if (LayoutGroup == null)
            {
                Debug.LogWarning($"Layout group not found: {LayoutType}");
                return;
            }

            propertiesTracker.Clear();

            var size = LayoutGroup.PerformLayout(_elements, setPositions);

            UISize = _elements.Count > 0 ? new Vector2(size.x, size.y) : Vector2.zero;
            BlockSize = new Vector2(UISize.x + MarginFullHorizontal, UISize.y + MarginFullVertical);
        }

        /// <summary>
        /// Set element size.
        /// </summary>
        /// <param name="element">Element.</param>
        public void SetElementSize(LayoutElementInfo element)
        {
            var drivenProperties = DrivenTransformProperties.AnchoredPosition |
                                   DrivenTransformProperties.AnchoredPositionZ;

            if (ChildrenWidth != ChildrenSize.DoNothing)
            {
                drivenProperties |= DrivenTransformProperties.SizeDeltaX;

                element.Rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, element.NewWidth);
            }

            if (ChildrenHeight != ChildrenSize.DoNothing)
            {
                drivenProperties |= DrivenTransformProperties.SizeDeltaY;

                element.Rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, element.NewHeight);
            }

            if (LayoutType == LayoutTypes.Ellipse || ResetRotation)
            {
                drivenProperties |= DrivenTransformProperties.Rotation;
                element.Rect.localEulerAngles = element.NewEulerAngles;
            }

            if (LayoutType == LayoutTypes.Ellipse)
            {
                drivenProperties |= DrivenTransformProperties.Pivot;
                element.Rect.pivot = element.NewPivot;
            }

            propertiesTracker.Add(this, element.Rect, drivenProperties);
        }

        /// <summary>
        /// Get element position in the group.
        /// </summary>
        /// <param name="element">Element.</param>
        /// <returns>Position.</returns>
        public EasyLayoutPosition GetElementPosition(RectTransform element)
        {
            return LayoutGroup.GetElementPosition(element);
        }

        /// <summary>
        /// Awake this instance.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            Upgrade();
        }

#if UNITY_EDITOR
        /// <summary>
        /// Update layout when parameters changed.
        /// </summary>
        protected override void OnValidate()
        {
            _layoutGroup = null;

            SetDirty();
        }
#endif

        [SerializeField] [HideInInspector] private int version;

#pragma warning disable 0618
        /// <summary>
        /// Upgrade to keep compatibility between versions.
        /// </summary>
        public virtual void Upgrade()
        {
            // upgrade to 1.6
            if (version == 0)
            {
                if (_controlWidth)
                {
                    ChildrenWidth = _maxWidth
                        ? ChildrenSize.SetMaxFromPreferred
                        : ChildrenSize.SetPreferred;
                }

                if (_controlHeight)
                {
                    ChildrenHeight = _maxHeight
                        ? ChildrenSize.SetMaxFromPreferred
                        : ChildrenSize.SetPreferred;
                }
            }

            version = 1;
        }
#pragma warning restore 0618

        /// <summary>
        /// Default property handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        public static void DefaultPropertyHandler(object sender, PropertyChangedEventArgs e)
        {
        }
    }
}