﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Padding.
    /// </summary>
    [Serializable]
    public struct Padding : IEquatable<Padding>
    {
        /// <summary>
        /// The left padding.
        /// </summary>
        [SerializeField] public float left;

        /// <summary>
        /// The right padding.
        /// </summary>
        [SerializeField] public float right;

        /// <summary>
        /// The top padding.
        /// </summary>
        [SerializeField] public float top;

        /// <summary>
        /// The bottom padding.
        /// </summary>
        [SerializeField] public float bottom;

        /// <summary>
        /// Summary horizontal padding.
        /// </summary>
        public float Horizontal => left + right;

        /// <summary>
        /// Summary vertical padding.
        /// </summary>
        public float Vertical => top + bottom;

        /// <summary>
        /// Initializes a new instance of the <see cref="Padding"/> struct.
        /// </summary>
        /// <param name="left">Left.</param>
        /// <param name="right">Right.</param>
        /// <param name="top">Top.</param>
        /// <param name="bottom">Bottom.</param>
        public Padding(float left, float right, float top, float bottom)
        {
            this.left = left;
            this.right = right;
            this.top = top;
            this.bottom = bottom;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return $"Padding(left: {left}, right: {right}, top: {top}, bottom: {bottom})";
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            return obj is Padding p && Equals(p);
        }

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="other">The object to compare with the current object.</param>
        /// <returns>true if the specified object is equal to the current object; otherwise, false.</returns>
        public bool Equals(Padding other)
        {
            return left.Equals(other.left) && right.Equals(other.right) &&
                   top.Equals(other.top) && bottom.Equals(other.bottom);
        }

        /// <summary>
        /// Hash function.
        /// </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Compare specified paddings.
        /// </summary>
        /// <param name="padding1">First padding.</param>
        /// <param name="padding2">Second padding.</param>
        /// <returns>true if the paddings are equal; otherwise, false.</returns>
        public static bool operator ==(Padding padding1, Padding padding2)
        {
            return Equals(padding1, padding2);
        }

        /// <summary>
        /// Compare specified paddings.
        /// </summary>
        /// <param name="padding1">First padding.</param>
        /// <param name="padding2">Second padding.</param>
        /// <returns>true if the paddings not equal; otherwise, false.</returns>
        public static bool operator !=(Padding padding1, Padding padding2)
        {
            return !Equals(padding1, padding2);
        }
    }
}