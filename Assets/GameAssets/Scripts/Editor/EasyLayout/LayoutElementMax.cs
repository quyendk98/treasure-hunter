﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Allow to control the maximum preferred sizes of the LayoutGroup.
    /// </summary>
    [RequireComponent(typeof(LayoutGroup))]
    public sealed class LayoutElementMax : UIBehaviour, ILayoutElement, ILayoutIgnorer
    {
        private LayoutGroup _layoutGroup;

        /// <summary>
        /// Layout Group.
        /// </summary>
        private LayoutGroup Layout
        {
            get
            {
                if (_layoutGroup == null)
                {
                    _layoutGroup = GetComponent<LayoutGroup>();
                }

                return _layoutGroup;
            }
        }

        [SerializeField] private bool pIgnoreLayout;

        [SerializeField] private int pLayoutPriority = 1;

        [SerializeField] private float maxWidth = -1f;

        [SerializeField] private float maxHeight = -1f;

        /// <summary>
        /// Maximum preferred height for the Layout Group.
        /// </summary>
        public float MaxHeight
        {
            get => maxHeight;

            set
            {
                if (maxHeight.Equals(value))
                {
                    return;
                }

                maxHeight = value;
                SetDirty();
            }
        }

        /// <summary>
        /// Maximum preferred width for the Layout Group.
        /// </summary>
        public float MaxWidth
        {
            get => maxWidth;

            set
            {
                if (maxWidth.Equals(value))
                {
                    return;
                }

                maxWidth = value;
                SetDirty();
            }
        }

        /// <summary>
        /// Should this RectTransform be ignored by the layout system?
        /// </summary>
        /// <remarks>
        /// Setting this property to true will make a parent layout group component not consider this RectTransform part of the group. The RectTransform can then be manually positioned despite being a child GameObject of a layout group.
        /// </remarks>
        public bool ignoreLayout
        {
            get => pIgnoreLayout;

            set
            {
                if (pIgnoreLayout == value)
                {
                    return;
                }

                pIgnoreLayout = value;
                SetDirty();
            }
        }

        /// <summary>
        /// The Priority of layout this element has.
        /// </summary>
        public int layoutPriority
        {
            get => pLayoutPriority;
            set => pLayoutPriority = value;
        }

        /// <summary>
        /// The minimum height this layout element may be allocated.
        /// </summary>
        public float minHeight => -1;

        /// <summary>
        /// The minimum width this layout element may be allocated.
        /// </summary>
        public float minWidth => -1;

        /// <summary>
        /// The preferred width this layout element should be allocated if there is sufficient space. The preferredWidth can be set to -1 to remove the size.
        /// </summary>
        public float preferredHeight => Mathf.Min(Layout.preferredHeight, MaxHeight);

        /// <summary>
        /// The preferred width this layout element should be allocated if there is sufficient space. The preferredWidth can be set to -1 to remove the size.
        /// </summary>
        public float preferredWidth => Mathf.Min(Layout.preferredWidth, MaxWidth);

        /// <summary>
        /// The extra relative height this layout element should be allocated if there is additional available space.
        /// </summary>
        public float flexibleHeight => -1;

        /// <summary>
        /// The extra relative width this layout element should be allocated if there is additional available space.
        /// </summary>
        public float flexibleWidth => -1;

        /// <summary>
        /// Calculates the layout input horizontal.
        /// </summary>
        public void CalculateLayoutInputHorizontal()
        {
        }

        /// <summary>
        /// Calculates the layout input vertical.
        /// </summary>
        public void CalculateLayoutInputVertical()
        {
        }

        /// <summary>
        /// Process instance enable event.
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();
            SetDirty();
        }

        /// <summary>
        /// Process transform parent changed event.
        /// </summary>
        protected override void OnTransformParentChanged()
        {
            SetDirty();
        }

        /// <summary>
        /// Process instance disable event.
        /// </summary>
        protected override void OnDisable()
        {
            SetDirty();
            base.OnDisable();
        }

        /// <summary>
        /// Process property animation event.
        /// </summary>
        protected override void OnDidApplyAnimationProperties()
        {
            SetDirty();
        }

        /// <summary>
        /// Process before transform parent changed event.
        /// </summary>
        protected override void OnBeforeTransformParentChanged()
        {
            SetDirty();
        }

        /// <summary>
        /// Mark the LayoutElement as dirty.
        /// </summary>
        /// <remarks>
        /// This will make the auto layout system process this element on the next layout pass. This method should be called by the LayoutElement whenever a change is made that potentially affects the layout.
        /// </remarks>
        private void SetDirty()
        {
            if (IsActive())
            {
                LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
            }
        }

#if UNITY_EDITOR
        /// <summary>
        /// Process validation event.
        /// </summary>
        protected override void OnValidate()
        {
            SetDirty();
        }
#endif
    }
}