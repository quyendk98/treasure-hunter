﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// Settings for the staggered layout.
    /// </summary>
    [Serializable]
    public class EasyLayoutEllipseSettings : INotifyPropertyChanged
    {
        [SerializeField] private bool widthAuto = true;

        /// <summary>
        /// Calculate with or not.
        /// </summary>
        public bool WidthAuto
        {
            get => widthAuto;

            set
            {
                if (widthAuto == value)
                {
                    return;
                }

                widthAuto = value;
                Changed("WidthAuto");
            }
        }

        [SerializeField] private float width;

        /// <summary>
        /// Width.
        /// </summary>
        public float Width
        {
            get => width;

            set
            {
                if (width.Equals(value))
                {
                    return;
                }

                width = value;
                Changed("Width");
            }
        }

        [SerializeField] private bool heightAuto = true;

        /// <summary>
        /// Calculate height or not.
        /// </summary>
        public bool HeightAuto
        {
            get => heightAuto;

            set
            {
                if (heightAuto == value)
                {
                    return;
                }

                heightAuto = value;
                Changed("HeightAuto");
            }
        }

        [SerializeField] private float height;

        /// <summary>
        /// Height.
        /// </summary>
        public float Height
        {
            get => height;

            set
            {
                if (height.Equals(value))
                {
                    return;
                }

                height = value;
                Changed("Height");
            }
        }

        [SerializeField] private float angleStart;

        /// <summary>
        /// Angle for the display first element.
        /// </summary>
        public float AngleStart
        {
            get => angleStart;

            set
            {
                if (angleStart.Equals(value))
                {
                    return;
                }

                angleStart = value;
                Changed("AngleStart");
            }
        }

        [SerializeField] private bool angleStepAuto;

        /// <summary>
        /// Calculate or not AngleStep.
        /// </summary>
        public bool AngleStepAuto
        {
            get => angleStepAuto;

            set
            {
                if (angleStepAuto == value)
                {
                    return;
                }

                angleStepAuto = value;
                Changed("AngleStepAuto");
            }
        }

        [SerializeField] private float angleStep = 20f;

        /// <summary>
        /// Angle distance between elements.
        /// </summary>
        public float AngleStep
        {
            get => angleStep;

            set
            {
                if (angleStep.Equals(value))
                {
                    return;
                }

                angleStep = value;
                Changed("AngleStep");
            }
        }

        [SerializeField] private EllipseFill fill = EllipseFill.Closed;

        /// <summary>
        /// Fill type.
        /// </summary>
        public EllipseFill Fill
        {
            get => fill;

            set
            {
                if (fill == value)
                {
                    return;
                }

                fill = value;
                Changed("Fill");
            }
        }

        [SerializeField] private float arcLength = 360f;

        /// <summary>
        /// Arc length.
        /// </summary>
        public float ArcLength
        {
            get => arcLength;

            set
            {
                if (arcLength.Equals(value))
                {
                    return;
                }

                arcLength = value;
                Changed("Length");
            }
        }

        [SerializeField] private EllipseAlign align;

        /// <summary>
        /// Align.
        /// </summary>
        public EllipseAlign Align
        {
            get => align;

            set
            {
                if (align == value)
                {
                    return;
                }

                align = value;
                Changed("Align");
            }
        }

        [SerializeField] [HideInInspector] private float angleScroll;

        /// <summary>
        /// Angle padding.
        /// </summary>
        public float AngleScroll
        {
            get => angleScroll;

            set
            {
                if (angleScroll.Equals(value))
                {
                    return;
                }

                angleScroll = value;
                Changed("AngleScroll");
            }
        }

        [SerializeField] [HideInInspector] private float angleFiller;

        /// <summary>
        /// Angle filler.
        /// </summary>
        public float AngleFiller
        {
            get => angleFiller;

            set
            {
                if (angleFiller.Equals(value))
                {
                    return;
                }

                angleFiller = value;
                Changed("AngleFiller");
            }
        }

        [SerializeField] private bool elementsRotate = true;

        /// <summary>
        /// Rotate elements.
        /// </summary>
        public bool ElementsRotate
        {
            get => elementsRotate;

            set
            {
                if (elementsRotate == value)
                {
                    return;
                }

                elementsRotate = value;
                Changed("ElementsRotate");
            }
        }

        [SerializeField] private float elementsRotationStart;

        /// <summary>
        /// Start rotation for elements.
        /// </summary>
        public float ElementsRotationStart
        {
            get => elementsRotationStart;

            set
            {
                if (elementsRotationStart.Equals(value))
                {
                    return;
                }

                elementsRotationStart = value;
                Changed("ElementsRotationStart");
            }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler
            PropertyChanged = EasyLayout.DefaultPropertyHandler;

        /// <summary>
        /// Property changed.
        /// </summary>
        /// <param name="propertyName">Property name.</param>
        protected void Changed(string propertyName)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}