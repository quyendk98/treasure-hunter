﻿using System;
using UnityEngine;

namespace GameAssets.Scripts.Editor.EasyLayout
{
    /// <summary>
    /// User interface position.
    /// </summary>
    [Serializable]
    public class UIPosition
    {
        /// <summary>
        /// The object.
        /// </summary>
        [SerializeField] public RectTransform @object;

        /// <summary>
        /// The active.
        /// </summary>
        [SerializeField] public bool active;

        /// <summary>
        /// The position.
        /// </summary>
        [SerializeField] public Vector3 position;

        /// <summary>
        /// The anchor max.
        /// </summary>
        [SerializeField] public Vector2 anchorMax;

        /// <summary>
        /// The anchor minimum.
        /// </summary>
        [SerializeField] public Vector2 anchorMin;

        /// <summary>
        /// The size delta.
        /// </summary>
        [SerializeField] public Vector2 sizeDelta;

        /// <summary>
        /// The pivot.
        /// </summary>
        [SerializeField] public Vector2 pivot;

        /// <summary>
        /// The rotation.
        /// </summary>
        [SerializeField] public Vector3 rotation;

        /// <summary>
        /// The scale.
        /// </summary>
        [SerializeField] public Vector3 scale;
    }
}