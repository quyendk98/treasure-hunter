﻿using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Ex.PhysicDebug;
using UnityEditor;
using UnityEngine;

namespace GameAssets.Scripts.Editor.PhysicDebug
{
    public class PhysicDebugWindow : EditorWindow
    {
        private string _selectedObjName = string.Empty;

        private bool _graphVelocity = true;
        private bool _graphAcceleration = true;
        private bool _graphVelocityAxis;
        private bool _graphAccelerationAxis;
        private bool _useVelocityColorLarge = true;
        private bool _useVelocityColorSmall;

        private bool _collisionStats;
        private bool _otherStats;

        private float _slowSpeed;
        private float _fastSpeed = 10f;

        private Color _slowSpeedColor = new Color(75f / 255, 255f / 255, 75f / 255);
        private Color _fastSpeedColor = new Color(255f / 255, 75f / 255, 75f / 255);

        private int _graphResolution = 250;
        private int _updateRate = 55;
        private int _dataRate = 55;

        private PhysicDebugGraph _selectedGraph;
        private List<PhysicDebugGraph> _pinnedGraphObjects = new List<PhysicDebugGraph>();

        private static GUISkin _editorSkin;

        [MenuItem("Window/Physic Debugger Graph")]
        public static void ShowWindow()
        {
            //Show existing window instance. If one doesn't exist, make one.
            GetWindow(typeof(PhysicDebugWindow));

            if (_editorSkin == null)
            {
                _editorSkin = (GUISkin) AssetDatabase.LoadAssetAtPath(
                    "Assets/GameAssets/Scripts/Editor/PhysicDebug/PhysicsDebugWindowSkin.guiskin",
                    typeof(GUISkin));
            }
        }

        public void OnInspectorUpdate()
        {
            if (!_selectedGraph)
            {
                GetSelectedObj();
            }

            var sceneGraphObjs = FindObjectsOfType<PhysicDebugGraph>().ToList();

            sceneGraphObjs.RemoveAll(go => _pinnedGraphObjects.Contains(go));
            sceneGraphObjs.Remove(_selectedGraph);

            foreach (var x in sceneGraphObjs)
            {
                x.Initialize();
            }

            _pinnedGraphObjects.RemoveAll(pgo => pgo == null);
            _pinnedGraphObjects.AddRange(sceneGraphObjs);

            if (_selectedGraph)
            {
                _selectedGraph.graphResolution = _graphResolution;
                _selectedGraph.dataRate = _dataRate;
            }

            _pinnedGraphObjects = _pinnedGraphObjects.Select(pgo =>
            {
                pgo.graphResolution = _graphResolution;
                return pgo;
            }).ToList();

            _pinnedGraphObjects = _pinnedGraphObjects.Select(pgo =>
            {
                pgo.dataRate = _dataRate;
                return pgo;
            }).ToList();
        }

        private int _frameCounter;
        private bool _onPlay = true;

        public void Update()
        {
            if (EditorApplication.isPlaying)
            {
                if (!_onPlay)
                {
                    _onPlay = true;
                    _pinnedGraphObjects.Clear();

                    DestroyImmediate(_selectedGraph);
                    GetSelectedObj();
                }

                _frameCounter++;

                if (_frameCounter % (61 - _updateRate) != 0)
                {
                    return;
                }

                _frameCounter = 0;

                if (_selectedGraph == null)
                {
                    GetSelectedObj();
                }

                Repaint();
            }
            else
            {
                if (!_onPlay)
                {
                    return;
                }

                _onPlay = false;
                _selectedGraph = null;

                GetSelectedObj();
                Repaint();
            }
        }


        private Vector2 _scrollPos;

        private void OnGUI()
        {
            _pinnedGraphObjects.RemoveAll(pgo => pgo == null);

            if (_editorSkin == null)
            {
                _editorSkin = (GUISkin) AssetDatabase.LoadAssetAtPath(
                    "Assets/GameAssets/Scripts/Editor/PhysicDebug/PhysicsDebugWindowSkin.guiskin",
                    typeof(GUISkin));
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Selected Object: ", EditorStyles.boldLabel, EditorStyles.boldLabel);
            EditorGUILayout.LabelField(_selectedObjName);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(15f);
            EditorGUILayout.LabelField("Graph Options", EditorStyles.boldLabel);

            _graphResolution = EditorGUILayout.IntSlider(new GUIContent("Graph Resolution",
                "Number of ticks that can fit on one graph."), _graphResolution, 10, 2500);

            _updateRate = EditorGUILayout.IntSlider(new GUIContent("Update Rate",
                    "Rate at which the graphs are re-drawn. With high resolutions a fast refresh rate may cause slow-down."),
                _updateRate, 1, 60);

            _dataRate = EditorGUILayout.IntSlider(new GUIContent("Data Collection Rate",
                    "Rate at which data is collected. 60 = every Physics update, 59 = every second Physics update, etc..."),
                _dataRate, 1, 60);

            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(10f));

            _graphVelocity = EditorGUILayout.Toggle(new GUIContent("Velocity Magnitude",
                "If true, display velocity graphs for objects."), _graphVelocity);

            GUILayout.Space(15f);

            _graphAcceleration = EditorGUILayout.Toggle(new GUIContent("Acceleration Magnitude",
                "If true, display acceleration graphs for objects."), _graphAcceleration);

            GUILayout.Space(15f);

            _useVelocityColorLarge = EditorGUILayout.Toggle(new GUIContent("Color Acc/Vel by Speed",
                "If true, color the velocity/acceleration graphs by speed of objects. This creates a color gradient for the lines and is"
                + "very helpful for seeing how fast something is moving at a glance."), _useVelocityColorLarge);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(10f));

            _graphVelocityAxis = EditorGUILayout.Toggle(new GUIContent("Velocity Axes",
                    "If true, display velocity graphs for the 3 world axes of objects."),
                _graphVelocityAxis);

            GUILayout.Space(15f);

            _graphAccelerationAxis = EditorGUILayout.Toggle(new GUIContent("Acceleration Axes",
                "If true, display acceleration graphs for the 3 world axes of objects."), _graphAccelerationAxis);

            GUILayout.Space(15f);

            _useVelocityColorSmall = EditorGUILayout.Toggle(new GUIContent("Color Axes by Speed",
                "If true, color the velocity/acceleration axes graphs by speed of objects. This creates a color gradient for the lines and is"
                + "very helpful for seeing how fast something is moving at a glance."), _useVelocityColorSmall);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(10f));

            _collisionStats = EditorGUILayout.Toggle(new GUIContent("Collision Stats",
                "If true, display collision statistics for objects."), _collisionStats);

            GUILayout.Space(15f);

            _otherStats = EditorGUILayout.Toggle(new GUIContent("Velocity/Acc Stats",
                "If true, display various physics statistics for objects."), _otherStats);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(10f));

            _slowSpeed = EditorGUILayout.FloatField(new GUIContent("Low Speed Threshold",
                "The speed at which to start the slowSpeedColor for coloring the " +
                "gradient lines when using the Velocity Color options."), _slowSpeed);

            GUILayout.Space(15f);

            _slowSpeedColor = EditorGUILayout.ColorField(new GUIContent("Low Speed Color",
                "The color to start with when coloring lines by velocity."), _slowSpeedColor);

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(10f));

            _fastSpeed = EditorGUILayout.FloatField(new GUIContent("High Speed Threshold",
                "The speed at which to stop coloring " +
                "the gradient lines when using the Velocity Color options."), _fastSpeed);

            GUILayout.Space(15f);

            _fastSpeedColor = EditorGUILayout.ColorField(new GUIContent("High Speed Color",
                "The color to end with when coloring lines by velocity."), _fastSpeedColor);

            EditorGUILayout.EndHorizontal();
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));

            _scrollPos = GUILayout.BeginScrollView(_scrollPos);

            if (_selectedGraph != null)
            {
                if (_selectedGraph.go)
                {
                    if (_selectedGraph.rb)
                    {
                        EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(15f));
                        GUILayout.Label(_selectedGraph.go.name);

                        if (GUILayout.Button("Pin") && _pinnedGraphObjects.Find(x => x.go == _selectedGraph.go) == null)
                        {
                            _pinnedGraphObjects.Add(_selectedGraph);
                            _selectedGraph = null;
                            _selectedObjName = "None";

                            Repaint();
                            return;
                        }

                        EditorGUILayout.EndHorizontal();
                        DrawGraphsForObj(_selectedGraph);

                        if (_collisionStats)
                        {
                            DrawCollisionStatsForObj(_selectedGraph);
                        }

                        if (_otherStats)
                        {
                            GUILayout.Space(10f);
                            DrawOtherStatsForObj(_selectedGraph);
                        }
                    }
                    else
                    {
                        GUILayout.Label("Selected object has no rigid body.");
                    }
                }
                else
                {
                    GUILayout.Label("No selected rigid body.");
                }
            }
            else
            {
                GUILayout.Label("No selected rigid body.");
            }

            foreach (var pGO in _pinnedGraphObjects)
            {
                GUILayout.Space(15f);
                EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(15f));
                GUILayout.Label(pGO.name);

                if (GUILayout.Button("Un-Pin"))
                {
                    _pinnedGraphObjects.Remove(pGO);
                    DestroyImmediate(pGO);
                    Repaint();
                    return;
                }

                if (!pGO.rb)
                {
                    GUILayout.Label("Object is static and will only display collision stats.");
                    EditorGUILayout.EndHorizontal();
                    DrawCollisionStatsForObj(pGO);
                }
                else
                {
                    EditorGUILayout.EndHorizontal();
                    DrawGraphsForObj(pGO);

                    if (_collisionStats)
                    {
                        DrawCollisionStatsForObj(pGO);
                    }

                    if (!_otherStats)
                    {
                        continue;
                    }

                    GUILayout.Space(10f);
                    DrawOtherStatsForObj(pGO);
                }
            }

            GUILayout.EndScrollView();
        }

        private Color GetVelocityColor(float inVel)
        {
            var tPct = VelocityPercentage(inVel);
            return GradientPercent(_slowSpeedColor, _fastSpeedColor, tPct);
        }

        private float VelocityPercentage(float force)
        {
            var dist = _fastSpeed - _slowSpeed;
            var forceAdjust = force - _fastSpeed;
            return Mathf.Clamp01(forceAdjust / dist);
        }

        private static Color GradientPercent(Color lower, Color upper, float percentage)
        {
            return new Color(lower.r + percentage * (upper.r - lower.r),
                lower.g + percentage * (upper.g - lower.g),
                lower.b + percentage * (upper.b - lower.b));
        }

        private void OnSelectionChange()
        {
            GetSelectedObj();
        }

        private void GetSelectedObj()
        {
            if (_selectedGraph)
            {
                DestroyImmediate(_selectedGraph);

                _selectedGraph = null;
                _selectedObjName = "None";
            }

            if (!Selection.activeGameObject || !Selection.activeGameObject.activeInHierarchy)
            {
                return;
            }

            var go = Selection.activeGameObject;

            if (go.GetComponent<PhysicDebugGraph>())
            {
                _selectedObjName = go.name;
                _selectedObjName += ", Already Pinned";
                return;
            }

            _selectedObjName = go.name;

            if (go.GetComponent<Rigidbody>())
            {
                _selectedObjName += ", Has RigidBody";

                if (_selectedGraph)
                {
                    DestroyImmediate(_selectedGraph);
                }

                _selectedGraph = go.AddComponent<PhysicDebugGraph>();

                _selectedGraph.Initialize();
            }
            else
            {
                _selectedObjName += ", No RigidBody";
            }

            Repaint();
        }

        private static void DrawCollisionStatsForObj(PhysicDebugGraph g)
        {
            GUI.enabled = false;
            GUI.color = new Color(1, 1, 1, 2);

            Handles.BeginGUI();

            const float BOX_WIDTH = 525;

            GUILayout.Space(5f);

            var lastRect = EditorGUILayout.BeginHorizontal();

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3f);

            Handles.color = Color.gray;

            Handles.DrawSolidRectangleWithOutline(
                new Rect(lastRect.x + 3, lastRect.yMax, BOX_WIDTH + 5, 152),
                new Color(0.9f, 0.9f, 0.9f), new Color(0.25f, 0.25f, 0.25f));

            var lastWidth = EditorGUIUtility.labelWidth;

            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(BOX_WIDTH));

            EditorGUIUtility.labelWidth = 60;

            EditorGUILayout.Toggle("Colliding: ", g.colliding);
            GUILayout.Space(10f);

            EditorGUIUtility.labelWidth = 100;

            EditorGUILayout.IntField("Collision Count: ", g.collisionCount);
            GUILayout.Space(05f);

            EditorGUIUtility.labelWidth = 125;

            EditorGUILayout.TextField("Last Collided Object: ", g.collidingObject);
            EditorGUILayout.EndHorizontal();

            EditorGUIUtility.labelWidth = lastWidth;

            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(BOX_WIDTH));
            EditorGUILayout.TextField("Last Collision Duration: ", g.collisionDuration.ToString("F2"));

            GUILayout.Space(10f);
            EditorGUILayout.TextField("Time Without Collision: ", g.noCollisionDuration.ToString("F2"));

            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(BOX_WIDTH));

            EditorGUIUtility.labelWidth = 125;

            EditorGUILayout.FloatField("Last Collision Force: ", g.collisionForce);
            GUILayout.Space(15f);

            EditorGUIUtility.labelWidth = 71;

            EditorGUILayout.FloatField("Min Force: ", g.minCollisionForce);
            GUILayout.Space(15f);

            EditorGUILayout.FloatField("Max Force: ", g.maxCollisionForce);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Vector3Field("Last Relative Collision Velocity: ",
                g.collisionVelocity, GUILayout.MaxWidth(BOX_WIDTH));

            GUILayout.Space(5f);
            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(BOX_WIDTH));

            EditorGUIUtility.labelWidth = 68;

            EditorGUILayout.Toggle("Triggering: ", g.triggering);
            GUILayout.Space(15f);

            EditorGUIUtility.labelWidth = 135;

            EditorGUILayout.TextField("Last Triggering Object: ", g.triggeringObject);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(BOX_WIDTH));

            EditorGUIUtility.labelWidth = 95;

            EditorGUILayout.IntField("Trigger Count: ", g.triggerCount);
            GUILayout.Space(15f);

            EditorGUIUtility.labelWidth = 135;

            EditorGUILayout.TextField("Last Trigger Duration: ", g.triggerDuration.ToString("F2"));
            EditorGUILayout.EndHorizontal();

            EditorGUIUtility.labelWidth = lastWidth;

            Handles.EndGUI();

            GUI.color = Color.white;
            GUI.enabled = true;
        }

        private static void DrawOtherStatsForObj(PhysicDebugGraph g)
        {
            GUI.enabled = false;
            GUI.color = new Color(1, 1, 1, 2);

            const float BOX_WIDTH = 525;

            GUILayout.Space(5f);

            var lastRect = EditorGUILayout.BeginHorizontal();

            EditorGUILayout.EndHorizontal();
            GUILayout.Space(3f);

            Handles.color = Color.gray;

            Handles.DrawSolidRectangleWithOutline(
                new Rect(lastRect.x + 3, lastRect.yMax, BOX_WIDTH + 5, 165),
                new Color(0.9f, 0.9f, 0.9f), new Color(0.25f, 0.25f, 0.25f));

            var lastWidth = EditorGUIUtility.labelWidth;

            EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(BOX_WIDTH));

            EditorGUIUtility.labelWidth = 45;

            EditorGUILayout.FloatField("Speed ", g.currentVelocity.magnitude);
            GUILayout.Space(15f);

            EditorGUIUtility.labelWidth = 72;

            EditorGUILayout.FloatField("Min Speed: ", g.minSpeed);
            GUILayout.Space(15f);

            EditorGUILayout.FloatField("Max Speed: ", g.maxSpeed);
            EditorGUILayout.EndHorizontal();

            EditorGUIUtility.labelWidth = lastWidth;

            EditorGUILayout.Vector3Field("Velocity: ", g.currentVelocity, GUILayout.MaxWidth(BOX_WIDTH));
            EditorGUILayout.Vector3Field("Min Velocity: ", g.minVelocity, GUILayout.MaxWidth(BOX_WIDTH));
            EditorGUILayout.Vector3Field("Max Velocity: ", g.maxVelocity, GUILayout.MaxWidth(BOX_WIDTH));
            EditorGUILayout.Vector3Field("Acceleration: ", g.currentAcceleration, GUILayout.MaxWidth(BOX_WIDTH));

            GUI.color = Color.white;
            GUI.enabled = true;
        }

        private void DrawGraphsForObj(PhysicDebugGraph g)
        {
            GUI.skin = _editorSkin;

            const float PADDING = 8f;

            var rectHeight = 125f;
            var rectWidth = position.width - PADDING * 2.75f;
            var halfWidth = rectWidth / 2f - PADDING / 2f;

            GUI.enabled = false;
            GUI.color = new Color(1, 1, 1, 2);

            var defRed = new Color(1f, 0.25f, 0.25f);
            var defGreen = new Color(0.25f, 1f, 0.25f);
            var defBlue = new Color(0.25f, 0.25f, 1f);

            GUILayout.BeginHorizontal();

            if (_graphVelocity)
            {
                var width = rectWidth;

                if (_graphAcceleration)
                {
                    width = halfWidth;
                }

                GUILayout.Button("", GUILayout.Width(width), GUILayout.Height(rectHeight));

                var lastRect = GUILayoutUtility.GetLastRect();

                Handles.BeginGUI();
                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Velocity Magnitude");

                DrawVelocityGraph(lastRect, g.velocityGraph, defGreen, _useVelocityColorLarge);

                Handles.EndGUI();
            }

            if (_graphAcceleration)
            {
                var width = rectWidth;

                if (_graphVelocity)
                {
                    width = halfWidth;
                }

                GUILayout.Button("", GUILayout.Width(width), GUILayout.Height(rectHeight));

                var lastRect = GUILayoutUtility.GetLastRect();

                Handles.BeginGUI();
                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Acceleration Magnitude");

                DrawAccGraph(lastRect, g.accelerationGraph, defGreen, _useVelocityColorLarge);

                Handles.EndGUI();
            }

            GUILayout.EndHorizontal();

            rectHeight *= 0.85f;

            rectWidth /= 3f;
            halfWidth /= 3f;

            halfWidth -= 3.3f;
            rectWidth -= 4.8f;

            if (_graphVelocityAxis)
            {
                GUILayout.BeginHorizontal();
                Handles.BeginGUI();
                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                var lastRect = GUILayoutUtility.GetLastRect();

                Handles.color = Color.white;

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Vel.X");

                DrawAccGraph(lastRect, g.velocityAxisGraph.Select(x => -x.x).ToList(), defRed, _useVelocityColorSmall);

                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                lastRect = GUILayoutUtility.GetLastRect();
                Handles.color = Color.white;

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Vel.Y");

                DrawAccGraph(lastRect, g.velocityAxisGraph.Select(x => -x.y).ToList(), defGreen,
                    _useVelocityColorSmall);

                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                lastRect = GUILayoutUtility.GetLastRect();
                Handles.color = Color.white;

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Vel.Z");

                DrawAccGraph(lastRect, g.velocityAxisGraph.Select(x => -x.z).ToList(), defBlue, _useVelocityColorSmall);

                Handles.EndGUI();
                GUILayout.EndHorizontal();
            }

            if (_graphAccelerationAxis)
            {
                GUILayout.BeginHorizontal();
                Handles.BeginGUI();
                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                var lastRect = GUILayoutUtility.GetLastRect();

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Acc.X");

                DrawAccGraph(lastRect, g.gGraph.Select(x => -x.x).ToList(), defRed, _useVelocityColorSmall);

                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                lastRect = GUILayoutUtility.GetLastRect();

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Acc.Y");

                DrawAccGraph(lastRect, g.gGraph.Select(x => -x.y).ToList(), defGreen, _useVelocityColorSmall);

                GUILayout.Button("", GUILayout.Width(rectWidth), GUILayout.Height(rectHeight));

                lastRect = GUILayoutUtility.GetLastRect();

                Handles.Label(new Vector3(lastRect.x + 5f, lastRect.y + 2f), "Acc.Z");

                DrawAccGraph(lastRect, g.gGraph.Select(x => -x.z).ToList(), defBlue, _useVelocityColorSmall);

                Handles.EndGUI();
                GUILayout.EndHorizontal();
            }

            GUI.enabled = true;
            GUI.skin = null;
            GUI.color = Color.white;
        }

        private void DrawVelocityGraph(Rect rect, IReadOnlyList<float> graph, Color drawColor, bool velColor)
        {
            if (graph == null)
            {
                return;
            }

            var lastRect = rect;
            var step = lastRect.width / _graphResolution;
            var closest = -1;

            if (lastRect.Contains(UnityEngine.Event.current.mousePosition) && EditorApplication.isPlaying)
            {
                var mX = UnityEngine.Event.current.mousePosition.x;

                Handles.color = new Color(1, 1, 1, 0.4f);
                Handles.DrawLine(new Vector3(mX, rect.y), new Vector3(mX, rect.yMax));

                var graphPoss = new List<float>();
                var tPoss = rect.xMax;

                for (var i = 0; i <= _graphResolution; i++)
                {
                    graphPoss.Add(tPoss);
                    tPoss -= step;
                }

                graphPoss.Reverse();

                var closestF = graphPoss.Aggregate((x, y) => Mathf.Abs(x - mX) < Mathf.Abs(y - mX) ? x : y);

                closest = graphPoss.IndexOf(closestF);
            }

            lastRect.height -= 1f;

            if (graph.Count <= 0)
            {
                return;
            }

            var max = Mathf.Clamp(graph.Max() + 2.75f, 3f, 999999f);
            var lastVel = 1f - graph[graph.Count - 1] / max;

            var lastGPos = new Vector3(lastRect.xMax, lastVel * lastRect.height + lastRect.y);
            var gPos = new Vector3(lastRect.xMax, 0f);

            gPos.x -= step;
            GUI.skin.label.alignment = TextAnchor.UpperRight;

            Handles.Label(new Vector3(rect.xMax, rect.y + 4f), max.ToString("F1"));

            GUI.skin.label.alignment = TextAnchor.LowerRight;

            Handles.Label(new Vector3(rect.xMax, rect.yMax), "0");

            GUI.skin.label.alignment = TextAnchor.UpperLeft;

            var drawPoint = false;
            var drawPointPos = Vector2.zero;

            for (var i = graph.Count - 1; i >= 0; i--)
            {
                var vel = graph[i];
                var velPct = 1f - vel / max;

                gPos.y = velPct * lastRect.height + lastRect.y;

                if (i + (_graphResolution - graph.Count) + 1 == closest)
                {
                    drawPoint = true;
                    drawPointPos = gPos;

                    if (rect.xMax - gPos.x < 80f)
                    {
                        GUI.skin.label.alignment = TextAnchor.LowerRight;

                        Handles.Label(gPos + new Vector3(-9, 3), vel.ToString("F2"));
                    }
                    else
                    {
                        GUI.skin.label.alignment = TextAnchor.LowerLeft;

                        Handles.Label(gPos + new Vector3(9, 3), vel.ToString("F2"));
                    }

                    GUI.skin.label.alignment = TextAnchor.UpperLeft;
                }

                if (i == graph.Count - 1)
                {
                    continue;
                }

                lastVel = i + 1 < graph.Count ? graph[i + 1] : 0f;
                Handles.color = velColor ? GetVelocityColor(Mathf.Abs(lastVel)) : drawColor;

                Handles.DrawLine(lastGPos, gPos);

                lastGPos = gPos;
                gPos.x -= step;
            }

            if (!drawPoint)
            {
                return;
            }

            Handles.color = new Color(1, 1, 1, 0.4f);

            Handles.DrawSolidDisc(drawPointPos, Vector3.forward, 4.9f);
        }

        private void DrawAccGraph(Rect rect, IReadOnlyList<float> graph, Color drawColor, bool velColor)
        {
            var lastRect = rect;
            var step = lastRect.width / _graphResolution;
            var closest = -1;

            Handles.color = new Color(1, 1, 1, 0.4f);

            var halfHeight = rect.y + rect.height / 2f;

            Handles.DrawLine(new Vector3(rect.x, halfHeight), new Vector3(rect.xMax, halfHeight));

            if (lastRect.Contains(UnityEngine.Event.current.mousePosition) && EditorApplication.isPlaying)
            {
                var mX = UnityEngine.Event.current.mousePosition.x;

                Handles.color = new Color(1, 1, 1, 0.4f);

                Handles.DrawLine(new Vector3(mX, rect.y), new Vector3(mX, rect.yMax));

                var graphPoss = new List<float>();
                var tPoss = rect.xMax;

                for (var i = 0; i <= _graphResolution; i++)
                {
                    graphPoss.Add(tPoss);

                    tPoss -= step;
                }

                graphPoss.Reverse();

                var closestF = graphPoss.Aggregate((x, y) => Mathf.Abs(x - mX) < Mathf.Abs(y - mX) ? x : y);

                closest = graphPoss.IndexOf(closestF);
            }

            Handles.color = drawColor;
            lastRect.height -= 1f;

            if (graph.Count <= 0)
            {
                return;
            }

            var max = Mathf.Clamp(graph.Max() + 1f, 2f, 999999f);
            var min = Mathf.Clamp(Mathf.Abs(graph.Min()) + 1f, 2f, 999999f);

            if (min > max)
            {
                max = min;
            }
            else
            {
                min = max;
            }

            min = -min;

            var scaledMax = max - min;
            var lastVel = graph[graph.Count - 1] - min;

            lastVel /= scaledMax;

            var lastGPos = new Vector3(lastRect.xMax, lastVel * lastRect.height + lastRect.y);
            var gPos = new Vector3(lastRect.xMax, 0f);

            gPos.x -= step;
            GUI.skin.label.alignment = TextAnchor.UpperRight;

            Handles.Label(new Vector3(rect.xMax, rect.y + 4f), max.ToString("F1"));

            GUI.skin.label.alignment = TextAnchor.LowerRight;

            Handles.Label(new Vector3(rect.xMax, rect.yMax), min.ToString("F1"));

            GUI.skin.label.alignment = TextAnchor.UpperLeft;

            var drawPoint = false;
            var drawPointPos = Vector2.zero;

            for (var i = graph.Count - 1; i >= 0; i--)
            {
                var acc = graph[i];

                acc -= min;

                var velPct = acc / scaledMax;

                gPos.y = velPct * lastRect.height + lastRect.y;

                if (i + (_graphResolution - graph.Count) + 1 == closest)
                {
                    drawPoint = true;
                    drawPointPos = gPos;

                    if (rect.xMax - gPos.x < 80f)
                    {
                        GUI.skin.label.alignment = TextAnchor.LowerRight;

                        Handles.Label(gPos + new Vector3(-9, 3), (-(acc + min)).ToString("F2"));
                    }
                    else
                    {
                        GUI.skin.label.alignment = TextAnchor.LowerLeft;

                        Handles.Label(gPos + new Vector3(9, 3), (-(acc + min)).ToString("F2"));
                    }

                    GUI.skin.label.alignment = TextAnchor.UpperLeft;
                }

                if (i == graph.Count - 1)
                {
                    continue;
                }

                lastVel = i + 1 < graph.Count ? graph[i + 1] : 0f;

                if (velColor)
                {
                    Handles.color = GetVelocityColor(Mathf.Abs(lastVel));
                }

                Handles.DrawLine(lastGPos, gPos);

                lastGPos = gPos;
                gPos.x -= step;
            }

            if (!drawPoint)
            {
                return;
            }

            Handles.color = new Color(1, 1, 1, 0.4f);

            Handles.DrawSolidDisc(drawPointPos, Vector3.forward, 4.9f);
        }
    }
}