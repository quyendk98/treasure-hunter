using System;
using System.Collections.Generic;
using System.Linq;
using GameAssets.Scripts.Base.Interface;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public class FormFieldEx : MonoBehaviour, IInitialize
    {
        [SerializeField] private List<TMP_InputField> inputFields = new List<TMP_InputField>();

        [SerializeField] private KeyCode keyToRunFunctionAfterAllInputEntered;

        [SerializeField] private bool disablePlaceHolderWhenSelectedInputField = true;

        [ShowIf("@this.keyToRunFunctionAfterAllInputEntered != UnityEngine.KeyCode.None")] [SerializeField]
        private bool checkInputValidatorBeforeRunFunction = true;

        public Action FunctionAfterAllInputEntered { get; set; }

        private readonly List<ValidatorEx> _validators = new List<ValidatorEx>();

        private int _input = -1;

        #region MonoBehaviour

        private void Awake()
        {
            for (var i = 0; i < inputFields.Count; i++)
            {
                var j = i;

                _validators.Add(inputFields[i].GetComponent<ValidatorEx>());

                inputFields[i].onSelect.AddListener(s =>
                {
                    _input = j;

                    if (disablePlaceHolderWhenSelectedInputField)
                    {
                        inputFields[j].placeholder.gameObject.SetActive(false);
                    }
                });

                if (disablePlaceHolderWhenSelectedInputField)
                {
                    inputFields[i].onDeselect.AddListener(s =>
                    {
                        _input = 0;

                        if (disablePlaceHolderWhenSelectedInputField)
                        {
                            inputFields[j].placeholder.gameObject.SetActive(true);
                        }
                    });
                }
            }
        }

        private void Update()
        {
#if UNITY_EDITOR || UNITY_WEBGL
            if (!gameObject.activeInHierarchy)
            {
                return;
            }

            if (Input.GetKeyDown(KeyCode.Tab))
            {
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    _input--;

                    if (_input < 0)
                    {
                        _input = inputFields.Count - 1;
                    }

                    SelectField();
                }
                else
                {
                    _input++;

                    if (_input > inputFields.Count - 1)
                    {
                        _input = 0;
                    }

                    SelectField();
                }
            }
            else if (Input.GetKeyDown(keyToRunFunctionAfterAllInputEntered))
            {
                Debug.Log("Run Function After All Input Entered");

                if (_validators.Count <= 0
                    || !checkInputValidatorBeforeRunFunction
                    || checkInputValidatorBeforeRunFunction && _validators.All(x => x.Success))
                {
                    FunctionAfterAllInputEntered?.Invoke();
                }
            }
#endif
        }

        private void OnValidate()
        {
            Initialize();
        }

        private void OnDestroy()
        {
            // Reset Function
            FunctionAfterAllInputEntered = () => { };
        }

        #endregion

        #region Public

        [Button]
        public void Initialize()
        {
            inputFields.Clear();
            inputFields.AddRange(GetComponentsInChildren<TMP_InputField>());
        }

        #endregion

        #region Private

        private void SelectField()
        {
            Debug.Log($"Next field: {inputFields[_input].transform.parent.name}");

            inputFields[_input].Select();
        }

        #endregion

        #region Protected

        #endregion
    }
}