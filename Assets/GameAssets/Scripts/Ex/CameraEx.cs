﻿using System.Collections;
using DG.Tweening;
using GameAssets.Scripts.Base;
using GameAssets.Scripts.Manager.Game;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GameAssets.Scripts.Ex
{
    public class CameraEx : BaseSingleton<CameraEx>
    {
        [ShowIf(nameof(followTarget))] [SerializeField] private Transform target;
        [SerializeField] private Transform background;
        [SerializeField] private ParticleSystem confettiFx;

        [Range(0f, 1f)] [SerializeField] private float lerpSpeed = 0.25f;

        [SerializeField] private bool followTarget;

        private Vector3 _originOffset;
        private Vector3 _originalPosition;

        private float _timeAtCurrentFrame;
        private float _timeAtLastFrame;
        private float _fakeDelta;

        private void Start()
        {
            _originalPosition = transform.position;

            if (followTarget)
            {
                _originOffset = transform.position - target.position;
            }

            if (background == null)
            {
                return;
            }

            background.GetComponent<SpriteRenderer>().sprite = GameManager.SelectedMap.icBackground;
            background.ResizeSpriteToScreen();
            
            background.localScale += new Vector3(0.5f, 0.1f, 0.5f);
        }

        public override void PreInnerUpdate()
        {
            _timeAtCurrentFrame = Time.realtimeSinceStartup;
            _fakeDelta = _timeAtCurrentFrame - _timeAtLastFrame;
            _timeAtLastFrame = _timeAtCurrentFrame;
        }

        public override void InnerLateUpdate()
        {
            if (followTarget)
            {
                transform.DOMove(target.position + _originOffset, lerpSpeed);
            }
        }

        public void EnableConfetti()
        {
            if (confettiFx != null)
            {
                confettiFx.SetActive();
            }
        }

        public static void Shake(float duration, float amount)
        {
            Instance.StopAllCoroutines();
            Instance.StartCoroutine(Instance.IEShake(duration, amount));
        }

        public static void StopShake()
        {
            Instance.StopAllCoroutines();
        }

        private IEnumerator IEShake(float duration, float amount)
        {
            var endTime = Time.time + duration;

            while (duration > 0)
            {
                transform.localPosition = _originalPosition + Random.insideUnitSphere * amount;
                duration -= _fakeDelta;
                yield return null;
            }

            transform.localPosition = _originalPosition;
        }
    }
}