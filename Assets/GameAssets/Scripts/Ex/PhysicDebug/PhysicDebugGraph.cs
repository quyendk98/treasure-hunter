﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameAssets.Scripts.Ex.PhysicDebug
{
    [AddComponentMenu("Physic/Physic Debug Graph")]
    public class PhysicDebugGraph : MonoBehaviour
    {
        [HideInInspector] public GameObject go;
        [HideInInspector] public Rigidbody rb;

        [HideInInspector] public new string name = string.Empty;

        [HideInInspector] public List<float> velocityGraph = new List<float>();
        [HideInInspector] public List<float> accelerationGraph = new List<float>();
        [HideInInspector] public List<Vector3> velocityAxisGraph = new List<Vector3>();
        [HideInInspector] public List<Vector3> gGraph = new List<Vector3>();

        [HideInInspector] public Vector3 lastVelocity = Vector3.zero;

        [HideInInspector] public int graphResolution = 100;
        [HideInInspector] public int dataRate = 60;
        [HideInInspector] public int collisionCount;

        [HideInInspector] public bool colliding;

        [HideInInspector] public string collidingObject = string.Empty;

        [HideInInspector] public float collisionForce;
        [HideInInspector] public float minCollisionForce = Mathf.Infinity;
        [HideInInspector] public float maxCollisionForce;

        [HideInInspector] public Vector3 collisionVelocity = Vector3.zero;

        [HideInInspector] public float collisionDuration;
        [HideInInspector] public float noCollisionDuration;

        [HideInInspector] public int triggerCount;
        
        [HideInInspector] public bool triggering;
        
        [HideInInspector] public string triggeringObject = string.Empty;
        
        [HideInInspector] public float triggerDuration;

        private float _lastTriggerTime;

        private float _lastCollideTime;
        private float _lastNoCollideTime;

        public void Initialize()
        {
            go = gameObject;
            rb = go.GetComponent<Rigidbody>();
            name = go.name;
        }

        private int _frameCounter;

        private void FixedUpdate()
        {
            colliding = false;
            triggering = false;
            _frameCounter++;

            if (_frameCounter % (61 - dataRate) != 0)
            {
                return;
            }

            _frameCounter = 0;
            
            GetData();
        }

        private void OnCollisionEnter(Collision collisionInfo)
        {
            collisionCount++;
            collisionForce = collisionInfo.relativeVelocity.magnitude;
            collisionVelocity = collisionInfo.relativeVelocity;
            collidingObject = collisionInfo.gameObject.name;
            _lastCollideTime = Time.time;
            _lastNoCollideTime = Time.time;

            if (collisionForce < minCollisionForce)
            {
                minCollisionForce = collisionForce;
            }

            if (collisionForce > maxCollisionForce)
            {
                maxCollisionForce = collisionForce;
            }
        }

        private void OnCollisionStay(Collision other)
        {
            colliding = true;
            collidingObject = other.gameObject.name;
            collisionDuration = Time.time - _lastCollideTime;
            _lastNoCollideTime = Time.time;
        }

        private void OnCollisionExit()
        {
            collisionDuration = Time.time - _lastCollideTime;
            _lastNoCollideTime = Time.time;
        }

        private void OnTriggerEnter(Collider other)
        {
            triggerCount++;
            triggeringObject = other.name;
            _lastTriggerTime = Time.time;
        }

        private void OnTriggerStay(Collider other)
        {
            triggering = true;
            triggeringObject = other.name;
            triggerDuration = Time.time - _lastTriggerTime;
        }

        private void OnTriggerExit(Collider other)
        {
            triggerDuration = Time.time - _lastTriggerTime;
        }

        [HideInInspector] public float minSpeed = Mathf.Infinity;
        [HideInInspector] public float maxSpeed;

        [HideInInspector] public Vector3 currentVelocity = Vector3.zero;
        [HideInInspector] public Vector3 currentAcceleration = Vector3.zero;

        [HideInInspector] public Vector3 minVelocity = Vector3.zero;
        [HideInInspector] public Vector3 maxVelocity = Vector3.zero;

        public void GetData()
        {
            if (!rb)
            {
                return;
            }

            currentVelocity = rb.velocity;
            currentAcceleration = lastVelocity - currentVelocity;

            if (accelerationGraph.Count > graphResolution)
            {
                accelerationGraph.RemoveRange(0, accelerationGraph.Count - graphResolution);
            }

            if (velocityGraph.Count > 0)
            {
                accelerationGraph.Add(velocityGraph.Last() - currentVelocity.magnitude);
            }
            else
            {
                accelerationGraph.Add(0f);
            }

            if (velocityGraph.Count > graphResolution)
            {
                velocityGraph.RemoveRange(0, velocityGraph.Count - graphResolution);
            }

            velocityGraph.Add(currentVelocity.magnitude);

            if (velocityAxisGraph.Count > graphResolution)
            {
                velocityAxisGraph.RemoveRange(0, velocityAxisGraph.Count - graphResolution);
            }

            velocityAxisGraph.Add(currentVelocity);

            if (gGraph.Count > graphResolution)
            {
                gGraph.RemoveRange(0, gGraph.Count - graphResolution);
            }

            gGraph.Add(currentAcceleration);

            if (currentVelocity.magnitude < minSpeed)
            {
                minSpeed = currentVelocity.magnitude;
            }

            if (currentVelocity.magnitude > maxSpeed)
            {
                maxSpeed = currentVelocity.magnitude;
            }

            if (currentVelocity.x < minVelocity.x)
            {
                minVelocity.x = currentVelocity.x;
            }

            if (currentVelocity.y < minVelocity.y)
            {
                minVelocity.y = currentVelocity.y;
            }

            if (currentVelocity.z < minVelocity.z)
            {
                minVelocity.z = currentVelocity.z;
            }

            if (currentVelocity.x > maxVelocity.x)
            {
                maxVelocity.x = currentVelocity.x;
            }

            if (currentVelocity.y > maxVelocity.y)
            {
                maxVelocity.y = currentVelocity.y;
            }

            if (currentVelocity.z > maxVelocity.z)
            {
                maxVelocity.z = currentVelocity.z;
            }

            noCollisionDuration = Time.time - _lastNoCollideTime;
            lastVelocity = currentVelocity;
        }
    }
}