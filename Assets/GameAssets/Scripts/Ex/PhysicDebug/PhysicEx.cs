using System;
using UnityEngine;
using UEPhysics = UnityEngine.Physics;
using UEPhysics2D = UnityEngine.Physics2D;

namespace GameAssets.Scripts.Ex.PhysicDebug
{
    public enum PreviewCondition
    {
        None,
        Editor,
        Game,
        Both
    }

    /// <summary>
    /// This is an extension for UnityEngine.Physics, it have all the cast, overlap, and checks with an option to preview them.
    /// </summary>
    public static class Physic
    {
        #region Unity Engine Physics

        private static readonly Quaternion Orientation = default;

        private const float MAX_DISTANCE = Mathf.Infinity;

        private const int LAYER_MASK = -1;

        private const QueryTriggerInteraction INTERACTION = QueryTriggerInteraction.UseGlobal;

        private static readonly Color CastColor = new Color(1, 0.5f, 0, 1);

        private static Color TrueColor(Func<bool> condition, Color? a = null, Color? b = null)
        {
            if (condition())
            {
                return a ?? Color.green;
            }

            return b ?? Color.red;
        }

        #region BoxCast

        #region Boxcast single

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out _, Orientation,
                MAX_DISTANCE, LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            Quaternion orientation, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out _, orientation, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            out RaycastHit rayInfo, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out rayInfo, Orientation,
                MAX_DISTANCE, LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            Quaternion orientation, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out _, orientation, maxDistance,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            out RaycastHit rayInfo, Quaternion orientation,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out rayInfo, orientation, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            Quaternion orientation, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out _, orientation, maxDistance,
                layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            out RaycastHit rayInfo, Quaternion orientation, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out rayInfo, orientation, distance,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            Quaternion orientation, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out _, orientation, maxDistance,
                layerMask, queryTriggerInteraction, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            out RaycastHit rayInfo, Quaternion orientation, float distance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(center, halfExtents, direction, out rayInfo, orientation, distance,
                layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction,
            out RaycastHit hitInfo, Quaternion orientation, float distance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.BoxCast(center, halfExtents, direction, out hitInfo,
                orientation, distance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            if (collided)
            {
                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);

                distance = hitInfo.distance;
            }

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(center, center + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(center, center + direction * distance, color, duration, true);
            }

            color = TrueColor(() => collided, hitColor, CastColor);

            DebugEx.DrawBox(center, halfExtents, CastColor, orientation, duration, preview, depth);
            DebugEx.DrawBox(center + direction * distance, halfExtents, color, orientation, duration, preview, depth);

            return collided;
        }

        #endregion

        #region Boxcast all

        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents,
            Vector3 direction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(center, halfExtents, direction, Orientation, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents,
            Vector3 direction, Quaternion orientation,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(center, halfExtents, direction, orientation, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents,
            Vector3 direction, Quaternion orientation, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(center, halfExtents, direction, orientation, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents,
            Vector3 direction, Quaternion orientation, float maxDistance, LayerMask layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(center, halfExtents, direction, orientation, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] BoxCastAll(Vector3 center, Vector3 halfExtents,
            Vector3 direction, Quaternion orientation, float maxDistance, LayerMask layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics.BoxCastAll(center, halfExtents, direction, orientation,
                maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            var collided = false;
            float maxDistanceRay = 0;

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawBox(center + direction * hit.distance, halfExtents,
                    hitColor ?? Color.green, orientation, duration, preview, depth);
            }

            var end = center + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(center, end, color, duration);
                Debug.DrawLine(end, center + direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(center, end, color, duration, true);
                GLDebug.DrawLine(end, center + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            DebugEx.DrawBox(center, halfExtents, CastColor, orientation, duration, preview, depth);

            DebugEx.DrawBox(center + direction * maxDistance, halfExtents,
                CastColor, orientation, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #region Boxcast non alloc

        public static int BoxCastNonAlloc(Vector3 center, Vector3 halfExtents, Vector3 direction,
            RaycastHit[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(center, halfExtents, direction, results, Orientation,
                MAX_DISTANCE, LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector3 center, Vector3 halfExtents, Vector3 direction,
            RaycastHit[] results, Quaternion orientation,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(center, halfExtents, direction, results, orientation,
                MAX_DISTANCE, LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector3 center, Vector3 halfExtents, Vector3 direction,
            RaycastHit[] results, Quaternion orientation, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(center, halfExtents, direction, results, orientation,
                maxDistance, LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector3 center, Vector3 halfExtents, Vector3 direction,
            RaycastHit[] results, Quaternion orientation, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(center, halfExtents, direction, results, orientation,
                maxDistance, layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector3 center, Vector3 halfExtents, Vector3 direction,
            RaycastHit[] results, Quaternion orientation, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.BoxCastNonAlloc(center, halfExtents, direction, results,
                orientation, maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var collided = false;
            float maxDistanceRay = 0;

            for (var i = 0; i < size; i++)
            {
                var hit = results[i];

                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawBox(center + direction * hit.distance, halfExtents,
                    hitColor ?? Color.green, orientation, duration, preview, depth);
            }

            var end = center + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(center, end, color, duration);
                Debug.DrawLine(end, center + direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(center, end, color, duration, true);
                GLDebug.DrawLine(end, center + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            DebugEx.DrawBox(center, halfExtents, CastColor, orientation, duration, preview, depth);

            DebugEx.DrawBox(center + direction * maxDistance, halfExtents,
                CastColor, orientation, duration, preview, depth);

            return size;
        }

        #endregion

        #endregion

        #region Capsule Cast

        #region Capsulecast single

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out _, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out _, maxDistance,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, out RaycastHit hitInfo,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out hitInfo, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out _, maxDistance,
                layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, out RaycastHit hitInfo, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out hitInfo, maxDistance,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out _, maxDistance,
                layerMask, queryTriggerInteraction, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(point1, point2, radius, direction, out hitInfo, maxDistance,
                layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CapsuleCast(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, out RaycastHit hitInfo, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.CapsuleCast(point1, point2, radius, direction, out hitInfo,
                maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            if (collided)
            {
                maxDistance = hitInfo.distance;

                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);
            }

            var midPoint = (point2 + point1) / 2;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            DebugEx.DrawCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(midPoint, midPoint + direction * maxDistance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(midPoint, midPoint + direction * maxDistance, color, duration, true);
            }

            color = TrueColor(() => collided, hitColor, CastColor);

            DebugEx.DrawCapsule(point1 + direction * maxDistance, point2 + direction * maxDistance,
                color, radius, duration, preview, depth);

            return collided;
        }

        #endregion

        #region Capsulecast all

        public static RaycastHit[] CapsuleCastAll(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(point1, point2, radius, direction, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] CapsuleCastAll(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(point1, point2, radius, direction, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] CapsuleCastAll(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(point1, point2, radius, direction, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] CapsuleCastAll(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics.CapsuleCastAll(point1, point2, radius, direction,
                maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            var collided = false;
            float maxDistanceRay = 0;

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawCapsule(point1 + direction * hit.distance, point2 + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            var midPoint = (point2 + point1) / 2;

            DebugEx.DrawCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            var endPoint = midPoint + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(midPoint, endPoint, color, duration);
                Debug.DrawLine(endPoint, midPoint + direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(midPoint, endPoint, color, duration, true);
                GLDebug.DrawLine(endPoint, midPoint + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawCapsule(point1 + direction * maxDistance, point2 + direction * maxDistance,
                CastColor, radius, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #region Capsulecast non alloc

        public static int CapsuleCastNonAlloc(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, RaycastHit[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(point1, point2, radius, direction, results, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, RaycastHit[] results, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(point1, point2, radius, direction, results, maxDistance,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, RaycastHit[] results, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(point1, point2, radius, direction, results, maxDistance,
                layerMask, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector3 point1, Vector3 point2, float radius,
            Vector3 direction, RaycastHit[] results, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.CapsuleCastNonAlloc(point1, point2, radius, direction, results,
                maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var collided = false;
            float maxDistanceRay = 0;

            for (var i = 0; i < size; i++)
            {
                collided = true;

                var hit = results[i];

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawCapsule(point1 + direction * hit.distance, point2 + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            var midPoint = (point2 + point1) / 2;

            DebugEx.DrawCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            var endPoint = midPoint + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(midPoint, endPoint, color, duration);
                Debug.DrawLine(endPoint, midPoint + direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(midPoint, endPoint, color, duration, true);
                Debug.DrawLine(endPoint, midPoint + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawCapsule(point1 + direction * maxDistance, point2 + direction * maxDistance,
                CastColor, radius, duration, preview, depth);

            return size;
        }

        #endregion

        #endregion

        #region Check Box

        public static bool CheckBox(Vector3 center, Vector3 halfExtents,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckBox(center, halfExtents, Orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CheckBox(Vector3 center, Vector3 halfExtents, Quaternion orientation,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckBox(center, halfExtents, orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CheckBox(Vector3 center, Vector3 halfExtents, Quaternion orientation,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckBox(center, halfExtents, orientation, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool CheckBox(Vector3 center, Vector3 halfExtents, Quaternion orientation,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.CheckBox(center, halfExtents, orientation, layerMask, queryTriggerInteraction);
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawBox(center, halfExtents, color, orientation, duration, preview, depth);
            }

            return collided;
        }

        #endregion

        #region Check Capsule

        public static bool CheckCapsule(Vector3 start, Vector3 end, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckCapsule(start, end, radius, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool CheckCapsule(Vector3 start, Vector3 end, float radius, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckCapsule(start, end, radius, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool CheckCapsule(Vector3 start, Vector3 end, float radius, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.CheckCapsule(start, end, radius, layerMask, queryTriggerInteraction);
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawCapsule(start, end, color, radius, duration, preview, depth);
            }

            return collided;
        }

        #endregion

        #region Check Sphere

        public static bool CheckSphere(Vector3 pos, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckSphere(pos, radius, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool CheckSphere(Vector3 pos, float radius, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CheckSphere(pos, radius, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool CheckSphere(Vector3 pos, float radius, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.CheckSphere(pos, radius, layerMask, queryTriggerInteraction);
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawWireSphere(pos, color, radius, duration, preview, depth);
            }

            return collided;
        }

        #endregion

        #region Linecast

        public static bool Linecast(Vector3 start, Vector3 end, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, out _, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Linecast(Vector3 start, Vector3 end, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, out _, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool Linecast(Vector3 start, Vector3 end, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, out _, layerMask, queryTriggerInteraction, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool Linecast(Vector3 start, Vector3 end, out RaycastHit hitInfo,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, out hitInfo, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Linecast(Vector3 start, Vector3 end, out RaycastHit hitInfo,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, out hitInfo, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static bool Linecast(Vector3 start, Vector3 end, out RaycastHit hitInfo,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.Linecast(start, end, out hitInfo, layerMask, queryTriggerInteraction);
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            if (collided)
            {
                end = hitInfo.point;

                DebugEx.DrawPoint(end, Color.red, 0.5f, duration, preview, depth);
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(start, end, color, duration);
            }
            else
            {
                GLDebug.DrawLine(start, end, color, duration);
            }

            return collided;
        }

        #endregion

        #region Overlap Box

        #region OverlapBox alloc

        public static Collider[] OverlapBox(Vector3 center, Vector3 halfExtents,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(center, halfExtents, Orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapBox(Vector3 center, Vector3 halfExtents,
            Quaternion orientation, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(center, halfExtents, orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapBox(Vector3 center, Vector3 halfExtents,
            Quaternion orientation, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(center, halfExtents, orientation, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapBox(Vector3 center, Vector3 halfExtents,
            Quaternion orientation, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics.OverlapBox(center, halfExtents, orientation, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            var color = TrueColor(() => colliders.Length > 0, hitColor, noHitColor);

            DebugEx.DrawBox(center, halfExtents, color, orientation, duration, preview, depth);
            return colliders;
        }

        #endregion

        #region OverlapBox non alloc

        public static int OverlapBoxNonAlloc(Vector3 center, Vector3 halfExtents,
            Collider[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(center, halfExtents, results, Orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector3 center, Vector3 halfExtents,
            Collider[] results, Quaternion orientation,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(center, halfExtents, results, orientation, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector3 center, Vector3 halfExtents,
            Collider[] results, Quaternion orientation, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(center, halfExtents, results, orientation, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector3 center, Vector3 halfExtents,
            Collider[] results, Quaternion orientation, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.OverlapBoxNonAlloc(center, halfExtents, results, orientation,
                layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var color = TrueColor(() => size > 0, hitColor, noHitColor);

            DebugEx.DrawBox(center, halfExtents, color, orientation, duration, preview, depth);
            return size;
        }

        #endregion

        #endregion

        #region Overlap Capsule

        #region OverlapCapsule alloc

        public static Collider[] OverlapCapsule(Vector3 point0, Vector3 point1, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsule(point0, point1, radius, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapCapsule(Vector3 point0, Vector3 point1, float radius,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsule(point0, point1, radius, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapCapsule(Vector3 point0, Vector3 point1, float radius,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics.OverlapCapsule(point0, point1, radius, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            var color = TrueColor(() => colliders.Length > 0, hitColor, noHitColor);

            DebugEx.DrawCapsule(point0, point1, color, radius, duration, preview, depth);
            return colliders;
        }

        #endregion

        #region OverlapCapsule non alloc

        public static int OverlapCapsuleNonAlloc(Vector3 point0, Vector3 point1, float radius,
            Collider[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleNonAlloc(point0, point1, radius, results, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCapsuleNonAlloc(Vector3 point0, Vector3 point1, float radius,
            Collider[] results, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleNonAlloc(point0, point1, radius, results, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCapsuleNonAlloc(Vector3 point0, Vector3 point1, float radius,
            Collider[] results, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.OverlapCapsuleNonAlloc(point0, point1, radius, results, layerMask,
                queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var color = TrueColor(() => size > 0, hitColor, noHitColor);

            DebugEx.DrawCapsule(point0, point1, color, radius, duration, preview, depth);
            return size;
        }

        #endregion

        #endregion

        #region Overlap Sphere

        #region OverlapSphere alloc

        public static Collider[] OverlapSphere(Vector3 pos, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapSphere(pos, radius, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapSphere(Vector3 pos, float radius, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapSphere(pos, radius, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider[] OverlapSphere(Vector3 pos, float radius, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics.OverlapSphere(pos, radius, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            var color = TrueColor(() => colliders.Length > 0, hitColor, noHitColor);

            DebugEx.DrawWireSphere(pos, color, radius, duration, preview, depth);
            return colliders;
        }

        #endregion

        #region OverlapSphere non alloc

        public static int OverlapSphereNonAlloc(Vector3 pos, float radius, Collider[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapSphereNonAlloc(pos, radius, results, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapSphereNonAlloc(Vector3 pos, float radius, Collider[] results,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapSphereNonAlloc(pos, radius, results, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapSphereNonAlloc(Vector3 pos, float radius, Collider[] results,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.OverlapSphereNonAlloc(pos, radius, results, layerMask,
                queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var color = TrueColor(() => size > 0, hitColor, noHitColor);

            DebugEx.DrawWireSphere(pos, color, radius, duration, preview, depth);
            return size;
        }

        #endregion

        #endregion

        #region Raycast

        #region Raycast single

        #region Vector3

        public static bool Raycast(Vector3 origin, Vector3 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, out _, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, out _, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, float maxDistance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, out _, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, float maxDistance,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, out _, maxDistance, layerMask,
                queryTriggerInteraction, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, out hitInfo, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo,
            float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return Raycast(origin, direction, out hitInfo, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo,
            float maxDistance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return Raycast(origin, direction, out hitInfo, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Vector3 origin, Vector3 direction, out RaycastHit hitInfo,
            float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.Raycast(origin, direction, out hitInfo, maxDistance,
                layerMask, queryTriggerInteraction);

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            var end = origin + direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);

            if (collided)
            {
                end = hitInfo.point;

                DebugEx.DrawPoint(end, Color.red, 0.5f, duration, preview, depth);
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration);
            }

            return collided;
        }

        #endregion

        #region Ray

        public static bool Raycast(Ray ray, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out _, MAX_DISTANCE, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out _, maxDistance, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, out RaycastHit hitInfo,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out hitInfo, MAX_DISTANCE, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out _, maxDistance, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out hitInfo, maxDistance, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out _, maxDistance, layerMask, queryTriggerInteraction,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(ray, out hitInfo, maxDistance, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance,
            LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.Raycast(ray, out hitInfo, maxDistance, layerMask, queryTriggerInteraction);
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            var end = ray.origin + ray.direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);

            if (collided)
            {
                end = hitInfo.point;

                DebugEx.DrawPoint(end, Color.red, 0.5f, duration, preview, depth);
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(ray.origin, end, color, duration);
            }
            else
            {
                GLDebug.DrawLine(ray.origin, end, color, duration);
            }

            return collided;
        }

        #endregion

        #endregion

        #region Raycast all

        #region Vector3

        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction, float maxDistance,
            LayerMask layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, maxDistance, (int) layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Vector3 origin, Vector3 direction, float maxDistance,
            LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var raycastInfo = UEPhysics.RaycastAll(origin, direction, maxDistance,
                layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return raycastInfo;
            }

            var end = origin + direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);
            var previewOrigin = origin;
            var sectionOrigin = origin;

            foreach (var hit in raycastInfo)
            {
                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }

                if ((origin - hit.point).sqrMagnitude > (origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = hit.point;
                }

                sectionOrigin = hit.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }

            return raycastInfo;
        }

        #endregion

        #region Ray

        public static RaycastHit[] RaycastAll(Ray ray,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(ray, MAX_DISTANCE, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Ray ray, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(ray, maxDistance, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Ray ray, float maxDistance, LayerMask layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(ray, maxDistance, (int) layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] RaycastAll(Ray ray, float maxDistance, LayerMask layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var raycastInfo = UEPhysics.RaycastAll(ray, maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return raycastInfo;
            }

            var end = ray.origin + ray.direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);
            var previewOrigin = ray.origin;
            var sectionOrigin = ray.origin;

            foreach (var hit in raycastInfo)
            {
                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }

                if ((ray.origin - hit.point).sqrMagnitude > (ray.origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = hit.point;
                }

                sectionOrigin = hit.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }

            return raycastInfo;
        }

        #endregion

        #endregion

        #region Raycast non alloc

        #region Vector3

        public static int RaycastNonAlloc(Vector3 origin, Vector3 direction, RaycastHit[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector3 origin, Vector3 direction, RaycastHit[] results,
            float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector3 origin, Vector3 direction, RaycastHit[] results,
            float maxDistance, LayerMask layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector3 origin, Vector3 direction, RaycastHit[] results,
            float maxDistance, LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.RaycastNonAlloc(origin, direction, results, maxDistance, layerMask,
                queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var end = origin + direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);
            var previewOrigin = origin;
            var sectionOrigin = origin;

            for (var i = 0; i < size; i++)
            {
                var hit = results[i];

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }

                if ((origin - hit.point).sqrMagnitude > (origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = hit.point;
                }

                sectionOrigin = hit.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }

            return size;
        }

        #endregion

        #region Ray

        public static int RaycastNonAlloc(Ray ray, RaycastHit[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(ray, results, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Ray ray, RaycastHit[] results, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(ray, results, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Ray ray, RaycastHit[] results, float maxDistance,
            LayerMask layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return RaycastNonAlloc(ray, results, maxDistance, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Ray ray, RaycastHit[] results, float maxDistance,
            LayerMask layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.RaycastNonAlloc(ray, results, maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var end = ray.origin + ray.direction * (maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance);
            var previewOrigin = ray.origin;
            var sectionOrigin = ray.origin;

            for (var i = 0; i < size; i++)
            {
                var hit = results[i];

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(sectionOrigin, hit.point, hitColor ?? Color.green, duration);
                }

                if ((ray.origin - hit.point).sqrMagnitude > (ray.origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = hit.point;
                }

                sectionOrigin = hit.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, end, noHitColor ?? Color.red, duration);
            }

            return size;
        }

        #endregion

        #endregion

        #endregion

        #region Sphere Cast

        #region Spherecast single

        #region Vector3

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(origin, radius, direction, out _, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return SphereCast(origin, radius, direction, out _, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return SphereCast(origin, radius, direction, out _, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(origin, radius, direction, out _, maxDistance, layerMask,
                queryTriggerInteraction, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            out RaycastHit hitInfo, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return SphereCast(origin, radius, direction, out hitInfo, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            out RaycastHit hitInfo, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(origin, radius, direction, out hitInfo, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            out RaycastHit hitInfo, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(origin, radius, direction, out hitInfo, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Vector3 origin, float radius, Vector3 direction,
            out RaycastHit hitInfo, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.SphereCast(origin, radius, direction, out hitInfo,
                maxDistance, layerMask, queryTriggerInteraction);

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            if (collided)
            {
                maxDistance = hitInfo.distance;

                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);
            }

            DebugEx.DrawWireSphere(origin, CastColor, radius, duration, preview, depth);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * maxDistance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * maxDistance, color, duration, true);
            }

            color = TrueColor(() => collided, hitColor, CastColor);

            DebugEx.DrawWireSphere(origin + direction * maxDistance, color, radius, duration, preview, depth);
            return collided;
        }

        #endregion

        #region Ray

        public static bool SphereCast(Ray ray, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out _, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out _, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, out RaycastHit hitInfo,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out hitInfo, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out _, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, out RaycastHit hitInfo,
            float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out hitInfo, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCast(ray, radius, out _, maxDistance, layerMask,
                queryTriggerInteraction, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, out RaycastHit hitInfo,
            float maxDistance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            return SphereCast(ray, radius, out hitInfo, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static bool SphereCast(Ray ray, float radius, out RaycastHit hitInfo,
            float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collided = UEPhysics.SphereCast(ray, radius, out hitInfo, maxDistance,
                layerMask, queryTriggerInteraction);

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.None)
            {
                return collided;
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            if (collided)
            {
                maxDistance = hitInfo.distance;

                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);
            }

            DebugEx.DrawWireSphere(ray.origin, CastColor, radius, duration, preview, depth);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(ray.origin, ray.origin + ray.direction * maxDistance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(ray.origin, ray.origin + ray.direction * maxDistance, color, duration, true);
            }

            color = TrueColor(() => collided, hitColor, CastColor);

            DebugEx.DrawWireSphere(ray.origin + ray.direction * maxDistance, color, radius, duration, preview, depth);
            return collided;
        }

        #endregion

        #endregion

        #region Spherecast all

        #region Vector3

        public static RaycastHit[] SphereCastAll(Vector3 origin, float radius, Vector3 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(origin, radius, direction, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(origin, radius, direction, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(origin, radius, direction, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Vector3 origin, float radius, Vector3 direction,
            float maxDistance, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics.SphereCastAll(origin, radius, direction, maxDistance,
                layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            var collided = false;
            float maxDistanceRay = 0;

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawWireSphere(origin + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            DebugEx.DrawWireSphere(origin, CastColor, radius, duration, preview, depth);

            var end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, origin + direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                GLDebug.DrawLine(end, origin + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawWireSphere(origin + direction * maxDistance, CastColor, radius, duration, preview, depth);
            return hitInfo;
        }

        #endregion

        #region Ray

        public static RaycastHit[] SphereCastAll(Ray ray, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(ray, radius, MAX_DISTANCE, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Ray ray, float radius, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(ray, radius, maxDistance, LAYER_MASK, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Ray ray, float radius, float maxDistance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastAll(ray, radius, maxDistance, layerMask, INTERACTION,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit[] SphereCastAll(Ray ray, float radius, float maxDistance,
            int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics.SphereCastAll(ray, radius, maxDistance, layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            var collided = false;
            float maxDistanceRay = 0;

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawWireSphere(ray.origin + ray.direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            DebugEx.DrawWireSphere(ray.origin, CastColor, radius, duration, preview, depth);

            var end = ray.origin + ray.direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(ray.origin, end, color, duration);
                Debug.DrawLine(end, ray.origin + ray.direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(ray.origin, end, color, duration, true);
                GLDebug.DrawLine(end, ray.origin + ray.direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawWireSphere(ray.origin + ray.direction * maxDistance, CastColor,
                radius, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #endregion

        #region Spherecast non alloc

        #region Vector3

        public static int SphereCastNonAlloc(Vector3 origin, float radius, Vector3 direction,
            RaycastHit[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(origin, radius, direction, results, MAX_DISTANCE,
                LAYER_MASK, INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Vector3 origin, float radius, Vector3 direction,
            RaycastHit[] results, float maxDistance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(origin, radius, direction, results, maxDistance, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Vector3 origin, float radius, Vector3 direction,
            RaycastHit[] results, float maxDistance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(origin, radius, direction, results, maxDistance, layerMask,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Vector3 origin, float radius, Vector3 direction,
            RaycastHit[] results, float maxDistance, int layerMask,
            QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.SphereCastNonAlloc(origin, radius, direction, results, maxDistance,
                layerMask, queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var collided = false;
            float maxDistanceRay = 0;

            foreach (var hit in results)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawWireSphere(origin + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            maxDistance = maxDistance.Equals(MAX_DISTANCE) ? 1000 * 1000 : maxDistance;

            DebugEx.DrawWireSphere(origin, CastColor, radius, duration, preview, depth);

            var end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, direction * maxDistance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                Debug.DrawLine(end, origin + direction * maxDistance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawWireSphere(origin + direction * maxDistance, CastColor, radius, duration, preview, depth);
            return size;
        }

        #endregion

        #region Ray

        public static int SphereCastNonAlloc(Ray ray, float radius, RaycastHit[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(ray, radius, results, MAX_DISTANCE, LAYER_MASK,
                INTERACTION, preview, duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Ray ray, float radius, RaycastHit[] results,
            float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(ray, radius, results, distance, LAYER_MASK, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Ray ray, float radius, RaycastHit[] results,
            float distance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return SphereCastNonAlloc(ray, radius, results, distance, layerMask, INTERACTION, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static int SphereCastNonAlloc(Ray ray, float radius, RaycastHit[] results,
            float distance, int layerMask, QueryTriggerInteraction queryTriggerInteraction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics.SphereCastNonAlloc(ray, radius, results, distance, layerMask,
                queryTriggerInteraction);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var collider = false;
            float maxDistanceRay = 0;

            foreach (var hit in results)
            {
                collider = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawWireSphere(ray.origin + ray.direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            DebugEx.DrawWireSphere(ray.origin, CastColor, radius, duration, preview, depth);

            var end = ray.origin + ray.direction * maxDistanceRay;
            var color = TrueColor(() => collider, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(ray.origin, end, color, duration);
                Debug.DrawLine(end, ray.origin + ray.direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(ray.origin, end, color, duration, true);
                Debug.DrawLine(end, ray.origin + ray.direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawWireSphere(ray.origin + ray.direction * distance, CastColor, radius, duration, preview, depth);
            return size;
        }

        #endregion

        #endregion

        #endregion

        #endregion
    }

    /// <summary>
    /// This is an extension for UnityEngine.Physics2D, it have all the cast and overlap with an option to preview them.
    /// </summary>
    public static class Physic2D
    {
        #region Unity Engine Physics

        //Global variables for use on default values, this is left here so that it can be changed easily
        private const float MAX_DISTANCE = Mathf.Infinity;
        private static readonly Color CastColor = new Color(1, 0.5f, 0, 1);

        private static Color TrueColor(Func<bool> condition, Color? a = null, Color? b = null)
        {
            if (condition())
            {
                return a ?? Color.green;
            }

            return b ?? Color.red;
        }

        #region BoxCast

        #region BoxCast Single

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(origin, size, angle, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(origin, size, angle, direction, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, LayerMask layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(origin, size, angle, direction, distance, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, LayerMask layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCast(origin, size, angle, direction, distance, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, LayerMask layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hit = UEPhysics2D.BoxCast(origin, size, angle, direction, distance,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hit;
            }

            size /= 2;
            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            var collided = hit.collider != null;
            var rot = Quaternion.Euler(0, 0, angle);

            if (collided)
            {
                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                distance = hit.distance;
            }

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * distance, color, duration, true);
            }

            color = TrueColor(() => collided, hitColor, CastColor);

            DebugEx.DrawBox(origin, size, CastColor, rot, duration, preview, depth);
            DebugEx.DrawBox(origin + direction * distance, size, color, rot, duration, preview, depth);

            return hit;
        }

        public static int BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results)
        {
            return BoxCast(origin, size, angle, direction, contactFilter, results, MAX_DISTANCE);
        }

        public static int BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.BoxCast(origin, size, angle, direction, contactFilter, results, distance);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            size /= 2;

            float maxDistanceRay = 0;
            var rot = Quaternion.Euler(0, 0, angle);

            foreach (var hit in results)
            {
                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawBox(origin + direction * hit.distance, size,
                    hitColor ?? Color.green, rot, duration, preview, depth);
            }

            Vector3 end = origin + direction * maxDistanceRay;
            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, noHitColor ?? Color.red, duration);
                Debug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, noHitColor ?? Color.red, duration, true);
                GLDebug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawBox(origin, size, CastColor, rot, duration, preview, depth);

            DebugEx.DrawBox(origin + direction * distance, size,
                CastColor, rot, duration, preview, depth);

            return count;
        }

        #endregion

        #region BoxCast All

        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(origin, size, angle, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(origin, size, angle, direction, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(origin, size, angle, direction, distance, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastAll(origin, size, angle, direction, distance, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics2D.BoxCastAll(origin, size, angle, direction, distance,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            var collided = false;
            float maxDistanceRay = 0;

            size /= 2;

            var rot = Quaternion.Euler(0, 0, angle);

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawBox(origin + direction * hit.distance, size,
                    hitColor ?? Color.green, rot, duration, preview, depth);
            }

            Vector3 end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                GLDebug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawBox(origin, size, CastColor, rot, duration, preview, depth);
            DebugEx.DrawBox(origin + direction * distance, size, CastColor, rot, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #region BoxCast non alloc

        public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, RaycastHit2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(origin, size, angle, direction, results, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(origin, size, angle, direction, results, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, RaycastHit2D[] results, float distance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(origin, size, angle, direction, results, distance, layerMask,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, RaycastHit2D[] results, float distance, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return BoxCastNonAlloc(origin, size, angle, direction, results, distance, layerMask,
                minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle,
            Vector2 direction, RaycastHit2D[] results, float distance, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.BoxCastNonAlloc(origin, size, angle, direction, results,
                distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            var collided = false;
            float maxDistanceRay = 0;

            size /= 2;

            var rot = Quaternion.Euler(0, 0, angle);

            for (var i = 0; i < count; i++)
            {
                var hit = results[i];
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawBox(origin + direction * hit.distance, size,
                    hitColor ?? Color.green, rot, duration, preview, depth);
            }

            Vector3 end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                GLDebug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawBox(origin, size, CastColor, rot, duration, preview, depth);
            DebugEx.DrawBox(origin + direction * distance, size, CastColor, rot, duration, preview, depth);

            return count;
        }

        #endregion

        #endregion

        #region CapsuleCast

        #region CapsuleCast single

        public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(origin, size, capsuleDirection, angle, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(origin, size, capsuleDirection, angle, direction, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(origin, size, capsuleDirection, angle, direction, distance,
                layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(origin, size, capsuleDirection, angle, direction, distance,
                layerMask, minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hit = UEPhysics2D.CapsuleCast(origin, size, capsuleDirection, angle,
                direction, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hit;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;
            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (hit.collider != null)
            {
                distance = hit.distance;

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);
            }

            if (capsuleDirection == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + origin;
            point2 = (Vector2) (rot * point2) + origin;

            var color = TrueColor(() => hit, hitColor, noHitColor);

            DebugEx.Draw1SidedCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * distance, color, duration, true);
            }

            color = TrueColor(() => hit, hitColor, CastColor);

            DebugEx.Draw1SidedCapsule(point1 + direction * distance, point2 + direction * distance,
                color, radius, duration, preview, depth);

            return hit;
        }

        public static int CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCast(origin, size, capsuleDirection, angle, direction, contactFilter,
                results, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCast(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.CapsuleCast(origin, size, capsuleDirection, angle, direction,
                contactFilter, results, distance);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;
            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (capsuleDirection == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + origin;
            point2 = (Vector2) (rot * point2) + origin;

            DebugEx.Draw1SidedCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            foreach (var hitInfo in results)
            {
                if (hitInfo.collider == null)
                {
                    continue;
                }

                distance = hitInfo.distance;

                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);
            }

            var color = TrueColor(() => count != 0, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * distance, color, duration, true);
            }

            color = TrueColor(() => count != 0, hitColor, CastColor);

            DebugEx.Draw1SidedCapsule(point1 + direction * distance, point2 + direction * distance,
                color, radius, duration, preview, depth);

            return count;
        }

        #endregion

        #region CapsuleCast All

        public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(origin, size, capsuleDirection, angle, direction, MAX_DISTANCE,
                UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, preview, duration,
                hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(origin, size, capsuleDirection, angle, direction, distance,
                UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, preview, duration,
                hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(origin, size, capsuleDirection, angle, direction, distance,
                layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastAll(origin, size, capsuleDirection, angle, direction, distance,
                layerMask, minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics2D.CapsuleCastAll(origin, size, capsuleDirection,
                angle, direction, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;
            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);
            var collided = false;
            float maxDistanceRay = 0;

            if (capsuleDirection == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + origin;
            point2 = (Vector2) (rot * point2) + origin;

            foreach (var hit in hitInfo)
            {
                collided = true;

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.Draw1SidedCapsule(point1 + direction * hit.distance, point2 + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            var midPoint = (point2 + point1) / 2;

            DebugEx.Draw1SidedCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            Vector3 endPoint = midPoint + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(midPoint, endPoint, color, duration);
                Debug.DrawLine(endPoint, midPoint + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(midPoint, endPoint, color, duration, true);
                GLDebug.DrawLine(endPoint, midPoint + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.Draw1SidedCapsule(point1 + direction * distance, point2 + direction * distance,
                CastColor, radius, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #region CapsuleCast non alloc

        public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            RaycastHit2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(origin, size, capsuleDirection, angle, direction, results, MAX_DISTANCE,
                UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(origin, size, capsuleDirection, angle, direction, results,
                distance, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(origin, size, capsuleDirection, angle, direction, results, distance, layerMask,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CapsuleCastNonAlloc(origin, size, capsuleDirection, angle, direction, results,
                distance, layerMask, minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size,
            CapsuleDirection2D capsuleDirection, float angle, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.CapsuleCastNonAlloc(origin, size, capsuleDirection, angle,
                direction, results, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            var collided = false;
            float maxDistanceRay = 0;

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;
            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (capsuleDirection == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + origin;
            point2 = (Vector2) (rot * point2) + origin;

            for (var i = 0; i < count; i++)
            {
                collided = true;

                var hit = results[i];

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.Draw1SidedCapsule(point1 + direction * hit.distance, point2 + direction * hit.distance,
                    hitColor ?? Color.green, radius, duration, preview, depth);
            }

            var midPoint = (point2 + point1) / 2;

            DebugEx.Draw1SidedCapsule(point1, point2, CastColor, radius, duration, preview, depth);

            Vector3 end = midPoint + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(midPoint, end, color, duration);
                Debug.DrawLine(end, midPoint + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(midPoint, end, color, duration, true);
                Debug.DrawLine(end, midPoint + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.Draw1SidedCapsule(point1 + direction * distance, point2 + direction * distance,
                CastColor, radius, duration, preview, depth);

            return count;
        }

        #endregion

        #endregion

        #region CircleCast

        #region CircleCast single

        public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCast(origin, radius, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction,
            float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCast(origin, radius, direction, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCast(origin, radius, direction, distance, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCast(origin, radius, direction, distance, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics2D.CircleCast(origin, radius, direction, distance,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            var collided = hitInfo.collider != null;

            if (collided)
            {
                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);

                distance = hitInfo.distance;
            }

            DebugEx.DrawCircle(origin, Vector3.forward, CastColor, radius, duration, preview, depth);

            var color = TrueColor(() => hitInfo, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * distance, color, duration, true);
            }

            color = TrueColor(() => hitInfo, hitColor, CastColor);

            DebugEx.DrawCircle(origin + direction * distance, Vector3.forward, color, radius, duration, preview, depth);
            return hitInfo;
        }

        public static int CircleCast(Vector2 origin, float radius, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCast(origin, radius, direction, contactFilter, results, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int CircleCast(Vector2 origin, float radius, Vector2 direction,
            ContactFilter2D contactFilter, RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.CircleCast(origin, radius, direction, contactFilter, results, distance);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            var notEmpty = false;

            foreach (var hitInfo in results)
            {
                var collided = hitInfo.collider != null;

                if (!collided)
                {
                    continue;
                }

                notEmpty = true;

                DebugEx.DrawPoint(hitInfo.point, Color.red, 0.5f, duration, preview, depth);

                distance = hitInfo.distance;
            }

            DebugEx.DrawCircle(origin, Vector3.forward, CastColor, radius, duration, preview, depth);

            var color = TrueColor(() => notEmpty, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, origin + direction * distance, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, origin + direction * distance, color, duration, true);
            }

            color = TrueColor(() => notEmpty, hitColor, CastColor);

            DebugEx.DrawCircle(origin + direction * distance, Vector3.forward, color, radius, duration, preview, depth);
            return count;
        }

        #endregion

        #region CircleCast All

        public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastAll(origin, radius, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction,
            float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastAll(origin, radius, direction, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastAll(origin, radius, direction, distance, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastAll(origin, radius, direction, distance, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction,
            float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics2D.CircleCastAll(origin, radius, direction, distance,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            float maxDistanceRay = 0;
            var collided = false;

            foreach (var hit in hitInfo)
            {
                if (hit.collider)
                {
                    collided = true;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawCircle(origin + direction * hit.distance, Vector3.forward,
                    hitColor ?? Color.green, radius, duration, preview, depth);

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            Vector3 end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                GLDebug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawCircle(origin, Vector3.forward, CastColor, radius, duration, preview, depth);

            DebugEx.DrawCircle(origin + direction * distance, Vector3.forward,
                CastColor, radius, duration, preview, depth);

            return hitInfo;
        }

        #endregion

        #region CircleCast non alloc

        public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction,
            RaycastHit2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastNonAlloc(origin, radius, direction, results, MAX_DISTANCE,
                UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, preview, duration,
                hitColor, noHitColor, depth);
        }

        public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction,
            RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastNonAlloc(origin, radius, direction, results, distance,
                UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, preview, duration,
                hitColor, noHitColor, depth);
        }

        public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastNonAlloc(origin, radius, direction, results, distance, layerMask,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return CircleCastNonAlloc(origin, radius, direction, results, distance, layerMask,
                minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction,
            RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.CircleCastNonAlloc(origin, radius, direction, results, distance,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            float maxDistanceRay = 0;
            var collided = false;

            foreach (var hit in results)
            {
                if (hit.collider)
                {
                    collided = true;
                }

                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                DebugEx.DrawCircle(origin + direction * hit.distance, Vector3.forward,
                    hitColor ?? Color.green, radius, duration, preview, depth);

                if (hit.distance > maxDistanceRay)
                {
                    maxDistanceRay = hit.distance;
                }
            }

            distance = distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance;

            Vector3 end = origin + direction * maxDistanceRay;
            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
                Debug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration, true);
                GLDebug.DrawLine(end, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            DebugEx.DrawCircle(origin, Vector3.forward, CastColor, radius, duration, preview, depth);

            DebugEx.DrawCircle(origin + direction * distance, Vector3.forward,
                CastColor, radius, duration, preview, depth);

            return count;
        }

        #endregion

        #endregion

        #region LineCast

        #region LineCast single

        public static RaycastHit2D Linecast(Vector2 start, Vector2 end,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Linecast(Vector2 start, Vector2 end, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Linecast(Vector2 start, Vector2 end, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Linecast(start, end, layerMask, minDepth, MAX_DISTANCE, preview, duration,
                hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Linecast(Vector2 start, Vector2 end, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var direction = end - start;
            return Raycast(start, direction, direction.magnitude, layerMask, minDepth, maxDepth,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int Linecast(Vector2 start, Vector2 end, ContactFilter2D contactFilter,
            RaycastHit2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var direction = end - start;
            return Raycast(start, direction, contactFilter, results, direction.magnitude, preview,
                duration, hitColor, noHitColor, depth);
        }

        #endregion

        #region LineCast All

        public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastAll(start, end, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastAll(start, end, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastAll(start, end, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var direction = end - start;
            return RaycastAll(start, direction, direction.magnitude, layerMask, minDepth, maxDepth,
                preview, duration, hitColor, noHitColor, depth);
        }

        #endregion

        #region LineCast non alloc

        public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastNonAlloc(start, end, results, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastNonAlloc(start, end, results, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return LinecastNonAlloc(start, end, results, layerMask, minDepth, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var direction = end - start;
            return RaycastNonAlloc(start, direction, results, direction.magnitude, layerMask,
                minDepth, maxDepth, preview, duration, hitColor, noHitColor, depth);
        }

        #endregion

        #endregion

        #region OverlapArea

        #region OverlapArea single

        public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapArea(pointA, pointB, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapArea(pointA, pointB, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapArea(pointA, pointB, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collider = UEPhysics2D.OverlapArea(pointA, pointB, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return collider;
            }

            var bottomLeft = new Vector2(pointA.x, pointB.y);
            var topRight = new Vector2(pointB.x, pointA.y);
            var color = TrueColor(() => collider != null, hitColor, noHitColor);

            //|
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointA, bottomLeft, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointA, bottomLeft, color, duration);
            }

            //_
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(bottomLeft, pointB, color, duration);
            }
            else
            {
                GLDebug.DrawLine(bottomLeft, pointB, color, duration);
            }

            // |
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointB, topRight, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointB, topRight, color, duration);
            }

            //-
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(topRight, pointA, color, duration);
            }
            else
            {
                GLDebug.DrawLine(topRight, pointA, color, duration);
            }

            return collider;
        }

        #endregion

        #region OverlapArea all

        public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaAll(pointA, pointB, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaAll(pointA, pointB, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaAll(pointA, pointB, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics2D.OverlapAreaAll(pointA, pointB, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            var bottomLeft = new Vector2(pointA.x, pointB.y);
            var topRight = new Vector2(pointB.x, pointA.y);
            var color = TrueColor(() => colliders.Length > 0, hitColor, noHitColor);

            //|
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointA, bottomLeft, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointA, bottomLeft, color, duration);
            }

            //_
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(bottomLeft, pointB, color, duration);
            }
            else
            {
                GLDebug.DrawLine(bottomLeft, pointB, color, duration);
            }

            // |
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointB, topRight, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointB, topRight, color, duration);
            }

            //-
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(topRight, pointA, color, duration);
            }
            else
            {
                GLDebug.DrawLine(topRight, pointA, color, duration);
            }

            return colliders;
        }

        #endregion

        #region OverlapArea non alloc

        public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaNonAlloc(pointA, pointB, results, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaNonAlloc(pointA, pointB, results, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapAreaNonAlloc(pointA, pointB, results, layerMask, minDepth, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics2D.OverlapAreaNonAlloc(pointA, pointB, results, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var bottomLeft = new Vector2(pointA.x, pointB.y);
            var topRight = new Vector2(pointB.x, pointA.y);
            var color = TrueColor(() => size > 0, hitColor, noHitColor);

            //|
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointA, bottomLeft, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointA, bottomLeft, color, duration);
            }

            //_
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(bottomLeft, pointB, color, duration);
            }
            else
            {
                GLDebug.DrawLine(bottomLeft, pointB, color, duration);
            }

            // |
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(pointB, topRight, color, duration);
            }
            else
            {
                GLDebug.DrawLine(pointB, topRight, color, duration);
            }

            //-
            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(topRight, pointA, color, duration);
            }
            else
            {
                GLDebug.DrawLine(topRight, pointA, color, duration);
            }

            return size;
        }

        #endregion

        #endregion

        #region OverlapBox

        #region OverlapBox single

        public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(point, size, angle, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(point, size, angle, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBox(point, size, angle, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collider = UEPhysics2D.OverlapBox(point, size, angle, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return collider;
            }

            size /= 2;

            var color = TrueColor(() => collider != null, hitColor, noHitColor);

            DebugEx.DrawBox(point, size, color, Quaternion.Euler(0, 0, angle), duration, preview, depth);
            return collider;
        }

        public static int OverlapBox(Vector2 point, Vector2 size, float angle,
            ContactFilter2D contactFilter, Collider2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapBox(point, size, angle, contactFilter, results);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            size /= 2;

            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            DebugEx.DrawBox(point, size, color, Quaternion.Euler(0, 0, angle), duration, preview, depth);
            return count;
        }

        #endregion

        #region OverlapBox all

        public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxAll(point, size, angle, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxAll(point, size, angle, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxAll(point, size, angle, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics2D.OverlapBoxAll(point, size, angle, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            size /= 2;

            var color = TrueColor(() => colliders != null && colliders.Length > 0, hitColor, noHitColor);

            DebugEx.DrawBox(point, size, color, Quaternion.Euler(0, 0, angle), duration, preview, depth);
            return colliders;
        }

        #endregion

        #region OverlapBox non alloc

        public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle,
            Collider2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(point, size, angle, results, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle,
            Collider2D[] results, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(point, size, angle, results, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle,
            Collider2D[] results, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapBoxNonAlloc(point, size, angle, results, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle,
            Collider2D[] results, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapBoxNonAlloc(point, size, angle, results, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            size /= 2;

            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            DebugEx.DrawBox(point, size, color, Quaternion.Euler(0, 0, angle), duration, preview, depth);
            return count;
        }

        #endregion

        #endregion

        #region OverlapCapsule

        #region OverlapCapsule single

        public static Collider2D OverlapCapsule(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsule(point, size, direction, angle, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCapsule(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsule(point, size, direction, angle, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCapsule(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsule(point, size, direction, angle, layerMask, minDepth, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCapsule(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask, float minDepth,
            float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collider = UEPhysics2D.OverlapCapsule(point, size, direction, angle, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return collider;
            }

            size /= 2;

            Vector2 point1;
            Vector2 point2;
            float radius;

            if (direction == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            var rot = Quaternion.Euler(0, 0, angle);
            var color = TrueColor(() => collider != null, hitColor, noHitColor);

            point1 = (Vector2) (rot * point1) + point;
            point2 = (Vector2) (rot * point2) + point;

            DebugEx.Draw1SidedCapsule(point1, point2, color, radius, duration, preview, depth);
            return collider;
        }

        public static int OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction,
            float angle, ContactFilter2D contactFilter, Collider2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapCapsule(point, size, direction, angle, contactFilter, results);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (direction == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + point;
            point2 = (Vector2) (rot * point2) + point;

            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            DebugEx.Draw1SidedCapsule(point1, point2, color, radius, duration, preview, depth);
            return count;
        }

        #endregion

        #region OverlapCapsule all

        public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleAll(point, size, direction, angle, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleAll(point, size, direction, angle, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleAll(point, size, direction, angle, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, int layerMask, float minDepth,
            float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics2D.OverlapCapsuleAll(point, size, direction, angle,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return colliders;
            }

            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (direction == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + point;
            point2 = (Vector2) (rot * point2) + point;

            var color = TrueColor(() => colliders != null && colliders.Length > 0, hitColor, noHitColor);

            DebugEx.Draw1SidedCapsule(point1, point2, color, radius, duration, preview, depth);
            return colliders;
        }

        #endregion

        #region OverlapCapsule non alloc

        public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, Collider2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleNonAlloc(point, size, direction, angle, results, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleNonAlloc(point, size, direction, angle, results, layerMask,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCapsuleNonAlloc(point, size, direction, angle, results, layerMask,
                minDepth, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size,
            CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapCapsuleNonAlloc(point, size, direction, angle, results,
                layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return count;
            }

            size /= 2;

            Vector2 point1;
            Vector2 point2;

            float radius;
            var rot = Quaternion.Euler(0, 0, angle);

            if (direction == CapsuleDirection2D.Vertical)
            {
                if (size.y > size.x)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.x;
            }
            else
            {
                if (size.x > size.y)
                {
                    point1 = new Vector3(0, 0 - size.y + size.x);
                    point2 = new Vector3(0, 0 + size.y - size.x);
                }
                else
                {
                    point1 = new Vector3(0 - .01f, 0);
                    point2 = new Vector3(0 + .01f, 0);
                }

                radius = size.y;
            }

            point1 = (Vector2) (rot * point1) + point;
            point2 = (Vector2) (rot * point2) + point;

            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            DebugEx.Draw1SidedCapsule(point1, point2, color, radius, duration, preview, depth);
            return count;
        }

        #endregion

        #endregion

        #region OverlapCircle

        #region OverlapCircle single

        public static Collider2D OverlapCircle(Vector2 point, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircle(point, radius, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCircle(Vector2 point, float radius, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircle(point, radius, layerMask, -MAX_DISTANCE, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCircle(Vector2 point, float radius, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircle(point, radius, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapCircle(Vector2 point, float radius, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collider = UEPhysics2D.OverlapCircle(point, radius, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => collider, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawCircle(point, Vector3.forward, color, radius, duration, preview, depth);
            }

            return collider;
        }

        public static int OverlapCircle(Vector2 point, float radius, ContactFilter2D contactFilter,
            Collider2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapCircle(point, radius, contactFilter, results);
            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawCircle(point, Vector3.forward, color, radius, duration, preview, depth);
            }

            return count;
        }

        #endregion

        #region OverlapCircle all

        public static Collider2D[] OverlapCircleAll(Vector2 point, float radius,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleAll(point, radius, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, int layerMask,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleAll(point, radius, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, int layerMask,
            float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleAll(point, radius, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, int layerMask,
            float minDepth, float maxDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null,
            bool depth = false)
        {
            var colliders = UEPhysics2D.OverlapCircleAll(point, radius, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => colliders != null && colliders.Length > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawCircle(point, Vector3.forward, color, radius, duration, preview, depth);
            }

            return colliders;
        }

        #endregion

        #region OverlapCircle non alloc

        public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleNonAlloc(point, radius, results, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleNonAlloc(point, radius, results, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapCircleNonAlloc(point, radius, results, layerMask, minDepth, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapCircleNonAlloc(point, radius, results, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawCircle(point, Vector3.forward, color, radius, duration, preview, depth);
            }

            return count;
        }

        #endregion

        #endregion

        #region OverlapPoint

        #region OverlapPoint single

        public static Collider2D OverlapPoint(Vector2 point, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPoint(point, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE, size,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapPoint(Vector2 point, int layerMask, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPoint(point, layerMask, -MAX_DISTANCE, MAX_DISTANCE, size, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapPoint(Vector2 point, int layerMask, float minDepth,
            float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPoint(point, layerMask, minDepth, MAX_DISTANCE, size, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D OverlapPoint(Vector2 point, int layerMask, float minDepth,
            float maxDepth, float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var collider = UEPhysics2D.OverlapPoint(point, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => collider, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawPoint(point, color, size, duration, preview, depth);
            }

            return collider;
        }

        public static int OverlapPoint(Vector2 point, ContactFilter2D contactFilter,
            Collider2D[] results, float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapPoint(point, contactFilter, results);
            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawPoint(point, color, size, duration, preview, depth);
            }

            return count;
        }

        #endregion

        #region OverlapPoint all

        public static Collider2D[] OverlapPointAll(Vector2 point, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointAll(point, UEPhysics2D.AllLayers, -MAX_DISTANCE, MAX_DISTANCE,
                size, preview, duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapPointAll(Vector2 point, int layerMask, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointAll(point, layerMask, -MAX_DISTANCE, MAX_DISTANCE, size, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapPointAll(Vector2 point, int layerMask, float minDepth,
            float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointAll(point, layerMask, minDepth, MAX_DISTANCE, size, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static Collider2D[] OverlapPointAll(Vector2 point, int layerMask, float minDepth,
            float maxDepth, float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var colliders = UEPhysics2D.OverlapPointAll(point, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => colliders != null && colliders.Length > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawPoint(point, color, size, duration, preview, depth);
            }

            return colliders;
        }

        #endregion

        #region OverlapPoint non alloc

        public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointNonAlloc(point, results, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, size, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, int layerMask,
            float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointNonAlloc(point, results, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                size, preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, int layerMask,
            float minDepth, float size = 6, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return OverlapPointNonAlloc(point, results, layerMask, minDepth, MAX_DISTANCE, size,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, int layerMask,
            float minDepth, float maxDepth, float size = 6,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var count = UEPhysics2D.OverlapPointNonAlloc(point, results, layerMask, minDepth, maxDepth);
            var color = TrueColor(() => count > 0, hitColor, noHitColor);

            if (preview != PreviewCondition.None)
            {
                DebugEx.DrawPoint(point, color, size, duration, preview, depth);
            }

            return count;
        }

        #endregion

        #endregion

        #region RayCast

        #region RayCast single

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, MAX_DISTANCE, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, distance, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, distance, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, distance, layerMask, minDepth, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hitInfo = UEPhysics2D.Raycast(origin, direction, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hitInfo;
            }

            Vector3 end = origin + direction * (distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance);
            var collided = false;

            if (hitInfo.collider != null)
            {
                collided = true;
                end = hitInfo.point;

                DebugEx.DrawPoint(end, Color.red, 0.5f, duration, preview, depth);
            }

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration);
            }

            return hitInfo;
        }

        public static int Raycast(Vector2 origin, Vector2 direction, ContactFilter2D contactFilter,
            RaycastHit2D[] results, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return Raycast(origin, direction, contactFilter, results, MAX_DISTANCE, preview,
                duration, hitColor, noHitColor, depth);
        }

        public static int Raycast(Vector2 origin, Vector2 direction, ContactFilter2D contactFilter,
            RaycastHit2D[] results, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics2D.Raycast(origin, direction, contactFilter, results, distance);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            Vector3 end = origin + direction * (distance.Equals(MAX_DISTANCE) ? 1000 * 1000 : distance);
            var collided = false;

            foreach (var hitInfo in results)
            {
                if (hitInfo.collider == null)
                {
                    continue;
                }

                collided = true;
                end = hitInfo.point;

                DebugEx.DrawPoint(end, Color.red, 0.5f, duration, preview, depth);
            }

            var color = TrueColor(() => collided, hitColor, noHitColor);

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(origin, end, color, duration);
            }
            else
            {
                GLDebug.DrawLine(origin, end, color, duration);
            }

            return size;
        }

        #endregion

        #region RayCast all

        public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, distance, UEPhysics2D.AllLayers, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance,
            int layerMask, PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, distance, layerMask, -MAX_DISTANCE, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance,
            int layerMask, float minDepth, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastAll(origin, direction, distance, layerMask, minDepth, MAX_DISTANCE,
                preview, duration, hitColor, noHitColor, depth);
        }

        public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance,
            int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var hit = UEPhysics2D.RaycastAll(origin, direction, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return hit;
            }

            var previewOrigin = origin;
            var sectionOrigin = origin;

            foreach (var x in hit)
            {
                DebugEx.DrawPoint(x.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(sectionOrigin, x.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(sectionOrigin, x.point, hitColor ?? Color.green, duration);
                }

                if ((origin - x.point).sqrMagnitude > (origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = x.point;
                }

                sectionOrigin = x.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            return hit;
        }

        #endregion

        #region RayCast non alloc

        public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, MAX_DISTANCE, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results,
            float distance, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, distance, UEPhysics2D.AllLayers,
                -MAX_DISTANCE, MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results,
            float distance, int layerMask, PreviewCondition preview = PreviewCondition.None,
            float duration = 0, Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, distance, layerMask, -MAX_DISTANCE,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results,
            float distance, int layerMask, float minDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            return RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth,
                MAX_DISTANCE, preview, duration, hitColor, noHitColor, depth);
        }

        public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results,
            float distance, int layerMask, float minDepth, float maxDepth,
            PreviewCondition preview = PreviewCondition.None, float duration = 0,
            Color? hitColor = null, Color? noHitColor = null, bool depth = false)
        {
            var size = UEPhysics2D.RaycastNonAlloc(origin, direction, results, distance, layerMask, minDepth, maxDepth);

            if (preview == PreviewCondition.None)
            {
                return size;
            }

            var previewOrigin = origin;
            var section = origin;

            foreach (var hit in results)
            {
                DebugEx.DrawPoint(hit.point, Color.red, 0.5f, duration, preview, depth);

                if (preview == PreviewCondition.Editor)
                {
                    Debug.DrawLine(section, hit.point, hitColor ?? Color.green, duration);
                }
                else
                {
                    GLDebug.DrawLine(section, hit.point, hitColor ?? Color.green, duration);
                }

                if ((origin - hit.point).sqrMagnitude > (origin - previewOrigin).sqrMagnitude)
                {
                    previewOrigin = hit.point;
                }

                section = hit.point;
            }

            if (preview == PreviewCondition.Editor)
            {
                Debug.DrawLine(previewOrigin, origin + direction * distance, noHitColor ?? Color.red, duration);
            }
            else
            {
                GLDebug.DrawLine(previewOrigin, origin + direction * distance, noHitColor ?? Color.red, duration);
            }

            return size;
        }

        #endregion

        #endregion

        #endregion
    }

    /// <summary>
    /// Class used to draw additional debugs, this was based of the Debug Drawing Extension from the asset store (https://www.assetstore.unity3d.com/en/#!/content/11396)
    /// </summary>
    public static class DebugEx
    {
        public static void DebugSquare(Vector3 origin, Vector3 halfExtents, Color color, Quaternion orientation, 
            float duration = 0, PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var forward = orientation * Vector3.forward;
            var up = orientation * Vector3.up;
            var right = orientation * Vector3.right;

            var topMinY1 = origin + right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;
            var topMaxY1 = origin - right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;
            var botMinY1 = origin + right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;
            var botMaxY1 = origin - right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                Debug.DrawLine(topMinY1, botMinY1, color, duration, depth);
                Debug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);
                Debug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
                Debug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
            }

            if (!game)
            {
                return;
            }

            GLDebug.DrawLine(topMinY1, botMinY1, color, duration, depth);
            GLDebug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);
            GLDebug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
            GLDebug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
        }

        public static void DrawBox(Vector3 origin, Vector3 halfExtents, Vector3 direction,
            float maxDistance, Color color, Quaternion orientation, Color endColor, bool drawBase = true,
            float duration = 0, PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var end = origin + direction * (float.IsPositiveInfinity(maxDistance) ? 1000 * 1000 : maxDistance);
            var forward = orientation * Vector3.forward;
            var up = orientation * Vector3.up;
            var right = orientation * Vector3.right;

            #region Coords

            #region End coords

            var topMinX0 = end + right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMaxX0 = end - right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMinY0 = end + right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;
            var topMaxY0 = end - right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;

            var botMinX0 = end + right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMaxX0 = end - right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMinY0 = end + right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;
            var botMaxY0 = end - right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;

            #endregion

            #region Origin coords

            var topMinX1 = origin + right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMaxX1 = origin - right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMinY1 = origin + right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;
            var topMaxY1 = origin - right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;

            var botMinX1 = origin + right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMaxX1 = origin - right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMinY1 = origin + right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;
            var botMaxY1 = origin - right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;

            #endregion

            #endregion

            #region Draw lines

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                #region Origin box

                if (drawBase)
                {
                    Debug.DrawLine(topMinX1, botMinX1, color, duration, depth);
                    Debug.DrawLine(topMaxX1, botMaxX1, color, duration, depth);
                    Debug.DrawLine(topMinY1, botMinY1, color, duration, depth);
                    Debug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);

                    Debug.DrawLine(topMinX1, topMaxX1, color, duration, depth);
                    Debug.DrawLine(topMinX1, topMinY1, color, duration, depth);
                    Debug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
                    Debug.DrawLine(topMaxY1, topMaxX1, color, duration, depth);

                    Debug.DrawLine(botMinX1, botMaxX1, color, duration, depth);
                    Debug.DrawLine(botMinX1, botMinY1, color, duration, depth);
                    Debug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
                    Debug.DrawLine(botMaxY1, botMaxX1, color, duration, depth);
                }

                #endregion

                #region Connection between boxes

                Debug.DrawLine(topMinX0, topMinX1, color, duration, depth);
                Debug.DrawLine(topMaxX0, topMaxX1, color, duration, depth);
                Debug.DrawLine(topMinY0, topMinY1, color, duration, depth);
                Debug.DrawLine(topMaxY0, topMaxY1, color, duration, depth);

                Debug.DrawLine(botMinX0, botMinX1, color, duration, depth);
                Debug.DrawLine(botMinX0, botMinX1, color, duration, depth);
                Debug.DrawLine(botMinY0, botMinY1, color, duration, depth);
                Debug.DrawLine(botMaxY0, botMaxY1, color, duration, depth);

                #endregion

                #region End box

                color = endColor;

                Debug.DrawLine(topMinX0, botMinX0, color, duration, depth);
                Debug.DrawLine(topMaxX0, botMaxX0, color, duration, depth);
                Debug.DrawLine(topMinY0, botMinY0, color, duration, depth);
                Debug.DrawLine(topMaxY0, botMaxY0, color, duration, depth);

                Debug.DrawLine(topMinX0, topMaxX0, color, duration, depth);
                Debug.DrawLine(topMinX0, topMinY0, color, duration, depth);
                Debug.DrawLine(topMinY0, topMaxY0, color, duration, depth);
                Debug.DrawLine(topMaxY0, topMaxX0, color, duration, depth);

                Debug.DrawLine(botMinX0, botMaxX0, color, duration, depth);
                Debug.DrawLine(botMinX0, botMinY0, color, duration, depth);
                Debug.DrawLine(botMinY0, botMaxY0, color, duration, depth);
                Debug.DrawLine(botMaxY0, botMaxX0, color, duration, depth);

                #endregion
            }

            if (!game)
            {
                return;
            }

            #region Origin box

            if (drawBase)
            {
                GLDebug.DrawLine(topMinX1, botMinX1, color, duration, depth);
                GLDebug.DrawLine(topMaxX1, botMaxX1, color, duration, depth);
                GLDebug.DrawLine(topMinY1, botMinY1, color, duration, depth);
                GLDebug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);

                GLDebug.DrawLine(topMinX1, topMaxX1, color, duration, depth);
                GLDebug.DrawLine(topMinX1, topMinY1, color, duration, depth);
                GLDebug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
                GLDebug.DrawLine(topMaxY1, topMaxX1, color, duration, depth);

                GLDebug.DrawLine(botMinX1, botMaxX1, color, duration, depth);
                GLDebug.DrawLine(botMinX1, botMinY1, color, duration, depth);
                GLDebug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
                GLDebug.DrawLine(botMaxY1, botMaxX1, color, duration, depth);
            }

            #endregion

            #region Connection between boxes

            GLDebug.DrawLine(topMinX0, topMinX1, color, duration, depth);
            GLDebug.DrawLine(topMaxX0, topMaxX1, color, duration, depth);
            GLDebug.DrawLine(topMinY0, topMinY1, color, duration, depth);
            GLDebug.DrawLine(topMaxY0, topMaxY1, color, duration, depth);

            GLDebug.DrawLine(botMinX0, botMinX1, color, duration, depth);
            GLDebug.DrawLine(botMinX0, botMinX1, color, duration, depth);
            GLDebug.DrawLine(botMinY0, botMinY1, color, duration, depth);
            GLDebug.DrawLine(botMaxY0, botMaxY1, color, duration, depth);

            #endregion

            #region End box

            color = endColor;

            GLDebug.DrawLine(topMinX0, botMinX0, color, duration, depth);
            GLDebug.DrawLine(topMaxX0, botMaxX0, color, duration, depth);
            GLDebug.DrawLine(topMinY0, botMinY0, color, duration, depth);
            GLDebug.DrawLine(topMaxY0, botMaxY0, color, duration, depth);

            GLDebug.DrawLine(topMinX0, topMaxX0, color, duration, depth);
            GLDebug.DrawLine(topMinX0, topMinY0, color, duration, depth);
            GLDebug.DrawLine(topMinY0, topMaxY0, color, duration, depth);
            GLDebug.DrawLine(topMaxY0, topMaxX0, color, duration, depth);

            GLDebug.DrawLine(botMinX0, botMaxX0, color, duration, depth);
            GLDebug.DrawLine(botMinX0, botMinY0, color, duration, depth);
            GLDebug.DrawLine(botMinY0, botMaxY0, color, duration, depth);
            GLDebug.DrawLine(botMaxY0, botMaxX0, color, duration, depth);

            #endregion

            #endregion
        }

        public static void DrawBox(Vector3 origin, Vector3 halfExtents, Color color,
            Quaternion orientation, float duration = 0,
            PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var forward = orientation * Vector3.forward;
            var up = orientation * Vector3.up;
            var right = orientation * Vector3.right;

            var topMinX1 = origin + right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMaxX1 = origin - right * halfExtents.x + up * halfExtents.y - forward * halfExtents.z;
            var topMinY1 = origin + right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;
            var topMaxY1 = origin - right * halfExtents.x + up * halfExtents.y + forward * halfExtents.z;

            var botMinX1 = origin + right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMaxX1 = origin - right * halfExtents.x - up * halfExtents.y - forward * halfExtents.z;
            var botMinY1 = origin + right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;
            var botMaxY1 = origin - right * halfExtents.x - up * halfExtents.y + forward * halfExtents.z;

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                Debug.DrawLine(topMinX1, botMinX1, color, duration, depth);
                Debug.DrawLine(topMaxX1, botMaxX1, color, duration, depth);
                Debug.DrawLine(topMinY1, botMinY1, color, duration, depth);
                Debug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);

                Debug.DrawLine(topMinX1, topMaxX1, color, duration, depth);
                Debug.DrawLine(topMinX1, topMinY1, color, duration, depth);
                Debug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
                Debug.DrawLine(topMaxY1, topMaxX1, color, duration, depth);

                Debug.DrawLine(botMinX1, botMaxX1, color, duration, depth);
                Debug.DrawLine(botMinX1, botMinY1, color, duration, depth);
                Debug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
                Debug.DrawLine(botMaxY1, botMaxX1, color, duration, depth);
            }

            if (!game)
            {
                return;
            }

            GLDebug.DrawLine(topMinX1, botMinX1, color, duration, depth);
            GLDebug.DrawLine(topMaxX1, botMaxX1, color, duration, depth);
            GLDebug.DrawLine(topMinY1, botMinY1, color, duration, depth);
            GLDebug.DrawLine(topMaxY1, botMaxY1, color, duration, depth);

            GLDebug.DrawLine(topMinX1, topMaxX1, color, duration, depth);
            GLDebug.DrawLine(topMinX1, topMinY1, color, duration, depth);
            GLDebug.DrawLine(topMinY1, topMaxY1, color, duration, depth);
            GLDebug.DrawLine(topMaxY1, topMaxX1, color, duration, depth);

            GLDebug.DrawLine(botMinX1, botMaxX1, color, duration, depth);
            GLDebug.DrawLine(botMinX1, botMinY1, color, duration, depth);
            GLDebug.DrawLine(botMinY1, botMaxY1, color, duration, depth);
            GLDebug.DrawLine(botMaxY1, botMaxX1, color, duration, depth);
        }

        public static void Draw1SidedCapsule(Vector3 start, Vector3 end, Color color = default,
            float radius = 1, float duration = 0,
            PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var up = (end - start).normalized * radius;
            var forward = Vector3.Slerp(up, -up, 0.5f);
            var right = Vector3.Cross(up, forward).normalized * radius;

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                //Side lines
                Debug.DrawLine(start + right, end + right, color, duration, depth);
                Debug.DrawLine(start - right, end - right, color, duration, depth);

                //Draw end caps
                for (var i = 1; i < 26; i++)
                {
                    //Start end cap
                    Debug.DrawLine(Vector3.Slerp(right, -up, i / 25.0f) + start,
                        Vector3.Slerp(right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-right, -up, i / 25.0f) + start,
                        Vector3.Slerp(-right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                    //End end cap
                    Debug.DrawLine(Vector3.Slerp(right, up, i / 25.0f) + end,
                        Vector3.Slerp(right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-right, up, i / 25.0f) + end,
                        Vector3.Slerp(-right, up, (i - 1) / 25.0f) + end, color, duration, depth);
                }
            }

            if (!game)
            {
                return;
            }

            //Side lines
            GLDebug.DrawLine(start + right, end + right, color, duration, depth);
            GLDebug.DrawLine(start - right, end - right, color, duration, depth);

            //Draw end caps
            for (var i = 1; i < 26; i++)
            {
                //Start end cap
                GLDebug.DrawLine(Vector3.Slerp(right, -up, i / 25.0f) + start,
                    Vector3.Slerp(right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-right, -up, i / 25.0f) + start,
                    Vector3.Slerp(-right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                //End end cap
                GLDebug.DrawLine(Vector3.Slerp(right, up, i / 25.0f) + end,
                    Vector3.Slerp(right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-right, up, i / 25.0f) + end,
                    Vector3.Slerp(-right, up, (i - 1) / 25.0f) + end, color, duration, depth);
            }
        }

        public static void DrawCapsule(Vector3 start, Vector3 end, Color color = default,
            float radius = 1, float duration = 0,
            PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var up = (end - start).normalized * radius;
            var forward = Vector3.Slerp(up, -up, 0.5f);
            var right = Vector3.Cross(up, forward).normalized * radius;

            //Radial circles
            DrawCircle(start, up, color, radius, duration, preview, depth);
            DrawCircle(end, -up, color, radius, duration, preview, depth);

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                //Side lines
                Debug.DrawLine(start + right, end + right, color, duration, depth);
                Debug.DrawLine(start - right, end - right, color, duration, depth);

                Debug.DrawLine(start + forward, end + forward, color, duration, depth);
                Debug.DrawLine(start - forward, end - forward, color, duration, depth);

                //Draw end caps
                for (var i = 1; i < 26; i++)
                {
                    //End end cap
                    Debug.DrawLine(Vector3.Slerp(right, up, i / 25.0f) + end,
                        Vector3.Slerp(right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-right, up, i / 25.0f) + end,
                        Vector3.Slerp(-right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(forward, up, i / 25.0f) + end,
                        Vector3.Slerp(forward, up, (i - 1) / 25.0f) + end, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-forward, up, i / 25.0f) + end,
                        Vector3.Slerp(-forward, up, (i - 1) / 25.0f) + end, color, duration, depth);

                    //Start end cap
                    Debug.DrawLine(Vector3.Slerp(right, -up, i / 25.0f) + start,
                        Vector3.Slerp(right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-right, -up, i / 25.0f) + start,
                        Vector3.Slerp(-right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(forward, -up, i / 25.0f) + start,
                        Vector3.Slerp(forward, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                    Debug.DrawLine(Vector3.Slerp(-forward, -up, i / 25.0f) + start,
                        Vector3.Slerp(-forward, -up, (i - 1) / 25.0f) + start, color, duration, depth);
                }
            }

            if (!game)
            {
                return;
            }

            //Side lines
            GLDebug.DrawLine(start + right, end + right, color, duration, depth);
            GLDebug.DrawLine(start - right, end - right, color, duration, depth);

            GLDebug.DrawLine(start + forward, end + forward, color, duration, depth);
            GLDebug.DrawLine(start - forward, end - forward, color, duration, depth);

            //Draw end caps
            for (var i = 1; i < 26; i++)
            {
                //End end cap
                GLDebug.DrawLine(Vector3.Slerp(right, up, i / 25.0f) + end,
                    Vector3.Slerp(right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-right, up, i / 25.0f) + end,
                    Vector3.Slerp(-right, up, (i - 1) / 25.0f) + end, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(forward, up, i / 25.0f) + end,
                    Vector3.Slerp(forward, up, (i - 1) / 25.0f) + end, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-forward, up, i / 25.0f) + end,
                    Vector3.Slerp(-forward, up, (i - 1) / 25.0f) + end, color, duration, depth);

                //Start end cap
                GLDebug.DrawLine(Vector3.Slerp(right, -up, i / 25.0f) + start,
                    Vector3.Slerp(right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-right, -up, i / 25.0f) + start,
                    Vector3.Slerp(-right, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(forward, -up, i / 25.0f) + start,
                    Vector3.Slerp(forward, -up, (i - 1) / 25.0f) + start, color, duration, depth);

                GLDebug.DrawLine(Vector3.Slerp(-forward, -up, i / 25.0f) + start,
                    Vector3.Slerp(-forward, -up, (i - 1) / 25.0f) + start, color, duration, depth);
            }
        }

        public static void DrawCircle(Vector3 pos, Vector3 up, Color color,
            float radius = 1.0f, float duration = 0,
            PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            var upDir = up.normalized * radius;
            var forwardDir = Vector3.Slerp(upDir, -upDir, 0.5f);
            var rightDir = Vector3.Cross(upDir, forwardDir).normalized * radius;

            var matrix = new Matrix4x4
            {
                [0] = rightDir.x,
                [1] = rightDir.y,
                [2] = rightDir.z,
                [4] = upDir.x,
                [5] = upDir.y,
                [6] = upDir.z,
                [8] = forwardDir.x,
                [9] = forwardDir.y,
                [10] = forwardDir.z
            };

            var lastPoint = pos + matrix.MultiplyPoint3x4(new Vector3(Mathf.Cos(0), 0, Mathf.Sin(0)));
            var nextPoint = Vector3.zero;

            var editor = false;
            var game = false;

            color = color == default ? Color.white : color;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            for (var i = 0; i < 91; i++)
            {
                nextPoint.x = Mathf.Cos(i * 4 * Mathf.Deg2Rad);
                nextPoint.z = Mathf.Sin(i * 4 * Mathf.Deg2Rad);
                nextPoint.y = 0;

                nextPoint = pos + matrix.MultiplyPoint3x4(nextPoint);

                if (editor)
                {
                    Debug.DrawLine(lastPoint, nextPoint, color, duration, depth);
                }

                if (game)
                {
                    GLDebug.DrawLine(lastPoint, nextPoint, color, duration, depth);
                }

                lastPoint = nextPoint;
            }
        }

        public static void DrawPoint(Vector3 pos, Color color, float scale = 0.5f,
            float duration = 0, PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            color = color == default ? Color.white : color;

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            if (editor)
            {
                Debug.DrawRay(pos + Vector3.up * (scale * 0.5f), -Vector3.up * scale, color, duration, depth);

                Debug.DrawRay(pos + Vector3.right * (scale * 0.5f), -Vector3.right * scale,
                    color, duration, depth);

                Debug.DrawRay(pos + Vector3.forward * (scale * 0.5f), -Vector3.forward * scale,
                    color, duration, depth);
            }

            if (!game)
            {
                return;
            }

            GLDebug.DrawRay(pos + Vector3.up * (scale * 0.5f), -Vector3.up * scale, color, duration, depth);

            GLDebug.DrawRay(pos + Vector3.right * (scale * 0.5f), -Vector3.right * scale,
                color, duration, depth);

            GLDebug.DrawRay(pos + Vector3.forward * (scale * 0.5f), -Vector3.forward * scale,
                color, duration, depth);
        }

        public static void DrawWireSphere(Vector3 v, Color color, float radius = 1.0f,
            float duration = 0, PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            const float ANGLE = 10.0f;

            var x = new Vector3(v.x, v.y + radius * Mathf.Sin(0), v.z + radius * Mathf.Cos(0));
            var y = new Vector3(v.x + radius * Mathf.Cos(0), v.y, v.z + radius * Mathf.Sin(0));
            var z = new Vector3(v.x + radius * Mathf.Cos(0), v.y + radius * Mathf.Sin(0), v.z);

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            for (var i = 1; i < 37; i++)
            {
                var newX = new Vector3(v.x, v.y + radius * Mathf.Sin(ANGLE * i * Mathf.Deg2Rad),
                    v.z + radius * Mathf.Cos(ANGLE * i * Mathf.Deg2Rad));

                var newY = new Vector3(v.x + radius * Mathf.Cos(ANGLE * i * Mathf.Deg2Rad),
                    v.y, v.z + radius * Mathf.Sin(ANGLE * i * Mathf.Deg2Rad));

                var newZ = new Vector3(v.x + radius * Mathf.Cos(ANGLE * i * Mathf.Deg2Rad),
                    v.y + radius * Mathf.Sin(ANGLE * i * Mathf.Deg2Rad), v.z);

                if (editor)
                {
                    Debug.DrawLine(x, newX, color, duration, depth);
                    Debug.DrawLine(y, newY, color, duration, depth);
                    Debug.DrawLine(z, newZ, color, duration, depth);
                }

                if (game)
                {
                    GLDebug.DrawLine(x, newX, color, duration, depth);
                    GLDebug.DrawLine(y, newY, color, duration, depth);
                    GLDebug.DrawLine(z, newZ, color, duration, depth);
                }

                x = newX;
                y = newY;
                z = newZ;
            }
        }

        public static void DebugConeSight(Vector3 v, Vector3 dir, float length,
            Color color, float angle = 45, float duration = 0,
            PreviewCondition preview = PreviewCondition.Editor, bool depth = false)
        {
            angle = angle > 0 ? Mathf.Min(angle, 360) : Mathf.Max(angle, -360);

            var forward = dir * length;
            var upDir = Vector3.Slerp(forward, -forward, 0.5f);
            var rightDir = Vector3.Cross(forward, upDir).normalized * length;

            Vector3 up;
            Vector3 down;

            Vector3 right;
            Vector3 rightUp;
            Vector3 rightDown;

            Vector3 left;
            Vector3 leftDown;
            Vector3 leftUp;

            if (angle <= 180)
            {
                var per = angle / 180f;

                up = v + Vector3.Slerp(forward, -rightDir, per).normalized * length;
                rightUp = v + Vector3.Slerp(forward, -upDir - rightDir, per).normalized * length;
                right = v + Vector3.Slerp(forward, -upDir, per).normalized * length;
                rightDown = v + Vector3.Slerp(forward, -upDir + rightDir, per).normalized * length;

                down = v + Vector3.Slerp(forward, rightDir, per).normalized * length;
                leftDown = v + Vector3.Slerp(forward, upDir + rightDir, per).normalized * length;
                left = v + Vector3.Slerp(forward, upDir, per).normalized * length;
                leftUp = v + Vector3.Slerp(forward, upDir - rightDir, per).normalized * length;
            }
            else
            {
                var per = (angle - 180) / 180f;

                up = v + Vector3.Slerp(-rightDir, -forward, per).normalized * length;
                rightUp = v + Vector3.Slerp(-upDir - rightDir, -forward, per).normalized * length;
                right = v + Vector3.Slerp(-upDir, -forward, per).normalized * length;
                rightDown = v + Vector3.Slerp(-upDir + rightDir, -forward, per).normalized * length;

                down = v + Vector3.Slerp(rightDir, -forward, per).normalized * length;
                leftDown = v + Vector3.Slerp(upDir + rightDir, -forward, per).normalized * length;
                left = v + Vector3.Slerp(upDir, -forward, per).normalized * length;
                leftUp = v + Vector3.Slerp(upDir - rightDir, -forward, per).normalized * length;
            }

            var editor = false;
            var game = false;

            switch (preview)
            {
                case PreviewCondition.Editor:
                    editor = true;
                    break;

                case PreviewCondition.Game:
                    game = true;
                    break;

                case PreviewCondition.Both:
                    editor = true;
                    game = true;
                    break;
            }

            #region Rays Logic

            if (editor)
            {
                Debug.DrawRay(v, forward, color, duration, depth);

                Debug.DrawLine(v, leftDown, color, duration, depth);
                Debug.DrawLine(v, leftUp, color, duration, depth);
                Debug.DrawLine(v, rightDown, color, duration, depth);
                Debug.DrawLine(v, rightUp, color, duration, depth);

                Debug.DrawLine(v, left, color, duration, depth);
                Debug.DrawLine(v, right, color, duration, depth);
                Debug.DrawLine(v, down, color, duration, depth);
                Debug.DrawLine(v, up, color, duration, depth);
            }

            if (game)
            {
                GLDebug.DrawRay(v, forward, color, duration, depth);

                GLDebug.DrawLine(v, leftDown, color, duration, depth);
                GLDebug.DrawLine(v, leftUp, color, duration, depth);
                GLDebug.DrawLine(v, rightDown, color, duration, depth);
                GLDebug.DrawLine(v, rightUp, color, duration, depth);

                GLDebug.DrawLine(v, left, color, duration, depth);
                GLDebug.DrawLine(v, right, color, duration, depth);
                GLDebug.DrawLine(v, down, color, duration, depth);
                GLDebug.DrawLine(v, up, color, duration, depth);
            }

            #endregion

            #region Circles

            var midUp = (up + v) / 2;
            var midDown = (down + v) / 2;

            var endPoint = (up + down) / 2;
            var midPoint = (midUp + midDown) / 2;

            DrawCircle(endPoint, dir, color, (up - down).sqrMagnitude / 2, duration, preview, depth);
            DrawCircle(midPoint, dir, color, (midUp - midDown).sqrMagnitude / 2, duration, preview, depth);

            #endregion

            #region Cone base sphere logic

            var lastLdPos = leftDown;
            var lastLuPos = leftUp;
            var lastRdPos = rightDown;
            var lastRuPos = rightUp;

            var lastLPos = left;
            var lastRPos = right;
            var lastDPos = down;
            var lastUPos = up;

            var index = 1;

            for (var i = 0; i < 7; i++)
            {
                var tempAngle = angle - index;

                Vector3 nextLd;
                Vector3 nextLu;
                Vector3 nextRd;
                Vector3 nextRu;

                Vector3 nextL;
                Vector3 nextR;
                Vector3 nextD;
                Vector3 nextU;

                if (tempAngle <= 180)
                {
                    var per = tempAngle / 180f;

                    nextLd = v + Vector3.Slerp(forward, upDir + rightDir, per).normalized * length;
                    nextLu = v + Vector3.Slerp(forward, upDir - rightDir, per).normalized * length;
                    nextRd = v + Vector3.Slerp(forward, -upDir + rightDir, per).normalized * length;
                    nextRu = v + Vector3.Slerp(forward, -upDir - rightDir, per).normalized * length;

                    nextD = v + Vector3.Slerp(forward, rightDir, per).normalized * length;
                    nextU = v + Vector3.Slerp(forward, -rightDir, per).normalized * length;
                    nextR = v + Vector3.Slerp(forward, -upDir, per).normalized * length;
                    nextL = v + Vector3.Slerp(forward, upDir, per).normalized * length;
                }
                else
                {
                    var per = (tempAngle - 180) / 180f;

                    nextLd = v + Vector3.Slerp(upDir + rightDir, -forward, per).normalized * length;
                    nextLu = v + Vector3.Slerp(upDir - rightDir, -forward, per).normalized * length;
                    nextRd = v + Vector3.Slerp(rightDir - upDir, -forward, per).normalized * length;
                    nextRu = v + Vector3.Slerp(-upDir - rightDir, -forward, per).normalized * length;

                    nextD = v + Vector3.Slerp(rightDir, -forward, per).normalized * length;
                    nextU = v + Vector3.Slerp(-rightDir, -forward, per).normalized * length;
                    nextR = v + Vector3.Slerp(-upDir, -forward, per).normalized * length;
                    nextL = v + Vector3.Slerp(upDir, -forward, per).normalized * length;
                }

                if (editor)
                {
                    Debug.DrawLine(lastLdPos, nextLd, color, duration, depth);
                    Debug.DrawLine(lastLuPos, nextLu, color, duration, depth);
                    Debug.DrawLine(lastRdPos, nextRd, color, duration, depth);
                    Debug.DrawLine(lastRuPos, nextRu, color, duration, depth);

                    Debug.DrawLine(lastDPos, nextD, color, duration, depth);
                    Debug.DrawLine(lastUPos, nextU, color, duration, depth);
                    Debug.DrawLine(lastRPos, nextR, color, duration, depth);
                    Debug.DrawLine(lastLPos, nextL, color, duration, depth);
                }

                if (game)
                {
                    GLDebug.DrawLine(lastLdPos, nextLd, color, duration, depth);
                    GLDebug.DrawLine(lastLuPos, nextLu, color, duration, depth);
                    GLDebug.DrawLine(lastRdPos, nextRd, color, duration, depth);
                    GLDebug.DrawLine(lastRuPos, nextRu, color, duration, depth);

                    GLDebug.DrawLine(lastDPos, nextD, color, duration, depth);
                    GLDebug.DrawLine(lastUPos, nextU, color, duration, depth);
                    GLDebug.DrawLine(lastRPos, nextR, color, duration, depth);
                    GLDebug.DrawLine(lastLPos, nextL, color, duration, depth);
                }

                lastLdPos = nextLd;
                lastLuPos = nextLu;
                lastRdPos = nextRd;
                lastRuPos = nextRu;

                lastDPos = nextD;
                lastUPos = nextU;
                lastRPos = nextR;
                lastLPos = nextL;
                index += 60;
            }

            #endregion
        }
    }
}