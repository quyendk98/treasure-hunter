﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameAssets.Scripts.Ex.PhysicDebug
{
    [AddComponentMenu("Physics/Physic Debugger")]
    public class PhysicDebugger : MonoBehaviour
    {
        [Header("Line Settings")] [Tooltip("Time in seconds before all debug lines get destroyed.")]
        public float debugDuration = 10f;

        [Tooltip("The width of all debug lines.")]
        public float lineWidth = 0.05f;

        [Tooltip("The length of collision normals will be multiplied by this number when drawn.")]
        public float lineLengthMultiplier = 1f;

        [Tooltip(
            "If true, debug lines for this object's collision normals will be drawn in world space.")]
        public bool drawWorldCollisionNormals = true;

        [Tooltip(
            "If true, debug lines for this object's collision normals will be drawn in local space.")]
        public bool drawLocalCollisionNormals;

        [Tooltip(
            "If true, debug lines will be scaled by impact force, i.e. the faster the object collides, the longer the line.")]
        public bool scaleLinesByForce = true;

        [Tooltip(
            "If true, all debug lines will be colored based on the objects velocity in a gradient between \"slowSpeedColor\" and \"fastSpeedColor\"."
            + " slowSpeedColor will start at slowSpeedValue and fade to fastSpeedColor as the object's velocity approaches fastSpeedValue.")]
        public bool useVelocityDebugColor = true;

        [Header("Trail Settings")]
        [Tooltip(
            "If true, this object will leave a trail. The trail's color can be mapped to the objects velocity using the \"useVelocityDebugColor\" option.")]
        public bool useTrail;

        [Tooltip("If true, the trail will be colored by velocity, otherwise use default color.")]
        public bool useVelocityTrailColor = true;

        [Tooltip("Time in seconds before the trail starts to disappear.")]
        public float trailDuration = 8f;

        [Header("Trajectory Settings")]
        [Tooltip("If true, a line displaying this objected probable trajectory will be drawn.")]
        public bool drawTrajectory;

        [Range(0.7f, 0.9999f)]
        [Tooltip("The accuracy of the trajectory. This controls the distance between steps in calculation.")]
        public float accuracy = 0.985f;

        [Tooltip("Limit on how many steps the trajectory can take before stopping.")]
        public int iterationLimit = 150;

        [Tooltip(
            "Stop the trajectory where the line hits an object? Objects can be set to ignore this collision by putting them on the Ignore Raycast layer.")]
        public bool stopOnCollision = true;

        [Tooltip(
            "If true, the trajectory line will be colored by velocity, otherwise use default color. This option is separate from "
            + "\"useVelocityDebugColor\" because it's slower to draw the trajectory line with this option enabled, so keep that in mind.")]
        public bool useVelocityForColor;

        [Header("Colors/Mats")] [Tooltip("The lower speed bound for determining velocity debug color.")]
        public float slowSpeedValue;

        [Tooltip("The upper speed bound for determining velocity debug color.")]
        public float fastSpeedValue = 15f;

        [Tooltip(
            "The lower bound color to use for debug lines when using the \"useVelocityDebugColor\" option.")]
        public Color slowSpeedColor = new Color(75f / 255, 255f / 255, 75f / 255);

        [Tooltip(
            "The upper bound color to use for debug lines when using the \"useVelocityDebugColor\" option.")]
        public Color fastSpeedColor = new Color(255f / 255, 75f / 255, 75f / 255);

        [Tooltip(
            "The color to use for debug lines when not using the \"useVelocityDebugColor\" option.")]
        public Color defaultColor = new Color(210f / 255, 70f / 255, 255f / 255);

        [Tooltip(
            "Material to use for the debug line renderer. This is best left on Sprites/Default, but a custom one can be added if desired.")]
        public Material debugLineMaterial;

        [HideInInspector] public Color velocityColor;

        private Color _lastVColor = Color.black;

        private void Reset()
        {
            debugLineMaterial = new Material(Shader.Find("Sprites/Default"));
        }

        private Rigidbody _rb;

        private void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _lastVColor = slowSpeedColor;
            _lastPos = transform.position;

            if (!_rb)
            {
                Debug.Log($"{gameObject.name}: Physics Debugger does not work without a rigidBody!");
            }
        }

        private Vector3 _lastPos = Vector3.zero;

        private void LateUpdate()
        {
            if (!_rb)
            {
                return;
            }

            var velPct = VelocityPercentage(_rb.velocity.magnitude);

            _lastVColor = velocityColor;
            velocityColor = GradientPercent(slowSpeedColor, fastSpeedColor, velPct);

            if (velPct > 0f)
            {
                if (useTrail)
                {
                    if (useVelocityTrailColor)
                    {
                        DebugTwoLine(_lastPos, transform.position, _lastVColor, velocityColor);
                    }
                    else
                    {
                        DebugTwoLine(_lastPos, transform.position, defaultColor, defaultColor);
                    }
                }

                if (drawTrajectory)
                {
                    _pos = transform.position;
                    _vel = _rb.velocity;
                    _trajectoryDuration = Time.unscaledDeltaTime;

                    PerformPrediction();
                }
            }

            _lastPos = transform.position;
        }

        public Color GetVelocityColor(float inVel)
        {
            return !_rb ? Color.clear : GradientPercent(slowSpeedColor, fastSpeedColor, VelocityPercentage(inVel));
        }

        private void OnCollisionEnter(Collision collision)
        {
            var collisionMag = collision.relativeVelocity.magnitude;

            if (!(collisionMag >= slowSpeedValue))
            {
                return;
            }

            foreach (var contact in collision.contacts)
            {
                var adjustedNormal = contact.normal;

                if (scaleLinesByForce)
                {
                    adjustedNormal *= collisionMag / 2f;
                }

                adjustedNormal *= lineLengthMultiplier;

                if (drawWorldCollisionNormals)
                {
                    DebugLine(contact.point, adjustedNormal, defaultColor, collisionMag);
                }

                if (drawLocalCollisionNormals)
                {
                    DebugLine(contact.point, adjustedNormal, defaultColor, collisionMag, false);
                }
            }
        }

        private Transform _lineParent;

        private void DebugLine(Vector3 point, Vector3 normal, Color color, float force,
            bool world = true)
        {
            var sPos = point;
            var ePos = point + normal;

            if (!world)
            {
                ePos = point + -normal;
                sPos = transform.InverseTransformPoint(sPos);
                ePos = transform.InverseTransformPoint(ePos);
            }


            if (!_lineParent)
            {
                _lineParent = transform.Find($"{transform.name}_Lines");

                if (!_lineParent)
                {
                    var transform1 = transform;

                    _lineParent = new GameObject($"{transform1.name}_Lines").transform;

                    _lineParent.SetParent(transform1);

                    _lineParent.localPosition = Vector3.zero;
                    _lineParent.localScale = Vector3.one;
                    _lineParent.localEulerAngles = Vector3.zero;
                }
            }

            var lineObj = new GameObject($"{transform.name}_DebugLine");

            lineObj.transform.SetParent(_lineParent);

            lineObj.transform.localPosition = Vector3.zero;
            lineObj.transform.localScale = Vector3.one;
            lineObj.transform.localEulerAngles = Vector3.zero;

            var debugLr = lineObj.AddComponent<LineRenderer>();

            debugLr.useWorldSpace = world;
            debugLr.receiveShadows = false;
            debugLr.sharedMaterial = debugLineMaterial;
            debugLr.startWidth = lineWidth;
            debugLr.endWidth = lineWidth;
            debugLr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            if (useVelocityDebugColor)
            {
                var colorGrad = GradientPercent(slowSpeedColor, fastSpeedColor, VelocityPercentage(force));

                debugLr.startColor = colorGrad;
                debugLr.endColor = colorGrad;
            }
            else
            {
                debugLr.startColor = color;
                debugLr.endColor = color;
            }

            debugLr.SetPositions(new[] {sPos, ePos});

            var delayDestroy = lineObj.AddComponent<DelayDestroy>();

            delayDestroy.delay = debugDuration;

            delayDestroy.StartUp();
        }

        private void DebugTwoLine(Vector3 startPoint, Vector3 endPoint, Color sColor, Color eColor)
        {
            if (!_lineParent)
            {
                _lineParent = transform.Find($"{transform.name}_Lines");

                if (!_lineParent)
                {
                    var t = transform;

                    _lineParent = new GameObject($"{t.name}_Lines").transform;

                    _lineParent.SetParent(t);

                    _lineParent.localPosition = Vector3.zero;
                    _lineParent.localScale = Vector3.one;
                    _lineParent.localEulerAngles = Vector3.zero;
                }
            }

            var lineObj = new GameObject($"{transform.name}_DebugLine");

            lineObj.transform.SetParent(_lineParent);

            lineObj.transform.localPosition = Vector3.zero;
            lineObj.transform.localScale = Vector3.one;
            lineObj.transform.localEulerAngles = Vector3.zero;

            var debugLr = lineObj.AddComponent<LineRenderer>();

            debugLr.useWorldSpace = true;
            debugLr.receiveShadows = false;
            debugLr.sharedMaterial = debugLineMaterial;
            debugLr.startWidth = lineWidth;
            debugLr.endWidth = lineWidth;
            debugLr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            debugLr.startColor = sColor;
            debugLr.endColor = eColor;

            debugLr.SetPositions(new[] {startPoint, endPoint});

            var delayDestroy = lineObj.AddComponent<DelayDestroy>();

            delayDestroy.delay = trailDuration;

            delayDestroy.StartUp();
        }

        public float VelocityPercentage(float force)
        {
            var dist = fastSpeedValue - slowSpeedValue;
            var forceAdjust = force - slowSpeedValue;
            return Mathf.Clamp01(forceAdjust / dist);
        }

        public static Color GradientPercent(Color lower, Color upper, float percentage)
        {
            return new Color(lower.r + percentage * (upper.r - lower.r),
                lower.g + percentage * (upper.g - lower.g),
                lower.b + percentage * (upper.b - lower.b));
        }

        private readonly List<Vector3> _predictionPoints = new List<Vector3>();

        private Vector3 _vel = Vector3.zero;
        private Vector3 _pos = Vector3.zero;

        private void PerformPrediction()
        {
            var done = false;
            var i = 0;

            var compAcc = 1f - accuracy;
            var gravAdd = Physics.gravity * compAcc;
            var dragMult = Mathf.Clamp01(1f - _rb.drag * compAcc);

            _predictionPoints.Clear();

            while (!done && i < iterationLimit)
            {
                _vel += gravAdd;
                _vel *= dragMult;

                var toPos = _pos + _vel * compAcc;
                var dir = toPos - _pos;

                _predictionPoints.Add(_pos);

                var dist = Vector3.Distance(_pos, toPos);

                if (stopOnCollision)
                {
                    var ray = new Ray(_pos, dir);

                    if (Physics.Raycast(ray, out var hit, dist))
                    {
                        done = true;

                        _predictionPoints.Add(hit.point);
                    }
                }

                _pos = toPos;
                i++;
            }

            if (useVelocityForColor)
            {
                LineDebugColored(_predictionPoints);
            }
            else
            {
                LineDebug(_predictionPoints);
            }
        }

        private GameObject _trajectoryLineObj;
        
        private float _trajectoryDuration = 0.05f;

        private void LineDebug(List<Vector3> pointList)
        {
            StopAllCoroutines();

            if (_trajectoryLineObj)
            {
                Destroy(_trajectoryLineObj);
            }

            _trajectoryLineObj = new GameObject
            {
                name = "Trajectory Line"
            };

            _trajectoryLineObj.transform.SetParent(transform);

            var debugLine = _trajectoryLineObj.AddComponent<LineRenderer>();

            debugLine.startColor = defaultColor;
            debugLine.endColor = defaultColor;
            debugLine.startWidth = lineWidth;
            debugLine.endWidth = lineWidth;
            debugLine.useWorldSpace = true;
            debugLine.receiveShadows = false;
            debugLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            var spriteDefault = Shader.Find("Sprites/Default");

            if (spriteDefault)
            {
                debugLine.sharedMaterial = new Material(spriteDefault);
            }

            debugLine.positionCount = pointList.Count;

            debugLine.SetPositions(pointList.ToArray());

            StartCoroutine(KillTrajectoryDelay(_trajectoryDuration));
        }

        private void LineDebugColored(IReadOnlyList<Vector3> pointList)
        {
            if (pointList.Count < 2)
            {
                return;
            }

            StopAllCoroutines();

            if (_trajectoryLineObj)
            {
                Destroy(_trajectoryLineObj);
            }

            _trajectoryLineObj = new GameObject
            {
                name = "Trajectory Line"
            };

            _trajectoryLineObj.transform.SetParent(transform);

            var lastTColor = slowSpeedColor;
            var lastPos = pointList[0];
            var compAcc = 1f - accuracy;

            for (var i = 1; i < pointList.Count; i++)
            {
                var tLineObj = new GameObject
                {
                    name = "Trajectory segment"
                };

                tLineObj.transform.SetParent(_trajectoryLineObj.transform);

                var curPos = pointList[i];
                var curVel = (curPos - lastPos).magnitude / compAcc;

                var curTColor = GetVelocityColor(curVel);
                var debugLine = tLineObj.AddComponent<LineRenderer>();

                debugLine.startColor = lastTColor;
                debugLine.endColor = curTColor;

                debugLine.startWidth = lineWidth;
                debugLine.endWidth = lineWidth;
                debugLine.useWorldSpace = true;
                debugLine.receiveShadows = false;
                debugLine.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                var spriteDefault = Shader.Find("Sprites/Default");

                if (spriteDefault)
                {
                    debugLine.sharedMaterial = new Material(spriteDefault);
                }

                debugLine.positionCount = 2;

                debugLine.SetPosition(0, lastPos);
                debugLine.SetPosition(1, curPos);

                lastPos = curPos;
                lastTColor = curTColor;
            }

            StartCoroutine(KillTrajectoryDelay(_trajectoryDuration));
        }

        private IEnumerator KillTrajectoryDelay(float delay)
        {
            yield return new WaitForSeconds(delay);

            if (_trajectoryLineObj)
            {
                Destroy(_trajectoryLineObj);
            }
        }

        private void OnDestroy()
        {
            if (!_trajectoryLineObj)
            {
                return;
            }

            if (_trajectoryLineObj)
            {
                Destroy(_trajectoryLineObj);
            }

            if (_lineParent)
            {
                Destroy(_lineParent);
            }

            Resources.UnloadUnusedAssets();
        }
    }
}