﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameAssets.Scripts.Ex
{
    public static class RbEx
    {
        public static void SetAffectGravity(this Rigidbody2D rb, bool value)
        {
            rb.isKinematic = value;
        }

        public static IEnumerable<GameObject> Spawner(this Collider2D c, List<GameObject> gos, int amount)
        {
            var clones = new List<GameObject>();
            var index = 0;

            while (index < amount)
            {
                var rdBound = c.bounds.Random();
                var rdPos3D = new Vector3(rdBound.x, rdBound.y, rdBound.z);
                var rdPos = c.ClosestPoint(rdPos3D);

                if (!MathfEx.Equal(rdPos.x, rdPos3D.x) || !MathfEx.Equal(rdPos.y, rdPos3D.y))
                {
                    continue;
                }

                var rd = Random.Range(0, gos.Count - 1);
                var clone = SpawnerEx.CreateSpawner(Vector3.zero, c.transform, gos[rd]);

                clone.position = rdPos3D;

                clones.Add(clone.gameObject);

                index++;
            }

            return clones;
        }

        public static void ResetInertia(this Rigidbody2D rb)
        {
            if (rb.bodyType == RigidbodyType2D.Static)
            {
                return;
            }

            rb.angularVelocity = 0f;
            rb.velocity = Vector2.zero;
        }
    }
}