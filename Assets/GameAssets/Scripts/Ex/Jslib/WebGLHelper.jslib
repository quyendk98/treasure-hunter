mergeInto(LibraryManager.library,
{

	OpenLink: function(link)
    {
    	var url = Pointer_stringify(link);
		
        document.onmouseup = function()
        {
        	window.open(url);
        	document.onmouseup = null;
        }
    },
	
	VisibilityChanged: function()
	{
        document.addEventListener("visibilitychange", function()
        {
            SendMessage("Game Extension", "OnVisibilityChange", document.visibilityState);
        });
        
        if (document.visibilityState != "visible")
        {
            SendMessage("Game Extension", "OnVisibilityChange", document.visibilityState);
        }
    }
});