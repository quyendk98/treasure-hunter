﻿using System.Runtime.InteropServices;
using GameAssets.Scripts.General;
using GameAssets.Scripts.Manager.Game;
using UnityEngine;

namespace GameAssets.Scripts.Ex.Jslib
{
    public partial class JslibEx : MonoBehaviour
    {
        [DllImport("__Internal")]
        private static extern void OpenLink(string url);

        [DllImport("__Internal")]
        private static extern void VisibilityChanged();

        private static void OnVisibilityChange(string state)
        {
            var visible = state == "visible";

            if (visible)
            {
                GameManager.Instance.SetGameState(GameState.Pause);
            }
            else if (GameManager.Instance.IsGameState(GameState.Pause))
            {
                GameManager.Instance.SetGameState(GameState.Playing);
            }

            System.Console.WriteLine(
                $"[{System.DateTime.Now}] the game switched to {(visible ? "foreground" : "background")}");
        }
    }
}