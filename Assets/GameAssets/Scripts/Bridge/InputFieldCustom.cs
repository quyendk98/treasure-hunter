using TMPro;
using UnityEngine.EventSystems;

namespace GameAssets.Scripts.Bridge
{
    public class InputFieldCustom : TMP_InputField
    {
        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            var m_CurrentInputField = eventData.selectedObject.GetComponent<InputFieldCustom>();

            Bridge.Instance.SetCurrentInputField(m_CurrentInputField);

#if !UNITY_EDITOR && UNITY_WEBGL
            Bridge.Instance.SelectInput();
#endif
        }
    }
}