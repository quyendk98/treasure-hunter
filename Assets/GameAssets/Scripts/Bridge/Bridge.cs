using System.Runtime.InteropServices;
using UnityEngine;

namespace GameAssets.Scripts.Bridge
{
    public class Bridge : MonoBehaviour
    {
        public static Bridge Instance { get; set; }

        [DllImport("__Internal")]
        private static extern void Keyboard();

        [DllImport("__Internal")]
        private static extern void BeginGame();

        private InputFieldCustom m_CurrentInputField;

        void Start()
        {
            Instance = this;
        }

        public void SelectInput()
        {
            Keyboard();
        }

        public void StopLoading()
        {
            BeginGame();
        }

        public void ReviceText(string text)
        {
            m_CurrentInputField.text = text;
        }

        public void SetCurrentInputField(InputFieldCustom m_input)
        {
            m_CurrentInputField = m_input;
        }
    }
}