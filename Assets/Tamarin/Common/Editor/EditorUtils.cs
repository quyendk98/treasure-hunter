#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
namespace Tamarin.Common
{
    public static class EditorUtils
    {
        public static void DrawBranding(string name, string discord = "https://discord.gg/gQ2a3dvVQZ")
        {
            var style = new GUIStyle(EditorStyles.inspectorFullWidthMargins) { normal = { background = GetTexture(new Color(0.15f, 0.15f, 0.15f)) }, fixedHeight = 40 };
            style.normal.background = GetTexture(new Color(0.15f, 0.15f, 0.15f));
            GUILayout.BeginHorizontal(style);

            style = new GUIStyle(EditorStyles.largeLabel) { normal = { textColor = Color.white }, fixedHeight = 40, padding = new RectOffset(20, 10, 10, 0) };
            EditorGUILayout.LabelField(name, style);

            style = new GUIStyle(EditorStyles.miniLabel) { normal = { textColor = Color.white }, alignment = TextAnchor.UpperRight, padding = new RectOffset(2, 2, 12, 0) };
            if (GUILayout.Button("TwistedTamarin", style))
                Application.OpenURL("https://twistedtamarin.com");

            style.padding = new RectOffset(2, 10, 12, 0);
            if (GUILayout.Button("Discord", style))
                Application.OpenURL(discord);

            GUILayout.EndHorizontal();
        }

        public static Texture2D GetTexture(Color color)
        {
            var tex = new Texture2D(1, 1);
            tex.SetPixel(0, 0, color);
            tex.Apply();
            return tex;
        }
    }
}

#endif