﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Tamarin.Common
{
    public static class Waiter
    {
        public static IEnumerator Until(Func<bool> predicate)
        {
            yield return new WaitUntil(predicate);
        }

        public static IEnumerator ForSeconds(float seconds)
        {
            yield return new WaitForSecondsRealtime(seconds);
        }

        public static IEnumerator ForFrames(int frameCount)
        {
            if (frameCount <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(frameCount), "Cannot wait for less that 1 frame");
            }

            while (frameCount > 0)
            {
                frameCount--;
                yield return null;
            }
        }
    }
}