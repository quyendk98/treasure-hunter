using UnityEditor;
using UnityEngine;
using Tamarin.Common;

#if TT_FIREBASE && UNITY_EDITOR
using Newtonsoft.Json;
namespace Tamarin.FirebaseX
{
    [CustomEditor(typeof(FirebaseWebHelper))]
    [CanEditMultipleObjects]
    public class FirebaseWebHelperEditor : Editor
    {
        FirebaseWebHelper _helper;
        void OnEnable()
        {
            _helper = target as FirebaseWebHelper;
        }

        public override bool UseDefaultMargins() { return false; }
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorUtils.DrawBranding("API for Firebase WebGL", FirebaseSetup.discord);
            GUILayout.BeginVertical(new GUIStyle(EditorStyles.inspectorDefaultMargins));

            //EditorGUILayout.PropertyField(serializedObject.FindProperty("config"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.version"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.region"));

            GUILayout.Label("");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.appId"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.apiKey"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.projectId"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.authDomain"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.databaseURL"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.measurementId"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.storageBucket"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.messagingSenderId"));

            GUILayout.Label("");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.webApiKey"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("config.vapidKey"));

            GUILayout.EndVertical();
            EditorGUILayout.Space(10);
            serializedObject.ApplyModifiedProperties();

            GUILayout.Space(20);
            GUILayout.Label("Thank you for choosing API for Firebase. If you are happy please leave a review! If you have any trouble please contact us on our discord for help!", EditorStyles.wordWrappedMiniLabel);
        }
    }
}
#endif

#if !TT_FIREBASE && UNITY_EDITOR
namespace Tamarin.FirebaseX
{
    [CustomEditor(typeof(FirebaseWebHelper))]
    public class FirebaseWebHelperEditor : Editor
    {
        public override bool UseDefaultMargins() { return false; }
        public override void OnInspectorGUI()
        {
            EditorUtils.DrawBranding("API for Firebase");
            GUILayout.BeginVertical(new GUIStyle(EditorStyles.inspectorDefaultMargins));

            GUILayout.Space(15);
            if (GUILayout.Button("Enable API"))
                FirebaseSetup.Init();
            GUILayout.Space(10);

            GUILayout.EndVertical();
        }
    }
}
#endif