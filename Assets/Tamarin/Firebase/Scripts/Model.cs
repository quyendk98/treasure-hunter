/* TAMARIN FIREBASE API */
/* Copyright (c) 2021 TwistedTamarin ltd All Rights Reserved (https://twistedtamarin.com) */

using System;
using System.Collections.Generic;
using UnityEngine;

#if TT_FIREBASE
using Newtonsoft.Json;
namespace Tamarin.FirebaseX
{
    public class FirebaseQuery
    {
        public string op;
        public string prop;
        public object value;
        public List<object> list;

        public FirebaseQuery() { }
        public FirebaseQuery(string prop, string op, object value)
        {
            this.op = op;
            this.prop = prop;
            this.value = value;
        }
        public FirebaseQuery(string prop, string op, List<object> list)
        {
            this.op = op;
            this.prop = prop;
            this.list = list;
        }
    };

    public class RealtimeQuery
    {
        public string op;
        public string key;
        public object value;

        public RealtimeQuery() { }
        public RealtimeQuery(string op, object value)
        {
            this.op = op;
            this.value = value;
        }
        public RealtimeQuery(string op, object value, string key)
        {
            this.op = op;
            this.key = key;
            this.value = value;
        }
    };

    [Serializable]
    public class FirebaseConfig
    {
        public string appId;
        public string apiKey;
        public string projectId;
        public string authDomain;
        public string databaseURL;
        public string measurementId;
        public string storageBucket;
        public string messagingSenderId;

        public string webApiKey;
        public string vapidKey;

        public string region = "europe_west";
        public string version = "8.5.0";
        public List<string> apis = new List<string> {
            "firebase-app", "firebase-auth", "firebase-functions", "firebase-firestore",
            "firebase-database", "firebase-storage", "firebase-messaging", "firebase-analytics", "firebase-remote-config"
        };
    }

    [Serializable]
    public class FirebaseMessage
    {
        public string To;
        public string From;

        public string Tag;
        public string Link;
        public string Body;
        public string Icon;
        public string Sound;
        public string Badge;
        public string Title;
        public string Color;
        public Dictionary<string, string> Data;

        public string Priority;
        public string MessageId;
        public string MessageType;
        public string ClickAction;

        public string Error;
        public string ErrorDescription;

#if !UNITY_WEBGL || UNITY_EDITOR
        public FirebaseMessage(Firebase.Messaging.MessageReceivedEventArgs data)
        {
            this.To = data.Message.To;
            this.From = data.Message.From;
            this.Tag = data.Message.Notification.Tag;
            this.Body = data.Message.Notification.Body;
            this.Icon = data.Message.Notification.Icon;
            this.Sound = data.Message.Notification.Sound;
            this.Badge = data.Message.Notification.Badge;
            this.Title = data.Message.Notification.Title;
            this.Color = data.Message.Notification.Color;
            this.Priority = data.Message.Priority;
            this.MessageId = data.Message.MessageId;
            this.MessageType = data.Message.MessageType;
            this.Error = data.Message.Error;
            this.ErrorDescription = data.Message.ErrorDescription;
            this.ClickAction = data.Message.Notification.ClickAction;

            this.Data = new Dictionary<string, string>(data.Message.Data);
            if (data.Message.Link != null)
                this.Link = data.Message.Link.ToString();
        }
#endif
    }

    public class FirebaseUser
    {
        public bool status { get; set; }
        public string code { get; set; }
        public string message { get; set; }

        public string UserId { get; set; }
        public string ProviderId { get; set; }
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Uri PhotoUrl { get; set; }

        public bool IsEmailVerified { get; set; }
        public bool IsAnonymous { get; set; }
        public string AccesToken { get; set; }
        public string IdToken { get; set; }

#if !UNITY_WEBGL || UNITY_EDITOR
        public Firebase.Auth.FirebaseUser user;
        public FirebaseUser(Firebase.Auth.FirebaseUser user)
        {
            if (user == null || user.UserId == null)
                return;

            this.user = user;
            this.status = user != null && user.UserId != null;
            this.UserId = user.UserId;
            this.ProviderId = user.ProviderId;
            this.DisplayName = user.DisplayName;
            this.PhoneNumber = user.PhoneNumber;
            this.Email = user.Email;
            this.PhotoUrl = user.PhotoUrl;

            this.IsEmailVerified = user.IsEmailVerified;
            this.IsAnonymous = user.IsAnonymous;
        }
        public FirebaseUser(string code, string message)
        {
            this.status = false;
            this.code = code;
            this.message = message;
        }
#endif
    }
}

#if UNITY_WEBGL && !UNITY_EDITOR
namespace Tamarin.FirebaseX
{   
    public class FirebaseAuth
    {
        public FirebaseUser CurrentUser { get; set; }
    }
    public class FirebaseStatusModel
    {
        public bool status { get; set; }
        public string id { get; set; } 
    }
    public class StatusModel
    {
        public bool status { get; set; }
        public string id { get; set; } 
    } 
}

namespace Firebase.Firestore
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FirestorePropertyAttribute : Attribute
    {
        public FirestorePropertyAttribute() { }
        public FirestorePropertyAttribute(string name) { }

        public string Name { get; set; }
        public Type ConverterType { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate)]
    public class FirestoreDataAttribute : Attribute
    {
        public FirestoreDataAttribute() { }
        public FirestoreDataAttribute(string name) { }

        public string Name { get; set; }
        public Type ConverterType { get; set; }
    }
}
#endif

#endif