﻿using System;
using Client.Base;

namespace Client.Interface
{
    public interface IConnect
    {
        void Disconnect();
        void ReceiveCallBack(IAsyncResult iar);
        void SendData(Packet packet);
        bool HandleData(byte[] data);
    }
}