﻿using System.Net.Sockets;
using Client.Base;
using TcpClient = System.Net.Sockets.TcpClient;

namespace Client.Interface
{
    public interface ITcp : IConnect
    {
        TcpClient Socket { get; set; }
        NetworkStream Stream { get; set; }
        Packet ReceivedData { get; set; }

        byte[] ReceiveBuffer { get; set; }
    }
}