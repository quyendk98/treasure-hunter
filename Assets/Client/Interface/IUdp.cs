﻿using System.Net;
using System.Net.Sockets;

namespace Client.Interface
{
    public interface IUdp : IConnect
    {
        UdpClient Socket { get; set; }
        IPEndPoint IPEndPoint { get; set; }
    }
}