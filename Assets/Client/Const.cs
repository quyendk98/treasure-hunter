﻿namespace Client
{
    public static class Const
    {
        public const string WEB_PLAYER_QUIT_URL = "https://google.com";
        public const int DATA_BUFFER_SIZE = 4096;
    }
}