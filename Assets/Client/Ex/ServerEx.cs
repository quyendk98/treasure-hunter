﻿using UnityEngine;

namespace Client.Ex
{
    public static class ServerEx
    {
        public static void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
            Application.OpenURL(Const.WEB_PLAYER_QUIT_URL);
#else
            Application.Quit();
#endif
        }
    }
}