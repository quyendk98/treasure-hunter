﻿using Client.Base;

namespace Client.Ex
{
    public static class SendEx
    {
        public static void SendTcpData(Packet packet)
        {
            packet.WriteLength();
            
            Server.TcpClient.SendData(packet);
        }

        public static void SendUdpData(Packet packet)
        {
            packet.WriteLength();
            
            Server.UdpClient.SendData(packet);
        }
    }
}