﻿using System;
using System.Net;
using Client.Ex;
using Client.Interface;
using Client.Model;

namespace Client.Base
{
    public class UdpClient : IUdp
    {
        public System.Net.Sockets.UdpClient Socket { get; set; }
        public IPEndPoint IPEndPoint { get; set; }

        #region Constructor

        public UdpClient(ServerConfig serverConfig)
        {
            IPEndPoint = new IPEndPoint(IPAddress.Parse(serverConfig.ip), serverConfig.port);
        }

        #endregion

        #region Public

        public void Connect(int localPort)
        {
            Socket = new System.Net.Sockets.UdpClient(localPort);

            Socket.Connect(IPEndPoint);
            Socket.BeginReceive(ReceiveCallBack, null);

            using var packet = new Packet();

            SendData(packet);
        }

        public void Disconnect()
        {
            Socket.Dispose();
            Socket.Close();

            IPEndPoint = null;
        }

        public void ReceiveCallBack(IAsyncResult iar)
        {
            try
            {
                var ipEndPoint = IPEndPoint;
                var data = Socket.EndReceive(iar, ref ipEndPoint);

                IPEndPoint = ipEndPoint;

                Socket.BeginReceive(ReceiveCallBack, null);

                if (data.Length < 4)
                {
                    // TODO: Disconnect
                    return;
                }

                HandleData(data);
            }
            catch
            {
                // TODO: Disconnect
            }
        }

        public void SendData(Packet packet)
        {
            try
            {
                packet.InsertInt(Server.ClientId);
                Socket?.BeginSend(packet.ToArray(), packet.Length(), null, null);
            }
            catch (Exception ex)
            {
                Log.Error($"Error sending data to server via UDP: {ex}");
            }
        }

        public bool HandleData(byte[] data)
        {
            using (var packet = new Packet(data))
            {
                var packetLength = packet.ReadInt();

                data = packet.ReadBytes(packetLength);
            }

            Thread.RunOnMainThread(() =>
            {
                using var packet = new Packet(data);
                var id = packet.ReadInt();

                Server.ClientHandlers[id](packet);
            });

            return true;
        }

        #endregion

        #region Private

        #endregion

        #region Protected

        #endregion
    }
}