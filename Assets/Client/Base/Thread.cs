﻿using System;
using System.Collections.Generic;
using Client.Ex;
using UnityEngine;

namespace Client.Base
{
    public class Thread : MonoBehaviour
    {
        private static readonly List<Action> ExecuteOnMainThread = new List<Action>();
        private static readonly List<Action> ExecuteCopiedOnMainThread = new List<Action>();

        private static bool _actionToExecuteOnMainThread;

        private void Update()
        {
            UpdateMain();
        }

        /// <summary>Sets an action to be executed on the main thread.</summary>
        /// <param name="action">The action to be executed on the main thread.</param>
        public static void RunOnMainThread(Action action)
        {
            if (action == null)
            {
                Log.Debug("No action to execute on main thread!");
                return;
            }

            lock (ExecuteOnMainThread)
            {
                ExecuteOnMainThread.Add(action);

                _actionToExecuteOnMainThread = true;
            }
        }

        /// <summary>Executes all code meant to run on the main thread. NOTE: Call this ONLY from the main thread.</summary>
        public static void UpdateMain()
        {
            if (!_actionToExecuteOnMainThread)
            {
                return;
            }

            ExecuteCopiedOnMainThread.Clear();

            lock (ExecuteOnMainThread)
            {
                ExecuteCopiedOnMainThread.AddRange(ExecuteOnMainThread);
                ExecuteOnMainThread.Clear();

                _actionToExecuteOnMainThread = false;
            }

            foreach (var x in ExecuteCopiedOnMainThread)
            {
                x();
            }
        }
    }
}